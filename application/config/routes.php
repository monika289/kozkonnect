<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 | -------------------------------------------------------------------------
 | URI ROUTING
 | -------------------------------------------------------------------------
 | This file lets you re-map URI requests to specific controller functions.
 |
 | Typically there is a one-to-one relationship between a URL string
 | and its corresponding controller class/method. The segments in a
 | URL normally follow this pattern:
 |
 |	example.com/class/method/id/
 |
 | In some instances, however, you may want to remap this relationship
 | so that a different class/function is called than the one
 | corresponding to the URL.
 |
 | Please see the user guide for complete details:
 |
 |	http://codeigniter.com/user_guide/general/routing.html
 |
 | -------------------------------------------------------------------------
 | RESERVED ROUTES
 | -------------------------------------------------------------------------
 |
 | There are three reserved routes:
 |
 |	$route['default_controller'] = 'welcome';
 |
 | This route indicates which controller class should be loaded if the
 | URI contains no data. In the above example, the "welcome" class
 | would be loaded.
 |
 |	$route['404_override'] = 'errors/page_missing';
 |
 | This route will tell the Router which controller/method to use if those
 | provided in the URL cannot be matched to a valid route.
 |
 |	$route['translate_uri_dashes'] = FALSE;
 |
 | This is not exactly a route, but allows you to automatically route
 | controller and method names that contain dashes. '-' isn't a valid
 | class or method name character, so it requires translation.
 | When you set this option to TRUE, it will replace ALL dashes in the
 | controller and method URI segments.
 |
 | Examples:	my-controller/index	-> my_controller/index
 |		my-controller/my-method	-> my_controller/my_method
 */
 

$route['default_controller'] = 'frontend/Home/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* ********************* Frontend ************************ */
$route['about-us'] = 'frontend/Home/aboutus';
$route['register'] = 'frontend/Home/register';
$route['login'] = 'frontend/Home/login';
$route['otp'] = 'frontend/Home/otp';
$route['reset-password'] = 'frontend/Home/resetpassword';
$route['contact-us'] = 'frontend/Home/contactus';
$route['email-verify'] = 'frontend/Home/emailverify';
$route['who-you'] = 'frontend/Home/whoyou';
$route['your-goal'] = 'frontend/Home/yourgoal';
$route['why-contribute'] = 'frontend/Home/whycontribute';
$route['privacy-policy'] = 'frontend/Home/privacypolicy';
$route['faq'] = 'frontend/Home/faq';
$route['terms-use'] = 'frontend/Home/termsuse';
$route['dashboard'] = 'frontend/Home/dashboard';
$route['myKonnect'] = 'frontend/Home/myKonnect';
$route['forgot-password'] = 'frontend/Home/forgotpassword';
$route['myGroup'] = 'frontend/Home/myGroup';
$route['change-maker'] = 'frontend/Home/changemaker';
$route['activity'] = 'frontend/Home/activity';
$route['refund-policy'] = 'frontend/Home/refundpolicy';
$route['message'] = 'frontend/Home/message';
$route['notifications'] = 'frontend/Home/notifications';

$route['add-cause'] = 'frontend/Home/addcause';
$route['disaster-response'] = 'frontend/Home/disasterresponse';

$route['friend-profile'] = 'frontend/Home/friendprofile';

$route['friendProfile-About'] = 'frontend/Home/friendProfileAbout';
$route['profile'] = 'frontend/Home/profile';
$route['ngo-page'] = 'frontend/Home/ngopage';
$route['profile-edit'] = 'frontend/Home/profileedit';
$route['causes'] = 'frontend/Home/causes';



/* ********************* Backend ************************ */

$route['admin'] = 'backend/Login';
$route['admin/dashboard'] = 'backend/Login/dashboard';
$route['admin/login'] = 'backend/Login/adminlogin';
$route['admin/logout'] = 'backend/Login/logout';
$route['admin/ngo'] = 'backend/Ngo/ngoList';
$route['admin/new'] = 'backend/Ngo/newNgo';
$route['admin/new/([0-9]+)'] = 'backend/Ngo/newNgo/$1';
$route['admin/editngo'] = 'backend/Ngo/editNgo';
$route['admin/deletedoc/([0-9]+)/([0-9]+)'] = 'backend/Ngo/deleteDoc/$1/$2';
$route['admin/ngo/addNgo'] = 'backend/Ngo/addNgo';
$route['admin/category/new'] = 'backend/General/loadCatForm';
$route['admin/category/add'] = 'backend/General/addCategory';
$route['admin/category/list'] = 'backend/General/listCategories';
$route['admin/category/edit/([0-9]+)'] = 'backend/General/editCategory1/$1';
$route['admin/category/update'] = 'backend/General/updateCategory';
$route['admin/subcategory/new'] = 'backend/General/loadSubCatForm';
$route['admin/subcategory/add'] = 'backend/General/addSubCategory';
$route['admin/subcategory/list'] = 'backend/General/listSubCategories';
$route['admin/subcategory/edit/([0-9]+)'] = 'backend/General/editSubCategory1/$1';
$route['admin/subcategory/update'] = 'backend/General/updateSubCategory';
$route['admin/sdg'] = 'backend/Sdg/sdgList';
$route['admin/sdgnew'] = 'backend/Sdg/newSdg';
$route['admin/sdgnew/([0-9]+)'] = 'backend/Sdg/newsdg/$1';
$route['admin/sdg/addSdg'] = 'backend/Sdg/addSdg';
$route['admin/sdg/editsdg'] = 'backend/Sdg/editSdg';
$route['admin/individual'] = 'backend/Users/individualList';
$route['admin/individual/new'] = 'backend/Users/newIndividual';
$route['admin/individual/new/([0-9]+)'] = 'backend/Users/newIndividual/$1';
$route['admin/individual/add'] = 'backend/Users/addIndividual';
$route['admin/individual/edit'] = 'backend/Users/editIndividual';
$route['admin/institution'] = 'backend/Users/institutionList';
$route['admin/institution/new'] = 'backend/Users/newInstitution';
$route['admin/institution/add'] = 'backend/Users/addInstitution';
$route['admin/institution/add/([0-9]+)'] = 'backend/Users/newInstitution/$1';
$route['admin/institution/edit'] = 'backend/Users/editInstitution';

$route['admin/org'] = 'backend/Users/orgList';
$route['admin/org/new'] = 'backend/Users/newOrg';
$route['admin/org/add'] = 'backend/Users/addOrg';
$route['admin/org/add/([0-9]+)'] = 'backend/Users/newOrg/$1';
$route['admin/org/edit'] = 'backend/Users/editOrg';
//causes
$route['admin/cause'] = 'backend/Cause/causelist';

/* **************************** Tickets ***********************************/
$route['admin/ticket/subcategorynew'] = 'backend/Ticket/newSubCategory';
$route['admin/ticket/subcategoryadd'] = 'backend/Ticket/addSubCategory';
$route['admin/ticket/subcategorylist'] = 'backend/Ticket/getTicketSubCategoryList';
$route['admin/ticket/subcategoryedit/([0-9]+)'] = 'backend/Ticket/editSubCategory/$1';
$route['admin/ticket/subcategoryupdate'] = 'backend/Ticket/updateSubCategory';

$route['admin/ticket/categorynew'] = 'backend/Ticket/newCategory';
$route['admin/ticket/categoryadd'] = 'backend/Ticket/addCategory';
$route['admin/ticket/categorylist'] = 'backend/Ticket/getTicketCategoryList';
$route['admin/ticket/categoryedit/([0-9]+)'] = 'backend/Ticket/editCategory/$1';
$route['admin/ticket/categoryupdate'] = 'backend/Ticket/updateCategory';
$route['admin/ticket/turnoncategory/([0-9]+)'] = 'backend/Ticket/turnoncategory/$1';
$route['admin/ticket/turnoffcategory/([0-9]+)'] = 'backend/Ticket/turnoffcategory/$1';
$route['admin/ticket/turnonsubcat/([0-9]+)'] = 'backend/Ticket/turnonsubcat/$1';
$route['admin/ticket/turnoffsubcat/([0-9]+)'] = 'backend/Ticket/turnoffsubcat/$1';

$route['admin/ticket/statusnew'] = 'backend/Ticket/newStatus';
$route['admin/ticket/statusadd'] = 'backend/Ticket/addStatus';
$route['admin/ticket/statuslist'] = 'backend/Ticket/getTicketAllStatusList';
$route['admin/ticket/statusedit/([0-9]+)'] = 'backend/Ticket/editStatus/$1';
$route['admin/ticket/statusupdate'] = 'backend/Ticket/updateStatus';
$route['admin/ticket/turnonstatus/([0-9]+)'] = 'backend/Ticket/turnonstatus/$1';
$route['admin/ticket/turnoffstatus/([0-9]+)'] = 'backend/Ticket/turnoffstatus/$1';


$route['admin/ticket/tickets'] = 'backend/Ticket/tickets';
$route['admin/ticket/new'] = 'backend/Ticket/newTicket';
$route['admin/ticket/add'] = 'backend/Ticket/addTicket';
$route['admin/ticket/edit/([0-9]+)'] = 'backend/Ticket/editTicket/$1';
$route['admin/ticket/update'] = 'backend/Ticket/updateTicket';

//$route['admin/general/ticket'] = 'backend/Ticket/mainTicket';

$route['admin/ticket/getTicketSubCatId'] = 'backend/Ticket/getTicketSubCatId';
$route['admin/ticket/getuserorder'] = 'backend/Ticket/getuserorder';

$route['admin/ticket/view/([0-9]+)']   = 'backend/Ticket/viewTicket/$1';
$route['admin/ticket/comment']   =  'backend/Ticket/comment';
$route['admin/ticket/change/priority']   =  'backend/Ticket/changePriority';
$route['admin/ticket/history/([0-9]+)']   =  'backend/Ticket/leadHistory/$1';
$route['admin/ticket/status/history/([0-9]+)']   =  'backend/Ticket/statusHistory/$1';
$route['admin/ticket/priority/history/([0-9]+)']   =  'backend/Ticket/priorityHistory/$1';
$route['admin/ticket/assign/executive']   =  'backend/Ticket/assignLead';
$route['admin/ticket/change/status']   =  'backend/Ticket/changeStatusLead';
$route['admin/ticket/change/priority']   =  'backend/Ticket/changePriority';
/****************user details************------------------*/
$route['admin/user/bymobile'] = 'backend/Ticket/getUserByMobile';
$route['admin/user/byemail'] = 'backend/Ticket/getUserByEmail';
$route['admin/user/byname'] = 'backend/Ticket/getUserByName';
$route['admin/user/detail/([0-9]+)'] = 'backend/Ticket/userDetail/$1';
//causes
$route['admin/cause'] = 'backend/Cause/causelist';





/* ********************* API ************************ */

