<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Global_config {
	private $CI;
	private $fb_config;
	
	function load_config() {
		$this->CI = &get_instance();
		$app_config = parse_ini_file ( APPPATH . "config/APP.ini" );
		$this->CI->template->set('google_api_key',$app_config['google_api_key']);
		$this->CI->template->set('hbuserid',$this->CI->session->userdata('hbuserid'));
		$this->CI->template->set('hbusername',$this->CI->session->userdata('hbusername'));
		$this->CI->template->set('hbuseremail',$this->CI->session->userdata('hbuseremail'));
		$this->CI->template->set('hbusermobile',$this->CI->session->userdata('hbusermobile'));
		$this->CI->template->set('hbuseravatar',$this->CI->session->userdata('hbuseravatar'));
	}
	
	function initilize_config() {
		$this->CI->template->set('base_url',base_url());
		$this->CI->load->library('session');
		$this->CI->load->helper('cookie');
	}
	
}
