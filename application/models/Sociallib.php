<?php
class Sociallib {
	public function __construct() {
		$this->CI = & get_instance ();
                $this->CI->load->model('social/Social_Model', 'social_model' );
	}
	
	
	public function google_user_data($data) {
		$this->CI->load->helper('string');
                $password = random_string('alnum',5);
                $user_data = array(
                    'name' => $data['name']['givenName'],
                    'lname' => $data['name']['familyName'],
                    'source' => 1,
                    'email' => $data['emails'][0]['value'],
                    'avatar' => $data['image']['url'],
                    'created_on' => date('Y-m-d'), 
                    'original' => $password,
                    'password' => md5($password),
                    
                );
	        $this->CI->social_model->process_user_details($user_data);
		
	}
        public function facebook_user_data($data) {
		
		$this->CI->load->helper('string');
                $password = random_string('alnum',5);
                $user_data = array(
                    'name' => $data['first_name'],
                    'lname' => $data['last_name'],
                    'source' => 1,
                    'email' => $data['email'],
                    'avatar' => '',
                    'created_on' => date('Y-m-d'), 
                    'original' => $password,
                    'password' => md5($password),
                );
	        $this->CI->social_model->process_user_details($user_data);
	}
        public function instagram_user_data($data) {
		
		$response = $this->CI->social_model->process_user_details($data);
		return $response;
	}
        public function twitter_user_data($data) {
		
		$response = $this->CI->social_model->process_user_details($data);
		return $response;
	}
}