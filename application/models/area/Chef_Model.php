<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Chef_Model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	function addchef($data) 
	{  

		
		$params = array (
				
				'mobile'=>$data['mobile']
		);
		
		$this->db->select ( '*' )->from ( TABLES::$CHEFS )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r(count($result)); exit();
		
		if (count($result) > 0)
		{
			$data2['msg'] = "chef mobile number already exists.";
			$data2['status'] = 0;
			return $data2;
		}
		
		else
		{
			$this->db->insert ( TABLES:: $CHEFS, $data);
			$data2 ['status'] = 1;
			$data2 ['id'] = $this->db->insert_id();
			$data2 ['msg'] = "Added successfully";
			return $data2;
				
				
		}
		
		
	
	}
	
	
	function addcheflist()
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $CHEFS);
		$query = $this->db->get();
		return $query->result_array();
	
	
	}
	
	
	function turnonchef($id) 
	{
		
		//print_r($id); exit();
		$area ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$CHEFS, $area );
	}
	
	function turnofchef($id) {
		$area ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$CHEFS, $area );
	}
	
	
	
	function updatechef($id) 
	{
		
	
	$this->db->select('*')
	->from(TABLES::$CHEFS);
	$this->db->where ('id', $id);
	$query = $this->db->get();
	return $query->result_array();
	
	}
	
	
	
	
	function onlyselectarea($id)
	{
		$this->db->select('*')
		->from(TABLES::$AREA);
		$query = $this->db->get();
		return $query->result_array();
	
	}
	
	
	
	
	function selectarea($id) 
	{
	
		
		
		$this->db->select ( 'chefarea.*,chefs.id as chefid,area.name as areaname');
		$this->db->from ( TABLES::$CHEFS . ' AS chefs' );
		$this->db->join ( TABLES::$CHEFAREA . ' AS chefarea', 'chefarea.chefid=chefs.id' );
		$this->db->join ( TABLES::$AREA . ' AS area', 'chefarea.areaid=area.id' );
		$this->db->where ('chefarea.chefid', $id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
		
		
		
		
		/* $this->db->select('areaid')
		->from(TABLES::$CHEFAREA);
		$this->db->where ('chefid', $id);
		$query = $this->db->get();
		return $query->result_array(); */
	
	}
	
	
	
	
	
	function updatechef_data($data) {
		

		$params = array (
		
				'mobile'=>$data['mobile']
		);
		
		
		$where = "mobile ='".$data['mobile']."' AND id !=".$data['id'];
		
		$this->db->select ( '*' )->from ( TABLES::$CHEFS )->where ( $where );
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r(count($result)); exit();
		
		if (count($result) > 0)
		{
			$data2['msg'] = "chef Mobile number already exists.";
			$data2['status'] = 0;
			return $data2;
		}
		
		else
		{
			
			
	   $this->db->where('id',$data['id']);
		$this->db->update(TABLES:: $CHEFS, $data);
		$result ['status'] = 1;
		$result ['msg'] = "Updated Successfully";
		return $result;
		
		
		}
		
		
	
	}
	
	
	
	
	function addchefarea($data) 
	{
		//print_r($data); exit();
		
		 /* $this->db->where('chefid', $data['chefid']);
		$this->db->delete(TABLES:: $STORE_ATTRIBUTE); */ 
		
		 foreach($data['areaid'] as $week1)
		{
			$week['areaid'] = $week1;
			$week['chefid'] = $data['chefid'];
			$week['status']=$data['status'];
			$this->db->insert ( TABLES::$CHEFAREA, $week);
		} 
		$data2 ['status'] = 1;
		$data2 ['id'] = $this->db->insert_id();
		$data2 ['msg'] = "Added successfully";
		return $data2;  
	}
	
	
	function updatechefarea($data)
	{
		 $this->db->where('chefid', $data['chefid']);
		 $this->db->delete(TABLES:: $CHEFAREA);
		
		 /* $this->db->where ( 'attribute_group_id', $id );
		$this->db->update ( TABLES::$ATTRIBUTEGROUP, $lookbook );  */
		
		foreach($data['areaid'] as $week1)
		{
			$week['areaid'] = $week1;
			$week['chefid'] = $data['chefid'];
			$week['status']=$data['status'];
			$this->db->where ( 'chefid', $data['chefid']);
			$this->db->insert ( TABLES::$CHEFAREA, $week);
		}
		$data2 ['status'] = 1;
		$data2 ['id'] = $this->db->insert_id();
		$data2 ['msg'] = "Update successfully";
		return $data2;
	}
	
	
	
}	