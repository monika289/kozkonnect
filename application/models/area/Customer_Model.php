<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Customer_Model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	function addcustomer($data) 
	{  

			$params = array (
				'mobile'=>$data['mobile'],
			        'email'=>$data['email']
		);
		$email = $data['email'];	
	    $where = "mobile ='".$data['mobile']."' OR email = '$email'";
		
		$this->db->select ('*')->from ( TABLES::$USERS )->where($where);
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r(count($result)); exit();
		
		if (count($result) > 0)
		{
			$data2['msg'] = "Customer Mobileno/email already exists.";
			$data2['status'] = 0;
			return $data2;
		}
		
		else
		{
			$area_id = array (
					'id' =>$data['area_id']
			);
			
			$this->db->select ( 'name as named' )->from (TABLES::$AREA )->where ( $area_id );
			$query = $this->db->get ();
			$result = $query->result_array ();
			$data1['area']=$result[0]['named'];;
			$merge = array_merge($data1, $data);
			$this->db->insert ( TABLES:: $USERS, $merge);
			$data2 ['status'] = 1;
			$data2 ['id'] = $this->db->insert_id();
			$data2 ['msg'] = "Added successfully";
			return $data2;
			
		}
		
		}
		
		
		
		
		function customerarea()
		{
				
			$this->db->select('*');
			$this->db->from( TABLES:: $AREA);
			$query = $this->db->get();
			return $query->result_array();
				
		}
		
		
		
		function newuserregistermail($id)
		{
		
			$this->db->select('*');
			$this->db->from( TABLES:: $USERS);
			$this->db->where ( 'id', $id );
			$query = $this->db->get();
			return $query->result_array();
		}
		
		
		
		function customerlist()
		{
				
			$this->db->select('*');
			$this->db->from( TABLES:: $USERS);
			$query = $this->db->get();
			return $query->result_array();
				
		}
		
		function turnoncustomer($id) {
		
			//print_r($id); exit();
			$area ['status'] = 1;
			$this->db->where ( 'id', $id );
			$this->db->update ( TABLES::$USERS, $area );
		}
		
		function turnofcustomer($id) {
			$area ['status'] = 0;
			$this->db->where ( 'id', $id );
			$this->db->update ( TABLES::$USERS, $area );
		}
		
		function updatecustomer($id) 
		{
			$this->db->select('*')
			->from(TABLES::$USERS);
			$this->db->where ('id', $id);
			$query = $this->db->get();
			return $query->result_array();
			
		}
		
		function updatecustomer_data($data) 
		{
		
			$this->db->where('id',$data['id']);
			$this->db->update(TABLES:: $USERS, $data);
			$result ['status'] = 1;
			$result ['msg'] = "Updated Successfully";
			return $result;
		
		}
		
		function customedetails()
		{
			$this->db->select('*');
			$this->db->from( TABLES:: $USERS);
			$this->db->group_by ('area','ASC');
			$query = $this->db->get();
			return $query->result_array();
		
		}
		
		function addprimary_address($data)
		{
				
			$user_id = array (
					'address' =>$data['locality'],
					'latitude' =>$data['latitude'],
					'longitude' =>$data['longitude'],
					'landmark' =>$data['landmark']
			);
			$this->db->where('id',$data['user_id']);
			return $this->db->update ( TABLES::$USERS, $user_id );
		
		}
		
		function addcustomeraddress($data)
		{
			
			
			$params = array (
				'user_id' =>$data['user_id'] );
			
			$this->db->select ( '*' )->from ( TABLES::$USER_ADDRESS )->where ( $params );
			$query = $this->db->get ();
			$result = $query->result_array ();
			//print_r(count($result)); exit();
			
			if (count($result) > 0)
			{
				$data2['msg'] = "Data  Already exists.";
				$data2['status'] = 0;
				return $data2;
			}
			
			else
			{
			
			
			$user_id = array (
					'id' =>$data['user_id'] );
				
			$this->db->select ( 'area_id' )->from (TABLES::$USERS )->where ( $user_id );
			$query = $this->db->get ();
			$result = $query->result_array ();
			$data1['area_id']=$result[0]['area_id'];;
			$merge = array_merge($data1, $data);
			$this->db->insert ( TABLES:: $USER_ADDRESS, $merge);
			$data2 ['status'] = 1;
			$data2 ['id'] = $this->db->insert_id();
			$data2 ['msg'] = "Added successfully";
			return $data2;
		
			}
		}
		
		function customeraddresslist()
		{

			$this->db->select ( 'UA.*,TU.name,TU.area');
			$this->db->from ( TABLES::$USER_ADDRESS . ' AS UA' );
			$this->db->join ( TABLES::$USERS . ' AS TU', 'UA.user_id=TU.id' );
			$query = $this->db->get ();
			$result = $query->result_array();
			return $result;

			/* $this->db->select('*');
			$this->db->from( TABLES:: $USER_ADDRESS);
			$query = $this->db->get();
			return $query->result_array(); */
		
		}
		
		function updatecustomeraddress($id)
		{

			$this->db->select ( 'UA.*,TU.name,TU.area');
			$this->db->from ( TABLES::$USER_ADDRESS . ' AS UA' );
			$this->db->join ( TABLES::$USERS . ' AS TU', 'UA.user_id=TU.id', 'left' );
			$this->db->where ('UA.id', $id);
			$query = $this->db->get ();
			$result = $query->result_array ();
			return $result;

			/* 	
			$this->db->select('*')
			->from(TABLES::$USER_ADDRESS);
			$this->db->where ('id', $id);
			$query = $this->db->get();
			return $query->result_array();  */
		
		}
		
		
		
		function customeradd_users($data)
		{
			
			$user_id = array 
			(
					'id' =>$data['area_id'] 
					
			);
			$this->db->select ( 'name' )->from (TABLES::$AREA )->where ( $user_id );
			
			//$this->db->select ( 'area' )->from (TABLES::$USERS )->where ( $user_id );
			$query = $this->db->get ();
			$result = $query->result_array ();
			$data1['area']=$result[0]['name'];
			//$merge = array_merge($data1, $data);
						
			$user_id = array (
					'area_id'=>$data['area_id'],
					'address' =>$data['address'],
					'latitude' =>$data['latitude'],
					'longitude' =>$data['longitude'],
					'landmark' =>$data['landmark']
			);
			$merge = array_merge($data1, $user_id);
			
			//print_r($merge); exit();
			
			$this->db->where('id',$data['user_id']);
			$this->db->update ( TABLES::$USERS, $merge );
			//$result ['status'] = 1;
			//$result ['msg'] = "Updated Successfully";
			//return $result;
		
		}
		
		function addcustomeraddress_data($data)
		{
			$this->db->where('user_id',$data['user_id']);
			$this->db->update(TABLES:: $USER_ADDRESS, $data);
			$result ['status'] = 1;
			$result ['msg'] = "Updated Successfully";
			return $result; 
		}
		
		
		function adduserwallet($userid)
		{
			
			//print_r($userid); exit();
			
			$userwallet['userid']=$userid;
			$userwallet['amount']=0;
			$userwallet['created_date']=date("Y-m-d H:i:s");
			
			//print_r($userwallet); exit();
			
			$this->db->insert ( TABLES:: $USERWALLET, $userwallet);
			$data2 ['status'] = 1;
			$data2 ['id'] = $this->db->insert_id();
			$data2 ['msg'] = "Added successfully";
			return $data2;
			
		}
		
		
		function invoicerecord()
		{
			
					
			$this->db->select('*');
			$this->db->from( TABLES:: $BOOKING);
			$this->db->where('status = ',3);
                        $this->db->where('next_invoice_date = ',date('Y-m-d'));
			$query = $this->db->get();
			$result = $query->result_array();
			
			//print_r($result); exit();
			return $result;
		
		}
                
                function invoicerecord_cancel($param) {
                        $this->db->select('*');
			$this->db->from( TABLES:: $BOOKING);
			$this->db->where('status = ',3);
                        $this->db->where('bookingid = ', $param);
			$query = $this->db->get();
			$result = $query->result_array();
			
			//print_r($result); exit();
			return $result;
                }
                        
		function leavecount($bookingslot)
		{
		
			//print_r($bookingid); exit();
			$deviceIds = array();
			foreach ($bookingslot as $item)
			{
				$deviceIds[] = $item['bookingid'];
			}
			$bookingid = implode(',', $deviceIds);
			//$bookingid=array($bookingid1);
			//print_r($bookingid1); exit();
			
			$query = $this->db->query("SELECT SUM(day_difference) as day_difference, bookingid FROM tbl_user_leave  where bookingid IN ($bookingid) AND status =  1 group by bookingid");
			// echo $this->db->last_query();
			$leaverecord = $query->result_array();
                       
			return $leaverecord; 
		}
		
		
		function bookingslotrecord($booking)
		{
			
			//print_r($booking); exit();
			$new_data = array();
			foreach($booking['book'] as $service)
			{
			           $service1= $service['bookingid'];
					   $new_data[] = $service1;
			}
			$bookingid=implode( ", ", $new_data );
			
		    // $query= $this->db->query("SELECT * FROM tbl_booking_slots  where bookingid IN ($bookingid)");
			//$query= $this->db->query("SELECT *,sum(amount) as price FROM tbl_booking_slots  where bookingid IN ($bookingid) group by bookingid");
		   //$query= $this->db->query(" SELECT *,sum(amount) as price,GROUP_CONCAT(mealtype_name) as mealtypename,GROUP_CONCAT(slot) as slotname FROM tbl_booking_slots where bookingid IN ($bookingid) group by bookingid");
		 
			$query= $this->db->query("SELECT BOOKSLOT.*,sum(BOOKSLOT.amount) as price,GROUP_CONCAT(BOOKSLOT.mealtype_name) as mealtypename,GROUP_CONCAT(BOOKSLOT.slot) as slotname ,BOOK.name,BOOK.mobile,BOOK.email,BOOK.invoice_date,BOOK.next_invoice_date,BOOK.discount,BOOK.redeem FROM tbl_booking_slots BOOKSLOT
					INNER JOIN tbl_booking BOOK ON BOOK.bookingid=BOOKSLOT.bookingid
					where BOOKSLOT.bookingid IN ($bookingid) group by BOOKSLOT.bookingid");
			
			 // echo $this->db->last_query();
			  $q1=$query->result_array();
			  return $q1;
			    
		}	    
                
                function updatebooking($leavelist1) {
                    $invoice_date = date('Y-m-d');
                    $next_invoice_date = date("Y-m-d", strtotime("+30 days"));
                    $this->db->query("UPDATE tbl_booking SET invoice_date = '$invoice_date', redeem = '0', discount = '0', next_invoice_date = '$next_invoice_date' WHERE bookingid = ".$leavelist1['bookingid']);
                    
                    
                }
                function updatebooking_cancel($leavelist1) {
                    
                    $this->db->query("UPDATE tbl_booking SET  redeem = '0', discount = '0'  WHERE bookingid = ".$leavelist1['bookingid']);
                    
                    
                }
		
		function updateInvoice($leavelist1,$url)
		{
//			//echo "cdsc";  exit();
//	        // print_r($url); exit();
//		
//			//$data['bookingid']=$leavelist1['bookingid'];
//			
//			
//			$params = array (
//					'bookingid' => $leavelist1['bookingid'],
//					'invoice_date' =>date('Y-m-d')
//			);
//			
//			
//			//$where = "name ='".$data['name']."' AND cityid !=".$data['cityid'];
//			
//			$this->db->select ( '*' )->from ( TABLES::$BOOKING_INVOICE )->where ( $params );
//			$query = $this->db->get ();
//			$result = $query->result_array ();
//			//print_r(count($result)); exit();
//			
//			if (count($result) > 0)
//			{
//				
//			
//				$data2['msg'] = "Invoice record  already exists.";
//				$data2['status'] = 0;
//				return $data2;
//			}
//			
//			else
//			{
			
				$data['userid']=$leavelist1['userid'];
			$data['bookingid']=$leavelist1['bookingid'];
			$data['amount']=$leavelist1['amount'];
			$data['adjustment']=$leavelist1['adjustment'];
			$data['grand_total']=$leavelist1['total_amount'];
			$data['net_total']=$leavelist1['total_amount'];
			$data['from_date']=$leavelist1['from_date'];
			$data['to_date']=$leavelist1['to_date'];
                        $data['discount']=$leavelist1['discount'];
                        $data['service_tax']=$leavelist1['service_tax'];
			$data['invoice_url']=$url;
			$data['invoice_date']=date('Y-m-d');
			$data['status']=1;
			//print_r($data); exit();
			$this->db->insert ( TABLES:: $BOOKING_INVOICE, $data);
			$data2 ['status'] = 1;
			$data2 ['id'] = $this->db->insert_id();
			$data2 ['msg'] = "Added successfully";
			return $data2;
					
//			}
			
			
		
			/* $data['userid']=$leavelist1['userid'];
			$data['bookingid']=$leavelist1['bookingid'];
			$data['amount']=$leavelist1['amount'];
			$data['adjustment']=$leavelist1['adjustment'];
			$data['grand_total']=$leavelist1['total_amount'];
			$data['net_total']=$leavelist1['total_amount'];
			$data['from_date']=$leavelist1['from_date'];
			$data['to_date']=$leavelist1['to_date'];
			$data['invoice_url']=$url;
			$data['invoice_date']=date('Y-m-d');
			$data['status']=1;
			//print_r($data); exit();
			$this->db->insert ( TABLES:: $BOOKING_INVOICE, $data);
			$data2 ['status'] = 1;
			$data2 ['id'] = $this->db->insert_id();
			$data2 ['msg'] = "Added successfully";
			return $data2; */
				
		}
		
		
				
		
		
		
		function invoicemail($leavelist1)
		{
			$bookingid=$leavelist1['bookingid'];
			$userid=$leavelist1['userid'];
			$this->db->select('BI.*,U.name,U.email,U.mobile');
			$this->db->from ( TABLES::$BOOKING_INVOICE . ' AS BI' );
			$this->db->join ( TABLES::$USERS . ' AS U', 'BI.userid=U.id', 'left' );
			$this->db->where('BI.bookingid',$bookingid);
			$this->db->where('BI.userid',$userid);
			$this->db->where('BI.status',1);
			$query = $this->db->get();
			$result = $query->result_array();
		
			//print_r($result); exit();
			return $result;
				
		}
                function invoicemail_leave_status($param) {
                    $this->db->query('UPDATE tbl_user_leave SET status = 0 WHERE bookingid = '.$param);
                    
                }
		
		
		
		
}
	
	
	
	