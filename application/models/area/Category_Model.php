<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Category_Model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	 function addmealtype($data) 
	 {  
		$params = array (
				'name' =>$data['name']
		);
		
		$this->db->select ( '*' )->from ( TABLES::$MEALTYPE )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r(count($result)); exit();
		
		if (count($result) > 0)
		{
			$data2['msg'] = "Mealtype Name already exists.";
			$data2['status'] = 0;
			return $data2;
		}
		
		else
		{
			$this->db->insert ( TABLES:: $MEALTYPE, $data);
			$data2 ['status'] = 1;
			$data2 ['id'] = $this->db->insert_id();
			$data2 ['msg'] = "Added successfully";
			return $data2;
		}
		
		}
		
		function allmealtype()
		{	
			$this->db->select('*');
			$this->db->from( TABLES:: $MEALTYPE);
			$query = $this->db->get();
			return $query->result_array();
		}
		
		function turnonmealtype($id) 
		{
			//print_r($id); exit();
			$area ['status'] = 1;
			$this->db->where ( 'id', $id );
			$this->db->update ( TABLES::$MEALTYPE, $area );
		}
		
		function turnofmealtype($id) 
		{
			$area ['status'] = 0;
			$this->db->where ( 'id', $id );
			$this->db->update ( TABLES::$MEALTYPE, $area );
		}
		
		function categorymealtype($id)
		{
			$this->db->select('*')
			->from(TABLES::$MEALTYPE);
			$this->db->where ('id', $id);
			$query = $this->db->get();
			return $query->result_array();
		}
		
		function updatemealtype_data($data) 
		{
			
			$params = array (
					'name' => $data ['name'],
			
			);
			
			$where = "name ='".$data['name']."' AND id !=".$data['id'];
			
			$this->db->select ( '*' )->from ( TABLES::$MEALTYPE )->where ( $where );
			$query = $this->db->get ();
			$result = $query->result_array ();
			//print_r(count($result)); exit();
			
			if (count($result) > 0)
			{
				$data2['msg'] = "Meal Type already exists.";
				$data2['status'] = 0;
				return $data2;
			}
			
			else
			{
					
			$this->db->where('id',$data['id']);
			$this->db->update(TABLES:: $MEALTYPE, $data);
			$result ['status'] = 1;
			$result ['msg'] = "Updated Successfully";
			return $result;
			
			}
			
		
		}
		
		
		function addmealtimeslot($data)
		{
		
			
			//print_r($data); exit();
			
				$params = array 
				     (
					    'mealtype_id' => $data ['mealtype_id'],
						'slot' => $data ['slot'],
				     	'area_id'=>$data['area_id']	
			          );
			
			$this->db->select ( '*' )->from ( TABLES::$TIMESLOTS )->where ( $params );
			$query = $this->db->get ();
			$result = $query->result_array ();
			
			
			if (count($result) > 0)
			{
				$data2['msg'] = "Time slot already exists.";
				$data2['status'] = 0;
				return $data2;
			}
			
			else
			{
			    $this->db->insert ( TABLES:: $TIMESLOTS, $data);
		        $data2 ['status'] = 1;
		        $data2 ['id'] = $this->db->insert_id();
		        $data2 ['msg'] = "Added successfully";
		        return $data2; ;
					
			}
			
		}
		
		
		function mealtimeslotlist()
		{
			
			
			$this->db->select ( 'TS.*,MT.name,area.name as areaname');
			$this->db->from ( TABLES::$TIMESLOTS . ' AS TS' );
			$this->db->join ( TABLES::$MEALTYPE . ' AS MT', 'MT.id=TS.mealtype_id', 'left' );
			$this->db->join ( TABLES::$AREA . ' AS area', 'area.id=TS.area_id', 'left' );
			$query = $this->db->get ();
			$result = $query->result_array ();
			return $result;
			
		}
		
		
		
		
		function turnontimeslot($id)
		{
			//print_r($id); exit();
			$area ['status'] = 1;
			$this->db->where ( 'id', $id );
			$this->db->update ( TABLES::$TIMESLOTS, $area );
		}
		
		function turnoftimeslot($id)
		{
			$area ['status'] = 0;
			$this->db->where ( 'id', $id );
			$this->db->update ( TABLES::$TIMESLOTS, $area );
		}
		
		
		function updatetimeslot($id)
		{
			
			$this->db->select ( 'TS.*,MT.name');
			$this->db->from ( TABLES::$TIMESLOTS . ' AS TS' );
			$this->db->join ( TABLES::$MEALTYPE . ' AS MT', 'MT.id=TS.mealtype_id', 'left' );
			
			$this->db->where ('TS.id', $id);
			$query = $this->db->get ();
			$result = $query->result_array ();
			return $result;
			
			
		/*	$this->db->select ( 'TS.*,MT.name');
			$this->db->from ( TABLES::$TIMESLOTS . ' AS TS' );
			$this->db->join ( TABLES::$MEALTYPE . ' AS MT', 'MT.id=TS.mealtype_id', 'left' );
			$this->db->where ('TS.id', $id);
			$query = $this->db->get ();
			$result = $query->result_array ();
			return $result;
			 */
		}
		
		
		
		function mealtypelist()
		{
			$this->db->select('*')
			->from(TABLES::$MEALTYPE);
			$query = $this->db->get();
			return $query->result_array(); 
		}
		

		function headcount1()
		{
			$this->db->select('*')
			->from(TABLES::$HEADCOUNT);
			$query = $this->db->get();
		    $result = $query->result_array ();
			return $result;
		}
		
		
		
		
				
			
		function addpricelistdata($ABC)
		{
			
			
			//print_r($ABC); exit();
			
			
			foreach($ABC['data'] as $service)
			{
				$service1['mealtype_id']=$service['mealtype_id'];
				$service1['timeslots']=$service['id'];
				$service1['price']=$service['price'];
				$service1['increment_on_head']=$service['increment_on_head'];
				//$service1['id']=$service['id'];
				$service1['headcount']=$service['headcount'];
				$service1['area_id']=$service['area_id'];
				$service1['status']=$service['status'];
				
				//print_r($service1['headcount']); exit();
				if($service1['headcount']==1)
				{
					$service1['mealtype_id']=$service['mealtype_id'];
					$service1['timeslots']=$service['id'];
					$service1['price']=$service['price'] + 0;
					$service1['increment_on_head']=$service['increment_on_head'];
					$service1['headcount']=$service['headcount'];
					$service1['area_id']=$service['area_id'];
					$service1['status']=$service['status'];
					$this->db->insert ( TABLES::$PRICELIST, $service1);
					//print_r($service1); exit();
				}
				if($service1['headcount']==2)
				{
					$service1['mealtype_id']=$service['mealtype_id'];
					$service1['timeslots']=$service['id'];
					$service1['price']=$service['price'] + 0;
					$service1['increment_on_head']=$service['increment_on_head'];
					$service1['headcount']=$service['headcount'];
					$service1['area_id']=$service['area_id'];
					$service1['status']=$service['status'];
					
					//print_r($service1); exit();
					
					$this->db->insert ( TABLES::$PRICELIST, $service1);
					
				}				
			
				if($service1['headcount']==3)
				{
					$service1['mealtype_id']=$service['mealtype_id'];
					$service1['timeslots']=$service['id'];
					$service1['price']=$service['price'] +($service['increment_on_head']*1);
					$service1['increment_on_head']=$service['increment_on_head'];
					$service1['headcount']=$service['headcount'];
					$service1['area_id']=$service['area_id'];
					$service1['status']=$service['status'];
						
					//print_r($service1); exit();
						
					$this->db->insert ( TABLES::$PRICELIST, $service1);
						
				}
				
				if($service1['headcount']==4)
				{
					$service1['mealtype_id']=$service['mealtype_id'];
					$service1['timeslots']=$service['id'];
					$service1['price']=$service['price'] +($service['increment_on_head']*2);
					$service1['increment_on_head']=$service['increment_on_head'];
					$service1['headcount']=$service['headcount'];
					$service1['area_id']=$service['area_id'];
					$service1['status']=$service['status'];
				
					//print_r($service1); exit();
				
					$this->db->insert ( TABLES::$PRICELIST, $service1);
				
				}
				
				if($service1['headcount']==5)
				{
					$service1['mealtype_id']=$service['mealtype_id'];
					$service1['timeslots']=$service['id'];
					$service1['price']=$service['price'] +($service['increment_on_head']*3);
					$service1['increment_on_head']=$service['increment_on_head'];
					$service1['headcount']=$service['headcount'];
					$service1['area_id']=$service['area_id'];
					$service1['status']=$service['status'];
				
					//print_r($service1); exit();
				
					$this->db->insert ( TABLES::$PRICELIST, $service1);
				
				}
				
				
				if($service1['headcount']==6)
				{
					$service1['mealtype_id']=$service['mealtype_id'];
					$service1['timeslots']=$service['id'];
					$service1['price']=$service['price'] +($service['increment_on_head']*4);
					$service1['increment_on_head']=$service['increment_on_head'];
					$service1['headcount']=$service['headcount'];
					$service1['area_id']=$service['area_id'];
					$service1['status']=$service['status'];
				
					//print_r($service1); exit();
				
					$this->db->insert ( TABLES::$PRICELIST, $service1);
				
				}
				
				if($service1['headcount']==7)
				{
					$service1['mealtype_id']=$service['mealtype_id'];
					$service1['timeslots']=$service['id'];
					$service1['price']=$service['price'] +($service['increment_on_head']*5);
					$service1['increment_on_head']=$service['increment_on_head'];
					$service1['headcount']=$service['headcount'];
					$service1['area_id']=$service['area_id'];
					$service1['status']=$service['status'];
				
					//print_r($service1); exit();
				
					$this->db->insert ( TABLES::$PRICELIST, $service1);
				
				}
				
				if($service1['headcount']==8)
				{
					$service1['mealtype_id']=$service['mealtype_id'];
					$service1['timeslots']=$service['id'];
					$service1['price']=$service['price'] +($service['increment_on_head']*6);
					$service1['increment_on_head']=$service['increment_on_head'];
					$service1['headcount']=$service['headcount'];
					$service1['area_id']=$service['area_id'];
					$service1['status']=$service['status'];
				
					//print_r($service1); exit();
				
					$this->db->insert ( TABLES::$PRICELIST, $service1);
				
				}
				
				//print_r($service1); exit();
			   //$this->db->insert ( TABLES::$PRICELIST, $service1);
		    }
		    $data2 ['status'] = 1;
		    $data2 ['id'] = $this->db->insert_id();
		    $data2 ['msg'] = "Added successfully";
		    return $data2;
		}
		    
			 
				
		
		
		function updatetimeslot_data($data)
		{
			
			//print_r($data); exit();
			
			$where = "area_id='".$data['area_id']."'  AND  slot ='".$data['slot']."' AND  id !=".$data['id'];
			
			$this->db->select ( '*' )->from ( TABLES::$TIMESLOTS )->where ( $where );
			$query = $this->db->get ();
			$result = $query->result_array ();
			//print_r(count($result)); exit();
			
			if (count($result) > 0)
			{
				$data2['msg'] = "Area Name already exists.";
				$data2['status'] = 0;
				return $data2;
			}
			
			else
			{
			
			
			$this->db->where('id',$data['id']);
			$this->db->update(TABLES:: $TIMESLOTS, $data);
			//$result ['id'] = $this->db->update_id();
			$result ['status'] = 1;
			$result ['msg'] = "Updated Successfully";
			return $result;
			
			}
		}
		
		function updatepricelist_data($data1)
		{
			
			$this->db->where('timeslots',$data1['timeslots']);
                        
			$this->db->update(TABLES:: $PRICELIST, $data1);
			
			$areaid=$data1['area_id'];
			$mealtype=$data1['mealtype_id'];
			$this->db->select ( 'headcount,mealtype_id,timeslots,price,increment_on_head,id,area_id');
			$this->db->from ( TABLES::$PRICELIST );
			$this->db->where ('timeslots', $data1['timeslots']);
			$this->db->where ('area_id', $areaid);
			$query = $this->db->get ();
			$result = $query->result_array ();
			
					
			if($result[0]['headcount']==1)
			{
			
				$price=$result[0]['price'];
				$inc=$result[0]['increment_on_head'];
			
				//print_r($price); exit();
			
				$service['price']=$price+($inc*0);
			
				//print_r($service1); exit();
				$this->db->where ( 'area_id', $result[0]['area_id'] );
				//$this->db->where ( 'id', $result[0]['id'] );
				$this->db->where ( 'mealtype_id', $result[0]['mealtype_id'] );
				$this->db->where ( 'timeslots', $result[0]['timeslots'] );
				$this->db->update(TABLES:: $PRICELIST, $service);
				
				//echo $this->db->last_query();
			}
			
			
			 if($result[1]['headcount']==2)
			{
					
				$price=$result[1]['price'];
				$inc=$result[1]['increment_on_head'];
					
				//print_r($price); exit();
					
				$service['price']=$price+($inc*0);
					
				//print_r($service1); exit();
				$this->db->where ( 'area_id', $result[1]['area_id'] );
				$this->db->where ( 'id', $result[1]['id'] );
				$this->db->where ( 'mealtype_id', $result[1]['mealtype_id'] );
				$this->db->where ( 'timeslots', $result[1]['timeslots'] );
				$this->db->update(TABLES:: $PRICELIST, $service);
			}
			
			if($result[2]['headcount']==3)
			{
					
				$price=$result[2]['price'];
				$inc=$result[2]['increment_on_head'];
					
				//print_r($price); exit();
					
				$service['price']=$price+($inc*1);
				
				$id= $result[2]['id'];
				//print_r($id); exit();
				
				
				$this->db->where ( 'area_id', $result[2]['area_id'] );
				$this->db->where ( 'id', $result[2]['id'] );
				$this->db->where ( 'mealtype_id', $result[2]['mealtype_id'] );
				$this->db->where ( 'timeslots', $result[2]['timeslots'] );
                                
				$this->db->update(TABLES:: $PRICELIST, $service);
				
				
				
				
				
			}
			
			if($result[3]['headcount']==4)
			{
				
				$price=$result[3]['price'];
				$inc=$result[3]['increment_on_head'];
				
				//print_r($price); exit();
				
				$service['price']=$price+($inc*2);
				
				//print_r($service1); exit();
				$this->db->where ( 'area_id', $result[3]['area_id'] );
				$this->db->where ( 'id', $result[3]['id'] );
				$this->db->where ( 'mealtype_id', $result[3]['mealtype_id'] );
				$this->db->where ( 'timeslots', $result[3]['timeslots'] );
				$this->db->update(TABLES:: $PRICELIST, $service);
			}
			
			
			if($result[4]['headcount']==5)
			{
			
				$price=$result[4]['price'];
				$inc=$result[4]['increment_on_head'];
                                
				//print_r($price); exit();
			
				$service['price']=$price+($inc*3);
			
				//print_r($service1); exit();
				$this->db->where ( 'area_id', $result[4]['area_id'] );
				$this->db->where ( 'id', $result[4]['id'] );
				$this->db->where ( 'mealtype_id', $result[4]['mealtype_id'] );
				$this->db->where ( 'timeslots', $result[4]['timeslots'] );
				$this->db->update(TABLES:: $PRICELIST, $service);
			}
			
			

			if($result[5]['headcount']==6)
			{
					
				$price=$result[5]['price'];
				$inc=$result[5]['increment_on_head'];
					
				//print_r($price); exit();
					
				$service['price']=$price+($inc*4);
					
				//print_r($service1); exit();
				$this->db->where ( 'area_id', $result[5]['area_id'] );
				$this->db->where ( 'id', $result[5]['id'] );
				$this->db->where ( 'mealtype_id', $result[5]['mealtype_id'] );
				$this->db->where ( 'timeslots', $result[5]['timeslots'] );
				$this->db->update(TABLES:: $PRICELIST, $service);
			}
			
			
			
			if($result[6]['headcount']==7)
			{
					
				$price=$result[6]['price'];
				$inc=$result[6]['increment_on_head'];
					
				//print_r($price); exit();
					
				$service['price']=$price+($inc*5);
					
				//print_r($service1); exit();
				$this->db->where ( 'area_id', $result[6]['area_id'] );
				$this->db->where ( 'id', $result[6]['id'] );
				$this->db->where ( 'mealtype_id', $result[6]['mealtype_id'] );
				$this->db->where ( 'timeslots', $result[6]['timeslots'] );
				$this->db->update(TABLES:: $PRICELIST, $service);
			}
			
			
			
			if($result[7]['headcount']==8)
			{
					
				$price=$result[7]['price'];
				$inc=$result[7]['increment_on_head'];
					
				//print_r($price); exit();
					
				$service['price']=$price+($inc*6);
					
				//print_r($service1); exit();
				$this->db->where ( 'area_id', $result[7]['area_id'] );
				$this->db->where ( 'id', $result[7]['id'] );
				$this->db->where ( 'mealtype_id', $result[7]['mealtype_id'] );
				$this->db->where ( 'timeslots', $result[7]['timeslots'] );
				$this->db->update(TABLES:: $PRICELIST, $service);
				
				$result ['status'] = 1;
				$result ['msg'] = "Updated Successfully";
				return $result;
				
			}
			//print_r($result); exit();
			
			//return $result;
			
			
			
			
			
			
			//$mealtype=$data1['mealtype_id'];
			//print_r($mealtype); exit();
			
	   //  $this->db->select ( '*' )->from (TABLES::$PRICELIST )->where ($mealtype );
	    // $query = $this->db->get ();
	    // $result = $query->result_array ();
	    // $data1['headcount']=$result['headcount'];
			
			//print_r($result); exit();
			
			
			//print_r($data1); exit();
			
			
			
			
			
			/* $this->db->where('timeslots',$data1['timeslots']);
			$this->db->update(TABLES:: $PRICELIST, $data1);
			//$result ['id'] = $this->db->update_id();
			$result ['status'] = 1;
			$result ['msg'] = "Updated Successfully";
			return $result; */
		}
		
		
		
		function headcount()
		{
			
			$this->db->select('*');
			$this->db->from( TABLES:: $HEADCOUNT);
			$query = $this->db->get();
			return $query->result_array();
		}
		
		
		
		function addheadcount($data)
		{
			
			$params = array (
					'headcount' =>$data['headcount']
			);
			
			$this->db->select ( '*' )->from ( TABLES::$HEADCOUNT )->where ( $params );
			$query = $this->db->get ();
			$result = $query->result_array ();
			//print_r(count($result)); exit();
			
			if (count($result) > 0)
			{
				$data2['msg'] = "Headcount already exists.";
				$data2['status'] = 0;
				return $data2;
			}
			
			else
			{
					$this->db->insert ( TABLES:: $HEADCOUNT, $data);
					$data2 ['status'] = 1;
					$data2 ['id'] = $this->db->insert_id();
					$data2 ['msg'] = "Added successfully";
					return $data2;
			}
		}
		
		
		
		
		function addpricelist($offers)
		{
			//print_r($offers['data']); exit();
			
			
			foreach($offers['data'] as $service)
			
			{
				//$service1['price']= $service['price'];
				$service1['timeslots']= $service['Timeslots'];
				$service1['headcount']= $service['Headcount'];
				//$service1['discount']= $service['discount'];
				$service1['status']= $service['status']; 
				//print_r($service1); exit();
				$this->db->where('headcount',$service['Headcount']);
				$this->db->where('timeslots',$service['Timeslots']);
				$this->db->update(TABLES:: $PRICELIST, $service1);
			
		    }
		       $result ['status'] = 1;
				$result ['msg'] = "Updated Successfully";
				return $result;
			
		}
		
		
		
		function addpricelist1($offers)
		{
			//print_r($offers['data']); exit();
				
			foreach($offers['data'] as $service){
				$service1['price']= $service['price'];
				$service1['timeslots']= $service['Timeslots'];
				$service1['headcount']= $service['Headcount'];
				$service1['status']= $service['status'];
				//print_r($service1); exit();
		     
				$this->db->where('status',$service['status']);
				$this->db->where('headcount',$service['Headcount']);
				$this->db->where('timeslots',$service['Timeslots']);
				$this->db->update(TABLES:: $PRICELIST, $service1);
		
				//$this->db->insert ( TABLES::$PRICELIST, $service1);
			}
			$result ['status'] = 1;
			$result ['msg'] = "Updated Successfully";
			return $result;
				
		}
		
		function totalpricelist()
		{
			

		$this->db->select ( 'PRICELIST.*, MEALTYPE.id ,MEALTYPE.name, TIMESLOTS.slot' );
		$this->db->from ( TABLES::$PRICELIST . ' AS PRICELIST' );
		$this->db->join ( TABLES::$MEALTYPE . ' AS MEALTYPE', 'MEALTYPE.id=PRICELIST.mealtype_id', 'left' );
		$this->db->join ( TABLES::$TIMESLOTS . ' AS TIMESLOTS', 'PRICELIST.timeslots=TIMESLOTS.id', 'left' );
		
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
			//$this->db->select('*')
			//->from(TABLES::$PRICELIST);
			//$query = $this->db->get();
			//return $query->result_array();
		
		}
		
		
		
		
		function selectsociety()
		{
			$this->db->select('*');
			$this->db->from( TABLES:: $SOCIETY);
			$this->db->where('status',1);
			$query = $this->db->get();
			return $query->result_array();

		}
		
		
		
		function societylist($subcatid)
		{
			$this->db->select ( 'PRICELIST.*, MEALTYPE.id ,MEALTYPE.name, TIMESLOTS.slot' );
		$this->db->from ( TABLES::$PRICELIST . ' AS PRICELIST' );
		$this->db->join ( TABLES::$MEALTYPE . ' AS MEALTYPE', 'MEALTYPE.id=PRICELIST.mealtype_id', 'left' );
		$this->db->join ( TABLES::$TIMESLOTS . ' AS TIMESLOTS', 'PRICELIST.timeslots=TIMESLOTS.id', 'left' );
		$this->db->where('PRICELIST.area_id',$subcatid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		
		//print_r($result); exit();
		return $result;
		
		}
		
		
		function selectarea()
		{
		
			
			$this->db->select('*');
			$this->db->from( TABLES:: $AREA);
			$this->db->where('status',1);
			$query = $this->db->get();
			return $query->result_array();
		
		}
		
		
		
		function adddiscount($discount)
		{
		
			$data['discount']=$discount['discount'];
			//$data1['headcount']=$discount['headcount'];
		//	$data1['timeslots']=$discount['slot'];
			
			//print_r($data); exit();
			
		$this->db->where('headcount',$discount['headcount']);
		$this->db->where('timeslots',$discount['slot']);
		$this->db->update(TABLES:: $PRICELIST, $data);
		//$result ['id'] = $this->db->update_id();
		$result ['status'] = 1;
		$result ['msg'] = "Updated Successfully";
		return $result;
		}
		
		
		
		
		
		
}