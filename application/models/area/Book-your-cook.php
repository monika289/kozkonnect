<link href="<?php echo asset_url();?>frontend/css/date-time-picker.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo asset_url();?>frontend/js/datepicker.js"></script>
<div class="book-cook">
    <div class="row">
     <form class="custom-form"  method="post" id="booking-form" >
        <div class="col-md-8 col-lg-8 col-sm-8 white-bg">
            <h1>book your cook</h1>
            <p>Reserve your cook just by filling this simple form </p>
           		 <input type="hidden" value="" name="subTotal" id="subTotal">
            	<input type="hidden" value="<?php echo $hbuserid;?>" name="userid" id="userid">
                <div class="input-group">
                    <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/7.png" alt="scaleup" class=""></span>
                    <input type="text"  readonly class="form-control" name="name" placeholder="Enter your name" value="<?php echo $hbusername;?>">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/6.png" alt="scaleup" class=""></span>
                    <input type="email" readonly class="form-control" name="email" placeholder="Enter your email id" value="<?php echo $hbuseremail;?>">
                </div>
                 <div class="input-group">
                    <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/5.png" alt="scaleup" class=""></span>
                    <input type="text" readonly class="form-control" name="" placeholder="Enter your phone no." value="<?php echo $hbusermobile;?>">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/team.png" alt="scaleup" class=""></span>
                     <select name="head_count" id="head_count" class="form-control custom-select" onchange="headCountChanged();">
                   		 <?php foreach($headcounts as $headcount) {?>
                   		 <option value="<?php echo $headcount['headcount']; ?>"><?php echo $headcount['headcount'];?></option>
                   		 <?php }?>
                    </select>
                </div>
                <div class="input-group">
                    <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/team.png" alt="scaleup" class=""></span>
                    <select name="area" id="area" class="form-control custom-select" onchange="areaChanged();">
                   		 <option>Select Area</option>
                   		 <?php foreach($areas as $area) {?>
                   		 <option value="<?php echo $area['id']; ?>"><?php echo $area['name'];?></option>
                   		 <?php }?>
                    </select>
                </div>
                <div class="input-group">
                    <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/Contact-us/4.png" alt="scaleup" class=""></span>
                    <input type="text" class="form-control" name="address" id="address" placeholder="Enter your address">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/Contact-us/4.png" alt="scaleup" class=""></span>
                    <input type="text" class="form-control" name="locality" id="locality" placeholder="Enter your locality">
                     <input type="hidden" class="form-control" name="latitude" placeholder="" id="latitude">
                     <input type="hidden" class="form-control" name="longitude" placeholder="" id="longitude">
                </div>
                 <div class="input-group">
                    <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/Contact-us/4.png" alt="scaleup" class=""></span>
                    <input type="text" class="form-control" name="landmark" id="landmark" placeholder="Enter nearest landmark">
                </div>
               
                <div class="input-group" id="sandbox-container">
                    <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/calendar.png" alt="scaleup" class=""></span>
                    <input type="text" class="form-control" name="start_date" id="start_date" placeholder="Enter start date" id="sandbox-container">
                </div>
				<div id="timeslot-row">
	                <div class="row" >
	                    <div class="col-md-5 col-lg-5 col-sm-5">
	                        <div class="input-group">
	                            <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/cutlery.png" alt="scaleup" class=""></span>
	                            <select name="mealtype" id="mealtype"  class="form-control custom-select mealtype-select" onchange="getTimeslots(this)">
	                                <option disabled selected value='0' data-price="0">Enter meal type</option>
	                                <?php foreach($mealTypes as $mealtype) {?>
	                                	 <option value="<?php echo $mealtype['id'];?>"><?php echo $mealtype['name'];?></option>
	                                <?php }?>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="col-md-5 col-lg-5 col-sm-5">
	                        <div class="input-group">
	                            <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/clock.png" alt="scaleup" class=""></span>
	                            <select name="timeslot[]" id="timeslot" class="form-control custom-select time-slot-price" onchange="updateTotals();">
	                            	<option value='0' selected data-price="0">Enter meal type</option>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="col-md-2"><button type="button" data-toggle="tooltip" title="Add more meal" class="add-meal fright"><img src="<?php echo asset_url();?>frontend/images/form/plus-button.png" alt="scaleup"  onclick="addMealTypeRow();"></button></div>
	                </div>
                </div>
                <div class="row">
                	<b><p id="response"></p></b>
                </div>
                <div class="row text-left custom-margin">
                    <button type="submit" class="fright book-btn">Book your Cook</button>
                </div>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 grey-bg">
            <h3>Order Summary</h3>
            <div class="custom-line"></div>
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-8">
                    <p>Headcount</p>
                    <p>Meal Type</p>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-4 text-right">
                    <p><b id="headcountDisplay">1</b></p>
                    <p><b id="mealTypeDisplay"></b></p>
                </div>
            </div>
            <div class="custom-line"></div>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse1">
                              <div class="row">
                                <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10">
                                   <h4>Promotional code</h4>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 text-right">
                                    <span class="glyphicon glyphicon-menu-down down"></span>
                                    <span class="glyphicon glyphicon-menu-up up"></span>
                                </div>
                            </div>
                            </a>
                        </h4>
                    </div>
                <div id="collapse1" class="panel-collapse collapse in">
                    <div class="panel-body">
                            <div class="input-group">
                                <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/percent.png" alt="scaleup" class=""></span>
                                <input type="text" class="form-control" name="coupon_code" id="coupon_code" placeholder="Enter coupon code">
                                <span class="input-group-addon"> <button type="button" onclick="javascript:checkCoupon();" class="apply-coupon">APPLY</button> </span>
                            </div>
                            <p id="alert-coupon"></p>
                           <!--  <h4 class="text-center">OR</h4>
                            <div class="input-group">
                                <span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/discount.png" alt="scaleup" class=""></span>
                                <input type="text" class="form-control" name="" placeholder="Redeem points">
                                <span class="input-group-addon"><button type="button" class="apply-coupon">REDEEM</button></span>
                            </div>  -->
                    </div>
                </div>
            </div>
        </div>
          <div class="custom-line"></div>
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-8">
                <p>Monthly pay</p>
                <p>Discount</p>
                <p>Tax</p>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-4 text-right">
                <p>Rs <b id="subTotalDisplay"></b></p>
                <p>Rs <b id="discountDisplay"></b></p>
                <p>Rs <b id="taxDisplay"></b></p>
            </div>
        </div>
          <div class="custom-line"></div>
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-8">
                <h5>TOTAL</h5>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-4 text-right">
                <h5>Rs <b id="grandTotalDisplay"></b></h5>
            </div>
        </div>
        <div class="custom-line"></div>
     </div>
     </form>
   </div>
</div>
<script type="text/javascript" src="<?php echo asset_url();?>frontend/common/js/selectize.min.js"></script>

<script>
   $(".selectize").selectize({});
</script>
<script>
$.getScript("//maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyA92M4tQATmUa-sQahIxITnLLSOqXa0-6o&libraries=places&sensor=false&callback=initMap");

function initMap() {
	var options = {
	  	componentRestrictions: {country: 'in'}
	};
	var input =  document.getElementById('locality');
	var autocomplete = new google.maps.places.Autocomplete(input,options);
	autocomplete.addListener('place_changed', function() {
		var place = autocomplete.getPlace();
	    if (!place.geometry) {
	      window.alert("Autocomplete's returned place contains no geometry");
	      return;
	    }
	    $('#latitude').val(place.geometry.location.lat());
	    $('#longitude').val(place.geometry.location.lng());
	    $('#locality').removeClass('FieldError');
		$('#alert-landmark').css('color','#696969');
		$('#alert-landmark').html('');
	});
}

$('#sandbox-container input').datepicker({
    autoclose: true
});

$('#sandbox-container input').on('show', function(e) {
    console.debug('show', e.date, $(this).data('stickyDate'));

    if (e.date) {
        $(this).data('stickyDate', e.date);
    } else {
        $(this).data('stickyDate', null);
    }
});

$('#sandbox-container input').on('hide', function(e) {
    console.debug('hide', e.date, $(this).data('stickyDate'));
    var stickyDate = $(this).data('stickyDate');

    if (!e.date && stickyDate) {
        console.debug('restore stickyDate', stickyDate);
        $(this).datepicker('setDate', stickyDate);
        $(this).data('stickyDate', null);
    }
});
</script>
<script>
function addMealTypeRow(){
			var html ="";
			var html ="";
			html += '<div class="row" ><div class="col-md-5 col-lg-5 col-sm-5"><div class="input-group"><span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/cutlery.png" alt="scaleup" ></span>';
			html += '<select name="mealtype" id="mealtype"  class="form-control custom-select mealtype-select" onchange="getTimeslots(this)"> <option disabled selected value>Enter meal type</option><?php  foreach($mealTypes as $mealtype){?><option  value="<?php echo $mealtype["id"]?>"><?php echo $mealtype["name"]?></option><?php }?></select>';
			html +=' </div></div>';	
			html +=' <div class="col-md-5 col-lg-5 col-sm-5"><div class="input-group"><span class="input-group-addon"> <img src="<?php echo asset_url();?>frontend/images/form/clock.png" alt="scaleup" ></span>';
			html +=' <select name="timeslot[]" id="timeslot" class="form-control custom-select time-slot-price" onchange="updateTotals();"> </select>';
			html +=' </div></div>';
			html +=' <div class="col-md-2"><button type="button" data-toggle="tooltip" title="Add more meal" class="add-meal fright"><img src="<?php echo asset_url();?>frontend/images/form/remove.png" alt="scaleup"  onclick="removeMealTypeRow(this);"></button></div>';
			html +=' </div>';
		  $("#timeslot-row").append(html);
		}
function removeMealTypeRow(a){
	$(a).parent().parent().parent().remove();
	//$(a).closest('div').find('.row').remove();
	updateTotals();
 }

function headCountChanged(){
	$('.time-slot-price').each(function() {
		$(this).val('');
    });
	$('.mealtype-select').each(function() {
		$(this).val('');
		});
	updateTotals();
}
function areaChanged(){
	$('.time-slot-price').each(function() {
		$('option:selected', this).val('0');
    });
	$('.mealtype-select').each(function() {
		$('option:selected', this).val('0');
		});
	updateTotals();
}

function getTimeslots(mealtype){
	var mealtypeId = $(mealtype).val();
	var countm = 0;
	$('.mealtype-select').each(function() {
  		mealDisplay = $('option:selected', this).val();
  		
  		if(mealDisplay == mealtypeId){
  			countm++;
  	  		}
	  		 if(countm > 1) {
	  			$(this).val('');
		  	}
    });
	var area_id = $('#area option:selected').val();
	var head_count = $('#head_count').val();
	if(countm == 1) {
	if(area_id != '' && head_count != '' && mealtypeId != '') {
		 $.post(base_url+"slots_by_mealtype", { mealtypeId : mealtypeId, area_id:area_id, head_count:head_count }, function(data){
			if(data.status == 1){
				 $(mealtype).closest('div.row').find("#timeslot").html(data.html);
				}	
			},'json'); 
	} else{
			alert('Please Add all combinations.');
		}}else{
				alert('You have already selected this mealtype.');
			}
 }


function updateTotals(){
	var total ="0";
	//var discount = $('#discount').val();
	//var dis_type = $('#discount-type').val();
	var sTotal = 0;
	var total_tax = 18;
	var sgst = 0;
	var gTotal = 0;
	var discount =0;
	var mealDisplay ='';
  	$('.mealtype-select').each(function() {
  	  	var check_price = $(this).closest('div.row').find('time-slot-price');
  	  	var price = $('option:selected', check_price).attr('data-price');
  	  	//alert(price);
  		mealDisplay += $('option:selected', this).text()+",";
    	});

  	$('.time-slot-price').each(function() {
    	  sTotal += parseFloat($('option:selected', this).attr('data-price'));
    	  discount += parseFloat($('option:selected', this).attr('data-discount'));
      	});
  	
  	$('#subTotal').val(sTotal);
  	$('#subTotalDisplay').html(sTotal);
  	
  	var tax_amount = parseFloat((sTotal*total_tax)/100);
  	$('#taxDisplay').html(tax_amount);
  	var grandTotal = parseFloat((sTotal + tax_amount - discount));
	$('#headcountDisplay').html($('#head_count').val());
	$('#discountDisplay').html(discount);
	$('#mealTypeDisplay').html(mealDisplay);
  	$('#grandTotalDisplay').html(grandTotal);
  	//alert(tax_amount)
  	//alert(sTotal);
  }

//Coupon check
function checkCoupon() {
	if($("#coupon_code").val() == "") {
		alert("Please enter a coupon code.");
	} else {
		var area_id = $('#area option:selected').val();
		$.post("<?php echo base_url();?>applycoupon",{coupon_code: $("#coupon_code").val(), order_date:$("#start_date").val(),area_id:area_id,email: '<?php echo $hbuseremail;?>'},function(data){
			//alert(data.coupon['discount']);
			$('#coupon_code').addClass('FieldError');
			$('#alert-coupon').html(data.msg);
			$('#alert-coupon').css('color','#696969');
		},'json');
	}
}

</script>
<script src="<?php echo asset_url() ?>frontend/js/jquery.validate.js"></script>
<script src="<?php echo asset_url() ?>frontend/common/js/jquery.form.js"></script>
<script>

jQuery.validator.addMethod('validlongup', function(value, element) {
	 var longt = jQuery("#longitude_update2").val();
	    if(longt !=''){
			return 1;
		    } else {
				return 0;
			    }
	}, 'Select locality from google address.');

jQuery.validator.setDefaults({
    ignore: []
});

	  $("#booking-form").validate({
			rules: { 
			    name: "required", 
				mobileNo :{
					 required: true,
		                minlength: 10,
		                maxlength: 10,
		                number: true,
		        },  
				email: {
					required:true,
					email:true, 
				},
				address : {
					required :true,
					},
				locality : {
						required: true,
					}
				},
				messages:{
	
					mobileNo:{
						//required :'Enter Mobile number',
						remote:'Mobile number already exist'
					}	
			   }, 
				submitHandler :function(form){
					// Prevent form submission
					//debugger;
					$("#btnSubmit").attr('disabled', 'disabled');
					addBooking();
				}
		});  

	  function addBooking() {
		  ajaxindicatorstart("Loading...");
			var options = {
			 		target : '#response', 
			 		beforeSubmit : showAddRequest,
			 		success :  showAddResponse,
			 		url : base_url+'booking',
			 		semantic : true,
			 		dataType : 'json'
			 	};
		   	$('#booking-form').ajaxSubmit(options);
		}

		function showAddRequest(formData, jqForm, options){
			$("#response").hide();
		   	var queryString = $.param(formData);
			return true;
		}
		   	
		function showAddResponse(resp, statusText, xhr, $form){
			alert(resp.status);
			if(resp.status == 1) {
				ajaxindicatorstop(); 
				window.location.href = base_url+'My-profile';
		  	} else {
				$("#btnSubmit").removeAttr("disabled");
		  		$("#response").removeClass('alert-danger');
		        $("#response").addClass('alert-success');
		        $("#response").html(resp.msg);
		        $("#response").show();
		  	}
		}
		/*  ************************* Booking END ***********************************/
</script>

<script>
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<script>
$(".add-meal").click(function() {
    $(".more-meal-type").show();
});

</script>