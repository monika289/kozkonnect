<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Booking_Model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	
	function bookingconfirmname()
	{
		 $this->db->select('*');
		 $this->db->from( TABLES:: $BOOKING);
		 $this->db->where('status',1);
		 $query = $this->db->get();
		 return $query->result_array();  
	}
	
	function customerbookname()
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $USERS);
		$query = $this->db->get();
		return $query->result_array(); 
	}
	function booking_user_detail($bookin_id) {
                 $this->db->select('*');
		 $this->db->from( TABLES:: $BOOKING);
		 $this->db->where('bookingid',$bookin_id);
		 $query = $this->db->get();
		 return $query->result_array();  
            
        }
        function meal_slot($bookin_id) {
                 $this->db->select('*');
		 $this->db->from('tbl_booking_slots');
		 $this->db->where('bookingid',$bookin_id);
		 $query = $this->db->get();
		 return $query->result_array();  
            
        }
	function customerarea()
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $AREA);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	
	
	
	function bookingmobile($usermobile)
	{
		$this->db->select ( 'users.*,society.id as societyid,society.name as societyname');
		$this->db->from ( TABLES::$USERS . ' AS users' );
		$this->db->join ( TABLES::$SOCIETY. ' AS society', 'users.area_id=society.area_id', 'left' );
		$this->db->like ( 'users.mobile', $usermobile,'both' );
		//$this->db->where('users.id',$userlist);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	
	
	
	
	function userdetail($userlist)
	{
		$this->db->select ( 'users.*,society.id as societyid,society.name as societyname');
		$this->db->from ( TABLES::$USERS . ' AS users' );
		$this->db->join ( TABLES::$SOCIETY. ' AS society', 'users.area_id=society.area_id', 'left' );
		$this->db->like ( 'users.name', $userlist,'both' );
		//$this->db->where('users.id',$userlist);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	
	function adduserdetail($data)
	{
		
		$username=$data['name'];
		$this->db->select ( 'id' )->from ( TABLES::$USERS );
		$this->db->where('name',$username);
		$query = $this->db->get ();
		$result = $query->result_array ();
		$userid=$result[0]['id'];
		//$this->db->select ( 'userid' )->from ( TABLES::$BOOKING );
		//$this->db->where('userid',$userid);
		//$this->db->where('status',1);
		//$query = $this->db->get ();
		//$result = $query->result_array();
		//print_r($result); exit();
		//$userid_new=$result[0]['userid'];
		//print_r($userid_new); exit();
		$data['userid']=$userid;
		//$userid=$userid_new;
		$this->db->select('userid');
		$this->db->from( TABLES:: $BOOKING);
		$this->db->where('userid',$userid);
		$query = $this->db->get();
		$result = $query->result_array();
		
		if(!empty($result))
		{
			$area['status'] = 0;
		$this->db->where ( 'areaid',$data['areaid']);
		$this->db->where ( 'userid', $data['userid'] );
		$this->db->update ( TABLES::$BOOKING, $area );
		$this->db->insert(TABLES::$BOOKING,$data);
		return $this->db->insert_id();
		}
		else 
		{
			 $this->db->insert ( TABLES:: $BOOKING, $data);
			 $data2 ['status'] = 1;
			 $data2 ['id'] = $this->db->insert_id();
			 $data2 ['msg'] = "Added successfully";
			 return $data2; 
		}
		
	}
	
	function bookinglist()
	{
		
	
		$this->db->select ( 'booking.*,users.area,society.name as societyname,bstatus.status as booking_status');
		$this->db->from ( TABLES::$BOOKING . ' AS booking' );
		$this->db->join ( TABLES::$USERS. ' AS users', 'users.area_id=booking.areaid', 'left' );
		$this->db->join ( TABLES::$SOCIETY. ' AS society', 'society.id=booking.society_id', 'left' );
		$this->db->join ( TABLES::$BOOKINGSTATUS. ' AS bstatus', 'bstatus.id=booking.status', 'left' );
		$this->db->where ('booking.status !=',0);
		$this->db->group_by('booking.bookingid');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
		
	}
        function bookinglist_status($status){
                $this->db->select ( 'booking.*,users.area,society.name as societyname,bstatus.status as booking_status');
		$this->db->from ( TABLES::$BOOKING . ' AS booking' );
		$this->db->join ( TABLES::$USERS. ' AS users', 'users.area_id=booking.areaid', 'left' );
		$this->db->join ( TABLES::$SOCIETY. ' AS society', 'society.id=booking.society_id', 'left' );
		$this->db->join ( TABLES::$BOOKINGSTATUS. ' AS bstatus', 'bstatus.id=booking.status', 'left' );
		$this->db->where ('booking.status =', $status);
		$this->db->group_by('booking.bookingid');
                $this->db->order_by('booking.bookingid','desc');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
        }
	
	function bookingstatus()
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $BOOKINGSTATUS);
		$query = $this->db->get();
		return $query->result_array();
	}

	function turnonbookinglist($id) {
	
		//print_r($id); exit();
		$area ['status'] = 1;
		$this->db->where ( 'bookingid', $id );
		$this->db->update ( TABLES::$BOOKING, $area );
		
	}
	
	function turnofbookinglist($id) {
		$area ['status'] = 0;
		$this->db->where ( 'bookingid', $id );
		$this->db->update ( TABLES::$BOOKING, $area );
	}
	
	
	function updatebooking($id)
	{
		$this->db->select ( 'booking.*,society.name as societyname,users.area,bstatus.status as bookstatus');
		$this->db->from ( TABLES::$BOOKING . ' AS booking' );
		$this->db->join ( TABLES::$SOCIETY. ' AS society', 'society.id=booking.society_id', 'left' );
		$this->db->join ( TABLES::$USERS. ' AS users', 'users.area_id=booking.areaid', 'left' );
		$this->db->join ( TABLES::$BOOKINGSTATUS. ' AS bstatus', 'bstatus.id=booking.status', 'left' );
		$this->db->where('booking.bookingid',$id);
		$this->db->group_by('booking.bookingid');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	
	function chefallocation($id)
	{
	    	$bookingid=$id;
            $this->db->select ( 'areaid' )->from ( TABLES::$BOOKING )
			->where('bookingid',$bookingid);
			$query = $this->db->get ();
			$resultarea = $query->result_array ();
			$service1= $resultarea[0]['areaid'];
			$this->db->select ( 'chefarea.chefid,chefs.name');
			$this->db->from ( TABLES::$CHEFAREA . ' AS chefarea' );
			$this->db->join ( TABLES::$USERS. ' AS users', 'users.area_id=chefarea.areaid', 'left' );
			$this->db->join ( TABLES::$CHEFS. ' AS chefs', 'chefs.id=chefarea.chefid', 'left' );
			$this->db->where('chefarea.areaid',$service1);
			$this->db->group_by('chefarea.chefid');
			$query = $this->db->get ();
			$result = $query->result_array ();
			return $result;
	     
	}
	
	
	function chefallocationadd($offers)
	{
		$chefid['chefid']=$offers['chefid'];
		$this->db->where('bookingid',$offers['bookingid']);
		$this->db->where('head_count',$offers['head_count']);
		$this->db->where('slot',$offers['slot']);
		$this->db->where('mealtype_name',$offers['mealtype_name']);
		$this->db->update(TABLES:: $BOOKINGSLOT, $chefid);
		$result ['status'] = 1;
		$result ['msg'] = "Updated Successfully";
		return $result;
		
	}

	function updatename($id)
	{
	   
		if($id['name']!=='')
		{
		$this->db->where ( 'bookingid', $id['bookingid'] );
		$this->db->update ( TABLES::$BOOKING, $id );
		$result ['status'] = 1;
		$result ['msg'] = "Updated Successfully";
		return $result;
		}
		else {
			$result ['status'] = 0;
			$result ['msg'] = "UserName can not be null!";
			return $result;
		}
	}
	
	function updateemail($id)
	{
	
		$email=$id['email'];
		$regex = '/^[^0-9][_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
		
		if (preg_match($regex, $email)) 
		{
	    $this->db->where ( 'bookingid', $id['bookingid'] );
		$this->db->update ( TABLES::$BOOKING, $id );
		$result ['status'] = 1;
		$result ['msg'] = "Updated Successfully";
		return $result;
		} 
		else 
		{
		
		$result ['status'] = 0;
		$result ['msg'] = "Please Enter valid Emailid";
		return $result;
		}
	}
	
	
	function updatemobile($id)
	{
		$mobile=$id['mobile'];
	    if(preg_match("/^[0-9]{10}+$/", $mobile))
	    {
		
		$this->db->where ( 'bookingid', $id['bookingid'] );
		$this->db->update ( TABLES::$BOOKING, $id );
		$result ['status'] = 1;
		$result ['msg'] = "Updated Successfully";
		return $result;
		
	    }
		else 
		{
			$result ['status'] = 0;
			$result ['msg'] = "Please Enter Correct 10digit mobile no.";
			return $result;
			
		}
	
	}
	
	function bookingslot($id)
	{
		$this->db->select ( 'bslot.slot,bslot.slotid,bslot.mealtype_name,bslot.mealtype_id,bslot.head_count,bslot.amount,bslot.chefid');
		$this->db->from ( TABLES::$BOOKING . ' AS booking' );
		$this->db->join ( TABLES::$BOOKINGSLOT. ' AS bslot', 'bslot.bookingid=booking.bookingid', 'left' );
		$this->db->where('booking.bookingid',$id);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	function selectallarea()
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $AREA);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function updatebooking_dada($data)
	{
		$this->db->where('bookingid',$data['bookingid']);
		$this->db->update(TABLES:: $BOOKING, $data);
		$result ['status'] = 1;
		$result ['msg'] = "Updated Successfully";
		return $result;
	}
	
	function selectslot()
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $TIMESLOTS);
		$this->db->where('status', 1);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function mealtypelist()
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $MEALTYPE);
		$this->db->where('status', 1);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function headcount()
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $HEADCOUNT);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	
	function addbookingslot($data1)
	{
		
		
		
		$username=$data1['book'][0]['userid'];
		$this->db->select ( 'id' )->from ( TABLES::$USERS );
		$this->db->where('name',$username);
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r($result); exit();
		$userid=$result[0]['id'];
		
		
		 //print_r($userid); exit();
		
	      // $userid=$data1['book'][0]['userid'];
	       $enddate=date("Y-m-d");
	       $nextdate= date('Y-m-d', strtotime(' +1 day'));
	       
	     //  $userid=$data1['userid'];
	       $this->db->select ( 'userid' )->from ( TABLES::$BOOKINGSLOT )
	       ->where('userid',$userid);
	       $query = $this->db->get ();
	       $result = $query->result_array ();
	       
	       //print_r($result); exit();
	       
	       if(!empty($result))
	       {
	       	$area ['status'] = 0;
	       	$area['end_date']=$enddate;
	       	$this->db->where ( 'userid', $userid );
	       	$this->db->update ( TABLES::$BOOKINGSLOT, $area );
	        foreach($data1['book'] as $service){
				$service1['created_datetime']= $service['created_datetime'];
				$service1['start_date']= $nextdate; 
			//	$service1['start_date']= $service['start_date'];
				$service1['bookingid']= $service['bookingid'];
				$service1['userid']= $userid;
				$service1['mealtype_id']= $service['mealtype_id'];
				$service1['status']= $service['status'];
				$service1['slotid']= $service['slotid'];
				$service1['head_count']= $service['head_count'];
				$service1['amount']= $service['amount'];
				$i=0;
				$this->db->select ( 'name' )->from ( TABLES::$MEALTYPE )
				->where('id',$service1['mealtype_id']);
				$query = $this->db->get ();
				$result = $query->result_array ();
				//print_r($result[$i]['name']); exit();
				$this->db->select ( 'slot' )->from ( TABLES::$TIMESLOTS )
				->where('id',$service1['slotid']);
				$query = $this->db->get ();
				$resultslot = $query->result_array ();
				$service1['slot']= $resultslot[$i]['slot'];
				$service1['mealtype_name']= $result[$i]['name'];
				$offer_id = $this->db->insert ( TABLES::$BOOKINGSLOT, $service1);
			}
			$i++;
	       
	       }
	       else 
			{
			foreach($data1['book'] as $service){
					
				$service1['created_datetime']= $service['created_datetime'];
				//$service1['start_date']= $nextdate;
				$service1['start_date']= $service['start_date'];
				$service1['bookingid']= $service['bookingid'];
				$service1['userid']= $userid;
				$service1['mealtype_id']= $service['mealtype_id'];
				$service1['status']= $service['status'];
				$service1['slotid']= $service['slotid'];
				$service1['head_count']= $service['head_count'];
				$service1['amount']= $service['amount'];
			
				$i=0;
				$this->db->select ( 'name' )->from ( TABLES::$MEALTYPE )
				->where('id',$service1['mealtype_id']);
				$query = $this->db->get ();
				$result = $query->result_array ();
				$this->db->select ( 'slot' )->from ( TABLES::$TIMESLOTS )
				->where('id',$service1['slotid']);
				$query = $this->db->get ();
				$resultslot = $query->result_array ();
					
					
				$service1['slot']= $resultslot[$i]['slot'];
				$service1['mealtype_name']= $result[$i]['name'];
					
					
				$offer_id = $this->db->insert ( TABLES::$BOOKINGSLOT, $service1);
			}
			$i++;
	       }
		
	}
	
	
	function addmealtyperecord($mealtype,$slot,$headcount)
	{
		$this->db->select('price,discount');
		$this->db->from( TABLES:: $PRICELIST);
		$this->db->where ( 'mealtype_id', $mealtype );
		$this->db->where ( 'headcount', $headcount );
		$this->db->where ( 'timeslots', $slot );
		$query = $this->db->get();
		return $query->result_array();
		
		
		
		
		
	}
	//Coupon code Added By Suraj
	
	public function getBookingsByEmail($email) {
		$this->db->select('count(bookingid) as bookings', FALSE)
		->from(TABLES::$BOOKING);
		$this->db->where('email',$email);
		$this->db->where('status !=', 0);
		$query = $this->db->get();
		$result = $query->result_array();
		if(count($result) > 0) {
			return $result[0]['bookings'];
		} else {
			return 0;
		}
	}
	
	public function getBookingsByEmailAndCoupon($email,$coupon) {
		$this->db->select('count(bookingid) as bookings', FALSE)
		->from(TABLES::$BOOKING);
		$this->db->where('email',$email);
		$this->db->where('coupon_code',$coupon);
		$this->db->where('status !=', 0);
		$query = $this->db->get();
		$result = $query->result_array();
		if(count($result) > 0) {
			return $result[0]['bookings'];
		} else {
			return 0;
		}
	}
	
	
	
	function leaverecord($userid)
	{
		
		//print_r($userid); exit();
		
		$this->db->select ('bookingid');
		$this->db->from ( TABLES::$BOOKING  );
		$this->db->like ( 'name', $userid,'both' );
		//$this->db->where('name',$userid);
		$this->db->where('status !=',0);
		$query = $this->db->get ();
		$result = $query->result_array ();
		
		$bookingid=$result[0]['bookingid'];
		
		//print_r($bookingid); exit();
		
	    $this->db->select ( '*');
		$this->db->from ( TABLES::$BOOKING  );
		$this->db->where('bookingid',$bookingid);
		$this->db->where('status !=',0);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	
	}
	
	
	
	
	
	function usermobileleave($mobile)
	{
		$this->db->select ('bookingid');
		$this->db->from ( TABLES::$BOOKING  );
		$this->db->like ( 'mobile', $mobile,'both' );
	   //$this->db->where('mobile',$mobile);
		$this->db->where('status',1);
		$query = $this->db->get ();
		$result = $query->result_array ();
		
		$bookingid=$result[0]['bookingid'];
		
		//print_r($bookingid); exit();
		
		$this->db->select ( '*');
		$this->db->from ( TABLES::$BOOKING  );
		$this->db->where('bookingid',$bookingid);
		$this->db->where('status',1);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
		
	}
	
	
	
	
	
	function usermobileleave1($mobile)
	{
	
		//print_r($mobile); exit();
	
		$this->db->select ('bookingid');
		$this->db->from ( TABLES::$BOOKING  );
		$this->db->like ( 'mobile', $mobile,'both' );
		//$this->db->where('mobile',$mobile);
		$this->db->where('status',1);
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r($result); exit();
		$bookingid=$result[0]['bookingid'];
		
		//print_r($bookingid); exit();
	
		$this->db->select ( 'head_count,mealtype_name');
		$this->db->from ( TABLES::$BOOKINGSLOT  );
		$this->db->where('bookingid',$bookingid);
		$this->db->where('status',1);
		$query = $this->db->get ();
		$result = $query->result_array ();
	
		//print_r($result); exit();
		return $result;
	}
	
	
	
	
	function user_leave($userid)
	{

		//print_r($userid); exit();
		
		$this->db->select ('bookingid');
		$this->db->from ( TABLES::$BOOKING  );
		$this->db->like ( 'name', $userid,'both' );
		//$this->db->where('name',$userid);
		$this->db->where('status !=',0);
		$query = $this->db->get ();
		$result = $query->result_array ();
	    //print_r($result); exit();
		$bookingid=$result[0]['bookingid'];
		//print_r($bookingid); exit();
		
		$this->db->select ( 'head_count,mealtype_name');
		$this->db->from ( TABLES::$BOOKINGSLOT  );
		$this->db->where('bookingid',$bookingid);
		$this->db->where('status',1);
		$query = $this->db->get ();
		$result = $query->result_array ();
		
		//print_r($result); exit();
		return $result;
	}
	
	
	
	function userselect($username)
	{
		
		//print_r($username); exit();
		
		$this->db->select ( '*');
		$this->db->from ( TABLES::$BOOKING  );
		$this->db->like ( 'name', $username,'both' );
		$this->db->where('status !=',0);
		$query = $this->db->get ();
		$result = $query->result_array ();
		
		//print_r($result); exit();
		
		return $result;
	}
	
	
	
	function bookingmobile2($username)
	{
		//print_r($username); exit();
		$this->db->select ( '*');
		$this->db->from ( TABLES::$USERS  );
		$this->db->like ( 'mobile', $username,'both' );
		$this->db->where('status',1);
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r($result); exit();
		return $result;
	}
	
	
	function username2($username)
	{
	
		
		//print_r($username); exit();
		 $this->db->select ( '*');
		$this->db->from ( TABLES::$USERS  );
		$this->db->like ( 'name', $username,'both' );
		$this->db->where('status',1);
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r($result); exit();
		return $result; 
	}
	
	
	
	
	
	function userselectmobile($mobile)
	{
	
		//print_r($username); exit();
	
		$this->db->select ( '*');
		$this->db->from ( TABLES::$BOOKING  );
		$this->db->like ( 'mobile', $mobile,'both' );
		$this->db->where('status',1);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	
	
	function adduserleave($data)
	{
		
		foreach($data['data1'] as $service)
		{
			$username=$service['username'];
			
						
			$service1['headcount']=$service['headcount'];
			$service1['mealtype'] = $service['mealtype'];
			$service1['start_date'] = $service['start_date'];
			$service1['end_date'] = $service['end_date'];
			$i=0;
			$this->db->select('userid');
			$this->db->from( TABLES::$BOOKING);
			$this->db->where ( 'name', $username );
			$this->db->where ( 'status !=', 0);
			$query = $this->db->get();
			$result = $query->result_array ();
			$service1['userid']= $result[$i]['userid'];
			
			
			$this->db->select ('bookingid');
			$this->db->from ( TABLES::$BOOKING  );
			$this->db->where('name',$username);
			$this->db->where('status !=',0);
			$query = $this->db->get ();
			$result = $query->result_array ();
			//print_r($result); exit();
			$service1['bookingid']=$result[$i]['bookingid'];
			
			
			
			//print_r($service1); exit();
			
			$params = array 
			(
					'mealtype' =>$service['mealtype'],
					'start_date' =>$service['start_date'],
					'end_date' =>$service['end_date']
			);
			$this->db->select ( '*' )->from ( TABLES::$USERLEAVE )->where ( $params );
			$query = $this->db->get ();
			$result = $query->result_array ();
			if (count($result) > 0)
			{
				$data2['msg'] = "customer leave  already exists.";
				$data2['status'] = 0;
				return $data2;
			}
			else
			{
				$Start_date = $service['start_date'];
				$end_date  = $service['end_date'];
			   if(!empty($Start_date)||($end_date))
			   {
			   	
			   	$date1 = new DateTime($Start_date);
			   	$date2 = new DateTime($end_date);
			   	$diff = $date2->diff($date1)->format("%a");
			   	$service1['day_difference']=$diff+1;
				$this->db->insert ( TABLES::$USERLEAVE, $service1);
			    }
				   
				   else 
				   {
				$data2 ['status'] = 2;
		        $data2 ['msg'] = "Please select leave date";
		         return $data2; 
				   	
				   }
			}
			//print_r($result); exit();
		}
		$data2 ['status'] = 1;
		$data2 ['id'] = $this->db->insert_id();
		$data2 ['msg'] = "Added successfully";
		return $data2; 
		
		
	
	}
	
	
	
	function updateleave($id)
	{
	
		$this->db->select ( 'uleave.*,booking.name');
		$this->db->from ( TABLES::$USERLEAVE . ' AS uleave' );
		$this->db->join ( TABLES::$BOOKING. ' AS booking', 'booking.bookingid=uleave.bookingid', 'left' );
		$this->db->where('id',$id);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	
	}
	
	
	function updateleave_data($data)
	{
		$Start_date = $data['start_date'];
		$end_date  = $data['end_date'];
		//print_r($Start_date); echo "ff"; print_r($end_date);exit();
		
			$date1 = new DateTime($Start_date);
			$date2 = new DateTime($end_date);
			$diff = ($date2->diff($date1)->format("%a"));
			
			$data1['day_difference']=$diff+1;
		    $data1['start_date']=$data['start_date'];
		    $data1['end_date']=$data['end_date'];
		    //$data1['bookingid']=$data['bookingid'];
		
                    
		$this->db->where('id',$data['id']);
		$this->db->update(TABLES:: $USERLEAVE, $data1);
		
		
	   //$orderid=$cash['bookingid'];
		
//		$this->db->select ('*');
//		$this->db->from ( TABLES::$BOOKING  );
//		$this->db->where('bookingid',$data['bookingid']);
//		$this->db->where('status',1);
//		$query = $this->db->get ();
//		$result = $query->result_array ();
//		
//		$bookingid['userid']=$result[0]['userid'];
//		
//		//print_r($bookingid); exit();
//		
//		$this->db->where('id',$data['id']);
//		$this->db->update(TABLES:: $USERLEAVE, $bookingid);
		
		$data2 ['status'] = 1;
		$data2 ['msg'] = "update successfully";
		return $data2;
	
	}
	
	
	
	function leavelist()
	{
		
	 	$this->db->select ( 'uleave.*,booking.name,booking.bookingid,booking.email,booking.mobile');
		$this->db->from ( TABLES::$USERLEAVE . ' AS uleave' );
		$this->db->join ( TABLES::$BOOKING. ' AS booking', 'booking.bookingid=uleave.bookingid', 'left' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	
	}
	
	function bookslotuserdetail()
	{
		$this->db->select ('*');
		$this->db->from ( TABLES::$BOOKINGSLOT  );
		$this->db->where('status',1);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	
	}
	
	
	function addleavehead($data)
	{
		$this->db->insert ( TABLES:: $LEAVEHEAD, $data);
		$data2 ['status'] = 1;
		$data2 ['id'] = $this->db->insert_id();
		$data2 ['msg'] = "Added successfully";
		return $data2; 
	
	}
	
	function selectchef($data)
	{
		
		
		$this->db->select ( 'chefarea.chefid');
		$this->db->from ( TABLES::$CHEFAREA . ' AS chefarea' );
		$this->db->join ( TABLES::$USERS. ' AS users', 'users.area_id=chefarea.areaid', 'left' );
		$this->db->where('users.id',$data);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	
	}
	
	
	
	
	function chefdetail()
	{
		$this->db->select('*')
		->from(TABLES::$CHEFS);
		$query = $this->db->get();
		return $query->result_array();
	
	}
	
	
	function bookingdetail()
	{
		
		$this->db->select('bookingid,name');
		$this->db->from( TABLES:: $BOOKING);
		$this->db->where('status', 1);
		$query = $this->db->get();
		return $query->result_array();
		
			
	}
	
	function chefallocateadd($value)
	{
		
			foreach($value['data'] as $service)
			{
				$service1['chef_id']=$service['chef_id'];
				$service1['booking_id']=$service['booking_id'];
					
			
				$i=0;
				$this->db->select ( 'userid,society_id,areaid' )->from ( TABLES::$BOOKING )
				->where('bookingid',$service1['booking_id']);
				$query = $this->db->get ();
				$resultslot = $query->result_array ();
				//print_r($result[$i]['name']); exit();
					
				$service1['user_id']= $resultslot[$i]['userid'];
				$service1['society_id']= $resultslot[$i]['society_id'];
				$service1['area_id']= $resultslot[$i]['areaid'];
					
				$service1['date']=$service['date'];
				$service1['created_on']=$service['created_on'];
					
				//print_r($service1); exit();
				$this->db->insert ( TABLES:: $CHEFATTENDANCE, $service1);
			}
			 
			$i++;
			 
			$data2 ['status'] = 1;
			$data2 ['id'] = $this->db->insert_id();
			$data2 ['msg'] = "Added successfully";
			return $data2;
			 
			
		
	}



	
	
	function bookingslotupdate($book)
	{
		$bookslotid=$book['slotid'];
		
		$this->db->select ( 'slot' )->from ( TABLES::$BOOKINGSLOT )
		->where('slotid',$bookslotid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		
		$slotname=$result[0]['slot'];
		
		$bookslot['slotid']=$book['slotid'];
		$bookslot['slot']=$slotname;
		//print_r($bookslot); exit();
		
		
		$this->db->where('bookingid',$book['bookingid']);
		$this->db->where('head_count',$book['headcount']);
		$this->db->where('mealtype_name',$book['mealtype']);
		$this->db->update(TABLES:: $BOOKINGSLOT, $bookslot);
		
		$result ['status'] = 1;
		$result ['msg'] = "Updated Successfully";
		return $result;
		
	
	}
	
	function bookingmealtypeupdate($book)
	{
		
	
		$bookslotid=$book['mealtype'];
		//print_r($bookslotid); exit();
		
		$this->db->select ( 'mealtype_name' )->from ( TABLES::$BOOKINGSLOT )
		->where('mealtype_id',$bookslotid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		
		$mealtypename=$result[0]['mealtype_name'];
		
		$bookmealtype['mealtype_id']=$book['mealtype'];
		$bookmealtype['mealtype_name']=$mealtypename;
		//print_r($bookmealtype); exit();
		
		
		$this->db->where('bookingid',$book['bookingid']);
		$this->db->where('head_count',$book['headcount']);
		$this->db->where('slot',$book['slotid']);
		$this->db->update(TABLES:: $BOOKINGSLOT, $bookmealtype);
		
		$result ['status'] = 1;
		$result ['msg'] = "Updated Successfully";
		return $result;
		
	
	
	}
	
	function updatebookingstatus($book)
	{
		  $bookingstatus['status'] = $book['status'];
	          $this->db->where('bookingid',$book['bookingid']);
		  $this->db->update(TABLES:: $BOOKING, $bookingstatus);
                  $data['bookingid'] = $book['bookingid'];
                  $cmt = '';
                  switch ($book['status']) {
                    case 1:
                        $cmt = 'Status changed to Pending';
                        break;
                    case 2:
                        $cmt = 'Status changed to Recky';
                        break;
                    case 3:
                        $cmt = 'Status changed to Active';
                        break;
                    case 4:
                        $cmt = 'Status changed to Cancellation Request';
                        break;
                    case 5:
                        $cmt = 'Status changed to Cancelled';
                        break;
                    
                    default:
                        $cmt = '';
                } 
                  $data['comment'] = $cmt;
                  
                  $data['created_date'] = date('Y-m-d H:i:s ');
                  $this->db->insert('tbl_booking_log', $data);
		  $result ['status'] = 1;
		  $result ['msg'] = "Updated Successfully";
		  return $result;
	
	}
	
	
	
	function updatebooking_status($id)
	{
		$bookingid=$id['bookingid'];
		//print_r($bookingid); exit();
		$this->db->select ( 'a.*,b.name,b.email,b.mobile,bs.mealtype_name,bs.head_count,bs.amount');
		$this->db->from ( TABLES::$BOOKING . ' AS a' );
		$this->db->join ( TABLES::$USERS . ' AS b', 'a.userid=b.id', 'left' );
		$this->db->join ( TABLES::$BOOKINGSLOT . ' AS bs', 'a.userid=bs.userid', 'left' );
		$this->db->where ( 'a.bookingid', $bookingid );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	
	}
	
	
	
	
	function bookingchefupdate($book)
	{
		$createddate=date('Y-m-d H:i:s');
		
		$chefid=$book['chefid'];
		$this->db->select ( 'name' )->from ( TABLES::$CHEFS )
		->where('id',$chefid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		
		$chefname=$result[0]['name'];
		//print_r($chefname); exit();
		
		$bookingid=$book['bookingid'];
		$mealtype=$book['mealtype'];
		$timeslot=$book['slot'];
		$headcount=$book['headcount'];
		$chefadd['comment']="chef Allocated $chefname  for mealtype $mealtype and timeslot $timeslot " ;
		$chefadd['bookingid']=$book['bookingid'];
		$chefadd['created_date']=$createddate;
		
		$chefidadd['chefid']=$book['chefid'];
		
		$this->db->where('bookingid',$book['bookingid']);
		$this->db->where('head_count',$book['headcount']);
		$this->db->where('slot',$book['slot']);
		$this->db->where('mealtype_name',$book['mealtype']);
		$this->db->update(TABLES:: $BOOKINGSLOT, $chefidadd);
		$this->db->insert ( TABLES::$BOOKINGLOG, $chefadd);
		$data2 ['status'] = 1;
		$data2 ['id'] = $this->db->insert_id();
		$data2 ['msg'] = "Added successfully";
		return $data2;
	
	
	}
	
	
	
	function displaybookinglog($id)
	{
		
		 $this->db->select('*');
		 $this->db->from( TABLES:: $BOOKINGLOG);
		 $this->db->where('bookingid',$id);
		 $query = $this->db->get();
		 return $query->result_array(); 
		
	}
	
	function cashadd($data)
	{
		//print_r($data); exit();
		
		$params = array (
				'chefid'=>$data['chefid']
			  
		);
			
		// $where = "mobile ='".$data['mobile']."' OR email =".$data['email'];
		
		
		$this->db->select ('*')->from ( TABLES::$CHEFCOLLECTION )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r(count($result)); exit();
		
		if (count($result) > 0)
		{
			$data2['msg'] = "Chef  already exists.";
			$data2['status'] = 0;
			return $data2;
		}
		
		else
		{
			
		$this->db->insert ( TABLES:: $CHEFCOLLECTION, $data);
		$data2 ['status'] = 1;
		$data2 ['id'] = $this->db->insert_id();
		$data2 ['msg'] = "Added successfully";
		return $data2;
				
		}
				
		
	}
	
	
	function amountadduserwallet($id)
	{
		
		//print_r($id); exit();
		
	
		$this->db->select ( '*' )->from ( TABLES::$CHEFCOLLECTION )
		->where('id',$id);
		$query = $this->db->get ();
		$result = $query->result_array ();
		$bookingid=$result[0]['bookingid'];
		$wallet['amount']=$result[0]['price'];
		$wallet['orderid']=$result[0]['bookingid'];
		//$wallet['comment']=$result[0]['comment'];
		
		$this->db->select ( 'userid' )->from ( TABLES::$BOOKING )
		->where('bookingid',$bookingid);
		$query = $this->db->get ();
		$result1 = $query->result_array ();
		
		$wallet['userid']=$result1[0]['userid'];
		$wallet['is_debit']=0;
		$wallet['updated_by']=$result1[0]['userid'];
		$useridwallet=$result1[0]['userid'];
		$this->db->select ( 'amount' )->from ( TABLES::$USERWALLET )
		->where('userid',$useridwallet);
		$query = $this->db->get ();
		$resultamount = $query->result_array ();
		$useramount=$resultamount[0]['amount'];
		$user123=$result[0]['price'];
		$newuseramount=$useramount+$user123;
		$user['amount']=$newuseramount;
		$user['updated_date']=date("Y/m/d H:i:s");
	    $this->db->where ( 'userid', $useridwallet );
		$this->db->update ( TABLES::$USERWALLET, $user );
		
		$this->db->insert ( TABLES:: $USERWALLETTRANSACTION, $wallet);
		$data2 ['status'] = 1;
		$data2 ['id'] = $this->db->insert_id();
		$data2 ['msg'] = "Added successfully";
		return $data2;
		
	}
	
	
	function updatecollectionlist()
	{
		$this->db->select ( 'chefc.*,booking.name,booking.email,booking.mobile,chefs.name as chefname');
		$this->db->from ( TABLES::$CHEFCOLLECTION . ' AS chefc' );
		$this->db->join ( TABLES::$BOOKING . ' AS booking', 'booking.bookingid=chefc.bookingid', 'left' );
		$this->db->join ( TABLES::$CHEFS . ' AS chefs', 'chefs.id=chefc.chefid', 'left' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
		
	
	
	}
	
	
	function updatecheflist($id)
	{
		
		$this->db->select ( 'chefc.*,booking.name,booking.email,booking.mobile');
		$this->db->from ( TABLES::$CHEFCOLLECTION . ' AS chefc' );
		$this->db->join ( TABLES::$BOOKING . ' AS booking', 'booking.bookingid=chefc.bookingid', 'left' );
		$this->db->where('chefc.id',$id);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	
	}
	
	function editchefcollection($cash)
	{
		$orderid=$cash['bookingid'];
		$this->db->select ( '*' )->from ( TABLES::$BOOKING )
		->where('bookingid',$orderid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		
		$userid=$result[0]['userid'];
		
		$userwallet['userid']=$userid;
		$userwallet['amount']=$cash['price'];
		$userwallet['updated_by']=$userid;
		$userwallet['orderid']=$orderid;
		$userwallet['comment']=$cash['comment'];
		$userwallet['updated_date']=date("Y-m-d H:i:s");
		
		$this->db->where('id',$cash['id']);
		$this->db->update(TABLES:: $USERWALLETTRANSACTION, $userwallet);
		$this->db->where('id',$cash['id']);
		$this->db->update(TABLES:: $CHEFCOLLECTION, $cash);
		$result ['status'] = 1;
		$result ['msg'] = "Updated Successfully";
		return $result;
		
		
	
	}
	
  function chefattendancelist1()
	{
		$this->db->select ( 'chefat.*,chefs.name,booking.name as username');
		$this->db->from ( TABLES::$CHEFATTENDANCE . ' AS chefat' );
		$this->db->join ( TABLES::$CHEFS . ' AS chefs', 'chefs.id=chefat.chef_id', 'left' );
		$this->db->join ( TABLES::$BOOKING . ' AS booking', 'booking.bookingid=chefat.booking_id', 'left' );
		$query = $this->db->get();
		return $query->result_array(); 
	}
	
	
	function updatecheflist123($id)
	{
			$this->db->where ( 'id', $id );
			$result=$this->db->delete( TABLES::$CHEFATTENDANCE);
			//$query = $this->db->get ();
			//$result = $query->result_array ();
			return $result;
		
	}
	
	
	
	function cashmanagebooking($id)
	{
		$this->db->select('name,email,mobile');
		$this->db->from( TABLES:: $BOOKING);
		$this->db->where('bookingid',$id);
		$query = $this->db->get();
		return $query->result_array();
	
	}
	
	
	
	function addcoupon($data)
	{
	
		//print_r($data); exit();
		
	 	$data1['title']=$data['title'];
		$data1['description']=$data['description'];
		$data1['coupon_code']=$data['coupon_code'];
		$data1['discount_type']=$data['discount_type'];
		$data1['discount']=$data['discount'];
		$data1['coupon_type']=$data['coupon_type'];
		$data1['count_per_user']=1;
		//$data['cashback_type']=$data['cashback_type'];
		$data1['start_date']=$data['start_date'];
		$data1['end_date']=$data['end_date'];
		$data1['end_date']=$data['end_date'];
		$data1['created_date']=$data['created_date'];
		$data1['status']=$data['status']; 
		
		//print_r($data); exit();
		$this->db->insert(TABLES::$COUPONCODE,$data1);
		return $this->db->insert_id();
		//$this->db->update ( TABLES::$COUPONCODE, $data );
		//$result ['status'] = 1;
		//$result ['msg'] = "Updated Successfully";
		//return $result;
	}
	
	
	function couponlist()
	{
		$this->db->select ( '*');
		$this->db->from ( TABLES::$COUPONCODE );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	
	}
	
	
	function turnoncouponlist($id)
	{
		$area ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$COUPONCODE, $area );
	
	}
	
	function turnoffcouponlist($id)
	{
		$area ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$COUPONCODE, $area );
	
	}
	
	function coupon($id)
	{
		
		$this->db->select ( '*');
		$this->db->from ( TABLES::$COUPONCODE );
		$this->db->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	
	}
	
	function updatecouponlist_data($data)
	{
		
		//print_r($data); exit();
		
		
		$this->db->where('id',$data['id']);
		return $this->db->update(TABLES::$COUPONCODE,$data);
	
	}

}

