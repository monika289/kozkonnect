<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Addarea_Model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	function addarea($data) {  
		
		$area=$data['name'];
		$params = array (
				'name' => $data ['name'],
				'cityid' => $data['cityid']
		               );
		
		
		//$where = "name ='".$data['name']."' AND cityid !=".$data['cityid'];
		
		$this->db->select ( '*' )->from ( TABLES::$AREA )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r(count($result)); exit();
		
		if (count($result) > 0) 
		{
			$data2['msg'] = "Area Name already exists.";
			$data2['status'] = 0;
			return $data2;
		} 
		
		else 
		{
			$this->db->insert ( TABLES:: $AREA, $data);
			$data2 ['status'] = 1;
			$data2 ['id'] = $this->db->insert_id();
			$data2 ['msg'] = "Added successfully";
			return $data2;
			
		}
		
	}
	
	function addsocieties($data) {
		
		
		
		$area=$data['name'];
		$params = array (
				'name' => $data ['name'],
				'area_id'=>$data ['area_id'],
				'city_id'=>$data['city_id']
		);
		
		$this->db->select ( '*' )->from ( TABLES::$SOCIETY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r(count($result)); exit();
		
		if (count($result) > 0)
		{
			$data2['msg'] = "Society Name already exists.";
			$data2['status'] = 0;
			return $data2;
		}
		
		else
		{
	    $this->db->insert ( TABLES:: $SOCIETY, $data);
		$data2 ['status'] = 1;
		$data2 ['id'] = $this->db->insert_id();
		$data2 ['msg'] = "Added successfully";
		return $data2; 
				
				
		}
		
		
		
		/* $this->db->insert ( TABLES:: $SOCIETY, $data);
		$data2 ['status'] = 1;
		$data2 ['id'] = $this->db->insert_id();
		$data2 ['msg'] = "Added successfully";
		return $data2; */
	
	}
	
	
	function selectcityid() {
		
		//$this->db->select('TA.*,TS.name' );
		//$this->db->where('TA.status',1);
		//$this->db->from(TABLES:: $AREA. ' as TA');
		//$this->db->join(TABLES:: $CITY.' as  TS',TS.id=TA.cityid);
		//$query = $this->db->get();
		//return $query->result_array();
		
		
		
		$this->db->select('*');
		$this->db->from( TABLES:: $CITY);
		$this->db->where('status',1);
		$query = $this->db->get();
		return $query->result_array();
			
	}
	
	
	
	function selectarea() {
		
		$this->db->select('*');
		$this->db->from( TABLES:: $AREA);
		//$this->db->where('cityid',$id);
		$this->db->where('status',1);
		$query = $this->db->get();
		
		return $query->result_array();
			
	}
	
	
	function addarealist() {
		
	    $this->db->select('TA.*,TS.name as city_name' );		
		$this->db->from(TABLES:: $AREA. ' as TA');
		$this->db->join(TABLES:: $CITY.' as  TS','TS.id=TA.cityid');
		$query = $this->db->get();
		return $query->result_array();
		
	}
	
	function societieslist() {
		
				
		$this->db->select('TS.*,TC.name as city_name,area.name as areaname' );
		$this->db->from(TABLES:: $SOCIETY. ' as TS');
		$this->db->join(TABLES:: $CITY.' as  TC','TS.city_id=TC.id');
		$this->db->join(TABLES:: $AREA.' as  area','area.id=TS.area_id');
		$query = $this->db->get();
		return $query->result_array();
		
			
	}
	
	
	
	
	
	function turnonsociety($id) {
		$area ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$SOCIETY, $area );
	}
	
	function turnofsociety($id) {
		$area ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$SOCIETY, $area );
	}
	
	
	
	function turnonarea($id) {
		$area ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$AREA, $area );
		
			
	}
	function turnofarea($id) {
		
		$area ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$AREA, $area );
		
		
			
	}
	
	function updatearea($id) {
		
		
		$this->db->select('TA.*,TS.name as city_name' );
		$this->db->where ('TA.id', $id);
		$this->db->from(TABLES:: $AREA. ' as TA');
		$this->db->join(TABLES:: $CITY.' as  TS','TS.id=TA.cityid');
		$query = $this->db->get();
		return $query->result_array();
		
	}
	
	
	function updatesociety($id) {
		
		

		$this->db->select('TS.*,TS.area_id,TA.name' );
		$this->db->where ('TS.id', $id);
		$this->db->group_by ('TA.name','ASC');
		$this->db->from(TABLES:: $SOCIETY. ' as TS');
		$this->db->join(TABLES:: $AREA.' as  TA','TA.id=TS.area_id');
		$query = $this->db->get();
		return $query->result_array();
		
			
	}
	
	
	function updatesocietyname($id) {
	
		$this->db->select('TS.name,TA.id,TS.id' );
		$this->db->where ('TS.id', $id);
		$this->db->group_by ('TA.name','ASC');
		$this->db->from(TABLES:: $SOCIETY. ' as TS');
		$this->db->join(TABLES:: $AREA.' as  TA','TA.id=TS.area_id');
		$query = $this->db->get();
		return $query->result_array();
	
	}
	
	
	function updatearea_data($data) {
	
		$params = array (
				'name' => $data ['name'],
				
		);
		 $where = "name ='".$data['name']."' AND  cityid ='".$data['cityid']."'  AND id !=".$data['id'];
		
			$this->db->select ( '*' )->from ( TABLES::$AREA )->where ( $where );
			$query = $this->db->get ();
			$result = $query->result_array ();
		
		
		if (count($result) > 0)
		{
			$data2['msg'] = "City Area already exists.";
			$data2['status'] = 0;
			return $data2;
		}
		
		else
		{
			$this->db->where('id',$data['id']);
		    $this->db->update(TABLES:: $AREA, $data);
		   $result ['status'] = 1;
		   $result ['msg'] = "Updated Successfully";
		   return $result;
				
		}
	
			
	}
	
	function updatesociety_data($data) {
		
		//$area=$data['name'];
		$params = array (
				'name' => $data ['name'],
				
		
		);
		
		
		
		$where = "name ='".$data['name']."'  AND  city_id ='".$data['city_id']."' AND  id !=".$data['id'];
		
		$this->db->select ( '*' )->from ( TABLES::$SOCIETY )->where ( $where );
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r(count($result)); exit();
		
		if (count($result) > 0)
		{
			$data2['msg'] = "Society Name already exists.";
			$data2['status'] = 0;
			return $data2;
		}
		
		else
		{
			$this->db->where('id',$data['id']);
		    $this->db->update(TABLES:: $SOCIETY, $data);
			
		    $result ['status'] = 1;
		    $result ['msg'] = "Updated Successfully";
		    return $result;
		
		}
		
			
	}
	
	
	
}