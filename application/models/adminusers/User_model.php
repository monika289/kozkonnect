<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class User_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	

	public function getUserDetailByEmail($email) {
		return $this->db->select('*')
                            ->from(TABLES::$ADMIN_USER)
                            ->where('email', $email)
                            ->get()
                            ->result_array ();
	}

	
	public function getActiveUsers() {
		$this->db->select('*')
				 ->from(TABLES::$ADMIN_USER)
				 ->where('status',1)
				 ->order_by('first_name','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	
	
}
