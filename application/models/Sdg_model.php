<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CI_Model API for order management .
 *
 * <p>
 * We are using this model to add/update orders.
 * </p>
 * @package Orders
 * @subpackage orders-model
 * @author pradeep singh
 * @category CI_Model API
 */
class sdg_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}


	public function addSdg($map) {
		$data = array ();
		$params = array (
				'sdgname' => $map ['sdgname']
		);
		$this->db->select ( 'id' )->from ( TABLES::$SDG )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert(TABLES::$SDG,$map);
			$data ['status'] = $this->db->insert_id();
			$data ['msg'] = "SDG Added successfully.";
			return $data;
		} else {
			$data ['msg'] = "SDG name already exists.";
			$data ['status'] = 0;
			return $data;
		}

			
	}

	
    public function updateSdg($map,$sdgid) {
      
	    $data = array ();
		$params = array (
				'sdgname' => $map ['sdgname'],
				'id !=' => $sdgid
		);
		$this->db->select ( 'id' )->from ( TABLES::$SDG )->where ( $params );
		$query = $this->db->get ();
//	echo	$this->db->last_query();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $sdgid );
			$this->db->update ( TABLES::$SDG, $map );
			$data ['status'] = 1;
			$data ['msg'] = "SDG updated successfully.";
			return $data;
		} else {
			$data ['msg'] = "SDG name already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}


	public function getAllsdg() {
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$SDG );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	public function getsdgById($id) {
		$this->db->select ( 'a.*' );
		$this->db->from ( TABLES::$SDG .' AS a');
		$this->db->where ( 'a.id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}

	public function getuserImages($id) {
	$this->db->select ( 'a.*' );
	$this->db->from ( TABLES::$USER_DOCS . ' AS a' );
	
	$this->db->where( 'a.user_id', $id );
	
	$query = $this->db->get ();
	//echo $this->db->last_query();
	$result = $query->result_array ();
	return $result;
	}
	public function getuserdocsbyid($docid) {
	$this->db->select ( 'a.*' );
	$this->db->from ( TABLES::$USER_DOCS . ' AS a' );
	
	$this->db->where( 'a.id', $docid );
	
	$query = $this->db->get ();
	//echo $this->db->last_query();
	$result = $query->result_array ();
	return $result;
	}

	public function deleteUserdoc( $id ) {
		$this->db->where('id', $id);
		$this->db->delete(TABLES::$USER_DOCS);
	}
	public function saveDocs($map,$sdgid=null) {
		 // print_r($map);
		if(isset($sdgid))
		{
			$this->db->where( 'id', $sdgid );
			$this->db->update ( TABLES::$SDG, $map );
		}
	}
		
	

	
	
	
	
}
