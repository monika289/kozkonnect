<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Category_Model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	/* ************ Category Model******************************************* */
	function addCategory($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name']
		);
		$this->db->select ( 'id' )->from ( TABLES::$CAUSE_MAIN_CATEGORY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$CAUSE_MAIN_CATEGORY, $cat );
			$data ['status'] = 1;
			$data ['msg'] = "Category Added successfully.";
			return $data;
		} else {
			$data ['msg'] = "Category name already exists.";
			$data ['status'] = 0;
			return $data;
		}
		
	}
	function updateCategory($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name'],
				'id !=' => $cat ['id']
		);
		$this->db->select ( 'id' )->from ( TABLES::$CAUSE_MAIN_CATEGORY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $cat ['id'] );
			$this->db->update ( TABLES::$CAUSE_MAIN_CATEGORY, $cat );
			$data ['status'] = 1;
			$data ['msg'] = "Category updated successfully.";
			return $data;
		} else {
			$data ['msg'] = "Category name already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	function getAllCategories() {
		$this->db->select('a.*');
		$this->db->where('parent_id',0);
		$this->db->from(TABLES::$CAUSE_MAIN_CATEGORY.' as a');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	function getActiveCategoryList(){
		$this->db->select("*");
		//$this->db->where('parent_id',0);
		$this->db->from(TABLES::$SDG);
		$this->db->where('status',1);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	function getCategoryList(){
		$this->db->select("id,name");
		$this->db->where('parent_id',0);
		$this->db->from(TABLES::$CAUSE_MAIN_CATEGORY);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	function getCategoryById($id){
		$this->db->select("*");
		$this->db->from(TABLES::$CAUSE_MAIN_CATEGORY);
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	public function turnoffcat($id) {
		
		//$item ['status'] = 0;
		$item = array(
			'status' => 0	
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( TABLES::$CAUSE_MAIN_CATEGORY, $item );
		if($this->db->affected_rows() > 0){
			return $this->getCategoryById($id);
		}
	}
	
	public function turnoncat($id) {
		
		$item ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$CAUSE_MAIN_CATEGORY, $item );
		if($this->db->affected_rows() > 0){
			return $this->getCategoryById($id);
		}
	}
	
	/* **************************SUB Category ************************** */
	function addSubCategory($subcat) {
		$data = array ();
		$params = array (
				'name' => $subcat ['name'],
				'parent_id' => $subcat['parent_id']
		);
		$this->db->select ( 'id' )->from ( TABLES::$CAUSE_SUB_CATEGORY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$CAUSE_SUB_CATEGORY, $subcat );
			$data ['status'] = 1;
			$data ['msg'] = "Subcategory added successfully.";
			return $data;
		} else {
			$data ['msg'] = "Subcategory name with selected category already exists.";
			$data ['status'] = 0;
			return $data;
		}
	
	}
	function updateSubcategory($subcat) {
		$data = array ();
		$params = array (
				'name' => $subcat ['name'],
				'id !=' => $subcat ['id'],
				'parent_id' => $subcat['parent_id']
		);
		$this->db->select ( 'id' )->from ( TABLES::$CAUSE_SUB_CATEGORY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $subcat ['id'] );
			$this->db->update ( TABLES::$CAUSE_SUB_CATEGORY, $subcat );
			$data ['status'] = 1;
			$data ['msg'] = "Subcategory updated successfully.";
			return $data;
		} else {
			$data ['msg'] = "Subcategory name with selected category already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}

	function getAllSubcategories() {
		$this->db->select('a.*,b.sdgname as catName');
		$this->db->where('a.parent_id !=',0);
		$this->db->from(TABLES::$CAUSE_SUB_CATEGORY.' as a');
		$this->db->join(TABLES::$SDG.' as b','b.id=a.parent_id','left');
		$this->db->group_by('a.id');
		$this->db->order_by('a.id','ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	function getActiveSubCategoryList(){
		$this->db->select("*");
		$this->db->from(TABLES::$CAUSE_SUB_CATEGORY);
		$this->db->where('status',1);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	function getSubcategoryById($id){
		$this->db->select("*");
		$this->db->from(TABLES::$CAUSE_SUB_CATEGORY);
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	public function turnoffsubcat($id) {
		$item ['status'] = 0;
		$this->db->where ( 'id', $id );
	    $this->db->update ( TABLES::$SUB_CATEGORY, $item );
	    if($this->db->affected_rows() > 0){
	    	return $this->getSubcategoryById($id);
	    }
	}
	
	public function turnonsubcat($id) {
		$item ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$SUB_CATEGORY, $item );
		if($this->db->affected_rows() > 0){
			return $this->getSubcategoryById($id);
		}
	}
}