<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

/**
 * General Settings Model
 *
 * <p>
 * We are using this model to add, update, delete and get general setting like city,area etc.
 * </p>
 *
 * @package General
 * @author Pradeep Singh
 * @copyright Copyright &copy; 2015
 * @category CI_Model API
 *          
 */
class Settings_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	public function addCity($city) {
		$data = array ();
		$errors = array ();
		$params = array (
				'name' => $city ['name'] 
		);
		$this->db->select ( 'id' )->from ( TABLES::$CITY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$CITY, $city );
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$errors ['city'] = "City name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	/**
	 *
	 *
	 * update city name
	 * 
	 * @param
	 *        	city object
	 * @access public
	 * @return array
	 */
	public function updateCity($city) {
		$data = array ();
		$errors = array ();
		$params = array (
				'name' => $city ['name'],
				'id !=' => $city ['id'] 
		);
		$this->db->select ( 'id' )->from ( TABLES::$CITY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $city ['id'] );
			$this->db->update ( TABLES::$CITY, $city );
			$data ['status'] = 1;
			$data ['msg'] = "updated successfully";
			return $data;
		} else {
			$errors ['city'] = "City name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	/**
	 *
	 * change the status of city from 1 to 0
	 * 
	 * @access public
	 * @param $id of
	 *        	city
	 *        	
	 */
	public function turnOffCity($id) {
		$city ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$CITY, $city );
	}
	
	/**
	 *
	 * change the status of city from 0 to 1.
	 * 
	 * @access public
	 * @param
	 *        	id of city
	 */
	public function turnOnCity($id) {
		$city ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$CITY, $city );
	}
	
	/**
	 *
	 * get city name,fence and status of city by id
	 * 
	 * @param
	 *        	city id
	 * @access public
	 * @return result_set
	 */
	public function getCityById($id) {
		$this->db->select ( 'id,name,status' )->from ( TABLES::$CITY )->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	/**
	 * get all the cities.
	 * 
	 * @param
	 *        	no param
	 * @access public
	 * @return array_list
	 */
	public function getAllCities() {
		$this->db->select ( 'id,name,status' )->from ( TABLES::$CITY )->order_by ( 'name', 'asc' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function checkLocalitiesbyPincode($pincode) {
		$this->db->select ( '*' )->from ( TABLES::$AREA )->order_by ( 'name', 'asc' );
		$this->db->where ( 'pincode', $pincode );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function addLocality($locality) {
		$params = array (
				'name' => $locality ['name'],
				'cityid=' => $locality ['cityid'] 
		);
		$this->db->select ( 'id' )->from ( TABLES::$AREA )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$AREA, $locality );
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$errors ['locality'] = "Locality name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	/**
	 *
	 * update locality name, longitude, latitude and zone
	 * 
	 * @param
	 *        	locality object
	 * @access public
	 * @throws Exception
	 */
	public function updateLocality($locality) {
		$data = array ();
		$params = array (
				'name' => $locality ['name'],
				'cityid' => $locality ['cityid'],
				'id !=' => $locality ['id'] 
		);
		$this->db->select ( 'id' )->from ( TABLES::$AREA )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $locality ['id'] );
			$this->db->update ( TABLES::$AREA, $locality );
			$data ['status'] = 1;
			$data ['msg'] = "Updated successfully";
			return $data;
		} else {
			$errors ['locality'] = "Locality name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	/**
	 *
	 * change the status of locality from 1 to 0
	 * 
	 * @access public
	 * @param $id of
	 *        	locality
	 *        	
	 */
	public function turnOffLocality($id) {
		$locality ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$AREA, $locality );
	}
	
	/**
	 *
	 * change the status of locality from 0 to 1
	 * 
	 * @access public
	 * @param $id of locality
	 *        	
	 */
	public function turnOnLocality($id) {
		$locality ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$AREA, $locality );
	}
	
	/**
	 *
	 * get locality name, latitude, longitude and zone_id of zone by id
	 * 
	 * @param locality id
	 * @access public
	 * @return result_set
	 */
	public function getLocalityById($id) {
		$this->db->select ( '*' )->from ( TABLES::$AREA )->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	/**
	 * get all the localities.
	 * 
	 * @param no param
	 * @access public
	 * @return array_list
	 */
	public function getAllLocalities() {
		$this->db->select ( '*' )
                        ->from ( TABLES::$AREA )
                        ->order_by ( 'name', 'asc' )
                        ->where('status', '1');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getAreasByCityId($cityid) {
		$this->db->select ( 'a.*,b.name as zone_name' )
			 ->from ( TABLES::$AREA.' AS a' )
			 ->join ( TABLES::$ZONE.' AS b','a.zone_id=b.id','left')
			 ->order_by ( 'a.name', 'asc' );
		$this->db->where ( 'a.cityid', $cityid );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	function getActivemealtype()
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $MEALTYPE);
		$this->db->where ( 'status', 1 );
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function getHeadcount()
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $HEADCOUNT);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function getMealtypeById($id)
	{
		$this->db->select('*');
		$this->db->from( TABLES:: $MEALTYPE);
		$this->db->where('id', $id);
		$this->db->where('status', 1);
		$query = $this->db->get();
		return $query->result_array();
	}
	function  getActiveSlots($params)
	{
		$where = "p.area_id = ".$params['area_id']." AND p.headcount = ".$params['headcount']." AND p.mealtype_id = ".$params['mealtype_id'];
		$this->db->select('p.*, t.slot, m.name as mealtype_name');
		$this->db->from( TABLES:: $PRICELIST.' as p');
		$this->db->join( TABLES:: $TIMESLOTS.' as t', 'p.timeslots = t.id', 'left');
		$this->db->join( TABLES:: $MEALTYPE.' as m', 'p.mealtype_id = m.id', 'left');
		$this->db->where ($where);
		$this->db->where ( 'p.status', 1 );
		$this->db->where ( 't.status', 1 );
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function  getTimeSlotById($id)
	{
		$this->db->select('p.*, t.slot, m.name as mealtype_name');
		$this->db->from( TABLES:: $PRICELIST.' as p');
		$this->db->join( TABLES:: $TIMESLOTS.' as t', 'p.timeslots = t.id', 'left');
		$this->db->join( TABLES:: $MEALTYPE.' as m', 'p.mealtype_id = m.id', 'left');
		$this->db->where ( 'p.id', $id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function  getPriceActiveSlots($params)
	{
		$where = "p.area_id = ".$params['area_id']." AND p.headcount = ".$params['headcount']." AND p.mealtype_id = ".$params['mealtype_id'];
		$this->db->select('p.*, t.slot');
		$this->db->from( TABLES:: $PRICELIST.' as p');
		$this->db->join( TABLES:: $TIMESLOTS.' as t', 'p.timeslots = t.id', 'left');
		$this->db->where ($where);
		$this->db->where ( 'p.status', 1 );
		$this->db->where ( 't.status', 1 );
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function getCouponByCode($data) {
		$where = '(area_id ="'.$data['area_id'].'" or area_id = 0)';
		$this->db->select ( 'a.* ' )->from (TABLES::$COUPON_CODE.' AS a')
		->where('coupon_code',$data['coupon_code'])
		->where($where)
                ->where('status', '1');
		$this->db->where("'".$data['order_date']."' BETWEEN start_date and end_date",'',false);
		$query = $this->db->get ();
		$result = $query->result_array ();
		//echo $this->db->last_query();
		return $result;
	}
	
	//................ Added by Tushar Ticket model..........................
	
	public function addTicket($params) {
		$this->db->insert ( TABLES::$TICKET, $params );
		return $this->db->insert_id();
	}
	
	public function updateTicket($params) {
		$this->db->where('ticketid',$params['ticketid']);
		return $this->db->update(TABLES::$TICKET,$params);
	}
	
	public function getAllTickets() {
		$this->db->select ( '*' )->from ( TABLES::$TICKET )
				 ->order_by('status','ASC')
				 ->order_by('priority','DESC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getAllActiveTickets() {
		$this->db->select ( "a.*,b.name,b.mobile,concat(c.first_name,' ',c.last_name) as assigned_to_name,concat(d.first_name,' ',d.last_name) as created_by_name" );
		$this->db->from ( TABLES::$TICKET.' AS a' );
		$this->db->join ( TABLES::$USER.' AS b','a.userid=b.id','inner' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS c','a.assigned_to=c.id','left' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS d','a.created_by=d.id','left' );
		$this->db->order_by('a.created_date','DESC');
		$this->db->order_by('a.priority','DESC');
		$this->db->order_by('a.status','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getTicketById($ticketid) {
		$this->db->select ( "a.*,b.name,b.mobile,concat(c.first_name,' ',c.last_name) as assigned_to_name,concat(d.first_name,' ',d.last_name) as created_by_name" );
		$this->db->from ( TABLES::$TICKET.' AS a' );
		$this->db->join ( TABLES::$USER.' AS b','a.userid=b.id','inner' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS c','a.assigned_to=c.id','left' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS d','a.created_by=d.id','left' );
		$this->db->where('a.ticketid',$ticketid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
}