<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

/**
 * User model
 *
 * <p>
 * We are using this model to add, update, delete and get users.
 * </p>
 *
 * @package User
 * @author Pradeep Singh
 * @copyright Copyright &copy; 2015, FreightBazaar
 * @category CI_Model API
 */
class User_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	public function addUser($data) {
		$this->db->select ( 'id' );
		$this->db->from ( TABLES::$USERS );
		$this->db->where ( 'email', $data['email'] );
		$this->db->or_where ( 'mobile', $data['mobile'] );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count($result) <= 0) {
			$this->db->insert(TABLES::$USERS,$data);
			return $this->db->insert_id();
		} else {
			return $result[0]['id'];
		}
	}
	
	public function getCustomerByMobile($email,$mobile) {
		$this->db->select ( 'orderid' );
		$this->db->from ( TABLES::$ORDER_CUSTOMER );
		$this->db->where ( 'email', $email );
		$this->db->where ( 'mobile', $mobile );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	/**
	 * 
	 * @param unknown $otpverification
	 */
	public function updateUserByOtp($otpverification) {
		$otpverification['otp_verify'] = 1;
		$otpverification['status'] = 1;
		$this->db->where( 'id',$otpverification['id'] );
		$this->db->where( 'otp',$otpverification['otp'] );
		return $this->db->update(TABLES::$USERS,$otpverification);
	}
	
	/**
	 *
	 * @param unknown $userdata
	 */
	public function updateUserById($userdata) {
		$this->db->where( 'id',$userdata['id'] );
		$query = $this->db->update(TABLES::$USERS,$userdata);
		if($query)
			return 1;
		else
			return 0;
	}
	
	public function getUserById($id) {
		$this->db->select ( 'a.*' );
		$this->db->from ( TABLES::$USERS .' AS a');
		$this->db->where ( 'a.id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getUserOTPById($id) {
		$this->db->select ( 'a.id,a.otp,a.email,a.mobile,a.first_name,a.last_name' );
		$this->db->from ( TABLES::$USERS.' AS a' );
		$this->db->where ( 'a.id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	 
	public function getAllUsers() {
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USERS );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getUserByUserName($username) {
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USERS );
		$this->db->where ( 'email', $username );
		$this->db->or_where ( 'mobile', $username );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function editPassword($data)
	{
		$oldpassword = $data['oldpassword'];
		unset($data['oldpassword']);
		$this->db->where('id',$data['id']);
		$this->db->where('text_password',$oldpassword);
		$query = $this->db->update(TABLES::$USERS ,$data );
		if($query)
			return 1;
			else
				return 0;
	}
	
	public function checkPassword($data)
	{
		$this->db->select ( 'id,first_name' );
		$this->db->from ( TABLES::$USERS);
		$this->db->where ( 'text_password', $data['oldpassword'] );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getUserPermissions($user_role) {
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USER_PERMISSION);
		$this->db->where ( 'role_id', $user_role );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	/**
	 * fuction to update GCM Detail
	 * 
	 * @return 
	 */
	public  function GCMRegister($data)
	{
		$this->db->where( 'id',$data['id'] );
		$query = $this->db->update(TABLES::$USERS,$data);
		if($query)
			return 1;
			else
			return 0;
	}
	
	public function getProfile($id) {
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USERS );
		$this->db->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getProfileByEmail($email) {
		$this->db->select ( 'id,email,otp' );
		$this->db->from ( TABLES::$USERS );
		$this->db->like ( 'email', $email,'both' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getUserByEmail($email) {
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USERS );
		$this->db->where ( 'email', $email);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getProfileByMobile($mobile) {
		$this->db->select ( 'id,mobile' );
		$this->db->from ( TABLES::$USERS );
		$this->db->like ( 'mobile', $mobile,'both' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getProfileByName($name) {
		$this->db->select ( 'id,name' );
		$this->db->from ( TABLES::$USERS );
		$this->db->like ( 'name', $name,'both' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
}

