<?php
class UserLogin_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	public function userExistByMobile($data)
    {
        $this->db->select ('*');
		$this->db->from ( TABLES::$USERS );
		$this->db->where ( 'mobile', $data['mobile'] );
		//$this->db->where ( 'client_id', $data['client_id'] );
		$query = $this->db->get ();
		$result = $query->result_array ();
      	return $result;
   	} 
        
           function addfiftypoints($referral_code, $user_id) {
               $referral_user_id = $this->db->query("SELECT id FROM tbl_users WHERE my_ref_code = '$referral_code'")->result_array();
               if(!empty($referral_user_id)) {
                   $this->db->query('UPDATE tbl_user_wallet SET amount = amount + 50 WHERE userid = '.$user_id);
               }
               
           }

           public function userExist($data)
   	{
   		$this->db->select ('id,name,email,mobile, otp, status');
   		$this->db->from ( TABLES::$USERS);
   		$this->db->where ( 'email', $data['email'] );
   		$this->db->or_where( 'mobile', $data['mobile'] );
   		$query = $this->db->get ();
   		$result = $query->result_array ();
   		return $result;
   	}
	public function userRegistration($data) {
    	$this->db->insert ( TABLES::$USERS, $data );
		return $this->db->insert_id ();  
	}
	public function checkRedeem($points) {
            $data = $this->db->query('SELECT amount FROM tbl_user_wallet WHERE userid = '.$this->session->hbuserid)->result_array();
            if($data[0]['amount'] >= $points) {
                return TRUE;
            } else {
                return FALSE;
            }
            
        }
	public function otpMatch($map) {
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USERS );
		$this->db->where ( 'otp', $map['otp'] );
		$this->db->where ( 'id', $map['id'] );
		$query = $this->db->get ();
		$result = $query->result_array ();
		
		if (empty ( $result )) {
			$result [0] ['status'] = "0";
		} else {
			$status = array ( "status" => "1"  );
			$this->db->where ( 'id', $map['id'] );
			$this->db->update ( TABLES::$USERS, $status );
			$result [0] ['status'] = "1";
		}
		
		return $result;
	}
	
	public function login($params) {
		$this->db->select ( 'id, name, email, mobile, status, otp, avatar' );
		$this->db->from ( TABLES::$USERS );
		//$this->db->where ( $params );				
		$this->db->where("(email = '" . $params['username'] . "' OR mobile = '" . $params['username'] . "') ");			
		$this->db->where('password',$params['password']);
		//$this->db->where ( 'status', '1' );
		$query = $this->db->get ();	
		$preCheck = $query->num_rows();
		/* if($preCheck > 0){
			$prevResult = $query->row_array();
			$data['last_login'] = date("Y-m-d H:i:s");
			$update = $this->db->update(TABLES::$USERS,$data,array('id'=>$prevResult['id']));
		} */
		//echo $this->db->last_query();
		$result = $query->result_array ();
		return ($result);
	}
	
	public function addAddress($data) {
		$this->db->insert ( TABLES::$USERADDRESS, $data );
		return $this->db->insert_id ();
	}
	
	public function getAddressById($userid) {
		$this->db->select ( 'a.*,b.name as areaname,c.name as city' );
		$this->db->from ( TABLES::$USERADDRESS.' AS a' );
		$this->db->join ( TABLES::$AREA.' AS b','a.areaid=b.id','left' );
		$this->db->join ( TABLES::$CITY.' AS c','c.id=b.cityid','left' );
		$this->db->where ( 'a.userid', $userid );
		$this->db->order_by('a.id','DESC');
		$this->db->order_by('a.is_primary','DESC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function updateAddress($data) {
		$this->db->where ( 'id', $data ['id'] );
		$this->db->update ( TABLES::$USERADDRESS, $data );
	}
	
	public function getAddressByAddressId($id) {
			$this->db->select ( 'a.*' )->from ( TABLES::$USERADDRESS . ' AS a' )->where ( 'a.id', $id );
			//$this->db->join ( TABLES::$AREA . ' AS b', 'a.areaid=b.id', 'inner' );
			//$this->db->join ( TABLES::$CITY . ' AS c', 'b.cityid=c.id', 'inner' );
			$query = $this->db->get ();
			$result = $query->result_array ();
			//print_r($result);
			return $result;
	}
	
	public function getProfile($id) 
	{
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USERS );
		$this->db->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
        public function  updateUserProfile2($data) {
                $this->db->where ( 'id', $data['id'] );
		$this->db->update ( TABLES::$USERS, $data );
        }
	
	public function updateUserProfile ($data) {
		//$this->db->where ( 'id', $data['id'] );
		//$this->db->update ( TABLES::$USERS, $data );
		//$result = $this->db->affected_rows();
		//return $result;
		
				
		
		$params = array (
				'mobile' => $data['mobile']
		);
			
			
		//$where = "name ='".$data['name']."' AND cityid !=".$data['cityid'];
			
		$this->db->select ( '*' )->from ( TABLES::$USERS )->where ( $params )->where_not_in('id', $data['id']);;
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r(count($result)); exit();
			
		if (count($result) > 0)
		{
		
				
			$data2['msg'] = "User mobile number already exists.";
			$data2['status'] = 0;
			return $data2;
		}
			
		else
		{
				
		
		$this->db->where ( 'id', $data['id'] );
		$this->db->update ( TABLES::$USERS, $data );
		//$result = $this->db->affected_rows();
                $result['status'] = 1;
		return $result;
		
		}
		
		
		
	}
	/*public function forgetPassword ($mobile,$client_id){
		
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USERS );
		$this->db->where ( 'mobile',$mobile );
		$this->db->where ( 'client_id',$client_id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if(count($result)>0)
		{
			$data ['otp'] = mt_rand ( 100000, 999999 );
			$this->db->where ( 'id', $result[0]['id'] );
			$this->db->update ( TABLES::$USERS, $data );
			$result [0]['otp'] = $data ['otp'];
		}
		return $result;
	} */
	
	public function forgetPassword ($email){
	
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USERS );
		$this->db->where ( 'email',$email );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	
	public function getOrderDetailByUserId($id)
	{
		$this->db->select ( 'a.*,b.name' );
		$this->db->from ( TABLES::$ORDER.' AS a' );
		$this->db->join ( TABLES::$RESTAURANT.' AS b','a.restid=b.id','left' );
		$this->db->where ( 'userid',$id );
		$this->db->order_by('a.orderid','DESC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function subscribe($email)
	{   
	    $data=array();
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$SUBSCRIBE );
		$this->db->where ( $email );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if(count( $result ) > 0)
		{ 
			$data['message'] = "You have allready Subscribed to our newsletter . ";
		}
		else {
		$this->db->insert ( TABLES::$SUBSCRIBE, $email );
		$data['message'] = "Thank you for subscribing to our newsletter ! ";
		$data['email']=$email['email'];
		}
		return $data;
		
	}
	
	public function getFlatmatesbyUserId($userid){
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$FLATMATES );
		$this->db->where ( 'userid',$userid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
		
	}
	
	public function getflatmatebyId($id){
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$FLATMATES );
		$this->db->where ( 'id',$id);
		$query = $this->db->get ();
		$result = $query->row_array ();
		return $result;
		
	}
	
	public function addFlatmates($params){
		$this->db->insert ( TABLES::$FLATMATES, $params);
		$result = $this->db->insert_id();
		return $result;
		
	}
	public function updateflatmate($params){
		$this->db->where ( 'id', $params['id'] );
		$this->db->update ( TABLES::$FLATMATES, $params);
		$result = $this->db->affected_rows();
		return $result;
	}
	
	
}