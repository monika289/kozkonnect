<?php
class Wallet_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	public function createWallet($data) {
		$this->db->insert ( TABLES::$USERWALLET, $data );
	}
	
	public function addToWallet($data) {
    	$sql = "UPDATE ".TABLES::$USERWALLET." SET amount=amount+".$data['amount'].", updated_date='".date('Y-m-d H:i:s')."' WHERE userid=".$data['userid'];
    	$query = $this->db->query($sql);
    	$query->result_array ();
    	$trans = array();
    	$trans['userid'] = $data['userid'];
    	$trans['amount'] = $data['amount'];
    	$trans['is_debit'] = 0;
    	$trans['updated_by'] = $data['userid'];
    	$trans['updated_date'] = date('Y-m-d H:i:s');
    	if(!empty($data['orderid']))
    	$trans['orderid'] = $data['orderid'];
    	$this->db->insert ( TABLES::$USERWALLETTRANSACTION, $trans );
	}
	
	public function removeFromWallet($data) {
		$sql = "UPDATE ".TABLES::$USERWALLET." SET amount=amount-".$data['amount'].", updated_date='".date('Y-m-d H:i:s')."' WHERE userid=".$data['userid'];
		$query = $this->db->query($sql);
		$query->result_array ();
		$trans = array();
		$trans['userid'] = $data['userid'];
		$trans['amount'] = $data['amount'];
		$trans['is_debit'] = 0;
		$trans['updated_by'] = $trans['updated_by'];
		$trans['updated_date'] = date('Y-m-d H:i:s');
		if(!empty($data['orderid']))
			$trans['orderid'] = $data['orderid'];
		$this->db->insert ( TABLES::$USERWALLETTRANSACTION, $trans );
	}
	
	public function getWalletBalance($userid) {
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USERWALLET );
		$this->db->where ( 'userid', $userid );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getWalletTransactions($userid) {
		$this->db->select ( 'a.*,b.bookingcode' );
		$this->db->from ( TABLES::$USERWALLETTRANSACTION.' AS a' );
		$this->db->join ( TABLES::$BOOKING.' AS b','a.orderid=b.bookingid','left' );
		$this->db->where ( 'a.userid', $userid );
		$this->db->order_by('a.updated_date','DESC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	
}