<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CI_Model API for order management .
 *
 * <p>
 * We are using this model to add/update orders.
 * </p>
 * @package Orders
 * @subpackage orders-model
 * @author pradeep singh
 * @category CI_Model API
 */
class User_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}


	public function addUser($map) {
		$params = " email LIKE '".$map['email']."' OR mobile LIKE '".$map['mobile']."' ";
		$this->db->select ( 'id' )->from ( TABLES::$USERS )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		$msg='';
	switch ($map['user_type']) {
    case 1:
      $msg='User';
        break;
    case 2:
      $msg='Organization';
        break;
    case 3:
       $msg='NGO';
        break;
    case 4:
       $msg='Institutes';
        break;
    }
    
		if (count ( $result ) <= 0) {
			$this->db->insert(TABLES::$USERS,$map);
			$data ['status'] = $this->db->insert_id();
			$data ['msg'] =  $msg." Added successfully.";
			return $data;
		} else {
			$data ['msg'] =  $msg." already exists.";
			$data ['status'] = 0;
			return $data;
		}

		/*	$this->db->insert(TABLES::$USERS,$map);
			return $this->db->insert_id();  */
	}

	public function updateUser($map,$userid) {
		$params = " id !=".$userid." AND (email= '".$map['email']."' OR mobile = '".$map['mobile']."') ";
		$this->db->select ( 'id' )->from ( TABLES::$USERS )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
			$msg='';
		switch ($map['user_type']) {
	    case 1:
	      $msg='User';
	        break;
	    case 2:
	      $msg='Organization';
	        break;
	    case 3:
	       $msg='NGO';
	        break;
	    case 4:
	       $msg='Institutes';
	        break;
	    }
		if (count ( $result ) <= 0) {
			$this->db->where('id',$userid);
	        $this->db->update(TABLES::$USERS,$map);
			$data ['status'] = $this->db->affected_rows();
			$data ['msg'] =  $msg." update successfully.";
			return $data;
		} else {
			$data ['msg'] =  $msg." mobile or mail exists.";
			$data ['status'] = 0;
			return $data;
		}
     /*   $this->db->where('id',$userid);
	    $this->db->update(TABLES::$USERS,$map);
	    return $this->db->affected_rows(); */
	}

	public function saveDocs($map,$userid=null) {
		 // print_r($map);
		if(isset($userid))
		{
			$this->db->insert(TABLES::$USER_DOCS,$map);
			return $this->db->insert_id();
		}
	}
	public function getAllUsers($usertype=null) {
		$this->db->select ( '*' );
		if(isset($usertype))
		 $this->db->where('user_type',$usertype);
		$this->db->from ( TABLES::$USERS );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}

/*	public function getAllUsers() {
		$this->db->select ( '*' );
		$this->db->from ( TABLES::$USERS );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}*/
	public function getUserById($id) {
		$this->db->select ( 'a.*' );
		$this->db->from ( TABLES::$USERS .' AS a');
		$this->db->where ( 'a.id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}

	public function getuserImages($id) {
	$this->db->select ( 'a.*' );
	$this->db->from ( TABLES::$USER_DOCS . ' AS a' );
	
	$this->db->where( 'a.user_id', $id );
	
	$query = $this->db->get ();
	//echo $this->db->last_query();
	$result = $query->result_array ();
	return $result;
	}
	public function getuserdocsbyid($docid) {
	$this->db->select ( 'a.*' );
	$this->db->from ( TABLES::$USER_DOCS . ' AS a' );
	
	$this->db->where( 'a.id', $docid );
	
	$query = $this->db->get ();
	//echo $this->db->last_query();
	$result = $query->result_array ();
	return $result;
	}

	public function deleteUserdoc( $id ) {
		$this->db->where('id', $id);
		$this->db->delete(TABLES::$USER_DOCS);
	}
	//logo
	public function saveLogo($map,$userid=null) {
		 // print_r($map);
		if(isset($userid))
		{
			$this->db->where( 'id', $userid );
			$this->db->update ( TABLES::$USERS, $map );
		}
	}
		
	

	
	
	
	
}
