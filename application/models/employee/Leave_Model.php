<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Leave_Model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	

	 
	public function addLeave_data($pageData){  
			$this->db->insert ( TABLES::$LEAVE, $pageData);
			$detailid = $this->db->insert_id(); 
			
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
	} 
	
	/* GET ALL LEAVE LIST */
	
	public function leaveList(){
		$this->db->select('a.*,b.name');
		$this->db->from( TABLES::$LEAVE.' as a');
		$this->db->join( TABLES::$EMPLOYEE.' as b','a.emp_id=b.id','left');
		if($_SESSION['loginrole'] == 2){
			$this->db->where('b.company_id',$_SESSION['loginsp_id']);
		}else if($_SESSION['loginrole'] == 3){
			$this->db->where('b.company_id',$_SESSION['loginstore_id']);
		}
		$this->db->order_by('a.created_datetime','asc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	/*  
	 * @developer ajinkya 
	 *  get leave by id
	 *  	 
	 */
	
	public function getLeaveById($id){
		$this->db->select('*'); 
		$this->db->from(TABLES::$LEAVE);
		$this->db->where('leave_id',$id);
		$query = $this->db->get();
		return $query->row_array();
	}
	
	public function updateLeave($data){
		$this->db->where('leave_id',$data['leave_id']); 
		$this->db->update(tables::$LEAVE , $data);
		$data ['status'] = 1;
		$data ['msg'] = "Updated successfully";
		return $data;
		
		
	}
	
}