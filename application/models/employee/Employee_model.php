<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Employee_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	

	public function addRole($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name']
		);
		$this->db->select ( 'id' )->from ( TABLES::$ROLE )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$ROLE, $cat );
			return $this->db->insert_id();
		} else {
			$data ['msg'] = "Name already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	
	public function addRoleForm($data){
		
			foreach($data as $row){
				$para['role_id'] = $row['role_id'];
				$para['access_name'] = $row['access_name'];
				$para['edit'] = $row['edit'];
				$para['view'] = $row['view'];
				$para['no_access'] = $row['no_access'];
				
				$this->db->insert ( TABLES::$ROLEACCESS, $para);
				$detailid = $this->db->insert_id();
			}
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
	}
	
	public function getActiveRole() {
		$this->db->select ( '*' )->from ( TABLES::$ROLE );
		//$this->db->where ('status',1);
		$this->db->order_by ('id','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getActiveRole12() {
		$this->db->select ( '*' )->from ( TABLES::$ROLE );
		$this->db->where ('status',1);
		$this->db->order_by ('id','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	
	
	public function updateRole($map,$accessdata) {
	$params = array (
				'name' => $map ['name'],
				'id !=' => $map ['id'],
		);
		$this->db->select ( 'id' )->from ( TABLES::$ROLE )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where('id',$map['id']);
			$this->db->update(TABLES::$ROLE,$map);
			
			if(!empty($accessdata)){
				$this->db->where ( 'role_id', $map ['id']);
				$this->db->delete(TABLES::$ROLEACCESS);
				foreach($accessdata as $row){
					$para['role_id'] = $row['role_id'];
					$para['access_name'] = $row['access_name'];
					$para['edit'] = $row['edit'];
					$para['view'] = $row['view'];
					$para['no_access'] = $row['no_access'];
				
					$this->db->insert ( TABLES::$ROLEACCESS, $para);
					$detailid = $this->db->insert_id();
				}
			}
		
		  $data ['status'] = 1;
		  $data ['msg'] = "updated successfully";
		  return $data;
		} else {
		  $data ['msg'] = "Role name already exists.";
		  $data ['status'] = 0;
		  return $data;
		}
	}
	
    public function getRoleById($id) {
		$this->db->select('*')
		->from(TABLES::$ROLE);
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getRoleAccessById($id) {
		$this->db->select('b.*')
				 ->from(TABLES::$ROLE.' AS a')
				 ->join(TABLES::$ROLEACCESS.' AS b','a.id = b.role_id','inner');
		$this->db->where('a.id', $id);
		//$this->db->group_by ('a.id','ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function addEmp($map) {
		$data = array ();
		$params = array (
				'name' => $map ['name']
		);
		$emp = array(
			'name' => $map['name'],
			'email' => $map['email'],
			'mobile' => $map['mobile'],
			'role_id' => $map['role_id'],
			'upload_photo' => $map['upload_photo'],
			'upload_id' => $map['upload_id'],
			'status' => $map['status'],
			'emp_working_from' => $map['emp_working_from'],
			'emp_working_to' => $map['emp_working_to'],
			'company_id' => $map['company_id'],
			'company_type' => $map['company_type']
		);
		$this->db->select ( 'id' )->from ( TABLES::$EMPLOYEE )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$EMPLOYEE, $emp );
			$empid = $this->db->insert_id();
			if(!empty($map['expert'])){
				foreach($map['expert'] as $expert1){
					$expert['expert_id'] = $expert1;
					$expert['emp_id'] = $empid;
					$this->db->insert ( TABLES::$EMPEXPERT, $expert);
				}
			}
			if(!empty($map['week'])){
				foreach($map['week'] as $week1){
					$week['week_id'] = $week1;
					$week['emp_id'] = $empid;
					$this->db->insert ( TABLES::$EMPWEEK, $week);
				}
			}
			if(!empty($map['service'])){
				foreach($map['service'] as $service1){
					$service['service_id'] = $service1;
					$service['emp_id'] = $empid;
					$this->db->insert ( TABLES::$EMPSERVICE, $service);
				}
			}
			
			$data ['id'] = $empid;
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$data ['msg'] = "Employee name already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	
	public function addupload($data) {
		$result = array ();
		if(count($data['images']) > 0) {
				$this->db->insert_batch ( TABLES::$EMPLOYEE_DOC, $data ['images'] );
		}
		//echo $this->db->last_query();
		$result ['status'] = 1;
	    $result ['msg'] = "Added successfully";
	    return $result;
	}
	
	
	
	public function getActiveEmp() {
		$this->db->select ( 'a.*,b.name as role' , FALSE)
		->from ( TABLES::$EMPLOYEE.' AS a' )
		->join(TABLES::$ROLE.' AS b','a.role_id = b.id','inner');
		if($_SESSION['loginrole'] == 2){
			$this->db->where('a.company_id',$_SESSION['loginsp_id']);
		}else if($_SESSION['loginrole'] == 3){
			$this->db->where('a.company_id',$_SESSION['loginstore_id']);
		}
		//$this->db->where ('status',1);
		$this->db->order_by ('a.id','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getActiveForm() {
		$this->db->select ( '*' )->from ( TABLES::$FORM_ACCESS );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getActiveSubForm() {
		$this->db->select('a.*', FALSE)
		->from(TABLES::$SUBFORM_ACCESS.' AS a')
		->join(TABLES::$FORM_ACCESS.' AS b','a.form_id = b.id','inner');
		$this->db->order_by ('a.id','ASC');
		$query = $this->db->get ();
	//	echo $this->db->last_query();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getEmpById($category_id) {
	
	
		$this->db->select('a.*,b.name as role', FALSE)
		->from(TABLES::$EMPLOYEE.' AS a')
		->join(TABLES::$ROLE.' AS b','a.role_id = b.id','inner');
		$this->db->where ( 'a.id', $category_id );
		$this->db->order_by ('a.id','ASC');
		$query = $this->db->get ();
	//	echo $this->db->last_query();
		$result = $query->result_array ();
		return $result;
		
	}
	
	public function getUpload() {
		$this->db->select ( 'b.documents' );
		$this->db->from ( TABLES::$EMPLOYEE . ' AS a' );
		$this->db->join ( TABLES::$EMPLOYEE_DOC . ' AS b', 'a.id=b.emp_id', 'left' );
		$this->db->order_by ('a.id','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		//echo $this->db->last_query();
		return $result;
	}
	
	public function updateEmp($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name'],
				'id !=' => $cat ['id']
		);
		$this->db->select ( 'id' )->from ( TABLES::$EMPLOYEE )->where ( $params );
		$query = $this->db->get ();
		
		$result = $query->result_array ();
		$emplogo = '';
		$empphoto = '';
		if($cat['upload_id'] != ''){
			$emplogo = $cat['upload_id'];
		}else{
			$empphotoid = $this->getEmp($cat['id']);
			$emplogo = $empphotoid[0]['upload_id'];
		}
		if($cat['upload_photo'] != ''){
			$empphoto = $cat['upload_photo'];
		}else{
			$dbempphoto =  $this->getEmp($cat['id']);
			$empphoto = $dbempphoto[0]['upload_photo'];
		}
		$emp = array(
				'id'=>$cat['id'],
				'name' => $cat['name'],
				'email' => $cat['email'],
				'mobile' => $cat['mobile'],
				'role_id' => $cat['role_id'],
				'upload_photo' => $empphoto,
				'upload_id' => $emplogo,
				'status' => $cat['status'],
				'emp_working_from' => $cat['emp_working_from'],
				'emp_working_to' => $cat['emp_working_to'],
				'company_id' => $cat['company_id'],
				'company_type' => $cat['company_type']
		);
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $cat ['id'] );
			$this->db->update ( TABLES::$EMPLOYEE, $emp );
			//echo $this->db->last_query();
			$this->deleteEmpService($cat['id']);
			$this->deleteEmpWeek($cat['id']);
			$this->deleteEmpExpert($cat['id']);
			if(!empty($cat['expert'])){
				foreach($cat['expert'] as $expert1){
					$expert['expert_id'] = $expert1;
					$expert['emp_id'] = $cat['id'];
					$this->db->insert ( TABLES::$EMPEXPERT, $expert);
				}
			}
			if(!empty($cat['week'])){
				foreach($cat['week'] as $week1){
					$week['week_id'] = $week1;
					$week['emp_id'] = $cat['id'];
					$this->db->insert ( TABLES::$EMPWEEK, $week);
				}
			}
			if(!empty($cat['service'])){
				foreach($cat['service'] as $service1){
					$service['service_id'] = $service1;
					$service['emp_id'] = $cat['id'];
					$this->db->insert ( TABLES::$EMPSERVICE, $service);
				}
			}
			$data ['status'] = 1;
			$data ['msg'] = "updated successfully";
			return $data;
		} else {
			$data ['msg'] = "Name already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	
	public function updateupload($data) {
		$result = array ();
			$this->db->where ( 'emp_id', $data['emp_id'] );
			$result ['status'] = 1;
		$result ['msg'] = "Updated successfully";
		return $result;
	}
	
	public function getSubEmpById($id) {
		$this->db->select ( '*' )->from ( TABLES::$EMPLOYEE );
		$this->db->where ('id',$id);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getServices(){
		$this->db->select('a.id as id, GROUP_CONCAT(a.name,"-",c.name,"(",b.name,")") as name');
		$this->db->from(TABLES::$SERVICE.' AS a');
		$this->db->join(TABLES::$SUBCATEGORY.' AS b','b.id=a.subcategory_id','left');
		$this->db->join(TABLES::$CATEGORY.' AS c','c.id=b.parent_id','inner');
		$this->db->group_by('a.id');
		$this->db->order_by('a.id','DESC');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getActiveServices(){
		$this->db->select('a.id as id, GROUP_CONCAT(a.name,"-",c.name,"(",b.name,")") as name');
		$this->db->from(TABLES::$SERVICE.' AS a');
		$this->db->where('a.status',1);
		$this->db->where('b.status',1);
		$this->db->where('c.status',1);
		$this->db->join(TABLES::$SUBCATEGORY.' AS b','b.id=a.subcategory_id','left');
		$this->db->join(TABLES::$CATEGORY.' AS c','c.id=b.parent_id','left');
		$this->db->group_by('a.id');
		$this->db->order_by('a.id','DESC');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getExpertByEmpId($empid){
		$this->db->select('*');
		$this->db->from(TABLES::$EMPEXPERT);
		$this->db->where('emp_id',$empid);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	public function getWeekByEmpId($empid){
		$this->db->select('*');
		$this->db->from(TABLES::$EMPWEEK);
		$this->db->where('emp_id',$empid);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	public function getServicesByEmpId($empid){
		$this->db->select('*');
		$this->db->from(TABLES::$EMPSERVICE);
		$this->db->where('emp_id',$empid);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function deleteEmpService($empid){
		$this->db->where('emp_id', $empid);
		$this->db->delete(TABLES::$EMPSERVICE);
	}
	
	public function deleteEmpWeek($empid){
		$this->db->where('emp_id', $empid);
		$this->db->delete(TABLES::$EMPWEEK);
	}
	public function deleteEmpExpert($empid){
		$this->db->where('emp_id', $empid);
		$this->db->delete(TABLES::$EMPEXPERT);
	}
	
	public function getEmp($id){
		$this->db->select('*');
		$this->db->from(TABLES::$EMPLOYEE);
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result_array();
		return  $result;
	}
	
public function getActiveServicesStore($typeid){
		if($typeid == 2){
			return $this->getActiveServiceProviderList();
		}elseif ($typeid == 3){
			return $this->getActiveStoreList();
		}
	}
	
	public function getActiveServiceProviderList(){
		$this->db->select('id as id, provider_name as name');
		$this->db->from(TABLES::$SERVICE_PROVIDER);
		$this->db->where('status',1);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public  function getActiveStoreList(){
		$this->db->select('id as id, store_name as name');
		$this->db->from(TABLES::$STORE);
		$this->db->where('status',1);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getEmpList($roleid) {
		$this->db->select ( 'a.*,b.name as role' , FALSE);
		$this->db->from(TABLES::$EMPLOYEE.' AS a' );
		$this->db->join(TABLES::$ROLE.' AS b','a.role_id = b.id','left');
		if($roleid == 1){ 
			//$this->db->where('a.company_id',$roleid);
		}else{ 
			$this->db->where('a.company_id',$roleid);
		}
		//$this->db->where ('status',1);
		$this->db->order_by ('a.id','ASC');
		$query = $this->db->get();
		$result = $query->result_array ();
		return $result;
	}
	public function getExpertByStoreid($storeid,$weekday) {
		$this->db->select ( 'a.name,a.id')
		->from ( TABLES::$EMPLOYEE.' AS a' )
		->join(TABLES::$EMPWEEK.' AS b','a.id = b.emp_id','left')
		->where('a.company_id',$storeid)
		->where('b.week_id!=',$weekday)
		->group_by('emp_id');
		$this->db->order_by ('a.name','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	public function getEmpName($emp_id) {
		$this->db->select ( 'name')
		->from ( TABLES::$EMPLOYEE)
		->where('id=',$emp_id);
		$query = $this->db->get ();
		$result = $query->row_array ();
		return $result;
	}
}