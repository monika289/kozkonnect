<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

/**
 * General Settings Model
 *
 * <p>
 * We are using this model to add, update, delete and get general setting like city,area etc.
 * </p>
 *
 * @package General
 * @author Pradeep Singh
 * @copyright Copyright &copy; 2015
 * @category CI_Model API
 *          
 */
class Settings_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	public function addCity($city) {
		$data = array ();
		$errors = array ();
		$params = array (
				'name' => $city ['name'] 
		);
		$this->db->select ( 'id' )->from ( TABLES::$CITY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$CITY, $city );
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$errors ['city'] = "City name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	/**
	 *
	 *
	 * update city name
	 * 
	 * @param
	 *        	city object
	 * @access public
	 * @return array
	 */
	public function updateCity($city) {
		$data = array ();
		$errors = array ();
		$params = array (
				'name' => $city ['name'],
				'id !=' => $city ['id'] 
		);
		$this->db->select ( 'id' )->from ( TABLES::$CITY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $city ['id'] );
			$this->db->update ( TABLES::$CITY, $city );
			$data ['status'] = 1;
			$data ['msg'] = "updated successfully";
			return $data;
		} else {
			$errors ['city'] = "City name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	/**
	 *
	 * change the status of city from 1 to 0
	 * 
	 * @access public
	 * @param $id of
	 *        	city
	 *        	
	 */
	public function turnOffCity($id) {
		$city ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$CITY, $city );
	}
	
	/**
	 *
	 * change the status of city from 0 to 1.
	 * 
	 * @access public
	 * @param
	 *        	id of city
	 */
	public function turnOnCity($id) {
		$city ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$CITY, $city );
	}
	
	/**
	 *
	 * get city name,fence and status of city by id
	 * 
	 * @param
	 *        	city id
	 * @access public
	 * @return result_set
	 */
	public function getCityById($id) {
		$this->db->select ( 'id,name,status' )->from ( TABLES::$CITY )->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	/**
	 * get all the cities.
	 * 
	 * @param
	 *        	no param
	 * @access public
	 * @return array_list
	 */
	public function getAllCities() {
		$this->db->select ( 'id,name,status' )->from ( TABLES::$CITY )->order_by ( 'name', 'asc' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function checkLocalitiesbyPincode($pincode) {
		$this->db->select ( '*' )->from ( TABLES::$AREA )->order_by ( 'name', 'asc' );
		$this->db->where ( 'pincode', $pincode );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function addLocality($locality) {
		$params = array (
				'name' => $locality ['name'],
				'cityid=' => $locality ['cityid'] 
		);
		$this->db->select ( 'id' )->from ( TABLES::$AREA )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$AREA, $locality );
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$errors ['locality'] = "Locality name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	/**
	 *
	 * update locality name, longitude, latitude and zone
	 * 
	 * @param
	 *        	locality object
	 * @access public
	 * @throws Exception
	 */
	public function updateLocality($locality) {
		$data = array ();
		$params = array (
				'name' => $locality ['name'],
				'cityid' => $locality ['cityid'],
				'id !=' => $locality ['id'] 
		);
		$this->db->select ( 'id' )->from ( TABLES::$AREA )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $locality ['id'] );
			$this->db->update ( TABLES::$AREA, $locality );
			$data ['status'] = 1;
			$data ['msg'] = "Updated successfully";
			return $data;
		} else {
			$errors ['locality'] = "Locality name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	/**
	 *
	 * change the status of locality from 1 to 0
	 * 
	 * @access public
	 * @param $id of
	 *        	locality
	 *        	
	 */
	public function turnOffLocality($id) {
		$locality ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$AREA, $locality );
	}
	
	/**
	 *
	 * change the status of locality from 0 to 1
	 * 
	 * @access public
	 * @param $id of locality
	 *        	
	 */
	public function turnOnLocality($id) {
		$locality ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$AREA, $locality );
	}
	
	/**
	 *
	 * get locality name, latitude, longitude and zone_id of zone by id
	 * 
	 * @param locality id
	 * @access public
	 * @return result_set
	 */
	public function getLocalityById($id) {
		$this->db->select ( '*' )->from ( TABLES::$AREA )->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	/**
	 * get all the localities.
	 * 
	 * @param no param
	 * @access public
	 * @return array_list
	 */
	public function getAllLocalities() {
		$this->db->select ( '*' )->from ( TABLES::$AREA )->order_by ( 'name', 'asc' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getAreasByCityId($cityid) {
		$this->db->select ( 'a.*,b.name as zone_name' )
			 ->from ( TABLES::$AREA.' AS a' )
			 ->join ( TABLES::$ZONE.' AS b','a.zone_id=b.id','left')
			 ->order_by ( 'a.name', 'asc' );
		$this->db->where ( 'a.cityid', $cityid );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	/*public function getAllCuisines() {
		$this->db->select ( '*' )->from ( TABLES::$CUISINE )->order_by ( 'name', 'asc' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}*/
	
	public function getCuisineById($id) {
		$this->db->select ( '*' )->from ( TABLES::$CUISINE )->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function addCuisine($cuisine) {
		$params = array (
				'name' => $cuisine ['name'] 
		);
		$this->db->select ( 'id' )->from ( TABLES::$CUISINE )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$CUISINE, $cuisine );
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$errors ['locality'] = "Cuisine name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	public function updateCuisine($cuisine) {
		$data = array ();
		$params = array (
				'name' => $cuisine ['name'],
				'id !=' => $cuisine ['id'] 
		);
		$this->db->select ( 'id' )->from ( TABLES::$CUISINE )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $cuisine ['id'] );
			$this->db->update ( TABLES::$CUISINE, $cuisine );
			$data ['status'] = 1;
			$data ['msg'] = "Updated successfully";
			return $data;
		} else {
			$errors ['locality'] = "Cuisine name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	public function deleteCuisine($id) {
		$this->db->where ( 'id', $id );
		$this->db->delete ( TABLES::$CUISINE );
	}
	
	public function getRestaurantsByVendor($vendorid) {
		$this->db->select ( 'restid' )->from ( TABLES::$VENDOR )->where ( 'id', $vendorid );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getRestByAreaId($areaid) {
		$this->db->select ( 'a.*,b.name as areaname' )
				 ->from ( TABLES::$RESTAURANT.' AS a' )
				 ->from ( TABLES::$AREA.' AS b','a.areaid=b.id','inner' )
				 ->order_by ( 'a.name', 'asc' );
		$this->db->where ( 'b.id', $areaid );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function addReason($reason) {
		$params = array (
				'name' => $reason ['name']
		);
		$this->db->select ( 'id' )->from ( TABLES::$CANCEL_REASON )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$CANCEL_REASON, $reason );
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$errors ['locality'] = "Reason already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	public function updateReason($reason) {
		$data = array ();
		$params = array (
				'name' => $reason ['name'],
				'id !=' => $reason ['id']
		);
		$this->db->select ( 'id' )->from ( TABLES::$CANCEL_REASON )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $reason ['id'] );
			$this->db->update ( TABLES::$CANCEL_REASON, $reason );
			$data ['status'] = 1;
			$data ['msg'] = "Updated successfully";
			return $data;
		} else {
			$errors ['locality'] = "Reason already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	public function deleteReason($id) {
		$map['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$CANCEL_REASON , $map);
	}
	
	public function getActiveReasons(){
		$this->db->select ('*')->from ( TABLES::$CANCEL_REASON )->where ('status',1);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getReasonById($id){
		$this->db->select ('*')->from ( TABLES::$CANCEL_REASON )->where ('id',$id);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	//shinee 26May
	public function getCuisinesByArea($arealist) {
		$this->db->select ( 'distinct (c.id),c.name' )
		->from ( TABLES::$RESTAURANT . ' AS r' );
		$this->db->join ( TABLES::$RESTAURANT_CUISINES. ' AS rc', 'rc.restid = r.id', 'inner' );
		$this->db->join ( TABLES::$CUISINE. ' AS c', 'c.id = rc.cuisine_id', 'inner' );
		$this->db->where ( 'r.areaid IN ('.$arealist.')' );
		$this->db->group_by ( 'c.name' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	//.......................Added By Rohit Singh................................
	
	public function getZoneByCityId($cityid)
	{
		$this->db->select ('*')->from ( TABLES::$ZONE )->where ('city_id',$cityid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	public function getAreaByZoneId ($zoneid)
	{
		$this->db->select ('*')->from ( TABLES::$AREA )->where ('zone_id',$zoneid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
		
	}
	public function getRestaurantByZoneId($zoneid)
	{
		$this->db->select ('r.*,a.name as areaname')->from ( TABLES::$RESTAURANT.' AS r' );
		$this->db->join(TABLES::$AREA.' AS a','a.id = r.areaid','inner');
		$this->db->join(TABLES::$ZONE.' AS z','a.zone_id = z.id','inner');
		$this->db->where('z.id ='.$zoneid);
		$this->db->where('r.status = 1');
		$this->db->order_by('a.name','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r($result);
		return $result;
	}
	
	public function saveContactUs($params) {
		return $this->db->insert ( TABLES::$CONTACT_US, $params );
	}
	
	public function addFeedback($params) {
		return $this->db->insert ( TABLES::$FEEDBACK, $params );
	}
	
	public function getAreasByZoneId($zone_id) {
		$this->db->select ( '*' )
				 ->from ( TABLES::$AREA)
				 ->order_by ( 'name', 'asc' );
		$this->db->where ( 'zone_id', $zone_id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function saveRating($params) {
		$this->db->select ('count(id) as ratings,sum(rating) as total_rating')
				 ->from ( TABLES::$RESTAURANT_RATING);
		$this->db->where('restid',$params['restid']);
		$query = $this->db->get ();
		$result = $query->result_array ();
		if(count($result) > 0) {
			$total_rating = $result[0]['total_rating'];
			$ratings = $result[0]['ratings'];
		} else {
			$total_rating = 0;
			$ratings = 0;
		}
		$rating = round(($total_rating+$params['rating'])/($ratings+1),1);
		$this->db->insert ( TABLES::$RESTAURANT_RATING, $params );
		$this->db->where('id',$params['restid']);
		$this->db->update(TABLES::$RESTAURANT,array('rating'=>$rating));
	}
	
	public function saveReview($params) {
		$this->db->insert ( TABLES::$RESTAURANT_REVIEW, $params );
	}
	
	public function getRestaurantsReviews($restid) {
		$this->db->select ('a.*,b.name,b.avatar,c.rating')
				 ->from ( TABLES::$RESTAURANT_REVIEW.' AS a')
				 ->join(TABLES::$USERS.' AS b','a.userid=b.id','inner')
				 ->join(TABLES::$RESTAURANT_RATING.' AS c','a.userid=c.userid','left');
		$this->db->where('a.restid',$restid);
		$this->db->where('c.restid',$restid);
		$this->db->group_by('a.id');		 
		$this->db->order_by('a.review_on','DESC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	//................ Added by Tushar Ticket model..........................
	
	/* public function addTicket($params) {
		$this->db->insert ( TABLES::$TICKET, $params );
		return $this->db->insert_id();
	}
	
	public function updateTicket($params) {
		$this->db->where('ticketid',$params['ticketid']);
		return $this->db->update(TABLES::$TICKET,$params);
	}
	
	public function getAllTickets() {
		$this->db->select ( '*' )->from ( TABLES::$TICKET )
				 ->order_by('status','ASC')
				 ->order_by('priority','DESC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getAllActiveTickets() {
		$this->db->select ( "a.*,b.name,b.mobile,concat(c.first_name,' ',c.last_name) as assigned_to_name,concat(d.first_name,' ',d.last_name) as created_by_name" );
		$this->db->from ( TABLES::$TICKET.' AS a' );
		$this->db->join ( TABLES::$USER.' AS b','a.userid=b.id','inner' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS c','a.assigned_to=c.id','left' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS d','a.created_by=d.id','left' );
		$this->db->order_by('a.created_date','DESC');
		$this->db->order_by('a.priority','DESC');
		$this->db->order_by('a.status','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getTicketById($ticketid) {
		$this->db->select ( "a.*,b.name,b.mobile,concat(c.first_name,' ',c.last_name) as assigned_to_name,concat(d.first_name,' ',d.last_name) as created_by_name" );
		$this->db->from ( TABLES::$TICKET.' AS a' );
		$this->db->join ( TABLES::$USER.' AS b','a.userid=b.id','inner' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS c','a.assigned_to=c.id','left' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS d','a.created_by=d.id','left' );
		$this->db->where('a.ticketid',$ticketid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	} */
	
	public function addTicketCategory($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name']
		);
		$this->db->select ( 'id' )->from ( TABLES::$TICKET_CATEGORY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$TICKET_CATEGORY, $cat );
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$data ['msg'] = "Category name already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	public function getActiveTicketCategories() {
		$this->db->select ( '*' )->from ( TABLES::$TICKET_CATEGORY );
		$this->db->where ('status',1);
		$this->db->order_by ( 'id', 'ASC' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	public function getTicketCategories() {
		$this->db->select ( '*' )->from ( TABLES::$TICKET_CATEGORY );
		$this->db->order_by ( 'id', 'ASC' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	public function updateTicketCategory($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name'],
				'id !=' => $cat ['id']
		);
		$this->db->select ( 'id' )->from ( TABLES::$TICKET_CATEGORY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $cat ['id'] );
			$this->db->update ( TABLES::$TICKET_CATEGORY, $cat );
			$data ['status'] = 1;
			$data ['msg'] = "updated successfully";
			return $data;
		} else {
			$data ['msg'] = "Category name already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	public function getTicketCategoryById($id) {
		$this->db->select ( '*' )->from ( TABLES::$TICKET_CATEGORY );
		$this->db->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	public function addTicketSubCategory1($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name'],
		);
		$this->db->select ( 'id' )->from ( TABLES::$TICKET_SUBCATEGORY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$TICKET_SUBCATEGORY, $cat );
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$data ['msg'] = "Subcategory already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	public function addTicketSubCategory($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name'],
				'category_id' => $cat['category_id']
		);
		$this->db->select ( 'id' )->from ( TABLES::$TICKET_SUBCATEGORY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$TICKET_SUBCATEGORY, $cat );
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$data ['msg'] = "Subcategory already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	public function getSubActiveTicketCategories() {
		$this->db->select ( 'a.*,b.name as category', FALSE )->from ( TABLES::$TICKET_SUBCATEGORY . ' AS a' )->join ( TABLES::$TICKET_CATEGORY . ' AS b', 'a.category_id = b.id', 'left' );
		$this->db->where('a.status',1);
		//$this->db->where('b.status',1);
		$this->db->order_by ( 'a.id', 'DESC' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getSubAllTicketCategories() {
		$this->db->select ( 'a.*,b.name as category', FALSE )->from ( TABLES::$TICKET_SUBCATEGORY . ' AS a' )->join ( TABLES::$TICKET_CATEGORY . ' AS b', 'a.category_id = b.id', 'inner' );
		$this->db->order_by ( 'a.id', 'DESC' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	public function updateTicketSubCategory1($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name'],
				'id !=' => $cat ['id'],
		);
		$this->db->select ( 'id' )->from ( TABLES::$TICKET_SUBCATEGORY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $cat ['id'] );
			$this->db->update ( TABLES::$TICKET_SUBCATEGORY, $cat );
			$data ['status'] = 1;
			$data ['msg'] = "updated successfully";
			return $data;
		} else {
			$data ['msg'] = "Subcategory already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	
	public function updateTicketSubCategory($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name'],
				'id !=' => $cat ['id'],
				'category_id' => $cat['category_id']
		);
		$this->db->select ( 'id' )->from ( TABLES::$TICKET_SUBCATEGORY )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $cat ['id'] );
			$this->db->update ( TABLES::$TICKET_SUBCATEGORY, $cat );
			$data ['status'] = 1;
			$data ['msg'] = "updated successfully";
			return $data;
		} else {
			$data ['msg'] = "Subcategory name with selected category already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	public function getTicketSubCategoryById($id) {
		$this->db->select ( '*' )->from ( TABLES::$TICKET_SUBCATEGORY );
		$this->db->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getTicketSubCatId($id) {
		$this->db->select ( '*' )->from ( TABLES::$TICKET_SUBCATEGORY );
		$this->db->where('status',1);
		$this->db->where ( 'category_id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function addTicketStatus($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name']
		);
		$this->db->select ( 'id' )->from ( TABLES::$TICKET_STATUS )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$TICKET_STATUS, $cat );
			$data ['status'] = 1;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$data ['msg'] = "Status already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	public function getActiveTicketStatus() {
		$this->db->select ( '*' )->from ( TABLES::$TICKET_STATUS );
		$this->db->where ('status',1);
		$this->db->order_by ( 'id', 'ASC' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	public function getAllTicketStatus() {
		$this->db->select ( '*' )->from ( TABLES::$TICKET_STATUS );
		$this->db->order_by ( 'id', 'ASC' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	public function updateTicketStatus($cat) {
		$data = array ();
		$params = array (
				'name' => $cat ['name'],
				'id !=' => $cat ['id']
		);
		$this->db->select ( 'id' )->from ( TABLES::$TICKET_STATUS )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->where ( 'id', $cat ['id'] );
			$this->db->update ( TABLES::$TICKET_STATUS, $cat );
			$data ['status'] = 1;
			$data ['msg'] = "updated successfully";
			return $data;
		} else {
			$data ['msg'] = "Status already exists.";
			$data ['status'] = 0;
			return $data;
		}
	}
	public function getTicketStatusById($id) {
		$this->db->select ( '*' )->from ( TABLES::$TICKET_STATUS );
		$this->db->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function addTicket($params) {
		$this->db->insert ( TABLES::$TICKET, $params );
		return $this->db->insert_id();
	}
	
	public function addComment($params) {
		$this->db->insert ( TABLES::$TICKET_COMMENT, $params );
		$data ['status'] = 1;
		// $data ['msg'] = "Added successfully";
		return $data;
	}
	
	function updateLead($data) {
		$this->db->where('ticketid', $data['ticketid']);
		$this->db->update ( TABLES::$TICKET, $data);
		$result =array();
		if ($this->db->affected_rows() > 0)
		{
			$result['status'] = 1;
			$result['msg'] = 'Record updated successfully.';
	
		}
		else
		{
			$result['status'] = 0;
			$result['msg'] = 'Please try again.';
		}
		return $result;
	}
	
	public function updateTicket($params) {
		$this->db->where('ticketid',$params['ticketid']);
		return $this->db->update(TABLES::$TICKET,$params);
	}
	
	public function getAllTickets() {
		$this->db->select ( '*' )->from ( TABLES::$TICKET )
		->order_by('status_id','ASC')
		->order_by('priority','DESC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getAllActiveTickets() {
		//$this->db->select ( "a.*,b.name,b.mobile,b.email,concat(d.first_name,' ',d.last_name) as created_by_name,e.name as status,f.name as assigned_to_name" );
		$this->db->select ( "a.*,b.name,b.mobile,b.email,concat(d.first_name,' ',d.last_name) as created_by_name,e.name as status,concat(c.first_name,' ',c.last_name) as assigned_to_name" );
		$this->db->from ( TABLES::$TICKET.' AS a' );
		$this->db->join ( TABLES::$USER.' AS b','a.userid=b.id','inner' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS c','a.assigned_to=c.id','left' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS d','a.created_by=d.id','left' );
		$this->db->join ( TABLES::$TICKET_STATUS.' AS e','a.status_id=e.id','left' );
		//$this->db->join ( TABLES::$EMPLOYEE.' AS f','a.assigned_to=f.id','left' );
		$this->db->order_by('a.created_date','DESC');
		$this->db->order_by('a.priority','DESC');
		//	$this->db->order_by('a.status','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	public function getTicketsList() {
		//$this->db->select ( "a.*,b.name,b.mobile,b.email,concat(d.first_name,' ',d.last_name) as created_by_name,e.name as status,f.name as assigned_to_name" );
		$this->db->select ( "a.ticketid ,a.ticket_no,a.priority,a.created_date,b.name,b.mobile,concat(c.first_name,' ',c.last_name) as assigned_to_name,e.name as status" );
		$this->db->from ( TABLES::$TICKET.' AS a' );
		$this->db->join ( TABLES::$USERS.' AS b','a.user_id=b.id','left' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS c','a.assigned_to=c.id','left' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS d','a.created_by=d.id','left' );
		$this->db->join ( TABLES::$TICKET_STATUS.' AS e','a.status_id=e.id','left' );
		//$this->db->join ( TABLES::$EMPLOYEE.' AS f','a.assigned_to=f.id','left' );
		$this->db->order_by('a.created_date','DESC');
		$this->db->order_by('a.priority','DESC');
		//	$this->db->order_by('a.status','ASC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		//print_r($result);
		return $result;
	}
	
	public function getTicketById($ticketid) {
		$this->db->select ( "a.*,b.name,b.mobile,b.email as email1,concat(c.first_name,' ',c.last_name) as assigned_to_name,concat(d.first_name,' ',d.last_name) as created_by_name" );
		$this->db->from ( TABLES::$TICKET.' AS a' );
		$this->db->join ( TABLES::$USER.' AS b','a.userid=b.id','inner' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS c','a.assigned_to=c.id','left' );
		$this->db->join ( TABLES::$ADMIN_USER.' AS d','a.created_by=d.id','left' );
		$this->db->where('a.ticketid',$ticketid);
		$query = $this->db->get ();
		//echo $this->db->last_query();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getUserComment($ticketid) {
		$this->db->select ( "a.*" );
		$this->db->from ( TABLES::$TICKET_COMMENT.' AS a' );
		$this->db->where('a.ticketid',$ticketid);
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function addCompany($store) {
		$params = array (
				'contact_person_name' => $store ['contact_person_name']
		);
		$this->db->select ( 'id' )->from ( TABLES::$SERVICE_PROVIDER )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$SERVICE_PROVIDER, $store );
			$id = $this->db->insert_id();
			$data ['status'] = 1;
			$data ['id'] = $id;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$errors ['name'] = "Company name already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	public function addStore($store) {
		$params = array (
				'contact_person_name' => $store ['contact_person_name']
		);
		$this->db->select ( 'id' )->from ( TABLES::$STORE )->where ( $params );
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (count ( $result ) <= 0) {
			$this->db->insert ( TABLES::$STORE, $store );
			$id = $this->db->insert_id();
			$data ['status'] = 1;
			$data ['id'] = $id;
			$data ['msg'] = "Added successfully";
			return $data;
		} else {
			$errors ['name'] = "Store already exists.";
			$data ['status'] = 0;
			$data ['msg'] = $errors;
			return $data;
		}
	}
	
	public function addBilling($store) {
			$this->db->insert ( TABLES::$STORE_BILLING_CONFIG, $store );
		
	}
	
	public function addBillingFields($store) {
			$this->db->insert_batch ( TABLES::$STORE_BILLING_FIELDS, $store );
	
	}
	public function getAllCompony() {
		$this->db->select ( '*' )->from ( TABLES::$SERVICE_PROVIDER )->order_by ( 'id', 'asc' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getAllCompony1() {
		$this->db->select ( '*' )->from ( TABLES::$SERVICE_PROVIDER )->where( 'status', 1 );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getAllStore() {
		$this->db->select ( '*' )->from ( TABLES::$STORE )->order_by ( 'id', 'asc' );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getCompanyById($id) {
		$this->db->select ( '*' )->from ( TABLES::$SERVICE_PROVIDER )->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getStoreById($id) {
		$this->db->select ( '*' )->from ( TABLES::$STORE )->where ( 'id', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getBillingConfig($id) {
		$this->db->select ( '*' )->from ( TABLES::$STORE_BILLING_CONFIG )->where ( 'storeid', $id );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getBillingField($storeid, $field) {
		$curr_date = date ( 'Y-m-d' );
		$this->db->select ( '*' )->from ( TABLES::$STORE_BILLING_FIELDS );
		$this->db->where ( "storeid=" . $storeid . " AND billing_field='" . $field . "' AND '" . $curr_date . "' BETWEEN from_date AND to_date", '', FALSE );
		$this->db->or_where ( "storeid=" . $storeid . " AND billing_field='" . $field . "' AND to_date IS NULL", '', FALSE );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function updateCompany($data) {
		$this->db->where('id',$data['id']);
		return $this->db->update(TABLES::$SERVICE_PROVIDER,$data);
	}
	
	public function updateStore($data) {
		$logo = '';
		if(empty($data['logo'])){
			$prestorelogo = $this->getStoreById($data['id']);
			$logo = $prestorelogo[0]['logo'];
		}else{
			$logo = $data['logo'];
		}
		$store = array(
			'id' => $data['id'],
			'store_name' => $data['store_name'],
			'contact_person_name' => $data['contact_person_name'],
			'contact_person_mobile'=>$data['contact_person_mobile'],
			'contact_person_email' =>$data['contact_person_email'],
			'contact_person_designation'=> $data['contact_person_designation'],
			'service_provider' => $data['service_provider'],
			'status' => $data['status'],
			'sort_order' => $data['sort_order'],
			'logo' => $logo,
			'twitter' => $data['twitter'],
			'facebook' => $data['facebook'],
			'address' => $data['address'],
			'latitude' => $data['latitude'],
			'longitude' => $data['longitude'],
			'pincode' => $data['pincode'],
			'dashboard_access' =>$data['dashboard_access'],
			'date_modified' => $data['date_modified']
		);
		$this->deleteStoreService($data['id']);
		$this->deleteStoreWeek($data['id']);
		if(!empty($data['week'])){
			foreach($data['week'] as $week1){
				$week['week_id'] = $week1;
				$week['store_id'] = $data['id'];
				$this->db->insert( TABLES::$STOREWEEK, $week);
			}
		}
		if(!empty($data['service'])){
			foreach($data['service'] as $service1){
				$service['service_id'] = $service1;
				$service['store_id'] = $data['id'];
				$this->db->insert( TABLES::$STORESERVICE, $service);
			}
		}
		
		$this->db->where('id',$data['id']);
		
		
		return $this->db->update(TABLES::$STORE,$store);
	}
	
	public function updateBilling($params) {
		$this->db->where ( 'storeid', $params ['storeid'] );
		$flag = $this->db->update ( TABLES::$STORE_BILLING_CONFIG, $params );
		error_log($this->db->last_query());
		if ($flag) {
			$map ['status'] = 1;
			$map ['msg'] = 'Updated successfully.';
		} else {
			$map ['status'] = 1;
			$map ['msg'] = 'Updated successfully.';
		}
		return $map;
	}
	public function updateBillingfield($params) {
		$result = $this->getBillingFieldByDate ( $params ['storeid'], $params ['billing_field'], $params ['from_date'] );
		if (count ( $result ) > 0) {
			if ($result [0] ['to_date'] == null && $params ['from_date'] > $result [0] ['from_date']) {
				$newdate = date ( 'Y-m-d', strtotime ( '-1 day', strtotime ( $params ['from_date'] ) ) );
				$newparams = array ();
				$newparams ['to_date'] = $newdate;
				$this->db->where ( 'storeid', $params ['storeid'] );
				$this->db->where ( 'billing_field', $params ['billing_field'] );
				$this->db->update ( TABLES::$STORE_BILLING_FIELDS, $newparams );
				$this->db->insert ( TABLES::$STORE_BILLING_FIELDS, $params );
				error_log($this->db->last_query());
				$map ['status'] = 1;
				$map ['msg'] = 'Updated successfully.';
			} else {
				$map ['status'] = 0;
				$map ['msg'] = 'Invalid effective date.';
			}
		} else {
			$this->db->insert ( TABLES::$STORE_BILLING_FIELDS, $params );
			$map ['status'] = 1;
			$map ['msg'] = 'Updated successfully.';
			error_log($this->db->last_query());
		}
		return $map;
	}
	
	public function getBillingFieldByDate($storeid, $field, $date) {
		$curr_date = $date;
		$this->db->select ( '*' )->from ( TABLES::$STORE_BILLING_FIELDS );
		$this->db->where ( "storeid=" . $storeid . " AND billing_field='" . $field . "' AND '" . $curr_date . "' BETWEEN from_date AND to_date", '', FALSE );
		$this->db->or_where ( "storeid=" . $storeid . " AND billing_field='" . $field . "' AND to_date IS NULL", '', FALSE );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function userExist($data)
	{
		$this->db->select ('*');
		$this->db->from ( TABLES::$ADMIN_USER );
		$this->db->where ( 'email', $data['contact_person_email'] );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function addUser($users) {
		$data = array ();
		$user= array(
				'first_name' => $users['first_name'],
				'email' => $users['email'],
				'password' => $users['password'],
				'text_password' => $users['text_password'],
				'mobile' => $users['mobile'],
				'user_role' => $users['user_role'],
				'status' => $users['status'],
				'sprovider_id' => $users['sprovider_id'],
				'created_by' => $users['created_by'],
				'created_date' => $users['created_date'],
				'is_logged_in' => 0
		);
		$this->db->insert ( TABLES::$ADMIN_USER, $user );
		$id = $this->db->insert_id();
		if(!empty($users['week'])){
			foreach($users['week'] as $week1){
				$week['week_id'] = $week1;
				$week['store_id'] = $users['store_id'];
				$this->db->insert ( TABLES::$STOREWEEK, $week);
			}
		}
		if(!empty($users['service'])){
			foreach($users['service'] as $service1){
				$service['service_id'] = $service1;
				$service['store_id'] = $users['store_id'];
				$this->db->insert ( TABLES::$STORESERVICE, $service);
			}
		}
		$data ['status'] = 1;
		$data ['id'] = $id;
		$data ['msg'] = "Added successfully";
		return $data;
	
	}
	
	public function getServiceBystoreId($storeId){
		$this->db->select('*');
		$this->db->From(TABLES::$STORESERVICE);
		$this->db->where('store_id',$storeId);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getWeeksByStoreId($storeId){
		$this->db->select('*');
		$this->db->From(TABLES::$STOREWEEK);
		$this->db->where('store_id',$storeId);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function deleteStoreService($storeid){
		$this->db->where('store_id', $storeid);
		$this->db->delete(TABLES::$STORESERVICE);
	}
	
	public function deleteStoreWeek($storeid){
		$this->db->where('store_id', $storeid);
		$this->db->delete(TABLES::$STOREWEEK);
	}
	
	public function storeon($id){
		
		$item ['status'] = 1;
		$item['date_modified'] = date('Y-m-d H:i:s');
		$this->db->where ( 'id', $id );
		return $this->db->update ( TABLES::$STORE, $item );
	}
	
	public function storeoff($id){
		$item ['status'] = 0;
		$item['date_modified'] = date('Y-m-d H:i:s');
		$this->db->where ( 'id', $id );
		return $this->db->update ( TABLES::$STORE, $item );
	}
	
	public function getAllActiveCategories(){
		$this->db->select('*');
		$this->db->from(TABLES::$CATEGORY);
		$this->db->where('status',1);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getStoreListByAdminId($adminid){
		$this->db->select('*');
		$this->db->from(TABLES::$ADMIN_USER);
		$this->db->where('id',$adminid);
		$query = $this->db->get();
		$result = $query->result_array();
		$storeList = array();
		if($result[0]['user_role'] == 1){
			$storeList = $this->getAllActiveStores();
		}else{
			$storeList = $this->getStoreListByServiceProviderId($result[0]['sprovider_id']);
		}
		return $storeList;
	}
	
	public function getAllActiveStores(){
		$this->db->select('*');
		$this->db->from(TABLES::$STORE);
		$this->db->where('status',1);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
		
	}
	
	public function getStoreListByServiceProviderId($spid){
		$this->db->select('*');
		$this->db->from(TABLES::$STORE);
		$this->db->where('service_provider',$spid);
		$this->db->where('status',1);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	/*public function getservies($id){
		$this->db->select('b.*,CONCAT(b.name," - ",d.name,"(",c.name,")") as name');
		$this->db->from(TABLES::$STORESERVICE.' AS a');
		$this->db->join(TABLES::$SERVICE.' AS b','b.id=a.service_id','left');
		$this->db->join(TABLES::$SUBCATEGORY.' AS c','c.id = b.subcategory_id','left');
		$this->db->join(TABLES::$CATEGORY.' AS d','d.id=c.parent_id','left');
		$this->db->where('a.store_id',$id);
		$this->db->where('b.status',1);
		$this->db->where('c.status',1);
		$this->db->where('d.status',1);
		$this->db->group_by('a.id');
		$this->db->order_by('a.id','desc');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}*/
	
	public function getBillAmt($id){
		$this->db->select('*');
		$this->db->from(TABLES::$SERVICE);
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
/*	public function getcategoriesByAdminId($adminid){
		$this->db->select('*');
		$this->db->from(TABLES::$ADMIN_USER);
		$this->db->where('id',$adminid);
		$query = $this->db->get();
		$result = $query->result_array();
		$storeList = array();
		if($result[0]['user_role'] == 1){
			$storeList = $this->getAllActiveCategories();
			return $storeList;
		}else{
			//$storeList = $this->getStoreListByServiceProviderId($result[0]['sprovider_id']);
			$this->db->select('a.*');
			$this->db->from(TABLES::$CATEGORY.' AS a');
			$this->db->join(TABLES::$SUBCATEGORY.' AS b','b.parent_id=a.id','left');
			$this->db->join(TABLES::$SERVICE.' AS c','c.subcategory_id=b.id','left');
			$this->db->join(TABles::$STORESERVICE.' AS d','d.service_id = c.id','left');
			$this->db->join(TABLES::$STORE.' AS e','e.id=d.store_id','left');
			$this->db->join(TABLES::$ADMIN_USER.' AS f','f.sprovider_id=e.service_provider','left');
			$this->db->where('f.id',$adminid);
			$this->db->where('a.status',1);
			$this->db->where('b.status',1);
			$this->db->where('c.status',1);
			$this->db->group_by('a.id');
			$this->db->order_by('a.id','DESC');
			$query = $this->db->get();
			$result = $query->result_array();
			return $result;
		}
	}*/
	
	
	public function getcategoriesByAdminId($adminid){
		$this->db->select('*');
		$this->db->from(TABLES::$ADMIN_USER);
		$this->db->where('id',$adminid);
		$query = $this->db->get();
		$result = $query->result_array();
		$storeList = array();
		if($result[0]['user_role'] == 1){
			$storeList = $this->getAllActiveCategories();
			return $storeList;
		}else{
			//$storeList = $this->getStoreListByServiceProviderId($result[0]['sprovider_id']);
			$this->db->select('a.id as value, a.name as name');
			$this->db->from(TABLES::$CATEGORY.' AS a');
			$this->db->join(TABLES::$SUBCATEGORY.' AS b','b.parent_id=a.id','left');
			$this->db->join(TABLES::$SERVICE.' AS c','c.subcategory_id=b.id','left');
			$this->db->join(TABles::$STORESERVICE.' AS d','d.service_id = c.id','left');
			$this->db->join(TABLES::$STORE.' AS e','e.id=d.store_id','left');
			$this->db->join(TABLES::$ADMIN_USER.' AS f','f.sprovider_id=e.service_provider','left');
			$this->db->where('f.id',$adminid);
			$this->db->where('a.status',1);
			$this->db->where('b.status',1);
			$this->db->where('c.status',1);
			$this->db->group_by('a.id');
			$this->db->order_by('a.id','DESC');
			$query = $this->db->get();
			$result = $query->result_array();
			return $result;
		}
	}
	
	public function getservies($id){
		$this->db->select('d.id as value, d.name as name');
		$this->db->from(TABLES::$STORESERVICE.' AS a');
		$this->db->join(TABLES::$SERVICE.' AS b','b.id=a.service_id','left');
		$this->db->join(TABLES::$SUBCATEGORY.' AS c','c.id = b.subcategory_id','left');
		$this->db->join(TABLES::$CATEGORY.' AS d','d.id=c.parent_id','left');
		$this->db->where('a.store_id',$id);
		$this->db->where('b.status',1);
		$this->db->where('c.status',1);
		$this->db->where('d.status',1);
		$this->db->group_by('d.id');
		$this->db->order_by('d.id','desc');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getSubCategoriesByCatId($id,$storeid){
		$ids = explode(",",$id);
		$this->db->select('a.id as value, a.name as name');
		$this->db->from(TABLES::$SUBCATEGORY.' AS a');
		$this->db->join(TABLES::$CATEGORY.' AS b','b.id=a.parent_id','left');
		$this->db->join(TABLES::$SERVICE.' AS c','c.subcategory_id=a.id','left');
		$this->db->join(TABLES::$STORESERVICE.' AS d',' d.service_id=c.id','left');
		$this->db->where_in('b.id',$ids);
		$this->db->where('d.store_id',$storeid);
		$this->db->where('a.status',1);
		$this->db->where('b.status',1);
		$this->db->where('c.status',1);
		$this->db->group_by('a.id');
		$this->db->order_by('a.id','DESC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getServicesBySubCatId($id,$storeid){
		$ids = explode(",",$id);
		$this->db->select('c.id as value, c.name as name');
		$this->db->from(TABLES::$SUBCATEGORY.' AS a');
		$this->db->join(TABLES::$CATEGORY.' AS b','b.id=a.parent_id','left');
		$this->db->join(TABLES::$SERVICE.' AS c','c.subcategory_id=a.id','left');
		$this->db->join(TABLES::$STORESERVICE.' AS d',' d.service_id=c.id','left');
		$this->db->where_in('a.id',$ids);
		$this->db->where('d.store_id',$storeid);
		$this->db->where('a.status',1);
		$this->db->where('b.status',1);
		$this->db->where('c.status',1);
		$this->db->group_by('a.id');
		$this->db->order_by('a.id','DESC');
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getServiceDetails($id,$storeid){
		$ids = explode(",",$id);
		$this->db->select('a.*');
		$this->db->from(TABLES::$SERVICE.' AS a');
		$this->db->join(TABLES::$STORESERVICE.' AS b','b.service_id=a.id','left');
		$this->db->where_in('a.id',$ids);
		$this->db->where('b.store_id',$storeid);
		$this->db->where('a.status',1);
		$this->db->group_by('a.id');
		$this->db->order_by('a.id','DESC');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	public function checkBookedDate($checkdate,$storeid){
		$weekday = '';
		$this->db->select('*');
		$this->db->from(TABLES::$STOREWEEK);
		$this->db->where('store_id',$storeid);
		$this->db->where('week_id',$checkdate);
		$query = $this->db->get();
		$result = $query->result_array();
		if(sizeof($result) > 0){
			if($checkdate == 1){
				$weekday="Sunday";
			}elseif ($checkdate == 2){
				$weekday = "Monday";
			}elseif ($checkdate == 3){
				$weekday = 'Tuesday';
			}elseif ($checkdate == 4){
				$weekday = "Wednesday";
			}
			elseif ($checkdate == 5){
				$weekday = "Thursday";
			}
			elseif ($checkdate == 6){
				$weekday = "Friday";
			}
			elseif ($checkdate == 7){
				$weekday = "Saturday";
			}
			$msg['status'] =  0;
			$msg['msg'] ="Sorry Salon will remain close on ".$weekday.". Please select another day for booking.";
			return $msg;
		}else{
			$msg['status'] = 1;
			$msg['msg'] ="";
			return $msg;
		}
	}
	
	public function getExpertByService($id,$storeid){
		$ids = explode(",",$id);
		$this->db->select('a.id as id,a.name as name');
		$this->db->from(TABLES::$EMPLOYEE.' AS a');
		$this->db->join(TABLES::$EMPEXPERT.' AS b','b.emp_id=a.id','left');
		$this->db->join(TABLES::$STORESERVICE.' AS c','c.service_id=b.expert_id','left');
		$this->db->where_in('c.service_id',$ids);
		$this->db->where('c.store_id',$storeid);
		$this->db->group_by('a.id');
		$this->db->order_by('a.id','DESC');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	public function turnoncategory($id){
		$item ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$TICKET_CATEGORY, $item );
		if($this->db->affected_rows() > 0){
			return $this->getTicketCategoryById($id);
		}
	}
	
	public function turnoffcategory($id) {
		$item ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update (TABLES::$TICKET_CATEGORY, $item );
		if($this->db->affected_rows() > 0){
			return $this->getTicketCategoryById($id);
		}
	}
	public function turnonsubcat($id){
		$item ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$TICKET_SUBCATEGORY, $item );
		if($this->db->affected_rows() > 0){
			return $this->getTicketSubCategoryById($id);
		}
	}
	
	public function turnoffsubcat($id) {
		$item ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update (TABLES::$TICKET_SUBCATEGORY, $item );
		if($this->db->affected_rows() > 0){
			return $this->getTicketSubCategoryById($id);
		}
	}
	
	public function turnonstatus($id){
		
		$item ['status'] = 1;
		$this->db->where ( 'id', $id );
		$this->db->update ( TABLES::$TICKET_STATUS, $item );
		if($this->db->affected_rows() > 0){
			return $this->getTicketStatusById($id);
		}
	}
	
	public function turnoffstatus($id) {
		
		$item ['status'] = 0;
		$this->db->where ( 'id', $id );
		$this->db->update (TABLES::$TICKET_STATUS, $item );
		if($this->db->affected_rows() > 0){
			return $this->getTicketStatusById($id);
		}
	}
}
