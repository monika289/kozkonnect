<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Social_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function process_user_details($data) {
        $email = $data['email'];
        $user = $this->db->query("SELECT * FROM tbl_users WHERE email = '$email'")->result_array();
        if (empty($user)) {
            $this->db->insert('tbl_users', $data);
            $user_id = $this->db->insert_id();
            $update['my_ref_code'] = strtoupper(base_convert(strtotime(date('Y-m-d H:i:s')), 10, 36)).strtoupper(base_convert($user_id, 10, 36));
            $this->db->where('id', $user_id);
            $this->db->update('tbl_users', $update);
            $user = $this->db->query("SELECT * FROM tbl_users WHERE id = '$user_id'")->result_array();
            $wallets = array();
            $wallets['userid'] = $user_id;
            $wallets['amount'] = 0;
            $wallets['created_date'] = date('Y-m-d H:i:s');
            $wallets['updated_date'] = date('Y-m-d H:i:s');
            $this->db->insert ( TABLES::$USERWALLET, $wallets );
            $this->session->set_userdata('hbuserid', $user[0]['id']);
            $this->session->set_userdata('hbusername', $user[0]['name']);
            $this->session->set_userdata('hbuseremail', $user[0]['email']);
            $this->session->set_userdata('hbusermobile', $user[0]['mobile']);
            $this->session->set_userdata('hbuseravatar', $data[0]['avatar']);
            //$this->session->set_userdata('socialuserid', $user_id);
        } else {
            $this->session->set_userdata('hbuserid', $user[0]['id']);
            $this->session->set_userdata('hbusername', $user[0]['name']);
            $this->session->set_userdata('hbuseremail', $user[0]['email']);
            $this->session->set_userdata('hbusermobile', $user[0]['mobile']);
            $this->session->set_userdata('hbuseravatar', $data[0]['avatar']);
        }
    }

}
