<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Cause_Model extends  CI_Model{
	function __construct() {
		parent::__construct();
	}

	public function getCauseList(){
		$this->db->select('a.*,a.status as c_status,b.user_type,b.fname,b.lname,b.mobile,b.email,b.company_name,c.name as main_cat,d.name as sub_cat,f.company_name as ango');
		$this->db->from(TABlES::$CAUSE.' AS a');
		$this->db->join(TABlES::$USERS.' AS b','b.id=a.user_id','inner');
		$this->db->join(TABlES::$USERS.' AS f','f.id=a.assign_ngo','left');
		$this->db->join(TABLES::$CAUSE_MAIN_CATEGORY.' AS c','c.id=a.category','inner');
		$this->db->join(TABLES::$CAUSE_SUB_CATEGORY.' AS d','d.id=a.subcategory','inner');  
		//$this->db->where('a.order_status !=',7);
		$this->db->order_by("a.id","DESC");
		//$this->db->group_by('a.orderid');
		$query = $this->db->get();
	//echo	$this->db->last_query();  exit;
		$result = $query->result_array();
		return $result;  
	}
	
	
}