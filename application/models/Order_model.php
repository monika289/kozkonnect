<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CI_Model API for order management .
 *
 * <p>
 * We are using this model to add/update orders.
 * </p>
 * @package Orders
 * @subpackage orders-model
 * @author pradeep singh
 * @category CI_Model API
 */
class Order_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}


public function addOrder( $map ) {
	
	
	
	
	$this->db->select('userid');
	$this->db->from( TABLES:: $BOOKING);
	$this->db->where('userid',$map['userid']);
	$query = $this->db->get();
	$result = $query->result_array();
	
	//print_r($result); exit();
	
	if(!empty($result))
	{
		
	        $area['status'] = 0;
		$this->db->where ( 'areaid',$map['areaid']);
		$this->db->where ( 'userid', $map['userid'] );
		
		$update=$this->db->update ( TABLES::$BOOKING, $area );
		//print_r($update); exit();
		
		$this->db->insert(TABLES::$BOOKING,$map);
		return $this->db->insert_id();
		
		
	}
	else
	{
               
               
		$this->db->select('count(bookingid) as orders', FALSE)
				 ->from(TABLES::$BOOKING);
		$this->db->where('userid',$map['userid']);
		$this->db->where('status', 1);
		$query = $this->db->get();
		$result = $query->result_array();
		$this->db->insert(TABLES::$BOOKING,$map);
		return $this->db->insert_id();
	}
        
	
}
		
	
	public function updateOrder( $map ) {
		$this->db->where('bookingid',$map['bookingid']);
		return $this->db->update(TABLES::$BOOKING,$map);
		//echo $this->last_query();
	}
	
	public function addOrderSlot($params) {
		$user_id = $params[0]['userid'];
		$this->db->select('count(bookingid) as orders', FALSE)
		->from(TABLES::$BOOKING);
		$this->db->where('userid',$user_id);
		$this->db->where('status', 1);
		$query = $this->db->get();
		$result = $query->result_array();
                
		if(count($result) > 0 && $result[0]['orders'] > 0) {
			$umap = array('end_date'=> $params[0]['start_date'], 'status' => 0);
			$this->db->where ( 'userid', $user_id);
			 $this->db->update ( TABLES::$BOOKINGSLOT, $umap );
			 foreach($params as $param){
			 	//$param['start_date'] = date('Y-m-d', strtotime($param['start_date']. ' +1 day'));
			 	$this->db->insert(TABLES::$BOOKINGSLOT, $param);
			 }
			 
		 } else {
				 $this->db->insert_batch(TABLES::$BOOKINGSLOT, $params); 
		 }
                 $bookingid=$params[0]['bookingid'];
		//print_r($bookingid); exit();
		$this->db->select ( 'a.*,b.name,b.email,b.mobile,bs.mealtype_name,bs.head_count,bs.amount');
		$this->db->from ( TABLES::$BOOKING . ' AS a' );
		$this->db->join ( TABLES::$USERS . ' AS b', 'a.userid=b.id', 'left' );
		$this->db->join ( TABLES::$BOOKINGSLOT . ' AS bs', 'a.userid=bs.userid', 'left' );
		$this->db->where ( 'a.bookingid', $bookingid );
		$query = $this->db->get ();
		$result = $query->result_array ();
		return $result;
	}
	
	public function getOrderDetailsbyUserId($param){
		$this->db->select('a.*', FALSE)
		->from(TABLES::$BOOKING.' AS a');
		$this->db->where('a.userid', $param['userid']);
		//$this->db->where('a.status !=', 0);
		$this->db->order_by('a.booked_on', 'DESC');
		$query = $this->db->get();
		$result = $query->result_array();
		//echo $this->db->last_query();
		return $result;
	}
	
	public function getActiveBookingbyUserId($id){
		$this->db->select('a.*', FALSE)
		->from(TABLES::$BOOKING.' AS a');
		//$this->db->join(TABLES::$BOOKINGSLOT.' as b','a.bookingid = b.bookingid', 'left');
		$this->db->where('a.userid', $id);
		$this->db->where('a.status !=', 0);
		$query = $this->db->get();
		$result = $query->result_array();
		//echo $this->db->last_query();
		return $result;
	}
	
	public function getInvoiceDetails($param){
		$this->db->select('a.*', FALSE)
		->from(TABLES::$BOOKING_INVOICE.' AS a');
		$this->db->where('a.userid', $param['userid']);
		$query = $this->db->get();
		$result = $query->result_array();
		// echo $this->db->last_query();
		return $result;
	}
	
	public function getOrderSlotsbyUserId($param){
		
		$this->db->select('a.*', FALSE)
		->from(TABLES::$BOOKINGSLOT.' AS a');
		$this->db->where('a.userid', $param['userid']);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	public function getOrderDetailsbybookinid($param) {
            $this->db->select('a.*', FALSE)
		->from(TABLES::$BOOKINGSLOT.' AS a');
		$this->db->where('a.bookingid', $param);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
        }

        public function getOrderDetails($ordercode) {
	//echo "model";
		$this->db->select('a.*,b.name, b.mobile, b.email,b.outstanding,c.name as pickup_boy,c.mobile as pickup_boy_mobile,d.name as del_boy,d.mobile as del_boy_mobile,e.invoice_url,e.invoice_date,e.id as invoice_id,f.transactionid,f.status as payment_status,f.payment_date,f.longurl,g.print_url,h.key,h.salt,i.name as area', FALSE)
				 ->from(TABLES::$ORDER.' AS a')
				 ->join(TABLES::$USER.' AS b','a.userid = b.id','inner')
				 ->join(TABLES::$FIELD_EXECUTIVE.' AS c','a.pickup_exe_id = c.id','left')
				 ->join(TABLES::$FIELD_EXECUTIVE.' AS d','a.delivery_exe_id = d.id','left')
				 ->join(TABLES::$INVOICE.' AS e','a.orderid = e.orderid','left')
				 ->join(TABLES::$PAYMENT_DETAIL.' AS f','a.orderid = f.orderid','left')
				 ->join(TABLES::$PRINT.' AS g','a.orderid = g.orderid','left')
				 ->join(TABLES::$STORE_AREA.' AS j','a.areaid = j.areaid','inner')
				 ->join(TABLES::$STORE.' AS h','j.store_id = h.id','inner')
		         ->join(TABLES::$AREA.' AS i','a.areaid = i.id','left');
		$this->db->where('a.orderid', $ordercode);
		$query = $this->db->get();
		//echo $this->db->last_query();
		$result = $query->result_array();
		return $result;
	}
	
	public function getOrderDetailsByIds($orderids) {
		$this->db->select('a.*,b.name, b.mobile, b.email', FALSE)
				 ->from(TABLES::$ORDER.' AS a')
				 ->join(TABLES::$USER.' AS b','a.userid = b.id','inner');
		$this->db->where('a.orderid IN('.$orderids.')','',false);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function filterOrders($map) {
		if($map['store_id'] > 1)
		{
		$this->db->select('a.*,b.name, b.mobile, b.email,b.area,d.name as franchise');
		$this->db->from(TABLES::$ORDER.' AS a');
		$this->db->join(TABLES::$USER.' AS b','a.userid = b.id','inner');
		$this->db->join(TABLES::$STORE_AREA.' AS c','a.areaid = c.areaid','inner');
		$this->db->join(TABLES::$STORE.' AS d','c.store_id = d.id','inner');
		if($map['role_id'] != 1) {
			$this->db->where('c.store_id',$map['store_id']);
		}
		if($map['status'] != "")
		$this->db->where('a.status',$map['status']);
		if(!empty($map['pickup_date'])) {
			$pickup_date = date('Y-m-d',strtotime($map['pickup_date']));
			$this->db->where('a.pickup_date',$pickup_date);
		}
		if(!empty($map['delivery_date'])) {
			$delivery_date = date('Y-m-d',strtotime($map['delivery_date']));
			$this->db->where('a.tml_delivery_date',$delivery_date);
		}
		if(!empty($map['email'])) {
			$this->db->where('b.email',$map['email']);
		}
		if(!empty($map['mobile'])) {
			$this->db->where('b.mobile',$map['mobile']);
		}
		if(!empty($map['name'])) {
			$this->db->like('b.name',$map['name'],'both');
		}
		$this->db->order_by('a.orderid','desc');
		$query = $this->db->get();
		}
		else 
		{
			$this->db->select('a.*,b.name, b.mobile, b.email,b.area');
			$this->db->from(TABLES::$ORDER.' AS a');
			$this->db->join(TABLES::$USER.' AS b','a.userid = b.id','inner');
			$this->db->join(TABLES::$STORE_AREA.' AS c','a.areaid = c.areaid','inner');
			$this->db->join(TABLES::$STORE.' AS e','c.store_id = e.id','inner');
			if($map['role_id'] != 1) {
				$this->db->where('c.store_id',$map['store_id']);
			}
			if($map['status'] != "")
				$this->db->where('a.status',$map['status']);
			if(!empty($map['pickup_date'])) {
				$pickup_date = date('Y-m-d',strtotime($map['pickup_date']));
				$this->db->where('a.pickup_date',$pickup_date);
			}
			if(!empty($map['delivery_date'])) {
				$delivery_date = date('Y-m-d',strtotime($map['delivery_date']));
				$this->db->where('a.tml_delivery_date',$delivery_date);
			}
			if(!empty($map['email'])) {
				$this->db->where('b.email',$map['email']);
			}
			if(!empty($map['mobile'])) {
				$this->db->where('b.mobile',$map['mobile']);
			}
			if(!empty($map['name'])) {
				$this->db->like('b.name',$map['name'],'both');
			}
			$this->db->order_by('a.orderid','desc');
			$query = $this->db->get();
		}
		$result = $query->result_array();
		return $result;
	}
	
	public function addOrderTags($params) {
		//print_r($params);
		//return $this->db->insert_batch(TABLES::$PRINT,$params);
		foreach ($params as $params) {
			$this->db->select('*')
			->from(TABLES::$PRINT)
			->where("orderid",$params['orderid'])
			->where("item_id",$params['item_id']);
			$query = $this->db->get();
			$rowcount = $query->num_rows();
			$result = $query->result_array();
			if($rowcount > 0) {
				$this->db->where('orderid',$params['orderid'])
				->where('item_id',$params['item_id']);
				$this->db->update(TABLES::$PRINT,$params);
	
			} else {
				$this->db->insert(TABLES::$PRINT,$params);
			}
		}
		//echo $this->db->last_query();
	}
	
	public function updatePrint($map) {
		//print_r($map);
		foreach ($map as $map) {
			$this->db->where('orderid',$map['orderid'])
			->where('item_id',$map['item_id']);
			$this->db->update(TABLES::$PRINT,$map);
		}
		// $this->db->insert(TABLES::$INVOICE,$map);
		// echo $this->db->last_query();
	}
	
	public function getOrdersForPickup($store_id,$role_id) {
		$this->db->select('a.*,b.name, b.mobile, b.email,b.area,d.name as franchise');
		$this->db->from(TABLES::$ORDER.' AS a');
		$this->db->join(TABLES::$USER.' AS b','a.userid = b.id','inner');
		$this->db->join(TABLES::$STORE_AREA.' AS c','a.areaid = c.areaid','inner');
		$this->db->join(TABLES::$STORE.' AS d','c.store_id = d.id','inner');
		if($role_id != 1) {
			$this->db->where('c.store_id',$store_id);
		}
		$this->db->where('a.status',1);
		$this->db->order_by('a.orderid','desc');
		$this->db->limit(100);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getOrdersUnderProcess($store_id,$role_id) {
		$this->db->select('a.*,b.name, b.mobile, b.email,b.area,d.name as franchise');
		$this->db->from(TABLES::$ORDER.' AS a');
		$this->db->join(TABLES::$USER.' AS b','a.userid = b.id','inner');
		$this->db->join(TABLES::$STORE_AREA.' AS c','a.areaid = c.areaid','inner');
		$this->db->join(TABLES::$STORE.' AS d','c.store_id = d.id','inner');
		if($role_id != 1) {
			$this->db->where('c.store_id',$store_id);
		}
		$this->db->where('a.status',2);
		$this->db->order_by('a.orderid','desc');
		$this->db->limit(100);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getOrdersForDelivery($store_id,$role_id) {
		$date = date('Y-m-d',strtotime('-2 days'));
		$this->db->select('a.*,b.name, b.mobile, b.email,b.area,d.name as delivery_executive_name,e.name as franchise');
		$this->db->from(TABLES::$ORDER.' AS a');
		$this->db->join(TABLES::$USER.' AS b','a.userid = b.id','inner');
		$this->db->join(TABLES::$FIELD_EXECUTIVE.' AS d','a.delivery_exe_id = d.id','left');
		$this->db->join(TABLES::$STORE_AREA.' AS c','a.areaid = c.areaid','inner');
		$this->db->join(TABLES::$STORE.' AS e','c.store_id = e.id','inner');
		if($role_id != 1) {
			$this->db->where('c.store_id',$store_id);
		}
		$this->db->where("a.tml_delivery_date >=",$date);
		$this->db->where('a.status',3);
		$this->db->order_by('a.orderid','desc');
		$this->db->limit(100);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getTodaysOrders($store_id,$role_id) {
		$current_date = date('Y-m-d');
		$this->db->select('a.*,b.name, b.mobile, b.email,b.area,d.name as pickup_executive_name,e.name as franchise');
		$this->db->from(TABLES::$ORDER.' AS a');
		$this->db->join(TABLES::$USER.' AS b','a.userid = b.id','inner');
		$this->db->join(TABLES::$FIELD_EXECUTIVE.' AS d','a.pickup_exe_id = d.id','left');
		$this->db->join(TABLES::$STORE_AREA.' AS c','a.areaid = c.areaid','inner');
		$this->db->join(TABLES::$STORE.' AS e','c.store_id = e.id','inner');
		if($role_id != 1) {
			$this->db->where('c.store_id',$store_id);
		}
		$this->db->where('a.pickup_date',$current_date);
		$this->db->where('a.status !=',5);
		$this->db->order_by('a.orderid','desc');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getTodaysOrdersBooked($store_id,$role_id) {
		$current_date = date('Y-m-d');
		$this->db->select('a.*,b.name, b.mobile, b.email,b.area,d.name as pickup_executive_name,e.name as franchise');
		$this->db->from(TABLES::$ORDER.' AS a');
		$this->db->join(TABLES::$USER.' AS b','a.userid = b.id','inner');
		$this->db->join(TABLES::$FIELD_EXECUTIVE.' AS d','a.pickup_exe_id = d.id','left');
		$this->db->join(TABLES::$STORE_AREA.' AS c','a.areaid = c.areaid','inner');
		$this->db->join(TABLES::$STORE.' AS e','c.store_id = e.id','inner');
		if($role_id != 1) {
			$this->db->where('c.store_id',$store_id);
		}
		$this->db->where("DATE(a.ordered_on) = '".$current_date."'",'',false);
		$this->db->where('a.status !=',5);
		$this->db->order_by('a.orderid','desc');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getTodaysDeliveries($store_id,$role_id) {
		$current_date = date('Y-m-d');
		$this->db->select('a.*,b.name, b.mobile, b.email,b.area,d.name as delivery_executive_name,e.name as franchise');
		$this->db->from(TABLES::$ORDER.' AS a');
		$this->db->join(TABLES::$USER.' AS b','a.userid = b.id','inner');
		$this->db->join(TABLES::$FIELD_EXECUTIVE.' AS d','a.delivery_exe_id = d.id','left');
		$this->db->join(TABLES::$STORE_AREA.' AS c','a.areaid = c.areaid','inner');
		$this->db->join(TABLES::$STORE.' AS e','c.store_id = e.id','inner');
		if($role_id != 1) {
			$this->db->where('c.store_id',$store_id);
		}
		//$this->db->where('a.tml_delivery_date',$current_date);
		$this->db->where("(a.tml_delivery_date = '".$current_date."' OR a.delivery_date = '".$current_date."')",'',false);
		$this->db->where('a.status !=',5);
		$this->db->order_by('a.orderid','desc');
		$query = $this->db->get();
		$result = $query->result_array();
		//echo $this->db->last_query();
		return $result;
	}
	
	public function getTodaysPendingDeliveries() {
		$current_date = date('Y-m-d');
		$this->db->select('*');
		$this->db->from(TABLES::$ORDER.' AS a');
		$this->db->where('tml_delivery_date',$current_date);
		$this->db->where('status NOT IN(4,5)','',false);
		$this->db->order_by('orderid','desc');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getPreviousDayPendingDeliveries() {
		$current_date = date('Y-m-d',strtotime('-1 day'));
		$this->db->select('*');
		$this->db->from(TABLES::$ORDER.' AS a');
		$this->db->where('tml_delivery_date',$current_date);
		$this->db->where('status NOT IN(4,5)','',false);
		$this->db->order_by('orderid','desc');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getTodaysOrderCount($store_id,$role_id) {
		$date = date('Y-m-d');
		$this->db->select('count(a.orderid) as orders,status', FALSE);
				$this->db->from(TABLES::$ORDER.' AS a');
				if($role_id != 1) {
					$this->db->join(TABLES::$STORE_AREA.' AS c','a.areaid = c.areaid','inner');
					$this->db->where('c.store_id',$store_id);
				}
				$this->db->where("DATE(a.ordered_on) = '".$date."'",'',false);
		$this->db->group_by('a.status');
		$this->db->order_by('a.status','asc');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getTodaysPickupOrderCount($store_id,$role_id) {
		$date = date('Y-m-d');
		$this->db->select('count(a.orderid) as orders', FALSE);
				 $this->db->from(TABLES::$ORDER.' AS a');
				 if($role_id != 1) {
				 	$this->db->join(TABLES::$STORE_AREA.' AS c','a.areaid = c.areaid','inner');
				 	$this->db->where('c.store_id',$store_id);
				 }
				$this->db->where("a.pickup_date",$date);
		$this->db->where('a.status !=',5,false);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['orders'];
	}
	
	public function addCancelOrderReason($map) {
		return $this->db->insert(TABLES::$ORDER_CANCEL_REASON,$map);
	}
	
	public function getOrderDetailsByOrderId($orderid) {
		$this->db->select('a.*,b.name, b.mobile, b.email,b.area,b.gcm_reg_id,b.coupon_code as referral_code,b.is_new_customer,b.outstanding,c.name as pickup_executive,c.mobile as pickup_executive_mobile,c.dack_rider_code as pickup_dack_rider_code,d.name as delivery_executive,d.mobile as delivery_executive_mobile,d.dack_rider_code as delivery_dack_rider_code,e.id as invoice_id,f.status as payment_status')
				 ->from(TABLES::$ORDER.' AS a')
				 ->join(TABLES::$USER.' AS b','a.userid = b.id','inner')
				 ->join(TABLES::$FIELD_EXECUTIVE.' AS c','a.pickup_exe_id = c.id','left')
				 ->join(TABLES::$FIELD_EXECUTIVE.' AS d','a.delivery_exe_id = d.id','left')
				 ->join(TABLES::$INVOICE.' AS e','a.orderid = e.orderid','left')
				 ->join(TABLES::$PAYMENT_DETAIL.' AS f','a.orderid = f.orderid','left')
				 ->where('a.orderid',$orderid);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getOrderInfoByOrderId($orderid) {
		$this->db->select('*')
				 ->from(TABLES::$ORDER)
				 ->where('orderid',$orderid);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function searchOrders($params) {
		$this->db->select('a.*,b.name, b.mobile, b.email,c.name as areaname')
				 ->from(TABLES::$ORDER.' AS a')
				 ->join(TABLES::$USER.' AS b','a.userid = b.id','inner')
				 ->join(TABLES::$AREA.' AS c','a.areaid = c.id','left');
		if($params['status'] != '')
			$this->db->where('a.status',$params['status']);
		if(!empty($params['from_date']) && !empty($params['to_date']))
			$this->db->where("DATE(a.ordered_on) between '".$params['from_date']."' and '".$params['to_date']."'",'',false);
		$this->db->order_by('a.tml_delivery_date','asc');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function addOrderLogs($params) {
		return $this->db->insert(TABLES::$ORDER_LOGS,$params);
	}
	
	public function getOrderLogs($orderid) {
		$this->db->select("a.*,concat_ws(' ',b.first_name,b.last_name) as csename",false)
				 ->from(TABLES::$ORDER_LOGS.' AS a')
				 ->join(TABLES::$ADMIN_USER.' AS b','a.created_by = b.id','left')
				 ->where('a.orderid',$orderid);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function addOrderItems($params) {
		return $this->db->insert_batch(TABLES::$ORDER_ITEM,$params);
	}
	
	public function removeOrderItems($orderid) {
		$this->db->where('orderid',$orderid);
		return $this->db->delete(TABLES::$ORDER_ITEM);
	}
	
	public function getOrderItems($orderid) {
		$this->db->select('a.*,b.cat_id', FALSE)
				 ->from(TABLES::$ORDER_ITEM.' AS a')
				 ->join(TABLES::$ITEM.' AS b','a.item_id=b.id','inner')
				 ->where("a.orderid",$orderid);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getOrderItemCount($orderid) {
		$this->db->select('sum(a.quantity) as items,b.cat_id', FALSE)
				 ->from(TABLES::$ORDER_ITEM.' AS a')
				 ->join(TABLES::$ITEM.' AS b','a.item_id=b.id','inner')
				 ->where("a.orderid",$orderid);
		$this->db->group_by('b.cat_id');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function generateInvoice($map) {
		$this->db->insert(TABLES::$INVOICE,$map);
		return $this->db->insert_id();
	}
	
	public function updateInvoice($map) {
		$this->db->where('id',$map['id']);
		return $this->db->update(TABLES::$INVOICE,$map);
	}
	
	public function updateBulkInvoice($map) {
		return $this->db->update_batch(TABLES::$INVOICE,$map,'id');
	}
	
	public function updateBulkOrder($map) {
		return $this->db->update_batch(TABLES::$ORDER,$map,'orderid');
	}
	
	public function addPaymentDetail($map) {
		$this->db->select('*')
				 ->from(TABLES::$PAYMENT_DETAIL)
				 ->where("orderid",$map['orderid']);
		$query = $this->db->get();
		$result = $query->result_array();
		if(count($result) <= 0) {
			$this->db->insert(TABLES::$PAYMENT_DETAIL,$map);
		} else {
			$this->db->where('orderid',$map['orderid']);
			$this->db->update(TABLES::$PAYMENT_DETAIL,$map);
		}
	}
	
	public function updatePaymentDetail($map) {
		$this->db->where('transactionid',$map['transactionid']);
		$this->db->update(TABLES::$PAYMENT_DETAIL,$map);
	}
	
	public function getPaymentByTransId($transactionid) {
		$this->db->select('a.*,b.grand_total')
				 ->from(TABLES::$PAYMENT_DETAIL.' AS a')
				 ->join(TABLES::$ORDER.' AS b','a.orderid=b.orderid','left')
				 ->where("a.transactionid",$transactionid);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	private function pickup_sort( $records ) {
		$pickup_date = array();
		$pickup_time = array();
		$batch = array();
		foreach($records as $key=>$row)
		{
			$pickup_date[$key] = $row['pickup_date'];
			$slots = explode("-",$row['pickup_slot']);
			if(!empty($slots[0]))
				$ptime = date('H:i',strtotime(trim($slots[0])));
			else 
				$ptime = '';
			$pickup_time[$key] = $ptime;
			$records[$key]['pickup_time'] = $ptime;
			$batch [] = $records[$key];
		}
		array_multisort($pickup_date,SORT_DESC,$pickup_time,SORT_DESC,$batch);
		unset($pickup_date);
		unset($pickup_time);
		return $batch;
	}
	
	public function getBusinessReport($params) {
		$store_id= $_SESSION['adminsession']['store_id'];
		$role_id= $_SESSION['adminsession']['user_role'];
		$this->db->select('a.*,b.name,b.mobile,b.email,b.is_new_customer,c.name as areaname,d.name as pickup_executive,d.mobile as pickup_executive_mobile,e.name as delivery_executive,e.mobile as delivery_executive_mobile,f.status as payment_status,g.title')
		->from(TABLES::$ORDER.' AS a')
		->join(TABLES::$USER.' AS b','a.userid = b.id','inner')
		->join(TABLES::$AREA.' AS c','a.areaid = c.id','left')
		->join(TABLES::$FIELD_EXECUTIVE.' AS d','a.pickup_exe_id = d.id','left')
		->join(TABLES::$FIELD_EXECUTIVE.' AS e','a.delivery_exe_id = e.id','left')
		->join(TABLES::$PAYMENT_DETAIL.' AS f','a.orderid = f.orderid','left')
		->join(TABLES::$COUPON_CODE.' AS g','a.coupon_code=g.coupon_code','left')
		->join(TABLES::$STORE_AREA.' AS h','a.areaid = h.areaid','left');
		//if($params['status'] != '')
		$this->db->where('a.status !=',5);
		if($role_id != 1) {
			$this->db->where('h.store_id',$store_id);
		}
		if(!empty($params['from_date']) && !empty($params['to_date']))
			$this->db->where("DATE(a.pickup_date) between '".$params['from_date']."' and '".$params['to_date']."'",'',false);
			$this->db->order_by('a.pickup_date','asc');
			$query = $this->db->get();
			$result = $query->result_array();
			return $result;
	}
	
	public function getOrderItemCountByIds($orderids) {
		$this->db->select('sum(a.quantity) as items,sum(a.weight) as weights,sum(a.total_amount) as items_total,a.orderid,b.cat_id', FALSE)
				 ->from(TABLES::$ORDER_ITEM.' AS a')
				 ->join(TABLES::$ITEM.' AS b','a.item_id=b.id','inner');
		if($orderids != "") {
			$this->db->where("a.orderid IN(".$orderids.")",'',false);
		}
		$this->db->group_by('a.orderid');
		$this->db->group_by('b.cat_id');
		$this->db->order_by('a.orderid','ASC');
		$this->db->order_by('b.cat_id','ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function updateBatchDelivery($orderdata) {
		return $this->db->update_batch(TABLES::$ORDER,$orderdata,'orderid');
	}
	
	public function getOrdersByEmail($email) {
		$this->db->select('count(orderid) as orders', FALSE)
				 ->from(TABLES::$ORDER);
		$this->db->where('email',$email);
		$this->db->where('status', 4);
		$query = $this->db->get();
		$result = $query->result_array();
		if(count($result) > 0) {
			return $result[0]['orders'];
		} else {
			return 0;
		}
	}
	
	public function getOrdersByEmailAndCoupon($email,$coupon) {
		$this->db->select('count(orderid) as orders', FALSE)
		->from(TABLES::$ORDER);
		$this->db->where('email',$email);
		$this->db->where('coupon_code',$coupon);
		$this->db->where('discount >',0);
		$this->db->where('status', 4);
		$query = $this->db->get();
		$result = $query->result_array();
		if(count($result) > 0) {
			return $result[0]['orders'];
		} else {
			return 0;
		}
	}
	
	public function getCashCollectionReport($params) {
		$this->db->select('COUNT(a.orderid) as orders,round(SUM(a.grand_total),2) as ordertotal,round(SUM(a.amount_received),2) as total_received,a.delivery_date,b.name', FALSE)
				 ->from(TABLES::$ORDER.' AS a')
				 ->join(TABLES::$FIELD_EXECUTIVE.' AS b','a.delivery_exe_id = b.id','left')
				 ->join(TABLES::$PAYMENT_DETAIL.' AS c','a.orderid = c.orderid','left');
		$this->db->where('a.status', 4);
		$this->db->where("(c.status IS NULL OR c.status != 'Credit')",'',false);
		if(!empty($params['executive_id'])) {
			$this->db->where('b.id',$params['executive_id']);
		}
		if(!empty($params['from_date']) && !empty($params['to_date'])) {
			$this->db->where("a.tml_delivery_date BETWEEN '".$params['from_date']."' AND '".$params['to_date']."'",'',false);
		}
		$this->db->group_by('b.id');
		$this->db->order_by('b.name','ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getOnlineCollectionReport($params) {
		$this->db->select('COUNT(a.orderid) as orders,round(SUM(a.grand_total),2) as ordertotal,round(SUM(a.amount_received),2) as total_received,a.delivery_date,"Online Paid" as name', FALSE)
			 ->from(TABLES::$ORDER.' AS a')
			 ->join(TABLES::$FIELD_EXECUTIVE.' AS b','a.delivery_exe_id = b.id','inner')
			 ->join(TABLES::$PAYMENT_DETAIL.' AS c','a.orderid = c.orderid','inner');
		$this->db->where('a.status', 4);
		$this->db->where('c.status', 'Credit');
		if(!empty($params['from_date']) && !empty($params['to_date'])) {
			$this->db->where("a.tml_delivery_date BETWEEN '".$params['from_date']."' AND '".$params['to_date']."'",'',false);
		}
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getTodaysPendingPayments() {
		$current_date = date('Y-m-d');
		$this->db->select('a.orderid,a.grand_total,b.name, b.mobile, b.email,c.invoice_url,c.invoice_date,c.id as invoice_id,d.status as payment_status')
				 ->from(TABLES::$ORDER.' AS a')
				 ->join(TABLES::$USER.' AS b','a.userid = b.id','inner')
				 ->join(TABLES::$INVOICE.' AS c','a.orderid = c.orderid','left')
				 ->join(TABLES::$PAYMENT_DETAIL.' AS d','a.orderid = d.orderid','left');
		$this->db->where('a.tml_delivery_date',$current_date);
		$this->db->where('a.status',3);
		$this->db->order_by('a.orderid','desc');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function cancellation_request($params) {
		$response =array();
		$data['status'] = 4;
		$this->db->select('bookingid, status');
		$this->db->from(TABLES::$BOOKING);
		$this->db->where($params);
		$this->db->where($params);
		$query = $this->db->get();
		$result = $query->row_array();
		if(!empty($result)){
			$this->db->where($params);
			$this->db->update(TABLES::$BOOKING, $data);
			$affect = $this->db->affected_rows();
			if($affect){
				$response['status'] = 1;
				$response['message'] = "Request initiated.";
			}
			else {
				$response['status'] = 0;
				$response['message'] = "You are not authorized to cancel booking.";
			}
		}
		
		return $response;
	}
	
	
	public function getTotalOrders() {
	
		/*	$this->db->select ('*');
		 $this->db->from ( TABLES::$ORDER );
		 $this->db->where ( 'status', 1 );
		 $query = $this->db->get ();
		 $result = $query->result_array ();
		 return $result; */
		 
		$this->db->select ('*');
		$this->db->from ( TABLES::$ORDER );
		//$this->db->where ( 'status', 1 );
		$this->db->group_by('userid');
		$this->db->order_by('delivery_date','desc');
		$query = $this->db->get ();
		//echo $this->db->last_query();
		$result = $query->result_array ();
		return $result;
		 
	}
	
	
	
	
}
