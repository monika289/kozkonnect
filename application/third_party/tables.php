<?php
class TABLES {
	
	public static $ADMIN_USER = 'tbl_admin_user';
	public static $ADMIN_USER_ROLE = 'tbl_admin_user_role';
	public static $USERS = 'tbl_users';
	public static $USER_TYPE='tbl_user_type';
	public static $USER_DOCS='tbl_user_docs';
	public static $CAUSE_MAIN_CATEGORY = 'tbl_category';
	public static $CAUSE_SUB_CATEGORY = 'tbl_category';
	public static $SDG = 'tbl_sdg';
	public static $CAUSE='tbl_causes';
	public static $TICKET = 'tbl_ticket';
	public static $TICKET_COMMENT = 'tbl_ticket_comment';
	public static $TICKET_CATEGORY = 'tbl_ticket_category';
	public static $TICKET_SUBCATEGORY = 'tbl_ticket_subcategory';
	public static $TICKET_STATUS = 'tbl_ticket_status';
	
	
}
