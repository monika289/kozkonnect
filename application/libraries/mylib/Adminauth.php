<?php
class Adminauth {
	
	public function __construct(){
		$this->CI =& get_instance();
	}
	
	public function login($umap) {
		$this->CI->load->model ( 'adminusers/user_model', 'adminuser' );
		$user_details = $this->CI->adminuser->getUserDetailByEmail( $umap['email'] );
		
		//print_r($user_details); exit();
		
		$map = array();
		if (count($user_details) > 0) {
			if (MD5($umap['password']) === $user_details[0]['password']) {
				if ($user_details[0]['status'] == 1) {
					$user = array();
					$user['first_name'] = $user_details[0]['first_name'];
					$user['last_name'] = $user_details[0]['last_name'];
					$user['email'] = $user_details[0]['email'];
					$user['mobile'] = $user_details[0]['mobile'];
					$user['id'] = $user_details[0]['id'];
					$user['user_role'] = $user_details[0]['user_role']; 
					$map ['status'] = 1;
					$map ['msg'] = "Logged in successfully.";
					$map ['result'] = $user;
					return $map;
				} else {
					$map ['status'] = 0;
					$map ['msg'] = "Login to this site have been blocked by Admin.";
					$errors = array ();
					array_push ( $errors, "Unautharised access blocked." );
					$map ['errormsg'] [] = $errors;
					return $map;
				}
			} else {
				$map ['status'] = 0;
				$map ['msg'] = "Email or password is wrong.";
				$errors = array ();
				array_push ( $errors, "Email or password is wrong." );
				$map ['errormsg'] [] = $errors;
				return $map;
			}
		} else {
			$map ['status'] = 0;
			$map ['msg'] = "Email or password is wrong.";
			$errors = array ();
			array_push ( $errors, "Email or password is wrong." );
			$map ['errormsg'] [] = $errors;
			return $map;
		}
		
	}
	
	
	
}
