<?php
class Category {
	public function __construct() {
		$this->CI = & get_instance ();
	}
	
	
	public function addCategory($data) {
		$this->CI->load->model('general/Category_Model', 'category_model' );
		$response = $this->CI->category_model->addCategory ($data);
		return $response;
	}
	public function getAllcategories() {
		$this->CI->load->model('general/Category_Model', 'category_model' );
		$response = $this->CI->category_model->getAllCategories();
		return $response;
	}
	
	public function getCategoryById($id)
	{
		$this->CI->load->model ('general/Category_Model', 'category_model'  );
		$result =$this->CI->category_model->getCategoryById($id);
		return $result;
	}
	public function updateCategory($data) {
		$this->CI->load->model('general/Category_Model', 'category_model' );
		$response = $this->CI->category_model->updateCategory($data);
		return $response;
	}
	
	public function getActiveCategoryList(){
		$this->CI->load->model('general/Category_Model', 'category_model' );
		$response = $this->CI->category_model->getActiveCategoryList();
		return $response;
	}
	public function getCategoryList(){
		$this->CI->load->model('general/Category_Model', 'category_model' );
		$response = $this->CI->category_model->getCategoryList();
		return $response;
	}
	public function turnoncategory($id) {
		$this->CI->load->model ( 'general/Category_Model', 'category_model' );
		return $this->CI->category_model->turnoncat ( $id );
	}
	
	public function turnoffcategory($id) {
		$this->CI->load->model ( 'general/Category_Model', 'category_model' );
		return  $this->CI->category_model->turnoffcat ( $id );
	}
	
	public function turnonsubcat($id) {
		$this->CI->load->model ( 'general/Category_Model', 'category_model' );
		return $this->CI->category_model->turnonsubcat ( $id );
	}
	
	public function turnoffsubcat($id) {
		$this->CI->load->model ( 'general/Category_Model', 'category_model' );
		return  $this->CI->category_model->turnoffsubcat ( $id );
	}
}