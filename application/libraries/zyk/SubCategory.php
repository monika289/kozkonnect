<?php
class SubCategory {
	public function __construct() {
		$this->CI = & get_instance ();
	}
	
	public function addSubCategory($data) {
		$this->CI->load->model('general/Category_Model', 'category_model' );
		$response = $this->CI->category_model->addSubCategory ($data);
		return $response;
	}
	public function getAllSubcategories() {
		$this->CI->load->model('general/Category_Model', 'category_model' );
		$response = $this->CI->category_model->getAllSubcategories();
		return $response;
	}
	
	public function getSubcategoryById($id)
	{
		$this->CI->load->model ('general/Category_Model', 'category_model'  );
		$result =$this->CI->category_model->getSubcategoryById($id);
		return $result;
	}
	public function updateSubcategory($data) {
		$this->CI->load->model('general/Category_Model', 'category_model' );
		$response = $this->CI->category_model->updateSubcategory($data);
		return $response;
	}
	
	public function getActiveSubCategoryList(){
		$this->CI->load->model('general/Category_Model', 'category_model' );
		$response = $this->CI->category_model->getActiveSubCategoryList();
		return $response;
	}
}
