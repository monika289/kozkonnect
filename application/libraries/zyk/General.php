<?php
class General {
	public function __construct() {
		$this->CI = & get_instance ();
	}
	public function getCities() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$cities = $this->CI->settings->getAllCities ();
		return $cities;
	}
	public function getRestaurantsByVendor($vendorid) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$areas = $this->CI->settings->getRestaurantsByVendor ( $vendorid );
		return $areas;
	}
	public function getCityById($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$cities = $this->CI->settings->getCityById ( $id );
		return $cities;
	}
	public function turnOnCity($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$this->CI->settings->turnOnCity ( $id );
	}
	public function turnOffCity($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$this->CI->settings->turnOffCity ( $id );
	}
	public function addCity($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->addCity ( $params );
		return $response;
	}
	public function updateCity($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->updateCity ( $params );
		return $response;
	}
	
	public function getAreasByPincode($pincode) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$data = $this->CI->settings->checkLocalitiesbyPincode ($pincode);
		if (count ( $data ) > 0) {
			$data [0] ['status'] = 1;
			$data[0]['msg'] = 'Location is found';
		} else {
			$data [0] ['status'] = 0;
			$data[0]['msg'] = 'Location is not Found';
		}
		return $data;
		//return $areas;
	}
	
	public function getAreas() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$areas = $this->CI->settings->getAllLocalities ();
		return $areas;
	}
	public function getAreasByCityId($cityid) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$areas = $this->CI->settings->getAreasByCityId ( $cityid );
		return $areas;
	}
	public function getAreasById($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$areas = $this->CI->settings->getLocalityById ( $id );
		return $areas;
	}
	public function addArea($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->addLocality ( $params );
		return $response;
	}
	public function updateArea($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->updateLocality ( $params );
		return $response;
	}
	public function turnOnArea($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$this->CI->settings->turnOnLocality ( $id );
	}
	public function turnOffArea($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$this->CI->settings->turnOffLocality ( $id );
	}
	/*public function getCuisines() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$cuisines = $this->CI->settings->getAllCuisines ();
		return $cuisines;
	}*/
	public function getCuisineById($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$cuisines = $this->CI->settings->getCuisineById ( $id );
		return $cuisines;
	}
	public function addCuisine($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->addCuisine ( $params );
		return $response;
	}
	public function updateCuisine($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->updateCuisine ( $params );
		return $response;
	}
	public function deleteCuisine($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$this->CI->settings->deleteCuisine ( $id );
	}
	public function getRestByAreaId($cityid) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$areas = $this->CI->settings->getRestByAreaId ( $cityid );
		return $areas;
	}
	public function getAreaidandCityidByRestaurant($a) {
		$this->CI->load->model ( 'coupan/Coupan_model', 'coupon' );
		$areas = $this->CI->coupon->getAreaidandCityidByRestaurant ( $a );
		return $areas;
	}
	public function addReason($params) {
		$this->CI->load->model ('general2/Settings_model', 'settings');
		$response = $this->CI->settings->addReason($params);
		return $response;
	}
	
	public function updateReason($params) {
		$this->CI->load->model ('general2/Settings_model', 'settings');
		$response = $this->CI->settings->updateReason($params);
		return $response;
	}
	
	public function deleteReason($id) {
		$this->CI->load->model ('general2/Settings_model', 'settings');
		$this->CI->settings->deleteReason($id);
	}
	
	public function getActiveReasons() {
		$this->CI->load->model ('general2/Settings_model', 'settings');
		$response = $this->CI->settings->getActiveReasons();
		return $response;
	}
	
	public function getReasonById($id) {
		$this->CI->load->model ('general2/Settings_model', 'settings');
		$response = $this->CI->settings->getReasonById($id);
		return $response;
	}
	
	public function getCuisinesByArea($arealist) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$cuisines = $this->CI->settings->getCuisinesByArea($arealist);
		return $cuisines;
	}
		
	public function getZoneByCityId($cityid)
	{
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$zone = $this->CI->settings->getZoneByCityId($cityid);
		return $zone;
	}
	public function getAreaByZoneId ($zoneid)
	{
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$area = $this->CI->settings->getAreaByZoneId($zoneid);
		return $area;
	}
	public function getRestaurantByZoneId($zoneid)
	{
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$restaurant = $this->CI->settings->getRestaurantByZoneId($zoneid);
		return $restaurant;
	}
	
	public function saveContactUs($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->saveContactUs($params);
		$map = array();
		if($result) {
			$map['status'] = 1;
			$map['message'] = 'Query added successfully.';
		} else {
			$map['status'] = 0;
			$map['message'] = 'Failed to add query.';
		}
		return $map;
	}
	
	public function addFeedback($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->addFeedback($params);
		$map = array();
		if($result) {
			$map['status'] = 1;
			$map['message'] = 'Feedback added successfully.';
		} else {
			$map['status'] = 0;
			$map['message'] = 'Failed to add feedback.';
		}
		return $map;
	}
	
	public function getAreasByZoneId($zone_id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$areas = $this->CI->settings->getAreasByZoneId ( $zone_id );
		return $areas;
	}
	
	public function saveRating($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->saveRating ( $params );
		return $result;
	}
	
	public function saveReview($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->saveReview ( $params );
		return $result;
	}
	
	public function getRestaurantReviews($restid) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->getRestaurantsReviews ( $restid );
		return $result;
	}
	
	//................ Added by Tushar Ticket Model..........................
	
	/* public function addTicket($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->addTicket ($params);
		return $response;
	}
	
	public function updateTicket($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->updateTicket ($params);
		return $response;
	}
	
	public function getAllTickets() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getAllTickets ();
		return $response;
	}
	
	public function getAllActiveTickets() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getAllActiveTickets ();
		return $response;
	}
	
	public function getTicketById($ticketid) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getTicketById ($ticketid);
		return $response;
	}  */
	
	public function addTicketCategory($cat) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->addTicketCategory ( $cat );
		return $result;
	}
	public function getActiveTicketCategories() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getActiveTicketCategories ();
		return $response;
	}
	public function getTicketCategories() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getTicketCategories ();
		return $response;
	}
	public function updateTicketCategory($cat) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->updateTicketCategory ( $cat );
		return $result;
	}
	public function getTicketCategoryById($cat_id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getTicketCategoryById ( $cat_id );
		return $response;
	}
	public function addTicketSubCategory($cat) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->addTicketSubCategory ( $cat );
		return $result;
	}
	public function getSubActiveTicketCategories() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getSubActiveTicketCategories ();
		return $response;
	}
	public function getSubAllTicketCategories() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getSubAllTicketCategories ();
		return $response;
	}
	public function updateTicketSubCategory($cat) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->updateTicketSubCategory ( $cat );
		return $result;
	}
	public function getTicketSubCategoryById($cat_id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getTicketSubCategoryById ( $cat_id );
		return $response;
	}
	public function addTicketStatus($cat) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->addTicketStatus ( $cat );
		return $result;
	}
	public function getActiveTicketStatus() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getActiveTicketStatus ();
		return $response;
	}
	public function getAllTicketStatus() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getAllTicketStatus ();
		return $response;
	}
	public function getTicketStatusById($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getTicketStatusById ($id);
		return $response;
	}
	public function updateTicketStatus($cat) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->updateTicketStatus ( $cat );
		return $result;
	}
	
	public function sendTicketSMS($details) {
		$sms_msg = "Hi ".$details ['name'].", We would like to acknowledge that we have received your complaint and we will get back to you shortly. Regards, Wamaco";
		$this->CI->load->library ( 'pksms' );
		$map = array ();
		$map ['mobile'] = $details ['mobile'];
		$map ['message'] = $sms_msg;
		$this->CI->pksms->sendSms ( $map );
	}
	
	public function addTicket($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->addTicket ($params);
		return $response;
	}
	
	public function addComment($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->addComment($params);
		return $response;
	}
	
	public function updateTicket($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->updateTicket($params);
		return $response;
	}
	
	public function getAllTickets() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getTicketsList();
		return $response;
	}
	
	public function getAllActiveTickets() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getAllActiveTickets();
		return $response;
	}
	
	public function getTicketById($ticketid) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getTicketById($ticketid);
		return $response;
	}
	
	public function getUserComment($ticketid) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getUserComment($ticketid);
		return $response;
	}
	
	public function getUserCommentByTicketId($ticketid) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getUserCommentByTicketId($ticketid);
		return $response;
	}
	
	public function leadHistory($ticketid) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response ['priorities'] = $this->CI->settings->leadPriorityHistory( $ticketid );
		$response ['leadStatus'] = $this->CI->settings->leadStatusHistory( $ticketid );
		$response ['executives'] = $this->CI->settings->leadExecutiveHistory( $ticketid );
		return $response;
	}
	public function updateLead($data) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->updateLead ( $data );
		return $response;
	}
	
	public function userExist($data) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$exist = $this->CI->settings->userExist ( $data );
		if (count ( $exist ) > 0) {
	
			if($exist[0]['status']==0)
			{
				$exist['msg']="You are already registered with us.";
				$exist['is_verify']=1;
				$exist['exist']=1;
				$exist['id'] = $exist[0]['id'];
				$exist['email'] = $exist[0]['email'];
				return $exist;
			}
			else
			{
				$exist['msg']="Email already registered.";
				$exist['is_verify']=0;
				$exist['exist']=1;
				$exist['id'] = $exist[0]['id'];
				$exist['email'] = $exist[0]['email'];
				return $exist;
			}
		} else {
			$exist['exist'] = 0;
			return $exist;
		}
	}
	
	public function getAllCompony() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$cities = $this->CI->settings->getAllCompony ();
		return $cities;
	}
	
	public function getAllCompony1() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$cities = $this->CI->settings->getAllCompony1 ();
		return $cities;
	}
	
	public function getAllStore() {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$cities = $this->CI->settings->getAllStore ();
		return $cities;
	}
	
	public function getCompanyById($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->getCompanyById ( $id );
		return $result;
	}
	
	public function getStoreById($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = array();
		$stores = array();
		
		$store = $this->CI->settings->getStoreById ( $id );
		$services =  $this->CI->settings->getServiceBystoreId ( $id );
		$weekly  = $this->CI->settings->getWeeksByStoreId ( $id );
		foreach ($store as $key=>$row){
			$stores[$key] = $row;
			$pro_week = array();
			foreach ($weekly as $week){
				if($week['store_id'] == $row['id']){
					$pro_week[]=$week['week_id'];
				}
			}
			$pro_service = array();
			foreach ($services as $service){
				if($service['store_id'] == $row['id']){
					$pro_service[] = $service['service_id'];
				}
			}
			$stores[$key]['weekly'] = $pro_week;
			$stores[$key]['service'] = $pro_service;
		}
		$result = $stores;
		return $result;
	}
	
	public function getBillingConfig($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->getBillingConfig ( $id );
		return $result;
	}
	
	public function getBillingField($id,$field) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->getBillingField ( $id,$field );
		return $result;
	}
	
	public function addCompany($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->addCompany ( $params );
		return $response;
	}
	
	public function addStore($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$config = array();
		
		$response = $this->CI->settings->addStore($params['params']);
		if($response['status'] == 1) {
		 $config['storeid'] = $response['id'];
		 $config['basic'] = 1;
		
		 $config['billing'] = 1;
		 	
		 $params['billing']['storeid'] = $response['id'];
		 	
		 for ($i = 0; $i < count($params['billingfields']); $i++) {
		 $params['billingfields'][$i]['storeid'] = $response['id'];
		 }
		 	
		 $this->CI->settings->addBilling($params['billing']);
		 $this->CI->settings->addBillingFields($params['billingfields']);
		 	
		}
		return $response;
	}
	
	public function addBilling($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->addBilling ( $params );
		return $response;
	}
	
	public function addBillingFields($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->addBillingFields ( $params );
		return $response;
	}
	
	public function updateCompany($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->updateCompany ( $params );
		return $response;
	}
	
	public function updateStore($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->updateStore ( $params );
		return $response;
	}
	
	public function updateBilling($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->updateBilling ( $params );
		return $response;
	}
	
	public function updateBillingfield($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->updateBillingfield ( $params );
		return $response;
	}
	
	public function addUser($params) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->addUser ( $params );
		//$this->sendFranchiseEmail ( $params );
		//$this->sendSignUpSMS($params);
		return $response;
	}
	
	public function getTicketSubCatId($id) {
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getTicketSubCatId ( $id );
		return $response;
	}
	
	public function storeon($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->storeon ( $id);
		return $response;
	}
	public function storeoff($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->storeoff ( $id);
		return $response;
	}
	
	public function getStoreListByAdminId($adminid){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getStoreListByAdminId( $adminid);
		return $response;
	}
	
	public function getservies($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getservies( $id);
		return $response;
	}
	
	public function getBillAmt($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getBillAmt( $id);
		return $response;
	}
	
	public function getcategoriesByAdminId($adminid){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getcategoriesByAdminId($adminid);
		return $response;
	}
	
	public function getSubCategoriesByCatId($ids,$storeid){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getSubCategoriesByCatId($ids,$storeid);
		return $response;
	}
	
	public function getServicesBySubCatId($ids,$storeid){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getServicesBySubCatId($ids,$storeid);
		return $response;
	}
	
	public function getServiceDetails($ids,$storeid){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getServiceDetails($ids,$storeid);
		return $response;
	}
	
	public function checkBookedDate($checkdate,$storeid){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->checkBookedDate($checkdate,$storeid);
		return $response;
	}
	
	public function getExpertByService($checkdate,$storeid){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$response = $this->CI->settings->getExpertByService($checkdate,$storeid);
		return $response;
	}
	public function turnoncategory($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->turnoncategory ( $id );
		return $result;
	}
	public function turnoffcategory($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->turnoffcategory ( $id );
		return $result;
	}
	public function turnonsubcat($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->turnonsubcat ( $id );
		return $result;
	}
	public function turnoffsubcat($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->turnoffsubcat ( $id );
		return $result;
	}
	
	public function turnonstatus($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->turnonstatus ( $id );
		return $result;
	}
	public function turnoffstatus($id){
		$this->CI->load->model ( 'general2/Settings_model', 'settings' );
		$result = $this->CI->settings->turnoffstatus ( $id );
		return $result;
	}
	
	public function addOrdertoShipRocket($orders){
		
		if($orders['is_online_paid'] == 0){ 
			$payment_mode = "Cash On Delivery";
		} else { 
			$payment_mode = "Online payment"; 
		}
		$url = 'http://api.kartrocket.co/index.php?route=feed/web_api/addorder&key=prabhat';
		
		$data['import_order_id']= $orders['orderid'];
		$data['firstname']= $orders['name'];
		$data['lastname']= $orders['lname'];
		$data['email']= $orders['email'];
		$data['company']='';
		$data['address_1']= $orders['customeraddress'][0]['locality'];
		$data['address_2']= $orders['customeraddress'][0]['address'];
		$data['city']= $orders['customeraddress'][0]['city'];
		$data['postcode']= $orders['customeraddress'][0]['pincode'];
		$data['state']= $orders['customeraddress'][0]['state'];
		$data['country_code']='IN';
		$data['telephone']='';
		$data['mobile']= $orders['mobile'];
		$data['fax']='';
		$data['payment_method']= $payment_mode;
		$data['payment_code']='Payment Code';
		$data['shipping_method']='Shipment Method';
		$data['shipping_code']='Shipment Code';
		
		/*$data['products'][0]['name']='Apple iPhone 4Sx';
		$data['products'][0]['model']='MB0010';
		$data['products'][0]['sku']='MB0010';
		$data['products'][0]['quantity']='1';
		$data['products'][0]['price']='145';
		$data['products'][0]['total']='145';
		$data['products'][0]['tax']='6.9047619047619';
		
		$data['products'][1]['name']='Apple iPhone 4C';
		$data['products'][1]['model']='MB0011';
		$data['products'][1]['sku']='MB0011';
		$data['products'][1]['quantity']='1';
		$data['products'][1]['price']='145';
		$data['products'][1]['total']='145';
		$data['products'][1]['tax']='6.9047619047619'; */
		
		foreach ($orders['items'] as $item){
			
			$data['products']['name']= $item['name'];
			$data['products']['model']='MB0010';
			$data['products']['sku']='MB0010';
			$data['products']['quantity']= $item['quantity'];
			$data['products']['price']= $item['price'];
			$data['products']['total']= $item['total_amount'];
			$data['products']['tax']= $item['tax_amount'];
			
		}
		
		$data['totals']['handling']='0';
		$data['totals']['low_order_fee']='0';
		$data['totals']['sub_total']= $orders['sub_total'];
		$data['totals']['tax']= $orders['net_total'];
		$data['totals']['total']= $orders['grand_total'];
		$data['weight']='1';
		$data['comment']='';
		$data['total']= $orders['grand_total'];
		
		print_r($data); 
		exit();
		$params['data'] = json_encode($data);
		$request = http_build_query($params);
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($response, true);
		print_r($response);
		
	}
	
	public function sendOTPEmail($user) {
		$email = 'tushar@brandzgarage.com';
		$this->CI->load->library ( 'Pkemail' );
		$this->CI->pkemail->load_system_config ();
		$this->CI->pkemail->headline = 'Wamaco';
		$this->CI->pkemail->subject = 'New Mail from contactus';
		$this->CI->pkemail->mctag = 'otp-msg';
		$this->CI->pkemail->attachment = 0;
		$this->CI->pkemail->to = $email;
		$this->CI->template->set ( 'data', $user );
		$this->CI->template->set ( 'page', 'otp-message' );
		$this->CI->template->set_layout ( false );
		$text_body = $this->CI->template->build ( 'frontend/emails/contact-email', '', true );
		$this->CI->pkemail->send_email ( $text_body );
	}
	
	
}
