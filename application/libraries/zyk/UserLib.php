<?php
class UserLib {
	
	public function __construct(){
		$this->CI =& get_instance();
	}
	
	/**
	 * Function for Retrive particular User Details
	 * @param unknown $id
	 */
	public function getUserById($id)
	{
		$this->CI->load->model ('users/user_model','user');
		return $this->CI->user->getUserById($id);
	}
	public function getProfile_pic($id)
	{
		$this->CI->load->model ('users/user_model','user');
		return $this->CI->user->getProfile_pic($id);
	}
	
	
	public function signupUser($map) {
		$this->CI->load->model ('users/user_model','user');
		$map['created_on'] = date('Y-m-d');
		return $this->CI->user->addUser($map);
	}
	
	public function isMobileVerified($email,$mobile) {
		$this->CI->load->model ('users/user_model','user');
		$result = $this->CI->user->getCustomerByMobile($email,$mobile);
		if (count($result) > 0) {
			return 1;
		} else {
			return 0;
		}
	}
	
	/**
	 * 
	 * @param unknown $otpverification
	 */
	public function updateUserByOtp($otpverification)
	{
		$this->CI->load->model ('users/user_model','user');
		return $this->CI->user->updateUserByOtp($otpverification);
	}
	
	public function isOTPValid($userid,$otp) {
		$this->CI->load->model ('users/user_model','user');
		$result = $this->CI->user->getUserOTPById($userid);
		if(count($result) > 0) {
			if($result[0]['otp'] == $otp) {
				$this->sendRegistrationNotification($result[0]);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public function addAddress($data) {
		$this->CI->load->model ( 'users/UserLogin_model', 'userlogin' );
		$this->CI->userlogin->addAddress ( $data );
	}
	
	/**
	 *User GCM registration
	 *
	 */
	public  function GCMRegister($userdata)
	{
		$this->CI->load->model ('users/user_model','user');
		return $this->CI->user->GCMRegister($userdata);
	}
	/**
	 *fuction for get shortcode
	 *
	 */
	public  function getShortCode()
	{
		$this->CI->load->model ('users/user_model','user');
		return $this->CI->user->getShortCode();
	}
	
	public function sendOTPSMS($details) {
		$sms_msg = 'Please Use This OTP ' . $details ['otp'] . ' (system generated one time password) Your Otp is valid only for 15 mins.';
		$this->CI->load->library ( 'Fbsms' );
		$map = array ();
		$map ['mobile'] = $details ['mobile'];
		$map ['message'] = $sms_msg;
		$this->CI->fbsms->sendSms ( $map );
	}
	
	public function getAddressByAddressId($id) {
		$this->CI->load->model ( 'users/User_model', 'userlogin' );
		$result = $this->CI->userlogin->getUserAddressById ( $id );
		return $result;
	}
	
	public function getProfile($id) {
		$this->CI->load->model ( 'users/User_model', 'userlogin' );
		$result = $this->CI->userlogin->getProfile ( $id );
		return $result;
	}
	
	public function getProfileByEmail($email) {
		$this->CI->load->model ( 'users/User_model', 'userlogin' );
		$result = $this->CI->userlogin->getProfileByEmail ( $email );
		return $result;
	}
	
	public function getUserByEmail($email) {
		$this->CI->load->model ( 'users/User_model', 'userlogin' );
		$result = $this->CI->userlogin->getUserByEmail ( $email );
		return $result;
	}
	
	public function getProfileByMobile($mobile) {
		$this->CI->load->model ( 'users/User_model', 'userlogin' );
		$result = $this->CI->userlogin->getProfileByMobile ( $mobile );
		return $result;
	}
	
	public function getProfileByName($name) {
		$this->CI->load->model ( 'users/User_model', 'userlogin' );
		$result = $this->CI->userlogin->getProfileByName ( $name );
		return $result;
	}
	
	public function getUserOrder($userid) {
		$this->CI->load->model ( 'users/User_model', 'userlogin' );
		return $this->CI->userlogin->getUserOrder ( $userid );
	}
	
	public function addCustomerAddress($users,$newaddress){
		$this->CI->load->model ( 'users/User_model', 'user' );
		return $this->CI->user->addCustomerAddress($users,$newaddress);
	}
	public function getCustomerAddressById($id){
		$resp =array();
		$this->CI->load->model ( 'users/User_model', 'user' );
		$response =  $this->CI->user->getCustomerAddressById($id);
		$this->CI->load->model ( 'general/Pincode_Model', 'pincode' );
		foreach ($response as $key => $row){
			$resp[$key] = $row;
			$resp[$key]['delivery_charge'] = $this->CI->pincode->getDeliveryCharges($response[$key]['pincode']);
		}
		
	//	print_r($response);
		//print_r($resp);
		return $resp;
	}
	public function updateuser($data){
		$this->CI->load->model ( 'users/User_model', 'user' );
		return $this->CI->user->updateuser($data);
	}
	public function getUserAddress($id){
		$this->CI->load->model ( 'users/User_model', 'user' );
		return $this->CI->user->getUserAddressById($id);
	}
	public function updateUserAddress($data){
		$this->CI->load->model ( 'users/User_model', 'user' );
		return $this->CI->user->updateUserAddress($data);
	}
	public function getOrdersByuserId($userid){
		$this->CI->load->model ( 'users/User_model', 'user' );
		$order= $this->CI->user->getOrdersByuserId($userid);
		//echo json_encode($orderitems);
		//exit;
		$this->CI->load->model ('order/Order_FrontModel','order');
		$this->CI->load->model ('order/Order_Model', 'ordermodel');
		$cart = array();
		$response = array();
		foreach($order as $key => $row){
			$cart [$key] = $row;
			$id = $row['orderid'];
			$orderaddress = array();
			$orderitems = $this->CI->ordermodel->getOrderItemsByOrderid($id);
			$orderaddr = $this->CI->ordermodel->getOrderCustomersByOrderId($id);
			foreach ($orderaddr as $addr){
				if($addr['orderid'] == $row['orderid']){
					$orderaddress[] = $addr;
				}
			}
			
			$orderitem = array();
			foreach ($orderitems as $orderkey => $orderow){
				$orderitem[$orderkey] = $orderow;
				if($orderow['orderid'] == $row['orderid']){
					
					$this->CI->load->model ( 'product/Product_model', 'productmodel' );
					$productid=$orderow['itemid'];
					$products = $this->CI->productmodel->getProductByID($productid);
					$images = $this->CI->productmodel->getProductImages($productid);
					$itemvariant = $this->CI->order->getOrderedVariantByItemid($productid,$id);
					$product_array = array();
					foreach ($products as $product){
						if($product['product_id'] == $orderow['itemid']){
							$product_array[]=$product;
						}
					}
					$orderitem[$orderkey]['product'] = $product_array;
					
					$product_image = array();
					foreach ($images as $img){
						if($img['product_id'] == $orderow['itemid']){
							$product_image[] = $img;
						}
					}
					$orderitem[$orderkey]['productimage'] = $product_image;
					
					$product_variant = array();
					foreach ($itemvariant as $variant){
						if($variant['itemid'] == $orderow['itemid']){
							$product_variant[] = $variant;
						}
					}
					$orderitem[$orderkey]['productvariant'] = $product_variant;
				}
			}
			$cart[$key]['address'] = $orderaddress;
			$cart[$key]['items'] = $orderitem;
		}
		$response['cartitems'] = $cart;
		return $response;
	}
	
	public function getOrderhistoryByuserId($userid){
		$this->CI->load->model ( 'users/User_model', 'user' );
		$order= $this->CI->user->getOrderhistoryByuserId($userid);
		//echo json_encode($orderitems);
		//exit;
		$this->CI->load->model ('order/Order_Model', 'ordermodel');
		$cart = array();
		$response = array();
		foreach($order as $key => $row){
			$cart [$key] = $row;
			$id = $row['orderid'];
			$orderaddress = array();
			$orderitems = $this->CI->ordermodel->getOrderItemsByOrderid($id);
			$orderaddr = $this->CI->ordermodel->getOrderCustomersByOrderId($id);
			foreach ($orderaddr as $addr){
				if($addr['orderid'] == $row['orderid']){
					$orderaddress[] = $addr;
				}
			}
				
			$orderitem = array();
			foreach ($orderitems as $orderkey => $orderow){
				$orderitem[$orderkey] = $orderow;
				if($orderow['orderid'] == $row['orderid']){
						
					$this->CI->load->model ( 'product/Product_model', 'productmodel' );
					$productid=$orderow['itemid'];
					$products = $this->CI->productmodel->getProductByID($productid);
					$images = $this->CI->productmodel->getProductImages($productid);
					$product_array = array();
					foreach ($products as $product){
						if($product['product_id'] == $orderow['itemid']){
							$product_array[]=$product;
						}
					}
					$orderitem[$orderkey]['product'] = $product_array;
						
					$product_image = array();
					foreach ($images as $img){
						if($img['product_id'] == $orderow['itemid']){
							$product_image[] = $img;
						}
					}
					$orderitem[$orderkey]['productimage'] = $product_image;
	
					/*$varmap['itemid'] = $orderow['itemid'];
						$ordervar = $this->CI->productmodel->getVariantByProductId($varmap['itemid']);
					$pro_var = array();
					foreach($ordervar as $var){
					if($var['product_id'] == $orderow['itemid']) {
					$pro_var[] = $var;
					}
					}
					$orderitem[$orderkey]['variants'] = $pro_var;*/
						
				}
			}
			$cart[$key]['address'] = $orderaddress;
			$cart[$key]['items'] = $orderitem;
		}
		$response['cartitems'] = $cart;
		return $response;
	}
	
	public function getAllUserIds(){
		$this->CI->load->model ( 'users/User_model', 'userlogin' );
		$result = $this->CI->userlogin->getAllUserIds();
		return $result;
	}
	public function getWalletBalance($userid){
		$this->CI->load->model ( 'users/Wallet_model', 'wallet' );
		return $this->CI->wallet->getWalletBalance ( $userid );
	}
	public function getWalletTransactions($userid){
		$this->CI->load->model ( 'users/Wallet_model', 'wallet' );
		return $this->CI->wallet->getWalletTransactions ( $userid );
	}
	//nitin code
	public function getdocsByID($id) {
		
		$this->CI->load->model('user_model');
		$images = $this->CI->user_model->getuserImages($id);
		return $images;
		
	}
	public function deleteUserdoc($id,$userid) {
		$this->CI->load->model('user_model');
		$docarray=$this->CI->user_model->getuserdocsbyid($id);
		$upath = '.' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'userdocs' . DIRECTORY_SEPARATOR.	$userid. DIRECTORY_SEPARATOR.$docarray[0]['doc_url'];
		 
            unlink($upath);
		
		$post = $this->CI->user_model->deleteUserdoc( $id );
		return $post;
	}
	



}



