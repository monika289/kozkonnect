<?php
class EmployeeLib {
	
	public function __construct() {
		$this->CI = & get_instance ();
	}

	public function addRole($cat) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$result = $this->CI->employeemodel->addRole ($cat);
		return $result;
	}
	
	public function getActiveRole() {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getActiveRole();
		return $response;
	}
	
	public function getActiveRole12() {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getActiveRole12();
		return $response;
	}
	
	public function updateRole($params,$role_params) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$result = $this->CI->employeemodel->updateRole ( $params,$role_params);
		return $result;
	}
	
	public function getRoleById($id) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$role = $this->CI->employeemodel->getRoleById ($id);
		$roleaccess = $this->CI->employeemodel->getRoleAccessById ($id);
		$response = array();
		$roleacc = array ();
		foreach ( $role as $key => $row ) {
			$roleacc [$key] = $row;
			$pro_access = array();
			foreach($roleaccess as $access){
				if($access['role_id'] == $row['id']) {
					$pro_access[] = $access;
				}
			}
			$roleacc [$key]['access'] = $pro_access;
		}
		$response = $roleacc;
		return $response;
	} 
	
	public function addEmp($cat) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$result = $this->CI->employeemodel->addEmp ($cat);
		return $result;
	}
	
	public function addupload($data)
	{
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$creative = $this->CI->employeemodel->addupload ( $data );
		return $creative;
	
	}
	
	public function updateupload($data)
	{
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$creative = $this->CI->employeemodel->updateupload ( $data );
		return $creative;
	
	}
	
	public function getActiveEmp() {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getActiveEmp();
		return $response;
	}
	
	public function getActiveForm() {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getActiveForm();
		return $response;
	}
	
	public function getActiveSubForm() {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getActiveSubForm();
		return $response;
	}
	
	public function getEmpId($category_id) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getEmpId($category_id);
		return $response;
	}
	
	public function updateEmp($cat) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$result = $this->CI->employeemodel->updateEmp  ( $cat );
		return $result;
	}
	
	public function getEmpById($cat_id) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = array();
		$employee = array();
		
		$emp = $this->CI->employeemodel->getEmpById ( $cat_id );
		$experts = $this->CI->employeemodel->getExpertByEmpId ( $cat_id );
		$weekly = $this->CI->employeemodel->getWeekByEmpId (  $cat_id );
		$services = $this->CI->employeemodel->getServicesByEmpId ( $cat_id );
		foreach ($emp as $key=>$row){
			$employee[$key]= $row;
			$pro_expert = array();
			foreach ($experts as $expert){
				if($expert['emp_id']==$row['id']){
					$pro_expert[] = $expert['expert_id'];
				}
			}
			$pro_week = array();
			foreach ($weekly as $week){
				if($week['emp_id'] == $row['id']){
					$pro_week[]=$week['week_id'];
				}
			}
			$pro_service = array();
			foreach ($services as $service){
				if($service['emp_id'] == $row['id']){
					$pro_service[] = $service['service_id'];
				}
			}
			$employee[$key]['weekly'] = $pro_week;
			$employee[$key]['expert'] = $pro_expert;
			$employee[$key]['service'] = $pro_service;
		
			
		}
		$response = $employee;
		return $response;
	}
	
	public function getUpload() {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getUpload ();
		return $response;
	}
	
	public function addRoleForm($paras) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->addRoleForm ( $paras );
		return $response;
	}
	
	public function getServices(){
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getServices();
		return $response;
	}
	
	public function getActiveServices(){
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getActiveServices();
		return $response;
	}
	
	public function getActiveServicesStore($typeid){
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getActiveServicesStore($typeid);
		return $response;
	}
	
	public function getEmpList($roleid) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getEmpList($roleid);
		return $response;
	}
	public function getExpertByStoreid($storeid,$weekday) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getExpertByStoreid($storeid,$weekday);
		return $response;
	}
	public function getEmpName($empId) {
		$this->CI->load->model ( 'employee/Employee_model', 'employeemodel' );
		$response = $this->CI->employeemodel->getEmpName($empId);
		return $response;
	}
}