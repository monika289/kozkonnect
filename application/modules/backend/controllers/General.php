<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class General extends MX_Controller {
	
	public function __construct() {
		parent::__construct ();
		$this->load->helper ( 'url' );
		$this->load->helper ( 'cookie' );
		$fb_config = parse_ini_file ( APPPATH . "config/APP.ini" );
	}
	
	
	public function loadCatForm() {
		
		$this->load->library('zyk/Category', 'category');
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Category' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('general/CategoryAdd');
	}
	public function addCategory() {
		$admin_id = $this->session->userdata['adminsession']['id'];
		$name = $this->input->post('name');
		$status = $this->input->post('status');
		$isactive = $this->input->post('is_active');
		$sortorder = $this->input->post('sort_order');
		$data = array();
		$data = array(
				'name' => $name,
				'status' => $status,
				'is_active'=>$isactive,
				'sort_order' => $sortorder
		);
		$this->load->library('zyk/Category', 'category');
		$response = $this->category->addCategory($data);
		if($response['status'] == 1){
		$this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
	    redirect('/admin/category/list');	
	//	echo json_encode($response);

	}
	
	public function editCategory1($id) {
		$this->load->library('zyk/Category', 'category');
		$category = $this->category->getCategoryById($id);
		$this->template->set('category',$category[0]);
		
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Category' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('general/CategoryEdit');
	}
	
	public function editCategory($id) {
		$this->load->library('zyk/Category', 'category');
		$category = $this->category->getCategoryById($id);
		$this->template->set('category',$category[0]);
		$this->template->set_theme('default_theme');
		$this->template->set_layout (false);
		$html = $this->template->build ('general/pages/CategoryEdit');
		$html;
	
	}
	
	public function updateCategory(){
		$admin_id = $this->session->userdata['adminsession']['id'];
		$name = $this->input->post('name');
		$id = $this->input->post('cat_id');
		$status = $this->input->post('status');
		$date_modified = date('Y-m-d H:i:s');
		$isactive = $this->input->post('is_active');
		$sortorder = $this->input->post('sort_order');
		$data = array();
		$data = array(
				'id' => $id,
				'name' => $name,
				'status' => $status,
				'date_modified' => $date_modified,
				'is_active'=>$isactive,
				'sort_order'=>$sortorder
		);
		$this->load->library('zyk/Category', 'category');
		$response = $this->category->updateCategory($data);
		if($response['status'] == 1){
		$this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
	    redirect('/admin/category/list');
	}
	
	public function listCategories(){
	
		$this->load->library('zyk/Category', 'category');
		$category = $this->category->getAllCategories();
		$this->template->set('category',$category);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Category' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('general/CategoryList');
	}
	/* **************************** End Category Source ********************************** */
	public function loadSubCatForm() {
	
	//	$this->load->library('zyk/SubCategory', 'subcategory');
		$this->load->library('zyk/Category', 'category');
		$catlist = $this->category->getActiveCategoryList();
		$this->template->set('category',$catlist);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Sub Category' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('general/SubCategoryAdd');
	}
	public function addSubCategory() {
		//$admin_id = $this->session->userdata['adminsession']['id'];
		$catid = $this->input->post('catid');
		$name = $this->input->post('name');
		$status = $this->input->post('status');
		$isactive = $this->input->post('is_active');
		$sortorder = $this->input->post('sort_order');
		$date_modified = date('Y-m-d H:i:s');
		$data = array();
		$data = array(
				'parent_id' => $catid,
				'name' => $name,
				'status' => $status,
				'is_active'=>$isactive,
				'sort_order'=>$sortorder,
				'date_modified' => $date_modified,
		);
		$this->load->library('zyk/SubCategory', 'subcategory');
		$response = $this->subcategory->addSubCategory($data);
	
		if($response['status'] == 1){
		$this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
	    redirect('/admin/subcategory/list');
		//echo json_encode($response);
	
	}
	
	public function editSubCategory1($id) {
		$this->load->library('zyk/SubCategory', 'subcategory');
		
		$subcategory = $this->subcategory->getSubCategoryById($id);
		$this->template->set('subcategory',$subcategory[0]);
	    $this->load->library('zyk/Category','category');
	    $catlist = $this->category->getActiveCategoryList();
	    $this->template->set('catlist',$catlist);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Category' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('general/SubCategoryEdit');
	}

	
	public function editSubCategory($id) {
		$this->load->library('zyk/SubCategory', 'subcategory');
	
		$subcategory = $this->subcategory->getSubCategoryById($id);
		$this->template->set('subcategory',$subcategory[0]);
		$this->load->library('zyk/Category','category');
		$catlist = $this->category->getActiveCategoryList();
		$this->template->set('catlist',$catlist);
		$this->template->set_theme('default_theme');
		$this->template->set_layout (false);
		$html = $this->template->build ('general/pages/SubCategoryEdit');
		$html;
	}
	
	public function updateSubCategory(){
		//$admin_id = $this->session->userdata['adminsession']['id'];
		$categoryid = $this->input->post('catid'); 
		$name = $this->input->post('name');
		$id = $this->input->post('subcatid');
		$status = $this->input->post('status');
		$isactive = $this->input->post('is_active');
		$date_modified = date('Y-m-d H:i:s');
		$sortorder = $this->input->post('sort_order');
		$data = array();
		$data = array(
				'parent_id' => $categoryid,
				'id' => $id,
				'name' => $name,
				'status' => $status,
				'date_modified' => $date_modified,
				'is_active'=>$isactive,
				'sort_order'=>$sortorder
		);
	//	print_r($data); exit;
		$this->load->library('zyk/SubCategory', 'subcategory');
		$response = $this->subcategory->updateSubcategory($data);

		if($response['status'] == 1){
		$this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
	    redirect('/admin/subcategory/list');
		//echo json_encode($response);
	}
	
	public function listSubCategories(){
	
		$this->load->library('zyk/SubCategory', 'subcategory');
		$subcategory = $this->subcategory->getAllSubcategories();
		$this->template->set('subcategory',$subcategory);
		$this->load->library('zyk/Category', 'category');
		$catlist = $this->category->getActiveCategoryList();
		$this->template->set('category',$catlist);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Subcategory' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('general/SubCategoryList');
	}
	
	public function turnoncategory($id) {
		$this->load->library('zyk/Category', 'category');
		$result = $this->category->turnoncategory($id);
		echo  json_encode($result);
	}
	
	public function turnoffcategory($id) {
		$this->load->library('zyk/Category', 'category');
		$result = $this->category->turnoffcategory($id);
		echo json_encode($result);
	}
	
	public function turnonsubcat($id) {
		$this->load->library('zyk/Category', 'category');
		$result = $this->category->turnonsubcat($id);
		echo json_encode($result);
	
	}
	public function turnoffsubcat($id) {
		$this->load->library('zyk/Category', 'category');
		$result = $this->category->turnoffsubcat($id);
		echo json_encode($result);
	}
	
	/* ************************ Area management (Pincode) **************** */
	public function newPincode(){
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Pincode' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('general/PincodeAdd');
	}
	
	public function addPincode(){
		$data['pincode'] = $this->input->post('pincode');
		$data['is_active'] = $this->input->post('is_active');
		$data['status'] = $this->input->post('status');
		$data['message'] = $this->input->post('msg');
		$data['delivery_charges'] = $this->input->post('charges');
		$data['payment_mode'] = $this->input->post('paymentmode');
		//print_r($data);
		$this->load->library('zyk/PincodeLib', 'pincodelib');
		$response = $this->pincodelib->addPincode($data);
		echo json_encode($response);
	}
	
	public function updatePincode(){
	
		$id = $this->input->post('id');
		$pincode = $this->input->post('pincode');
		$isactive = $this->input->post('is_active');
		$status = $this->input->post('status');
		$msg = $this->input->post('msg');
		$charges = $this->input->post('charges');
		$paymentmode = $this->input->post('paymentmode');
		$newpincode = array(
			'id'=>$id,
			'pincode'=>$pincode,
			'is_active' => $isactive,
			'status' => $status,
			'delivery_charges' => $charges,
			'message' => $msg,
			'payment_mode' => $paymentmode
		);
		$this->load->library('zyk/PincodeLib', 'pincodelib');
		$response = $this->pincodelib->updatePincode($newpincode);
		echo json_encode($response);
	}
	public function getAllPincode(){
		$this->load->library('zyk/PincodeLib', 'pincodelib');
		$pincode = $this->pincodelib->getAllPincode();
		$this->template->set('pincode',$pincode);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Pincode' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('general/PincodeList');
	}
	public function turnoffpincode($id){
		$this->load->library('zyk/PincodeLib', 'pincodelib');
		$response = $this->pincodelib->turnoffpincode($id);
		echo json_encode($response);
	}
	public function turnonpincode($id){
		$this->load->library('zyk/PincodeLib', 'pincodelib');
		$response = $this->pincodelib->turnonpincode($id);
		echo json_encode($response);
	}
	public function getPincodeById($id){
		$this->load->library('zyk/PincodeLib', 'pincodelib');
		$pincode = $this->pincodelib->getPincodeById($id);
		$this->template->set('pincode',$pincode[0]);
		$this->template->set_theme('default_theme');
		$this->template->set_layout (false);
		$html = $this->template->build ('general/pages/PincodeEdit');
		$html;
	}
	
	public function addToShipRocket($orderid){
		$this->load->library('zyk/OrderLib','orderlib');
		$orders = $this->orderlib->getOrderById($orderid);
		$response = $this->orderlib->addOrdertoShipRocket($orders[0]);
		echo json_encode($response);
	}
	
	public function ShipRocketLogin(){
		$this->load->library('zyk/OrderLib','orderlib');
		
		$data = array(
				'email'=> 'tushar@brandzgarage.com',
				'password'=> 'Ap1US312'
		);
		$response = $this->orderlib->ShipRocketLogin($data);
	
	}
	
	public function getChannels(){
		$this->load->library('zyk/OrderLib','orderlib');
		$response = $this->orderlib->getChannels();
	}
	
	public function customerList()
	{
		$this->load->library('zyk/OrderLib','orderlib');
		$users = $this->orderlib->getCustomerList();
		$this->template->set('users',$users);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Users' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('customer/CustomerList');
	}
	
	
	/* **************************** End of Pincode ********************************* */
	
	
}