<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class Users extends MX_Controller {
	
	public function __construct() {
		parent::__construct ();
		$this->load->helper ( 'url' );
		$this->load->helper ( 'cookie' );
		$fb_config = parse_ini_file ( APPPATH . "config/APP.ini" );
		$this->load->library('zyk/UserLib','userlib');
		$this->load->model('User_model','user_model');
		
	}
	
	public function individualList() {
		//$this->load->library('zyk/OrderLib','orderlib');
		$ngolist = $this->user_model->getAllUsers(1);
		//print_r($ngolist);
		$this->template->set('users',$ngolist);

		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Users' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('users/individuallist');
		//$this->template->build ('addngo');
	}
	

	function newIndividual($userid=null){
		if(isset($userid))
		{
		$user = $this->user_model->getUserById($userid);
	//	print_r($user);
		$this->template->set('user',$user);
		$userdocs = $this->userlib->getdocsByID($userid);
	    $this->template->set('userdocs',$userdocs);

		}
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Users' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ((isset($userid) ? 'users/editindividual':'users/addindividual'));
	}

	

    
    function addIndividual()
    {
    	$postdata=$_POST;
    
      	$insertdata=array('fname'=>$postdata['fname'],
      		'lname'=>$postdata['lname'],
    		'designation'=>$postdata['designation'],
    		'company_name'=>$postdata['company_name'],
       		'contribution_type'=>$postdata['help'],
    		'email'=>$postdata['email'],
    		'mobile'=>$postdata['phone'],
    		'address'=>$postdata['locality'],
    		'created_on'=>date('Y-m-d H:m:s'),
    		'source'=>2,
    		'user_type'=>1

    	);

        $response=$this->user_model->addUser($insertdata);
         if($response['status'] >=1){
        	$responsefile=$this->do_upload_docs($response['status']);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
	    redirect('/admin/individual');
         
       /*  $responsefile=$this->do_upload_docs($userid);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok','Added Succesfully!');
    	
    
    	redirect('/admin/individual');  */
    }
    function editIndividual()
    {

    	$postdata=$_POST;
    	$insertdata=array('fname'=>$postdata['fname'],
      		'lname'=>$postdata['lname'],
    		'designation'=>$postdata['designation'],
    		'company_name'=>$postdata['company_name'],
       		'contribution_type'=>$postdata['help'],
    		'email'=>$postdata['email'],
    		'mobile'=>$postdata['phone'],
    		'address'=>$postdata['locality'],
    		'created_on'=>date('Y-m-d H:m:s'),
    		'source'=>2,
    		'user_type'=>1

    	);
 
        $response=$this->user_model->updateUser($insertdata,$postdata['user_id']);
          if($response['status'] >=1){
        	$responsefile=$this->do_upload_docs($postdata['user_id']);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
       /*   $responsefile=$this->do_upload_docs($postdata['user_id']);
    	
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok','Updated Succesfully!');  */
    	
    	redirect('/admin/individual');
    }


    //institute section
    public function institutionList() {
		//$this->load->library('zyk/OrderLib','orderlib');
		$instlist = $this->user_model->getAllUsers(4);
		//print_r($ngolist);
		$this->template->set('users',$instlist);

		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Users' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('users/institutionlist');
		//$this->template->build ('addngo');
	}
	function newInstitution($userid=null){
		if(isset($userid))
		{
		$user = $this->user_model->getUserById($userid);
		//print_r($user);  exit;
		$this->template->set('user',$user);
		//$userdocs = $this->userlib->getdocsByID($userid);
	 //   $this->template->set('userdocs',$userdocs);

		}
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Users' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ((isset($userid) ? 'users/editInstitution':'users/addinstitution'));
	}


	function addInstitution()
    {
    	$postdata=$_POST;
    /*	print_r($postdata);
    	exit;  */  
    
      	$insertdata=array(/*'fname'=>$postdata['fname'],
      		'lname'=>$postdata['lname'],
    		'designation'=>$postdata['designation'],*/
    		'company_name'=>$postdata['name'],
       		'contribution_type'=>$postdata['inst_sevice'],
    		'email'=>$postdata['email'],
    		'mobile'=>$postdata['phone'],
    		'address'=>$postdata['address1']."|".$postdata['locality'],
    		'created_on'=>date('Y-m-d H:m:s'),
    		'source'=>2,
    		'user_type'=>4

    	);

        $response=$this->user_model->addUser($insertdata);
         if($response['status'] >=1){
        	$responsefile=$this->do_upload_logo($response['status']);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
	    redirect('/admin/institution');
         
       /*  $responsefile=$this->do_upload_docs($userid);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok','Added Succesfully!');
    	
    
    	redirect('/admin/individual');  */
    }
      function editInstitution()
    {

    	$postdata=$_POST;
    		$insertdata=array(/*'fname'=>$postdata['fname'],
      		'lname'=>$postdata['lname'],
    		'designation'=>$postdata['designation'],*/
    		'company_name'=>$postdata['name'],
       		'contribution_type'=>$postdata['inst_sevice'],
    		'email'=>$postdata['email'],
    		'mobile'=>$postdata['phone'],
    		'address'=>$postdata['address1']."|".$postdata['locality'],
    		'created_on'=>date('Y-m-d H:m:s'),
    		'source'=>2,
    		'user_type'=>4

    	);
 
        $response=$this->user_model->updateUser($insertdata,$postdata['user_id']);
          if($response['status'] >=1){
        	$responsefile=$this->do_upload_logo($postdata['user_id']);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
       /*   $responsefile=$this->do_upload_docs($postdata['user_id']);
    	
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok','Updated Succesfully!');  */
    	
    	redirect('/admin/institution');
    }

    //corporate
    public function orgList() {
		//$this->load->library('zyk/OrderLib','orderlib');
		$orglist = $this->user_model->getAllUsers(2);
		//print_r($ngolist);
		$this->template->set('users',$orglist);

		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Users' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('users/orglist');
		//$this->template->build ('addngo');
	}
	function newOrg($userid=null){
		if(isset($userid))
		{
		$user = $this->user_model->getUserById($userid);
		//print_r($user);  exit;
		$this->template->set('user',$user);
		//$userdocs = $this->userlib->getdocsByID($userid);
	 //   $this->template->set('userdocs',$userdocs);

		}
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Users' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ((isset($userid) ? 'users/editorg':'users/addorg'));
	}

	function addOrg()
    {
    	$postdata=$_POST;
    /*	print_r($postdata);
    	exit;   */ 
    
      	$insertdata=array(/*'fname'=>$postdata['fname'],
      		'lname'=>$postdata['lname'],   */
      		'company_size'=>$postdata['members'],
    		'designation'=>$postdata['designation'],
    		'company_name'=>$postdata['name'],
       		'contribution_type'=>$postdata['org_help'],
    		'email'=>$postdata['email'],
    		'mobile'=>$postdata['phone'],
    		'address'=>$postdata['address1']."|".$postdata['locality'],
    		'created_on'=>date('Y-m-d H:m:s'),
    		'source'=>2,
    		'user_type'=>2

    	);

        $response=$this->user_model->addUser($insertdata);
         if($response['status'] >=1){
        	$responsefile=$this->do_upload_logo($response['status']);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
	   redirect('/admin/org');
         
       /*  $responsefile=$this->do_upload_docs($userid);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok','Added Succesfully!');
    	
    
    	redirect('/admin/individual');  */
    }
     function editOrg()
    {

    	$postdata=$_POST;
    			$insertdata=array(/*'fname'=>$postdata['fname'],
      		'lname'=>$postdata['lname'],   */
      		'company_size'=>$postdata['members'],
    		'designation'=>$postdata['designation'],
    		'company_name'=>$postdata['name'],
       		'contribution_type'=>$postdata['org_help'],
    		'email'=>$postdata['email'],
    		'mobile'=>$postdata['phone'],
    		'address'=>$postdata['address1']."|".$postdata['locality'],
    		'created_on'=>date('Y-m-d H:m:s'),
    		'source'=>2,
    		'user_type'=>2

    	);
 
        $response=$this->user_model->updateUser($insertdata,$postdata['user_id']);
          if($response['status'] >=1){
        	$responsefile=$this->do_upload_logo($postdata['user_id']);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
       /*   $responsefile=$this->do_upload_docs($postdata['user_id']);
    	
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok','Updated Succesfully!');  */
    	
    	//redirect('/admin/org');
    }
    


    public function do_upload_docs($userid)
    { 
       
       
     
	    $this->load->library('upload');
	    $dataInfo = array();
	    $files = $_FILES;
	    $cpt = count($_FILES['userfile']['name']);
		    for($i=0; $i<$cpt; $i++)
		    {           
		        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
		        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
		        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
		        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
		        $_FILES['userfile']['size']= $files['userfile']['size'][$i];  
		         if($_FILES['userfile']['name'] != ""){

		        $this->upload->initialize($this->set_upload_options($userid));
			        if ( ! $this->upload->do_upload('userfile')) {

		                return  $error = array('error' => $this->upload->display_errors());
		              }else{
			              $fileinfo = $this->upload->data();

			        $this->user_model->saveDocs(array('doc_url'=>$fileinfo['file_name'],'user_id'=>$userid,'date'=>date('Y-m-d H:m:s')),$userid);
			              
			        }
			    }
		    }
		    return false;

    }

	   
	

    //file upload configuration
	private function set_upload_options($userid)
	{   $dir=$userid;
        
          $upath = '.' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'userdocs' . DIRECTORY_SEPARATOR.	$dir. DIRECTORY_SEPARATOR;
        
            if (!file_exists($upath)) {
                mkdir($upath, 0777);
            }

	    //upload an image options
	    $config = array();
	    $config['upload_path'] = $upath;
	    $config['allowed_types'] = 'jpg|png|pdf';
	    $config['max_size']      = '2048';
	    $config['overwrite']     = FALSE;

	    return $config;
	    
	}
		public function deleteDoc($id,$userid){
	
		$user = $this->userlib->deleteUserdoc($id,$userid);
		$userdocs = $this->userlib->getdocsByID($userid);
		$doclist="";
		        foreach($userdocs as $docs ) { 
                        $doclist.="<img src=".asset_url().'userdocs/'.$userid.'/'.$docs['doc_url']." height=\"100\" width=\"100\" /><a href=\"javascript:deleteDocs(".$docs['id'].",".$userid.");\"><i class=\"material-icons dp48\">delete</i></a>";
               }  
	    
		echo $doclist;
	}

	//single file upload
	 public function do_upload_logo($userid)
    { 
           
        if($_FILES['userfile']['name'] != ""){
     
	    $this->load->library('upload');
	    $ext = get_file_extension($_FILES['userfile']['name']);
        $name="logo_".$userid.".".$ext;
	    $this->upload->initialize($this->set_upload_options_logo($name));
			         if ( ! $this->upload->do_upload('userfile'))
		                {
		                      return  $error = array('error' => $this->upload->display_errors());

		                }else{

				            $fileinfo = $this->upload->data();

					        $this->user_model->saveLogo(array('profile_img'=>$fileinfo['file_name']),$userid);  
					        return false;  	
		                }
		
	    }

	   
	    // $result_set = $this->tbl_products_model->insertUser($data);
    

    }

    //file upload configuration
	private function set_upload_options_logo($name)
	{  
        
          $upath = '.' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR. 'userlogos' . DIRECTORY_SEPARATOR;
        
            if (!file_exists($upath)) {
                mkdir($upath, 0777);
            }
     
      
       
	    //upload an image options
	    $config = array();
	    $config['upload_path'] = $upath;
	    $config['file_name'] = $name;
	    $config['allowed_types'] = 'jpg|png';
	    $config['max_size']      = '2048';
	    $config['overwrite']     = true;
	    $config['max_width'] = '400';
        $config['max_height'] = '400';

	    return $config;
	    
	}
		
	
	
}