<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class Ngo extends MX_Controller {
	
	public function __construct() {
		parent::__construct ();
		$this->load->helper ( 'url' );
		$this->load->helper ( 'cookie' );
		$fb_config = parse_ini_file ( APPPATH . "config/APP.ini" );
		$this->load->library('zyk/UserLib','userlib');
		$this->load->model('User_model','user_model');
		
	}
	
	public function ngoList() {
		//$this->load->library('zyk/OrderLib','orderlib');
		$ngolist = $this->user_model->getAllUsers();
		//print_r($ngolist);
		$this->template->set('users',$ngolist);

		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | order' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ngo/ngolist');
		//$this->template->build ('addngo');
	}
	

	function newNgo($userid=null){
		if(isset($userid))
		{
		$user = $this->user_model->getUserById($userid);
	//	print_r($user);
		$this->template->set('user',$user);
		$userdocs = $this->userlib->getdocsByID($userid);
	    $this->template->set('userdocs',$userdocs);

		}
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | order' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ((isset($userid) ? 'ngo/editngo':'ngo/addngo'));
	}

	

    
    function addNgo()
    {
    	$postdata=$_POST;
    	$insertdata=array('company_name'=>$postdata['name'],
    		'company_size'=>$postdata['members'],
    		'fname'=>$postdata['fname'],
    		'lname'=>$postdata['lname'],
       		'contribution_type'=>$postdata['ngoneed'],
    		'email'=>$postdata['email'],
    		'mobile'=>$postdata['phone'],
    		'address'=>$postdata['locality'],
    		'created_on'=>date('Y-m-d H:m:s'),
    		'source'=>2,
    		'user_type'=>3,

    	);

    	$response=$this->user_model->addUser($insertdata);
         if($response['status'] >=1){
        	$responsefile=$this->do_upload_docs($response['status']);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
    	//

    /*     $userid=$this->user_model->addUser($insertdata);
         
         $responsefile=$this->do_upload_docs($userid);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok','Added Succesfully!');  */
    	
    
    	redirect('/admin/ngo');
    }
    function editNgo()
    {

    	$postdata=$_POST;
    	$insertdata=array('company_name'=>$postdata['name'],
    		'company_size'=>$postdata['members'],
       		'contribution_type'=>$postdata['ngoneed'],
       		'fname'=>$postdata['fname'],
    		'lname'=>$postdata['lname'],
    		'email'=>$postdata['email'],
    		'mobile'=>$postdata['phone'],
    		'address'=>$postdata['locality'],
    		'created_on'=>date('Y-m-d H:m:s'),
    		'source'=>2,
    		'user_type'=>3,
    		

    	);

         $response=$this->user_model->updateUser($insertdata,$postdata['user_id']);
         
          if($response['status'] >=1){
        	$responsefile=$this->do_upload_docs($postdata['user_id']);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
	    redirect('/admin/ngo');
    	
    /*    	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok','Updated Succesfully!');  */
    	
    	
    }
    


    public function do_upload_docs($userid)
    { 
       
       
     
	    $this->load->library('upload');
	    $dataInfo = array();
	    $files = $_FILES;
	    $cpt = count($_FILES['userfile']['name']);
		    for($i=0; $i<$cpt; $i++)
		    {           
		        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
		        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
		        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
		        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
		        $_FILES['userfile']['size']= $files['userfile']['size'][$i];  

		         if($_FILES['userfile']['name'] != ""){

		        $this->upload->initialize($this->set_upload_options($userid));
			        if ( ! $this->upload->do_upload('userfile')) {

		                return  $error = array('error' => $this->upload->display_errors());
		              }else{
			              $fileinfo = $this->upload->data();

			        $this->user_model->saveDocs(array('doc_url'=>$fileinfo['file_name'],'user_id'=>$userid,'date'=>date('Y-m-d H:m:s')),$userid);
			            
			        }
			    }
		    }
		      return false;

    }

	   
	

    //file upload configuration
	private function set_upload_options($userid)
	{   $dir=$userid;
        
          $upath = '.' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'userdocs' . DIRECTORY_SEPARATOR.	$dir. DIRECTORY_SEPARATOR;
        
            if (!file_exists($upath)) {
                mkdir($upath, 0777);
            }

	    //upload an image options
	    $config = array();
	    $config['upload_path'] = $upath;
	    $config['allowed_types'] = 'gif|jpg|png';
	    $config['max_size']      = '2000';
	    $config['overwrite']     = FALSE;

	    return $config;
	    
	}
		public function deleteDoc($id,$userid){
	
		$user = $this->userlib->deleteUserdoc($id,$userid);
		$userdocs = $this->userlib->getdocsByID($userid);
		$doclist="";
		        foreach($userdocs as $docs ) { 
                        $doclist.="<img src=".asset_url().'userdocs/'.$userid.'/'.$docs['doc_url']." height=\"100\" width=\"100\" /><a href=\"javascript:deleteDocs(".$docs['id'].",".$userid.");\"><i class=\"material-icons dp48\">delete</i></a>";
               }  
	    
		echo $doclist;
	}
	
	
}