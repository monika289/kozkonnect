<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class Cause extends MX_Controller {
	
	public function __construct() {
		parent::__construct ();
		$this->load->helper ( 'url' );
		$this->load->helper ( 'cookie' );
		$fb_config = parse_ini_file ( APPPATH . "config/APP.ini" );
	$this->load->model('cause/Cause_model','cause');
		
	}
	
	public function causelist() {
	
		$causes = $this->cause->getCauseList();
		//print_r($causes); exit;
		$this->template->set('causes',$causes);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | causes' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('causes/causeList');
	}
	

}