<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Login extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('cookie');
        $fb_config = parse_ini_file(APPPATH . "config/APP.ini");
    }

    public function index() {
        $this->load->view('login');
    }

    public function dashboard() {
        $this->template->set_theme('default_theme');
        $this->template->set_layout('backend')
                ->title('Administrator | Administrator control panel')
                ->set_partial('header', 'partials/header')
                ->set_partial('leftnav', 'partials/sidebar')
                ->set_partial('footer', 'partials/footer');
        $this->template->build('dashboard');
    }

    public function adminlogin() {
        $data['email'] = trim($this->input->post('email'));
        $data['password'] = $this->input->post('password');
        $this->load->library('mylib/adminauth');
        $result = $this->adminauth->login($data);
        if($result['status']==1) {
            $this->session->set_userdata('adminsession',$result['result']);
        }

        echo json_encode($result);
    }


    public function logout() {
        $this->session->unset_userdata('adminsession');
        redirect(base_url() . "admin");
    }

}
