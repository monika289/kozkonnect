<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class Sdg extends MX_Controller {
	
	public function __construct() {
		parent::__construct ();
		$this->load->helper ( 'url' );
		$this->load->helper ( 'cookie' );
		$fb_config = parse_ini_file ( APPPATH . "config/APP.ini" );
		$this->load->library('zyk/UserLib','userlib');
		$this->load->model('Sdg_model','sdg_model');
		
	}
	
	public function sdgList() {
		//$this->load->library('zyk/OrderLib','orderlib');
		$sdglist = $this->sdg_model->getAllsdg();
		//print_r($ngolist);
		$this->template->set('sdgs',$sdglist);

		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | order' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('sdg/sdglist');
		//$this->template->build ('addngo');
	}
	

	function newSdg($sdgid=null){
		if(isset($sdgid))
		{
		$sdgs = $this->sdg_model->getsdgById($sdgid);
		//print_r($sdgs);
		$this->template->set('sdgs',$sdgs[0]);
	

		}
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | order' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ((isset($sdgid) ? 'sdg/editsdg':'sdg/addsdg'));
	}

	

    
    function addSdg()
    {
    	$postdata=$_POST;

    	$insertdata=array('sdgname'=>$postdata['name'],
    		
       		'description'=>$postdata['description'],
    		'sdgurl'=>$postdata['sdgurl'],
    		//'logo_url'=>$postdata['userfile'],
    		'create_date'=>date('Y-m-d H:m:s'),
    		'status'=>$postdata['status'],
    	
    	);

         $response=$this->sdg_model->addSdg($insertdata);
       
        if($response['status'] >=1){
        	$responsefile=$this->do_upload_docs($response['status']);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
	    redirect('admin/sdg');	
       
    }
    function editSdg()
    {

    	$postdata=$_POST;
    	$insertdata=array('sdgname'=>$postdata['name'],
    		
       		'description'=>$postdata['description'],
    		'sdgurl'=>$postdata['sdgurl'],
    		//'logo_url'=>$postdata['userfile'],
    		'create_date'=>date('Y-m-d H:m:s'),
    		'status'=>$postdata['status'],
    	
    	);

         $response=$this->sdg_model->updateSdg($insertdata,$postdata['sdgid']);
         if($response['status'] >=1){
        	$responsefile=$this->do_upload_docs($postdata['sdgid']);
        	if($responsefile)
        	$this->session->set_flashdata('notok',strip_tags(implode('<br>',$responsefile)));
        	else 	
		    $this->session->set_flashdata('ok',$response['msg']);
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
	    }
	    redirect('admin/sdg');	
        // $this->do_upload_docs($postdata['user_id']);
    	
    	/*$this->session->set_flashdata('ok', 'Updated Succesfully!');
    	redirect('/admin/ngo');   */
    }
    


    public function do_upload_docs($sdgid)
    { 
       
        if($_FILES['userfile']['name'] != ""){
     
	    $this->load->library('upload');
	    $ext = get_file_extension($_FILES['userfile']['name']);
        $name="logo_".$sdgid.".".$ext;
	    $this->upload->initialize($this->set_upload_options($name));
			         if ( ! $this->upload->do_upload('userfile'))
		                {
		                      return  $error = array('error' => $this->upload->display_errors());

		                }else{

				            $fileinfo = $this->upload->data();

					        $this->sdg_model->saveDocs(array('logo_url'=>$fileinfo['file_name']),$sdgid);  
					        return false;  	
		                }
			       
			 /*   for($i=0; $i<$cpt; $i++)
			    {           
			        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
			        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
			        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
			        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

			        $this->upload->initialize($this->set_upload_options($sdgid));
			         if ( ! $this->upload->do_upload('userfile'))
		                {
		                        $error = array('error' => $this->upload->display_errors());

		                      print_r($error);
		                }
			        $fileinfo = $this->upload->data();

			        $this->sdg_model->saveDocs(array('doc_url'=>$fileinfo['file_name'],'user_id'=>$userid,'date'=>date('Y-m-d H:m:s')),$userid);

			    }*/
	    }

	   
	    // $result_set = $this->tbl_products_model->insertUser($data);
    

    }

    //file upload configuration
	private function set_upload_options($name)
	{  
        
          $upath = '.' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'backend' . DIRECTORY_SEPARATOR. 'sdglogos' . DIRECTORY_SEPARATOR;
        
            if (!file_exists($upath)) {
                mkdir($upath, 0777);
            }
     
      
       
	    //upload an image options
	    $config = array();
	    $config['upload_path'] = $upath;
	    $config['file_name'] = $name;
	    $config['allowed_types'] = 'jpg|png';
	    $config['max_size']      = '2048';
	    $config['overwrite']     = true;
	    $config['max_width'] = '400';
        $config['max_height'] = '400';

	    return $config;
	    
	}
		public function deleteDoc($id,$userid){
	
		$user = $this->userlib->deleteUserdoc($id,$userid);
		$userdocs = $this->userlib->getdocsByID($userid);
		$doclist="";
		        foreach($userdocs as $docs ) { 
                        $doclist.="<img src=".asset_url().'userdocs/'.$userid.'/'.$docs['doc_url']." height=\"100\" width=\"100\" /><a href=\"javascript:deleteDocs(".$docs['id'].",".$userid.");\"><i class=\"material-icons dp48\">delete</i></a>";
               }  
	    
		echo $doclist;
	}
	
	
}