<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class Ticket extends MX_Controller {
	
	public function __construct() {
		parent::__construct ();
		$this->load->helper ( 'url' );
		$this->load->helper ( 'cookie' );
		$fb_config = parse_ini_file ( APPPATH . "config/APP.ini" );
		$this->load->library('zyk/General');
		
	}
	
	public function mainTicket() {
		$categories = $this->general->getActiveTicketCategories();
		$subcategories = $this->general->getSubActiveTicketCategories();
		$status = $this->general->getActiveTicketStatus();
		$this->template->set('status',$status);
		$this->template->set('categories',$categories);
		$this->template->set('subcategories',$subcategories);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Settings' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/mainticket');
	}
	
	public function newCategory() {
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Add Category' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/CategoryAdd');
	}
	
	public function getTicketCategoryList() {
		$categories = $this->general->getTicketCategories();
		$this->template->set('categories',$categories);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Category List' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/CategoryList');
	}
	
	public function addCategory() {
		$params = array();
		$params['name'] = $this->input->post('name');
		$params['status'] = $this->input->post('status');
		$params['created_datetime'] = date('Y-m-d H:i:s');
	
		$response = $this->general->addTicketCategory($params);
		if($response['status'] >=1){
		$this->session->set_flashdata('ok',$response['msg']);
		redirect('/admin/ticket/categorylist');
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
		redirect('admin/ticket/categorynew');
	    }
	   // redirect('admin/sdg');	
		//echo json_encode($response);
	}
	
	public function editCategory1($id) { 
		//$id=$this->input->post('id');
		$categories = $this->general->getTicketCategoryById($id);
		$this->template->set('categories',$categories);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Category Edit' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/CategoryEdit');
	}
	
	public function editCategory($id) {
		//$id=$this->input->post('id');
		$categories = $this->general->getTicketCategoryById($id);
		$this->template->set('categories',$categories[0]);
		$this->template->set_theme('default_theme');
		//$this->template->set_layout (false);
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Category Edit' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		 $this->template->build ('ticket/pages/CategoryEdit');
		
	}
	
	public function updateCategory() {
		$params = array();
		$params['id'] = $this->input->post('id');
		$params['name'] = $this->input->post('name');
		$params['status'] = $this->input->post('status');
		$params['updated_datetime'] = date('Y-m-d H:i:s');

	
		$response = $this->general->updateTicketCategory($params);
		//print_r($response); exit;
		if($response['status'] >=1){
		$this->session->set_flashdata('ok',$response['msg']);
		redirect('/admin/ticket/categorylist');
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
		redirect('admin/ticket/categoryedit/'.$params['id'].'');
	    }
		//echo json_encode($response);
	}
	
	public function newSubCategory() {
		$categories = $this->general->getActiveTicketCategories();
		$this->template->set('categories',$categories);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Add SubCategory' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/SubCategoryAdd');
	}
	
	public function addSubCategory() {
		$params = array();
		$params['name'] = $this->input->post('name');
		$params['category_id'] = $this->input->post('category_id');
		$params['status'] = $this->input->post('status');
		$params['created_datetime'] = date('Y-m-d H:i:s');
	
		$response = $this->general->addTicketSubCategory($params);
		if($response['status'] >=1){
		$this->session->set_flashdata('ok',$response['msg']);
		redirect('/admin/ticket/subcategorylist');
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
		redirect('admin/ticket/subcategorynew');
	    }
		//echo json_encode($response);
	}
	
	public function getTicketSubCategoryList() {
		$subcategories = $this->general->getSubAllTicketCategories();
		$categories = $this->general->getActiveTicketCategories();
		$this->template->set('subcategories',$subcategories);
		$this->template->set('category',$categories);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Category List' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/SubCategoryList');
	}
	
	public function editSubCategory1($id) {
		//$id=$this->input->post('id');
		$categories = $this->general->getActiveTicketCategories();
		$subcategories = $this->general->getTicketSubCategoryById($id);
		$this->template->set('categories',$categories);
		$this->template->set('subcategories',$subcategories[0]);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Edit SubCategory' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/SubCategoryEdit');
	}
	
	public function editSubCategory($id) {
		//$id=$this->input->post('id');
		$categories = $this->general->getActiveTicketCategories();
		$subcategories = $this->general->getTicketSubCategoryById($id);
		$this->template->set('categories',$categories);
		$this->template->set('subcategories',$subcategories[0]);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Edit SubCategory' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		//$this->template->set_layout (false);
		$this->template->build ('ticket/pages/SubCategoryEdit');
		
	}
	
	public function updateSubCategory() {
		$params = array();
		$params['id'] = $this->input->post('id');
		$params['name'] = $this->input->post('name');
		$params['category_id'] = $this->input->post('cat_id');
		$params['status'] = $this->input->post('status');
		$params['updated_datetime'] = date('Y-m-d H:i:s');
	
		$response = $this->general->updateTicketSubCategory($params);
		if($response['status'] >=1){
		$this->session->set_flashdata('ok',$response['msg']);
	    redirect('/admin/ticket/subcategorylist');
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
		redirect('admin/ticket/categoryedit/'.$params['id'].'');
	    }
		//echo json_encode($response);
	}
	
	public function newStatus() {
	
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Add Status' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/StatusAdd');
	}
	
	public function addStatus() {
		$params = array();
		$params['name'] = $this->input->post('name');
		$params['status'] = $this->input->post('status');
		$params['created_datetime'] = date('Y-m-d H:i:s');
	
		$response = $this->general->addTicketStatus($params);
		if($response['status'] >=1){
		$this->session->set_flashdata('ok',$response['msg']);
		redirect('/admin/ticket/statuslist');
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
		redirect('admin/ticket/statusnew');
	    }
		//echo json_encode($response);
	}
	
	public function getTicketAllStatusList() {
		$status = $this->general->getAllTicketStatus();
		$this->template->set('status',$status);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Category List' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/StatusList');
	}
	
	public function updateStatus() {
		$params = array();
		$params['id'] = $this->input->post('id');
		$params['name'] = $this->input->post('name');
		$params['status'] = $this->input->post('status');
		$params['updated_datetime'] = date('Y-m-d H:i:s');
	
		$response = $this->general->updateTicketStatus($params);
		if($response['status'] >=1){
		$this->session->set_flashdata('ok',$response['msg']);
		redirect('/admin/ticket/statuslist');
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
		redirect('admin/ticket/statusedit'.$params['id'].'');
	    }
		//echo json_encode($response);
	}
	
	public function editStatus1($id) {
		$status = $this->general->getActiveTicketStatus($id);
		$this->template->set('status',$status);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Edit Status' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/StatusEdit');
	}
	
	public function editStatus($id) {
		
		$status = $this->general->getTicketStatusById($id);
		$this->template->set('status',$status[0]);
		$this->template->set_theme('default_theme');
		//$this->template->set_layout (false);
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Edit Status' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/pages/StatusEdit');
	
	}
	
	public function viewTicket($id){
		$tickets = $this->general->getTicketById($id);
		$tickets1 = $this->general->getAllActiveTickets();
		$categories = $this->general->getActiveTicketCategories();
		$subcategories = $this->general->getSubActiveTicketCategories();
		$ticketstatus = $this->general->getActiveTicketStatus();
		$this->load->library('zyk/adminauth');
		$Emps = $this->adminauth->getUserList();
		//$Emps = $this->employeelib->getActiveEmp();
		$comments = $this->general->getUserComment($id);
		$this->template->set('comments',$comments);
		$this->template->set('ticketstatus',$ticketstatus);
		$this->template->set('categories',$categories);
		$this->template->set('subcategories',$subcategories);
		$this->template->set('ticket',$tickets[0]);
		$this->template->set('tickets1',$tickets1);
		$this->template->set('Emps',$Emps);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Ticket' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/view-lead');
	}
	
	public function viewTicket1($id){
		$this->load->library('zyk/Lead', 'lead');
		$leads = $this->lead->getLeadById($id);
		$this->template->set('lead',$leads[0]);
		$comments = $this->lead->getUserCommentByLeadId($id);
		$this->template->set('comments',$comments);
		$executives = $this->lead->getUserList();
		$this->template->set('executives',$executives);
		$leadStatus = $this->lead->getLeadStatus();
		$this->template->set('leadStatus',$leadStatus);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Product' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('lead/view-lead');
	}
	public function tickets() {
		$this->load->library('zyk/General');
	//	$tickets = $this->general->getAllTickets();
		$tickets=array();
		$this->template->set('tickets',$tickets);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Ticket' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/TicketList_new');
	}
	
	public function newTicket() {
		$this->load->library('zyk/Adminauth');
		$acps = $this->adminauth->getAdminUsers();
		$categories = $this->general->getActiveTicketCategories();
		$subcategories = $this->general->getSubActiveTicketCategories();
		$status = $this->general->getActiveTicketStatus();
		//$Emps = $this->employeelib->getActiveEmp();
		$this->template->set('status',$status);
		$this->template->set('categories',$categories);
		$this->template->set('subcategories',$subcategories);
		$this->template->set('acps',$acps);
		//$this->template->set('Emps',$Emps);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Ticket' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/TicketAdd');
	}
	
	public function addTicket() {
		$response = array();
		$params = $this->input->post('ticket');
		$this->load->library('zyk/General');
		$ticket = array();
		$ticket['priority'] = $params['priority'];
		$ticket['userid'] = $this->session->userdata('adminsession')['id'];
		
		if(!empty($params['orderid'])){
			$ticket['orderid'] = $params['orderid'];
		}else {
			$ticket['orderid'] = 0;
		}
		$ticket['subject'] = $params['subject'];
		$ticket['description'] = $params['description'];
		$ticket['category_id'] = $params['category_id'];
		$ticket['subcategory_id'] = $params['subcategory_id'];
		//$ticket['type'] = $params['type'];
		//$ticket['quantity'] = $params['quantity'];
		$ticket['assigned_to'] = $params['assigned_to'];
		$ticket['status_id'] = $params['status'];
		$ticket['created_date'] = date('Y-m-d H:i:s');
		$ticket['updated_date'] = date('Y-m-d H:i:s');
		$ticket['created_by'] = $this->session->userdata('adminsession')['id'];
		$id = $this->general->addTicket($ticket);
		$data=array();
		$data['ticketid'] = $id;
		$data['comment'] = $ticket['description'];
		$this->general->addComment($data);
		if(!empty($id)) {
			$ticket_no = 'TT'.$id;
			$tp = array();
			$tp['ticketid'] = $id;
			$tp['ticket_no'] = $ticket_no;
			$this->general->updateTicket($tp);
			$tsms = array();
			$tsms['name'] = $params['name'];
			$tsms['mobile'] = $params['mobile'];
			$temail['mobile'] = $params['email'];
			//$this->general->sendTicketSMS($tsms);
			//$this->general->sendTicketSMS($tsms);
			$response['status'] = 1;
			$response['msg'] = 'Ticket added successfully.';
		} else {
			$response['status'] = 0;
			$response['msg'] = 'Failed to add Ticket';
		}
		//echo json_encode($response);
		if($response['status'] >=1){
		$this->session->set_flashdata('ok',$response['msg']);
		redirect('/admin/ticket/new');
		}else {
		$this->session->set_flashdata('notok',$response['msg']);
		redirect('admin/ticket/tickets');
	    }
	}
	
	public function editTicket($ticketid) {
		$this->load->library('zyk/Adminauth');
		$this->load->library('zyk/General');
		$acps = $this->adminauth->getAdminUsers();
		$tickets = $this->general->getTicketById($ticketid);
		$categories = $this->general->getActiveTicketCategories();
		$subcategories = $this->general->getSubActiveTicketCategories();
		$status = $this->general->getActiveTicketStatus();
		$this->template->set('status',$status);
		$this->template->set('categories',$categories);
		$this->template->set('subcategories',$subcategories);
		$this->template->set('acps',$acps);
		$this->template->set('ticket',$tickets[0]);
		$this->template->set_theme('default_theme');
		$this->template->set_layout ('backend')
		->title ( 'Administrator | Ticket' )
		->set_partial ( 'header', 'partials/header' )
		->set_partial ( 'leftnav', 'partials/sidebar' )
		->set_partial ( 'footer', 'partials/footer' );
		$this->template->build ('ticket/TicketEdit');
	}
	
	public function updateTicket() {
		$response = array();
		$params = $this->input->post('ticket');
		$this->load->library('zyk/General');
		$ticket = array();
		$ticket['ticketid'] = $params['ticketid'];
		$ticket['priority'] = $params['priority'];
		$ticket['userid'] = $params['userid'];
		$ticket['orderid'] = $params['orderid'];
		$ticket['subject'] = $params['subject'];
		$ticket['description'] = $params['description'];
		$ticket['category_id'] = $params['category_id'];
		$ticket['subcategory_id'] = $params['subcategory_id'];
		//$ticket['type'] = $params['type'];
		//$ticket['quantity'] = $params['quantity'];
		$ticket['assigned_to'] = $params['assigned_to'];
		$ticket['status_id'] = $params['status'];
		//$ticket['resolution'] = $params['resolution'];
		$ticket['updated_date'] = date('Y-m-d H:i:s');
		$flag = $this->general->updateTicket($ticket);
		if($flag) {
			$response['status'] = 1;
			$response['msg'] = 'Ticket updated successfully.';
		} else {
			$response['status'] = 0;
			$response['msg'] = 'Failed to update Ticket';
		}
		echo json_encode($response);
	}
	
	
	public function comment(){
		$ticketid = $this->input->post('ticketid');
		$comment = $this->input->post('comment');
		$data = array();
		$data = array(
				'comment' => $comment,
				'ticketid' => $ticketid,
				//	'created_by' => $admin_id,
				'status' => 1,
		);
		$this->load->library('zyk/General');
		$result = $this->general->addComment($data);
		//print_r($result);
		if($result['status']==1){
			$comments = $this->general->getUserComment($ticketid);
			$this->template->set('comments',$comments);
			$this->template->set_theme('default_theme');
			$this->template->set_layout (false);
			$html= $this->template->build ('ticket/pages/comment', '', true);
			echo $html;
		}
		//echo json_encode($result);
	}
	
	public function leadHistory($ticketid){
		$this->load->library('zyk/Lead', 'lead');
		$leadHistory = $this->lead->leadHistory($ticketid);
		$this->template->set('leadHistory',$leadHistory);
		$this->template->set_theme('default_theme');
		$this->template->set_layout (false);
		$html= $this->template->build ('ticket/pages/leadHistory', '', true);
		echo $html;
	}
	public function priorityHistory($ticketid){
		$this->load->library('zyk/Lead', 'lead');
		$leadHistory = $this->lead->leadHistory($ticketid);
		$this->template->set('leadHistory',$leadHistory);
		$this->template->set_theme('default_theme');
		$this->template->set_layout (false);
		$html= $this->template->build ('ticket/pages/priority-history', '', true);
		echo $html;
	}
	public function statusHistory($ticketid){
		$this->load->library('zyk/Lead', 'lead');
		$leadHistory = $this->lead->leadHistory($ticketid);
		$this->template->set('leadHistory',$leadHistory);
		$this->template->set_theme('default_theme');
		$this->template->set_layout (false);
		$html= $this->template->build ('ticket/pages/status-history', '', true);
		echo $html;
	}
	
	public function assignLead(){
		$admin_id = $this->session->userdata['adminsession']['id'];
		$assigned_to = $this->input->post('user');
		$ticketid = $this->input->post('id');
		$data = array(
				'ticketid' => $lead_id,
				'assigned_to' => $user,
				//	'updated_by' => $admin_id,
	
		);
		/*	$lstatus = array(
		 'lead_id' => $lead_id,
		 'type' => 3,
		 'changed_id' => $user,
		 //	'created_by' => $admin_id,
		 'comment'=>'Executive assigned'
		);*/
		//	$leadStatus = $this->lead->changeLeadStatus($lstatus);
		$response = $this->general->updateLead($data);
		if($response['status']==1){
			//$this->lead->sendLeadAssignEmail($data);
		}
		echo json_encode($response);
	}
	public function changeStatusLead(){
		$admin_id = $this->session->userdata['adminsession']['id'];
		$status = $this->input->post('status');
		$lead_id = $this->input->post('id');
		$data = array(
				'id' => $lead_id,
				'lead_status_id' => $status,
				'updated_by' => $admin_id,
	
		);
		$lstatus = array(
				'lead_id' => $lead_id,
				'type' => 2,
				'changed_id' => $status,
				'created_by' => $admin_id,
				'comment'=>'Status changed'
		);
		$this->load->library('zyk/Lead', 'lead');
		$leadStatus = $this->lead->changeLeadStatus($lstatus);
		$response = $this->lead->updateLead($data);
		echo json_encode($response);
	}
	public function changePriority(){
		$admin_id = $this->session->userdata['adminsession']['id'];
		$priority= $this->input->post('priority');
		$lead_id = $this->input->post('id');
		$data = array(
				'id' => $lead_id,
				'priority' => $priority,
				'updated_by' => $admin_id,
	
		);
		$lstatus = array(
				'lead_id' => $lead_id,
				'type' => 1,
				'changed_id' => $priority,
				'created_by' => $admin_id,
				'comment'=>'Priority changed'
		);
		$this->load->library('zyk/Lead', 'lead');
		$leadStatus = $this->lead->changeLeadStatus($lstatus);
		$response = $this->lead->updateLead($data);
		echo json_encode($response);
	}
	
	public function getUserByEmail() {
		$email = $this->input->get('email');
		$this->load->library('zyk/UserLib');
		$user = $this->userlib->getProfileByEmail($email);
		echo json_encode($user);
	}
	
	public function getUserByMobile() {
		$mobile = $this->input->get('mobile');
		$this->load->library('zyk/UserLib');
		$user = $this->userlib->getProfileByMobile($mobile);
		echo json_encode($user);
	}
	
	public function getUserByName() {
		$name = $this->input->get('name');
		$this->load->library('zyk/UserLib');
		$user = $this->userlib->getProfileByName($name);
		echo json_encode($user);
	}
	
	public function userDetail($id) {
		$this->load->library('zyk/UserLib');
		$user = $this->userlib->getProfile($id);
		echo json_encode($user[0]);
	}
	
	public function getuserorder() {
		$userid = $this->input->post('userid');
		$this->load->library('zyk/UserLib');
		$emaildet = $this->userlib->getUserOrder($userid);
		echo json_encode($emaildet);
	}
	
	public function getTicketSubCatId() {
		$id = $this->input->post('cat_id');
		$this->load->library('zyk/General');
		$subcat = $this->general->getTicketSubCatId($id);
		echo json_encode($subcat);
	}
	
	public function turnoncategory($id){
		$this->load->library('zyk/General');
		$subcat = $this->general->turnoncategory($id);
		echo json_encode($subcat);
	}
	public function turnoffcategory($id){
		$this->load->library('zyk/General');
		$subcat = $this->general->turnoffcategory($id);
		echo json_encode($subcat);
	}
	
	public function turnonsubcat($id){
		$this->load->library('zyk/General');
		$subcat = $this->general->turnonsubcat($id);
		echo json_encode($subcat);
	}
	public function turnoffsubcat($id){
		$this->load->library('zyk/General');
		$subcat = $this->general->turnoffsubcat($id);
		echo json_encode($subcat);
	}
	
	public function turnoffstatus($id){
		$this->load->library('zyk/General');
		$subcat = $this->general->turnoffstatus($id);
		echo json_encode($subcat);
	}
	
	public function turnonstatus($id){
		$this->load->library('zyk/General');
		$subcat = $this->general->turnonstatus($id);
		echo json_encode($subcat);
	}
		
}