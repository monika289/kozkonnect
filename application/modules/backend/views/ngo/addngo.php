  <main class="mn-inner">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">Add NGO</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                  <span class="card-title">Add NGO</span>
                                <p>Fill up details and attach document if required.</p>
                                <!-- <a class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i></a> -->
                            <div class="row">
                                <form id="addngo" action="<?php echo base_url();?>admin/ngo/addNgo" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="user_id" value="">
                                    
                                      
                                                <!-- <div class="row"> -->
                                                    <!-- <div class="col m6"> -->
                                                        <div class="row">
                                                            <div class="input-field col m6 s12">
                                                                <label for="name">NGO Name</label>
                                                                <input id="name" name="name" type="text" class="required validate">
                                                            </div>
                                                            <div class="input-field col m6 s12">
                                                                <label for="locality"></label>
                                                                <input id="locality" name="locality" type="text" class="required validate">
                                                            </div>
                                                            
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="row">
                                                            <div class="input-field col m6 s12">
                                                                <label for="ngoneed">Type of NGO(Category)</label>
                                                                <input id="ngoneed" name="ngoneed" type="text" class="required validate">
                                                            </div>
                                                            <div class="input-field col m6 s12">
                                                                <label for="members">Number of members</label>
                                                                <input id="members" name="members" type="text" class="required validate">
                                                            </div>
                                                            
                                                        <!-- </div> -->
                                                  </div>
                                                    <div class="row">
                                                        <p >Responsible Person</p>
                                                            <div class="input-field col m6 s12">
                                                                <label for="fname">first Name</label>
                                                                <input id="fname" name="fname" type="text" class="required validate">
                                                            </div>
                                                            <div class="input-field col m6 s12">
                                                                <label for="lname">Last Name</label>
                                                                <input id="lname" name="lname" type="text" class="required validate">
                                                            </div>
                                                            
                                                            
                                                        <!-- </div> -->
                                                    </div>
                                <div class="row">
                                    <div class="input-field col m6 s12">
                                        <label for="email">Email</label>
                                        <input id="email" name="email" type="email" class="required validate">
                                    </div>
                                     <div class="input-field col m6 s12">
                                                                <label for="phone">Phone number</label>
                                                                <input id="phone" name="phone" type="tel" class="required validate">
                                     </div>
                                </div>
                                    <div class="row">
                                            <div class="file-field input-field">
                                                <div class="btn teal lighten-1">
                                                    <span>File</span>
                                                    <input type="file" name="userfile[]" multiple="multiple">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text" placeholder="Upload one or more files" class="required validate">
                                                </div>
                                            </div>
                                                           
                                                            
                                                        <!-- </div> -->
                                    </div>
                                              <!--     <div class="row">
          <a class="waves-effect waves-grey btn white modal-trigger" href="#uploadimg">Upload more images</a>
                          </div> -->
                                                <!-- </div> -->
       
                                                <input class="waves-effect waves-light btn blue m-b-xs" type="submit" name="" value="submit">
                                   
                                </form>
                         </div>
                </div>
                          
         
                        </div>
                    </div>
                </div>
       

            </main>

    <?php
        if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green', '<?= $this->session->flashdata('ok') ?>');
                });
            </script>
    <?php } ?>        


        <script src="<?= asset_url();?>backend\plugins\jquery-validation\jquery.validate.min.js"></script>
        <script src="<?= asset_url();?>backend\js\alpha.min.js"></script>
      
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqbaVKWXKDsrUY87hCEVlQMXpcCAzR-38&libraries=places&callback=initMap" async defer></script>
        <script type="text/javascript">
 
    function initMap() {
    var options = {
        componentRestrictions: {country: 'in'}
    };
    var input =  document.getElementById('locality');
    var autocomplete = new google.maps.places.Autocomplete(input,options);
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
          window.alert("Autocomplete's returned place contains no geometry");
          return;
        }
        $('#latitude').val(place.geometry.location.lat());
        $('#longitude').val(place.geometry.location.lng());
    });
}



//validate

    $( document ).ready(function() {
    	 // additional method for validate plug-in  letter and space -->
       jQuery.validator.addMethod("lettersonly", function(value, element) 
        {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "Letters and spaces only please");
       
    var validator = $("#addngo").validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            },
            'name': { lettersonly: true },
            'fname': { lettersonly: true },
            'lname': { lettersonly: true },
            'phone': {
                            required: true,
                            digits: true,
                            rangelength: [10, 10] 
                           },
             'members': {
                          
                            digits: true,
                          
                           },
        },
        massage:{
        	'phone': {                       
                        rangelength: 'enter valid mobile number',
                           },

        }
    });

   });



        </script>