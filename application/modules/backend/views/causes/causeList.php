    <!-- Styles -->
       
     
      <main class="mn-inner inner-active-sidebar">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">Causes List</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                  <!--   <a href="<?= base_url('admin/org/new'); ?>" class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i>Add Corporate</a> -->
                                <span class="card-title">Causes List</span>
                                <p>All Causes list for reject and  approve</p><br>
                                <table id="example" class="display responsive-table datatable-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
										    <th>Cause date</th>
										    <th>Title</th>
											<th>User</th>
											<th>Phone number</th>
											<!-- <th>Email id</th> -->
											<th>Selected NGO</th>
											<th>City</th>
											<th>Requirements</th>
											<th>Status</th>
			     							<th>Action</th>
                                        </tr>
                                    </thead>
                                   <!--  <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Salary</th>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>
                            <?php
                            if(!empty($causes)){
                             foreach ($causes as $cause){
                             	$status="";
                             	switch($cause['c_status'])
                             	{
                             		case 0:
                             		$status="Pending";
                             		break;
                             		case 1:
                             		$status="Approve";
                             		break;
                             		case 2:
                             		$status="Reject";
                             		break;

                             	}
                             	$username="";
                             	if($cause['user_type']==1)
                             	{
                             		$username=$cause['fname']." ".$cause['lname'];
                             	}else{
                             		$username=$cause['company_name'];	
                             	}

                             	?> 
                                        <tr>
                                        	<td><?php echo $cause['id'];?></td>
                                        	<td><?php echo date("jS M' y", strtotime($cause['created_on'])); ?></td>
                                            <td><?php echo $cause['title'];?></td>
                                            <td><?php echo $username;?></td>
                                            <td><?php echo $cause['mobile'];?></td>
                                            <!-- <td><?php echo $cause['email'];?></td> -->
                                            <td><?php echo $cause['ango'];?></td>
                                            <td><?php echo $cause['location'];?></td>
                                            <td><?php echo $cause['requirement_type'];?></td>
                                            <td><?php echo $status;?></td>
                                          
                                           
                                            <td><a href="<?php echo base_url();?>admin/org/add/<?php echo $cause['id'];?>" class="waves-effect waves-blue btn-flat m-b-xs"><i class="fa fa-pencil m-r-5"></i>View</a></a></td>
                                        </tr>
                            <?php } } ?>            
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
  <!--  <?php
        if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green', '<?= $this->session->flashdata('ok') ?>');
                });
            </script>
    <?php } ?>   --> 
<script>
$(document).ready(function() {
    $('#example').DataTable({
        language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>' 
        }
        }
    });
    $('.dataTables_length select').addClass('browser-default');
});
</script>