  <main class="mn-inner">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">Edit Ticket Category</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                  <span class="card-title">Edit Ticket Category</span>
                                <!-- <p>Fill up details and attach document if required.</p> -->
                                <!-- <a class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i></a> -->
                            <div class="row">
                                <form id="update-form" action="<?php echo base_url();?>admin/ticket/categoryupdate" method="POST" >
                                   <input type="hidden" id="id" name="id" value="<?php echo $categories['id']?>"> 
                                    
                                      
                                                <!-- <div class="row"> -->
                                                    <!-- <div class="col m6"> -->
                                                        <div class="row">
                                                            <div class="input-field col m6 s12">
                                                                <label for="name">Category Name</label>
                                                                <input id="name" name="name" type="text" class="required validate" value="<?php echo $categories['name']?>">
                                                            </div>
                                                              <div class="input-field col m6 s12">
                                                                <select id="status" name="status" class="required validate">
                                                                     
                                     <option value="" >Select Category Status</option>
                                    <option value="1" <?php if($categories['status'] == 1){echo "selected"; } ?>>Enable</option>
                                    <option value="0" <?php if($categories['status'] == 0){echo "selected"; } ?> >Disable</option>
                        

                                                                 </select>
                                                             </div>
                                                            
                                                        <!-- </div> -->
                                                    </div>
                                                
           
                                              <!--     <div class="row">
          <a class="waves-effect waves-grey btn white modal-trigger" href="#uploadimg">Upload more images</a>
                          </div> -->
                                                <!-- </div> -->
       
                                                <input class="waves-effect waves-light btn blue m-b-xs" type="submit" name="" value="submit">
                                   
                                </form>
                         </div>
                </div>
                          
         
                        </div>
                    </div>
                </div>
       

            </main>

    <?php
        if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green', '<?= $this->session->flashdata('ok') ?>');
                });
            </script>
    <?php } ?>        


        <script src="<?= asset_url();?>backend\plugins\jquery-validation\jquery.validate.min.js"></script>
        <script src="<?= asset_url();?>backend\js\alpha.min.js"></script>
      
       
        <script type="text/javascript">
 
   



//validate

    $( document ).ready(function() {
        // add the rule here
       $.validator.setDefaults({
       ignore: []
       });
     // additional method for validate plug-in  letter and space -->
       jQuery.validator.addMethod("lettersonly", function(value, element) 
        {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "Letters and spaces only please");
       
    var validator = $("#update-form").validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); },
        rules: {
                 name: { lettersonly: true },
                
        },
        messages: {

              //  userfile:{  required: "Select Image", }  

         } ,
     /*   submitHandler: function (form) {
                   var file= $('[name="userfile"]').val();
                if(file===''){
                 $('#filenotice').html('<label id="userfile-error" class="error" for="userfile">Please attach file!.</label>');

                }else{
                     form.submit();
                }

                    
                  
                          }  */
        });

   });



        </script>