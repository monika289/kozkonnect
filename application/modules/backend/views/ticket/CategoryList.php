    <!-- Styles -->
       
     
      <main class="mn-inner inner-active-sidebar">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">Ticket Category List</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                    <a href="<?php echo base_url()?>admin/ticket/categorynew" class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i>Add Ticket category</a>
                                <span class="card-title">Ticket Category List</span>
                                <p>All Ticket Category List  </p><br>
                                <table id="example" class="display responsive-table datatable-example">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                            <th style="width: 30%;">Name</th>
                                            <th>Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                   <!--  <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Salary</th>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>
                            <?php $i = 0;
                                foreach($categories as $cat){ 
                              $i++; ?>                                     
                        <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $cat['name'];?></td>
                                    <td>
                                             
                                    <?php if($cat['status']==1){?>
                                     Enable
                                    <?php }else{?>
                                                         Disable
                                                        <?php }?>        
                                    </td>
                                <td><a href="<?php echo base_url();?>admin/ticket/categoryedit/<?php echo $cat['id'];?>" class="waves-effect waves-blue btn-flat m-b-xs"><i class="fa fa-pencil m-r-5"></i> Edit</a></a></td>
                            </tr>                                      
                            <?php } ?> 

                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <div id="edit_ticket_category" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-content modal-md">
                        <div class="modal-body" id="ecategory">
                        </div>
                    </div>
                </div>
            </div>
            </main>
  <!--  <?php
        if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green', '<?= $this->session->flashdata('ok') ?>');
                });
            </script>
    <?php } ?>   --> 
<script>
$(document).ready(function() {
    $('#example').DataTable({
        language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>' 
        }
        }
    });
    $('.dataTables_length select').addClass('browser-default');
});

</script>