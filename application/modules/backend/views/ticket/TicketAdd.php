  <main class="mn-inner">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">Add Ticket</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                  <span class="card-title">Add Ticket</span>
                                <p>Fill up Ticket details .</p>
                                <!-- <a class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i></a> -->
                            <div class="row">
                                <form id="addticket" name="addticket" action="<?php echo base_url();?>admin/ticket/add" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="ticket[userid]" id="userid" value=""/>
                                    
                                      
                                                <!-- <div class="row"> -->
                                                    <!-- <div class="col m6"> -->
                                                    <div class="row">
                                                            <div class="input-field col m6 s12">
                                                                <label for="ticket[mobile]">Customer Mobile</label>
                                                                <input name="ticket[mobile]" id="mobile" type="text" class="required validate">
                                                            </div>
                                                             <div class="input-field col m6 s12">
                                                                <label for="ticket[email]">Email</label>
                                                                <input  name="ticket[email]" id="email" type="email" class="required validate">
                                                            </div>
                                                           
                                                            
                                                        <!-- </div> -->
                                                    </div>
                                    <div class="row">
                                        <div class="input-field col m6 s12">
                                            <label for="ticket[name]">Customer Name</label>
                                            <input name="ticket[name]" id="name" type="text" class="required validate">
                                        </div>
                                       <div class="input-field col m6 s12">
                                            <select name="ticket[category_id]" id="category_id" class="required validate">
                                                <option value="">Select Category</option>
                                               <?php foreach ($categories as $category) { ?>
                                            <option value="<?php echo $category['id'];?>"><?php echo $category['name'];?></option>
                                            <?php } ?>

                                             </select>
                                        </div>
                                                            
                                                        <!-- </div> -->
                                    </div>
                                                    <div class="row">
                                                      
                                                            <div class="input-field col m6 s12">
                                                            <select name="ticket[subcategory_id]" id="subcategory_id" class="required validate">
                                                                <option value="">Select SubCategory</option>
                                                               
                                                             </select>
                                                            </div>
                                                            <div class="input-field col m6 s12">
                                                                <select name="ticket[orderid]" id="orderid" class="required validate">
                                                                <option value="">Cause ID</option>
                                                               
                                                             </select>
                                                             
                                                            </div>
                                                            
                                                            
                                                        <!-- </div> -->
                                                    </div>
                                <div class="row">
                                    <div class="input-field col m6 s12">
                                        <label for="ticket[subject]">Subject</label>
                                        <input name="ticket[subject]" id="subject" type="text" class="required validate">
                                    </div>
                                     <div class="input-field col m6 s12">
                                                                <label for="phone">Comment</label>
                                                                <input name="ticket[description]" id="description" type="tel" class="required validate">
                                     </div>
                                </div>
                                  <div class="row">
                                      <div class="input-field col m6 s12">
                                            <select name="ticket[priority]" id="priority" class="required validate">
                                                <option value="">Priority</option>
                                                <option value="0">Low</option>
                                                <option value="1">Normal</option>
                                                <option value="2">High</option>
                                                <option value="3">Urgent</option>

                                             </select>
                                        </div>
                                     <div class="input-field col m6 s12">
                                            <select name="ticket[assigned_to]" id="assigned_to" class="required validate">
                                                <option value="">Assigned To</option>
                                                <option value="">Select Executive</option>
                                            <?php foreach ($acps as $acp) { ?>
                                            <option value="<?php echo $acp['id'];?>"><?php echo $acp['first_name'];?></option>
                                            <?php }  ?>

                                             </select>
                                        </div>
                                </div>
                             <div class="row">
                                      <div class="input-field col m6 s12">
                                            <select name="ticket[status]" id="status" class="required validate">
                                                <option value="">Status</option>
                                               <?php foreach ($status as $row) { ?>
                                                <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                                            <?php } ?>

                                             </select>
                                        </div>
                                     
                                </div>
                                 <!--    <div class="row">
                                            <div class="file-field input-field">
                                                <div class="btn teal lighten-1">
                                                    <span>File</span>
                                                    <input type="file" name="userfile[]" multiple="multiple">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text" placeholder="Upload one or more files" class="required validate">
                                                </div>
                                            </div>
                                                           
                                                            
                                                        </div>
                                    </div> -->
                                              <!--     <div class="row">
          <a class="waves-effect waves-grey btn white modal-trigger" href="#uploadimg">Upload more images</a>
                          </div> -->
                                                <!-- </div> -->
       
                                                <input class="waves-effect waves-light btn blue m-b-xs" type="submit" name="" value="submit">
                                   
                                </form>
                         </div>
                </div>
                          
         
                        </div>
                    </div>
                </div>
       

            </main>

    <?php
        if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green', '<?= $this->session->flashdata('ok') ?>');
                });
            </script>
    <?php } ?>        


        <script src="<?= asset_url();?>backend\plugins\jquery-validation\jquery.validate.min.js"></script>
        <script src="<?= asset_url();?>backend\js\alpha.min.js"></script>
        <script src="<?= asset_url();?>backend\js\bootstrap-typeahead.min.js"></script>
        <script src="<?= asset_url();?>backend\js\jquery.form.js"></script>
      
       <script type="text/javascript">



//validate

    $( document ).ready(function() {
           // add the rule here
       $.validator.setDefaults({
       ignore: []
       });
       // additional method for validate plug-in  letter and space -->
       jQuery.validator.addMethod("lettersonly", function(value, element) 
        {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "Letters and spaces only please");
       
    var validator = $("#addticket").validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            },
            'name': { lettersonly: true },
            'fname': { lettersonly: true },
            'lname': { lettersonly: true },
            'phone': {
                            required: true,
                            digits: true,
                            rangelength: [10, 10] 
                           },
             'members': {
                          
                            digits: true,
                          
                           },
        },
        massage:{
          'phone': {                       
                        rangelength: 'enter valid mobile number',
                           },

        }
    });

   });



    $("#mobile").typeahead({
    onSelect: function(item) {
        itemvalue = item.value;
        $.get(base_url+"admin/user/detail/"+item.value,{},function(result){
            $("#email").val(result.email);
            $("#name").val(result.fname);
            $("#userid").val(result.id);
          /*  $('#addticket').bootstrapValidator('revalidateField', 'ticket[name]');
            $('#addticket').bootstrapValidator('revalidateField', 'ticket[mobile]');
            $('#addticket').bootstrapValidator('revalidateField', 'ticket[email]');*/
        },'json');
    },
    ajax: {
        url: base_url+"admin/user/bymobile",
        timeout: 500,
        displayField: "mobile",
        triggerLength: 3,
        method: "get",
        loadingClass: "loading-circle",
        preDispatch: function (query) {
            return {
                name: query
            }
        },
        preProcess: function (data) {
            if (data.success === false) {
                return false;
            }
            return data;
        }
    }
    
}); 

$("#name").typeahead({
    onSelect: function(item) {
        itemvalue = item.value;
        $.get(base_url+"admin/user/detail/"+item.value,{},function(result){
            $("#email").val(result.email);
            $("#mobile").val(result.mobile);
            $("#userid").val(result.id);
         /*   $('#addticket').bootstrapValidator('revalidateField', 'ticket[email]');
            $('#addticket').bootstrapValidator('revalidateField', 'ticket[name]');
            $('#addticket').bootstrapValidator('revalidateField', 'ticket[mobile]');  */
        },'json');
    },
    ajax: {
        url: base_url+"admin/user/byname",
        timeout: 500,
        displayField: "name",
        triggerLength: 3,
        method: "get",
        loadingClass: "loading-circle",
        preDispatch: function (query) {
            return {
                name: query
            }
        },
        preProcess: function (data) {
            if (data.success === false) {
                return false;
            }
            return data;
        }
    }
    
});

$("#email").typeahead({
    onSelect: function(item) {
        itemvalue = item.value;
        $.get(base_url+"admin/user/detail/"+item.value,{},function(result){
            $("#name").val(result.name);
            $("#mobile").val(result.mobile);
            $("#userid").val(result.id);
       /*     $('#addticket').bootstrapValidator('revalidateField', 'ticket[name]');
            $('#addticket').bootstrapValidator('revalidateField', 'ticket[mobile]');
            $('#addticket').bootstrapValidator('revalidateField', 'ticket[email]'); */
        },'json');

     
    },
    ajax: {
        url: base_url+"admin/user/byemail",
        timeout: 500,
        displayField: "email",
        triggerLength: 3,
        method: "get",
        loadingClass: "loading-circle",
        preDispatch: function (query) {
            return {
                name: query
            }
        },
        preProcess: function (data) {
            if (data.success === false) {
                return false;
            }
            return data;
        }
    }
    
});

</script>
<script>
$(document).ready(function(){    
    $("#category_id").change(function() 
    {
        var cat_id =  $('#category_id').val();       
         //   console.log(cat_id);        
              $.post(base_url+"admin/ticket/getTicketSubCatId", {cat_id : cat_id}, function(data)
              {     
                  $('#subcategory_id').empty();
                  $('#subcategory_id').append("<option value=''>"+'Select Subcategory'+"</option>");
                        
               if(data.length > 0)
                   {            
                     for( var i=0; i < data.length; i++)
                     {                      
                          $('#subcategory_id').append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");   
                           $("#subcategory_id").material_select()
                                   
                      }     
                   }else{

                     $('#subcategory_id').append("<option value='"+data[0].id+"'>"+data[0].name+"</option>");
                   }       
              },'json'); 

                var userid =  $('#userid').val();       
               // console.log(userid);  
       /*     $.post(base_url+"admin/ticket/getuserorder", {userid : userid}, function(data)
              {
                  $('#orderid').empty();$('#orderid').append("<option value=''>"+'Select Order'+"</option>");           
               if(data.length > 0)
                   {          
                     for( var i=0; i < data.length; i++)
                         {                      
                               $('#orderid').append("<option value='"+data[i].orderid+"'>"+data[i].orderid+"</option>");            
                         }      
                    }      
              },'json');   */
     });
});
</script>