<div class="page-wrapper">
	<div class="content container-fluid">
		<div class="row">
			<div class="col-xs-8">
				<h4 class="page-title">Ticket List</h4>
			</div>
			<div class="col-xs-4 text-right m-b-30">
				 <a href="<?php echo base_url();?>admin/ticket/new" class="btn btn-primary rounded pull-right"><i class="fa fa-plus"></i> Add Ticket</a>
			</div>
		</div>
			<div class="row">
			<div class="col-md-12">
				<div class="table-responsive" id="tblleads">
					<table class="table table-striped custom-table" id="tblticket">
						<thead>
							<tr>
								<th>Sr.No.</th>
								<th>Status</th>
								<th>Priority</th>
								<th>Assigned To</th>
								<th>Contact Name</th>
								<th>Contact Mobile</th>
								<th>Ticket Date</th>
     							<th>Action</th>
							</tr>
						</thead>
							<tbody>			
							<?php if (!empty($tickets)) { ?>
							<?php  foreach($tickets as $ticket){?>
								<tr>
									<td><a href="<?php echo base_url();?>admin/ticket/edit/<?php echo $ticket['ticketid']?>"><?php echo $ticket['ticket_no'];?></a>
									</td>
									<td><?php echo $ticket['status'];?>
									
									</td>
									<td>
									<?php if($ticket['priority'] == 0) { ?>
										Low
										<?php } else if($ticket['priority'] == 1) { ?>
										Normal
										<?php } else if($ticket['priority'] == 2) { ?>
										High
										<?php } else if($ticket['priority'] == 3) { ?>
										Urgent
										<?php } ?>
									</td>
									<td>
										<?php echo $ticket['assigned_to_name'];?>
									</td>
									<td>
										<?php echo $ticket['name'];?>
									</td>
									<td>
										<?php echo $ticket['mobile'];?> 
									</td>
									<td>
										<?php echo date('j M Y',strtotime($ticket['created_date']));?>
									</td>
									 <td class="text-right">
									<div class="dropdown">
										<a href="#" class="action-icon dropdown-toggle"
											data-toggle="dropdown" aria-expanded="false"><i
											class="fa fa-ellipsis-v"></i></a>
										<ul class="dropdown-menu pull-right">
											<li><a href="<?php echo base_url();?>admin/ticket/edit/<?php echo $ticket['ticketid']?>"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
										    <!-- <li><a href="<?php echo base_url();?>admin/ticket/view/<?php echo $ticket['ticketid']?>"><i class="fa fa-pencil m-r-5"></i> View</a></li>-->
										</ul>
									</div>
								</td>
								</tr>
								<?php }?>
							<?php } else { ?>
								<tr><td colspan="4">Records not found.</td></tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
        
 <script>
 $(document).ready(function() {
	    $('#tblticket').DataTable();
	 
	});

 function turnOn(id) {
		$.get(base_url+'admin/category/turnoncat/'+id,{},function(){
			window.location.reload();
		});
	}
	function turnOff(id) {
		
		$.get(base_url+'admin/category/turnoffcat/'+id,{},function(){
			window.location.reload();
		});
	}
 </script>