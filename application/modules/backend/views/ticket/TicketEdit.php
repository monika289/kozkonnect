<div class="page-wrapper">
     <div class="content container-fluid">
     	    <div class="card-box">
     	    	<form id="addticket" name="addticket" action="" method="post" enctype="multipart/form-data">
     	    	 <input type="hidden" name="ticket[ticketid]" id="ticketid" value="<?php echo $ticket['ticketid'];?>"/>
				  <input type="hidden" name="ticket[userid]" id="userid" value="<?php echo $ticket['userid'];?>"/>
					<h3 class="card-title">Edit Ticket</h3>
						<div class="row">
							<div class="col-lg-6">
								<div class="">
				   					<?php echo $ticket['ticket_no'];?>
				   		 		</div>
							</div>
						</div>
						<div class="row">
							  <div class="col-md-6">
                                  <div class="form-group form-focus">
                                     <label class="control-label">Customer Mobile</label>
                                       <input type="text" name="ticket[mobile]" id="mobile" value="<?php echo $ticket['mobile'];?>" class="form-control floating" placeholder="Enter customer mobile number" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group form-focus">
                                     <label class="control-label">Customer Email</label>
                                       <input type="text"  name="ticket[email]" id="email" value="<?php echo $ticket['email1'];?>" class="form-control floating" placeholder="Enter customer email id" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                        </div>
                        <div class="row">
							   <div class="col-md-6">
                                  <div class="form-group form-focus">
                                     <label class="control-label">Customer Name</label>
                                       <input type="text" name="ticket[name]" id="name" value="<?php echo $ticket['name'];?>" class="form-control floating" placeholder="Enter customer name" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                              <div class="col-md-6">
                                    <div class="form-group form-focus select-focus">
                                      <label class="control-label">Select Category</label>
                                        <select name="ticket[category_id]" id="category_id" class="form-control floating">
                                         	<option value="" >Select Category</option>
                                           <?php foreach ($categories as $category) { ?>
											<option value="<?php echo $category['id'];?>" <?php if($ticket['category_id'] == $category['id']) {?>selected<?php }?>><?php echo $category['name'];?></option>
											<?php } ?>
                                        </select> 
                                    </div>
                                     <div class="messageContainer"></div>
                               </div>
                        </div>
                        <div class="row">
							  <div class="col-md-6">
                                    <div class="form-group form-focus select-focus">
                                      <label class="control-label">SubCategory</label>
                                        <select name="ticket[subcategory_id]" id="subcategory_id" class="form-control floating">
                                        	<option value="" >Select SubCategory</option>
                                         	<?php foreach ($subcategories as $subcategory) : ?>
											<option value="<?php echo $subcategory['id'];?>" <?php if($ticket['subcategory_id'] == $subcategory['id']) {?>selected<?php }?>><?php echo $subcategory['name'];?></option>
											<?php endforeach;?>	
                                        </select> 
                                    </div>
                                     <div class="messageContainer"></div>
                               </div>
                               <div class="col-md-6">
                                  <div class="form-group form-focus">
                                     <label class="control-label">Order ID</label>
                                       <input type="text" name="ticket[orderid]" readonly id="orderid" value="<?php if(!empty($ticket['orderid'])){ echo $ticket['orderid'];}?>" class="form-control floating" placeholder="Order number" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                               <div class="col-md-6">
                                    <div class="form-group form-focus select-focus">
                                      <label class="control-label">Order ID</label>
                                        <select  name="ticket[orderid]" id="orderid" class="form-control floating">
                                         	<option value=""> Select Order</option>	
                                         	<option value="<?php if(!empty($ticket['orderid'])){ echo $ticket['orderid'];}?>"><?php ?></option>
                                        </select> 
                                    </div>
                                     <div class="messageContainer"></div>
                               </div>
                        </div>
                        <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group form-focus">
                                     <label class="control-label">Subject</label>
                                       <input type="text"   name="ticket[subject]" id="subject" class="form-control floating" placeholder="Enter subject" value="<?php if(!empty($ticket['subject'])) {echo $ticket['subject'];}?>" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                               <div class="col-md-6">
                         	<div class="form-group form-focus">
                         	 <label class="control-label">Comment</label>
								<textarea  name="ticket[description]" id="description"  class="form-control floating" placeholder="Enter comment"><?php if(!empty($ticket['description'])) {echo $ticket['description'];}?></textarea>
							</div>
							   <div class="messageContainer"></div>
							</div>
                        </div>
                        <div class="row">
                         <div class="col-md-6">
                                    <div class="form-group form-focus select-focus">
                                      <label class="control-label">Priority</label>
                                        <select name="ticket[priority]" id="priority" class="form-control floating">
                                         	<option value="0" <?php if($ticket['priority'] == 0) { ?>selected<?php } ?>>Low</option>
											<option value="1" <?php if($ticket['priority'] == 1) { ?>selected<?php } ?>>Normal</option>
											<option value="2" <?php if($ticket['priority'] == 2) { ?>selected<?php } ?>>High</option>
											<option value="3" <?php if($ticket['priority'] == 3) { ?>selected<?php } ?>>Urgent</option>
                                        </select> 
                                    </div>
                                     <div class="messageContainer"></div>
                               </div>
                                <div class="col-md-6">
                                    <div class="form-group form-focus select-focus">
                                      <label class="control-label">Assigned To</label>
                                        <select name="ticket[assigned_to]" id="assigned_to" class="form-control floating">
                                         	<option value="">Select Executive</option>
											<?php foreach ($acps as $acp) { ?>
											<option value="<?php echo $acp['id'];?>" <?php if($ticket['assigned_to'] == $acp['id']) { ?>selected<?php } ?>><?php echo $acp['first_name'];?> <?php echo $acp['last_name'];?></option>
											<?php } ?>
                                        </select> 
                                    </div>
                                     <div class="messageContainer"></div>
                               </div>
							 
                             
                        </div>
                        <div class="row">
							  <div class="col-md-6">
                                    <div class="form-group form-focus select-focus">
                                      <label class="control-label">Status</label>
                                        <select name="ticket[status]" id="status" class="form-control floating">
                                         	<option value=""> Select status</option>
											<?php foreach ($status as $row) : ?>
												<option value="<?php echo $row['id'];?>" <?php if($ticket['status_id'] == $row['id']) {?>selected<?php }?>><?php echo $row['name'];?></option>
											<?php endforeach;?>
                                        </select> 
                                    </div>
                                     <div class="messageContainer"></div>
                               </div>
                             <div class="col-lg-6">
                       	 <div class="c-field u-mb-medium">
						    <label class="c-field__label">Created By</label>
						    <?php if(!empty($ticket['created_by_name'])) { echo $ticket['created_by_name'];} else { echo 'NA';}?>
						 </div>
                      </div>
                        </div>
                        <div class="row">
                   	   <div class="col-lg-6">
                       	 <div class="c-field u-mb-medium">
						    <label class="c-field__label">Created Date</label>
						    <?php echo date('j M Y h:i A',strtotime($ticket['created_date']));?>
						 </div>
                      </div>
                      <div class="col-lg-6">
                       	 <div class="c-field u-mb-medium">
						    <label class="c-field__label">Updated Date</label>
						    <?php echo date('j M Y h:i A',strtotime($ticket['updated_date']));?>
						 </div>
                      </div>
                   </div>
                               <div class="form-actions text-center" style="margin-top: 30px;">
                                   <!--<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check"></i> Save</button>-->
                                   <button class="btn btn-primary btn-lg" type="submit">update </button>
                                   <!--<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default btn-lg">Cancel</button>-->
                               </div>
                     </form>
		     </div>
		</div>
</div> 
<script src="<?php echo asset_url();?>js/bootstrap-typeahead.min.js"></script>
<script src="<?php echo asset_url();?>js/bootstrapValidator.min.js"></script>
<script src="<?php echo asset_url();?>js/jquery.form.js"></script>
<script>
$('#addticket').bootstrapValidator({
	container: function($field, validator) {
		return $field.parent().next('div.messageContainer');
   	},
    feedbackIcons: {
        validating: 'glyphicon glyphicon-refresh'
    },
    excluded: ':disabled',
    fields: {
    	'ticket[name]': {
            validators: {
                notEmpty: {
                    message: 'Customer Name is required and cannot be empty'
                }
            }
        },
        'ticket[mobile]': {
            validators: {
                notEmpty: {
                    message: 'Customer Mobile is required and cannot be empty'
                },
                regexp: {
                    regexp: '^[7-9][0-9]{9}$',
                    message: 'Invalid Mobile Number'
                }
            }
        },
        'ticket[email]': {
            validators: {
                notEmpty: {
                    message: 'Customer Email is required and cannot be empty'
                },
                regexp: {
                    regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                    message: 'The value is not a valid email address'
                }
            }
        },
        'ticket[subject]': {
            validators: {
            	notEmpty: {
                    message: 'Subject is required and cannot be empty'
                }
            }
        },
        'ticket[description]': {
            validators: {
            	notEmpty: {
                    message: 'Description is required and cannot be empty'
                }
            }
        },
        'ticket[assigned_to]': {
            validators: {
            	notEmpty: {
                    message: 'Assigned To is required and cannot be empty'
                }
            }
        },
        'ticket[category_id]':{
       	 validators: {
            	notEmpty: {
                    message: 'Category is required and cannot be empty'
                }
            }
       },
       'ticket[subcategory_id]':{
       	 validators: {
             	notEmpty: {
                     message: 'Category is required and cannot be empty'
                 }
            }
        },
        'ticket[status]':{
       	 validators: {
            	notEmpty: {
                    message: 'Status is required and cannot be empty'
                }
            }
        }
    }
}).on('success.form.bv', function(event,data) {
	// Prevent form submission
	event.preventDefault();
	addTicket();
});

function addTicket() {
	var options = {
	 		target : '#response', 
	 		beforeSubmit : showAddRequest,
	 		success :  showAddResponse,
	 		url : base_url+'admin/ticket/update',
	 		semantic : true,
	 		dataType : 'json'
	 	};
   	$('#addticket').ajaxSubmit(options);
}

function showAddRequest(formData, jqForm, options){
	$("#response").hide();
   	var queryString = $.param(formData);
	return true;
}
   	
function showAddResponse(resp, statusText, xhr, $form){
	if(resp.status == '0') {
		$("#response").removeClass('alert-success');
       	$("#response").addClass('alert-danger');
		$("#response").html(resp.msg);
		$("#response").show();
  	} else {
  		$("#response").removeClass('alert-danger');
        $("#response").addClass('alert-success');
        $("#response").html(resp.msg);
        $("#response").show();
        alert(resp.msg);
        window.location.href = base_url+"admin/ticket/tickets";
  	}
}

$("#mobile").typeahead({
    onSelect: function(item) {
        itemvalue = item.value;
        $.get(base_url+"admin/user/detail/"+item.value,{},function(result){
        	$("#email").val(result.email);
			$("#name").val(result.name);
			$("#userid").val(result.id);
			$('#addticket').bootstrapValidator('revalidateField', 'ticket[name]');
			$('#addticket').bootstrapValidator('revalidateField', 'ticket[mobile]');
			$('#addticket').bootstrapValidator('revalidateField', 'ticket[email]');
        },'json');
    },
    ajax: {
        url: base_url+"admin/user/bymobile",
        timeout: 500,
        displayField: "mobile",
        triggerLength: 3,
        method: "get",
        loadingClass: "loading-circle",
        preDispatch: function (query) {
            return {
            	name: query
            }
        },
        preProcess: function (data) {
            if (data.success === false) {
                return false;
            }
            return data;
        }
    }
    
});	

$("#name").typeahead({
    onSelect: function(item) {
        itemvalue = item.value;
        $.get(base_url+"admin/user/detail/"+item.value,{},function(result){
        	$("#email").val(result.email);
			$("#mobile").val(result.mobile);
			$("#userid").val(result.id);
			$('#addticket').bootstrapValidator('revalidateField', 'ticket[email]');
			$('#addticket').bootstrapValidator('revalidateField', 'ticket[mobile]');
			$('#addticket').bootstrapValidator('revalidateField', 'ticket[name]');
        },'json');
    },
    ajax: {
        url: base_url+"admin/user/byname",
        timeout: 500,
        displayField: "name",
        triggerLength: 3,
        method: "get",
        loadingClass: "loading-circle",
        preDispatch: function (query) {
            return {
            	name: query
            }
        },
        preProcess: function (data) {
            if (data.success === false) {
                return false;
            }
            return data;
        }
    }
    
});

$("#email").typeahead({
    onSelect: function(item) {
        itemvalue = item.value;
        $.get(base_url+"admin/user/detail/"+item.value,{},function(result){
        	$("#name").val(result.name);
			$("#mobile").val(result.mobile);
			$("#userid").val(result.id);
			$('#addticket').bootstrapValidator('revalidateField', 'ticket[email]');
			$('#addticket').bootstrapValidator('revalidateField', 'ticket[name]');
			$('#addticket').bootstrapValidator('revalidateField', 'ticket[mobile]');
        },'json');
    },
    ajax: {
        url: base_url+"admin/user/byemail",
        timeout: 500,
        displayField: "email",
        triggerLength: 3,
        method: "get",
        loadingClass: "loading-circle",
        preDispatch: function (query) {
            return {
            	name: query
            }
        },
        preProcess: function (data) {
            if (data.success === false) {
                return false;
            }
            return data;
        }
    }
    
});

</script>

<script>
/* $(document).ready(function(){    
	$("#model_id").change(function() 
			{
		var model_id =  $('#model_id').val();       
			console.log(model_id);	    
			  $.post(base_url+"admin/subcategorybycatid/", {model_id : model_id}, function(data)
					  {
				  $('#subcategory_id').empty();$('#subcategory_id').append("<option value=''>"+'Select SubCategory'+"</option>");		    
			   if(data.length > 0)
				   {		    
				     for( var i=0; i < data.length; i++)
					     {		   			    
					           $('#subcategory_id').append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");			
					            }	    
			              }	   
	               },'json');   
               });

	$("#category_id").change(function() 
			{
		var cat_id =  $('#category_id').val();       
			console.log(cat_id);	    
			  $.post(base_url+"admin/brandbycatid/", {cat_id : cat_id}, function(data)
					  {
				  $('#brand_id').empty();$('#brand_id').append("<option value=''>"+'Select Brand'+"</option>");		    
			   if(data.length > 0)
				   {		    
				     for( var i=0; i < data.length; i++)
					     {		   			    
					           $('#brand_id').append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");			
					            }	    
			              }	   
	               },'json'); 

				var userid =  $('#userid').val();       
				console.log(userid);  
			  $.post(base_url+"admin/details/", {userid : userid}, function(data)
					  {
				  
				  $('#orderid').empty();$('#orderid').append("<option value=''>"+'Select Order'+"</option>");		    
			   if(data.length > 0)
				   {		  
				     for( var i=0; i < data.length; i++)
					     {		   			    
					           $('#orderid').append("<option value='"+data[i].orderid+"'>"+data[i].orderid+"</option>");			
					            }	    
			              }	   
		           },'json'); 
              
               });

	$("#brand_id").change(function() 
			{
		var brand_id =  $('#brand_id').val();       
			console.log(brand_id);	    
			  $.post(base_url+"admin/modelbybrandid/", {brand_id : brand_id}, function(data)
					  {
				  $('#model_id').empty();$('#model_id').append("<option value=''>"+'Select Model'+"</option>");		    
			   if(data.length > 0)
				   {		    
				     for( var i=0; i < data.length; i++)
					     {		   			    
					           $('#model_id').append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");			
					            }	    
			              }	   
	               },'json');   
               });

	$("#subcategory_id").change(function() 
			{
		var subcat_id =  $('#subcategory_id').val();       
			console.log(subcat_id);	    
			  $.post(base_url+"admin/servicebycatid/", {subcat_id : subcat_id}, function(data)
					  {
				  $('#service_id').empty();$('#service_id').append("<option value=''>"+'Select Service'+"</option>");		    
			   if(data.length > 0)
				   {		    
				     for( var i=0; i < data.length; i++)
					     {		   			    
					           $('#service_id').append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");			
					            }	    
			              }	   
	               },'json');   
               });
    });  */
</script>
<script>
$(document).ready(function(){    
	$("#category_id").change(function() 
	{
		var cat_id =  $('#category_id').val();       
			console.log(cat_id);	    
			  $.post(base_url+"admin/ticket/getTicketSubCatId", {cat_id : cat_id}, function(data)
			  {
				  $('#subcategory_id').empty();$('#subcategory_id').append("<option value=''>"+'Select Subcategory'+"</option>");		    
			   if(data.length > 0)
				   {		    
				     for( var i=0; i < data.length; i++)
					 {		   			    
					      $('#subcategory_id').append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");			
					  }	    
			       }	   
	          },'json'); 

				var userid =  $('#userid').val();       
				console.log(userid);  
			  $.post(base_url+"admin/ticket/getuserorder", {userid : userid}, function(data)
			  {
				  $('#orderid').empty();$('#orderid').append("<option value=''>"+'Select Order'+"</option>");		    
			   if(data.length > 0)
				   {		  
				     for( var i=0; i < data.length; i++)
					     {		   			    
					           $('#orderid').append("<option value='"+data[i].orderid+"'>"+data[i].orderid+"</option>");			
					     }	    
			        }	   
		      },'json'); 
     });
});
</script>