    <!-- Styles -->
       
     
      <main class="mn-inner inner-active-sidebar">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">Ticket List</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                    <a href="<?= base_url('admin/ticket/new'); ?>" class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i>Add Corporate</a>
                                <span class="card-title">Ticket List</span>
                                <p>All Ticket List for reject and  approve</p><br>
                                <table id="example" class="display responsive-table datatable-example">
                                    <thead>
                                        <tr>
                                <th>Sr.No.</th>
								<th>Status</th>
								<th>Priority</th>
								<th>Assigned To</th>
								<th>Contact Name</th>
								<th>Contact Mobile</th>
								<th>Ticket Date</th>
     							<th>Action</th>
                                        </tr>
                                    </thead>
                                   <!--  <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Salary</th>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>
                            
                           <?php if (!empty($tickets)) { ?>
                            <?php  foreach($tickets as $ticket){?>
                                <tr>
                                    <td><a href="<?php echo base_url();?>admin/ticket/edit/<?php echo $ticket['ticketid']?>"><?php echo $ticket['ticket_no'];?></a>
                                    </td>
                                    <td><?php echo $ticket['status'];?>
                                    
                                    </td>
                                    <td>
                                    <?php if($ticket['priority'] == 0) { ?>
                                        Low
                                        <?php } else if($ticket['priority'] == 1) { ?>
                                        Normal
                                        <?php } else if($ticket['priority'] == 2) { ?>
                                        High
                                        <?php } else if($ticket['priority'] == 3) { ?>
                                        Urgent
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php echo $ticket['assigned_to_name'];?>
                                    </td>
                                    <td>
                                        <?php echo $ticket['name'];?>
                                    </td>
                                    <td>
                                        <?php echo $ticket['mobile'];?> 
                                    </td>
                                    <td>
                                        <?php echo date('j M Y',strtotime($ticket['created_date']));?>
                                    </td>

                                    <td><a href="<?php echo base_url();?>admin/ticket/edit/<?php echo $ticket['ticketid']?>" class="waves-effect waves-blue btn-flat m-b-xs"><i class="fa fa-pencil m-r-5"></i>edit</a></a></td>
                                  <!--    <td class="text-right">
                                    <div class="dropdown">
                                        <a href="#" class="action-icon dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false"><i
                                            class="fa fa-ellipsis-v"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="<?php echo base_url();?>admin/ticket/edit/<?php echo $ticket['ticketid']?>"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
                                            <!-- <li><a href="<?php echo base_url();?>admin/ticket/view/<?php echo $ticket['ticketid']?>"><i class="fa fa-pencil m-r-5"></i> View</a></li>-->
                                        <!-- </ul>
                                    </div>
                                </td> -->
                                </tr>
                                <?php }  }?>           
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
  <!--  <?php
        if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green', '<?= $this->session->flashdata('ok') ?>');
                });
            </script>
    <?php } ?>   --> 
<script>
$(document).ready(function() {
    $('#example').DataTable({
        language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>' 
        }
        }
    });
    $('.dataTables_length select').addClass('browser-default');
});
</script>