    <!-- Styles -->
       
     
      <main class="mn-inner inner-active-sidebar">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title" >Sub Category List</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                    <a href="<?= base_url('admin/subcategory/new'); ?>" class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i>Add Cause Sub Category</a>
                                <span class="card-title">Sub Category List</span>
                                <p>All Sub Category list for edit add</p><br>
                                <table id="example" class="display responsive-table datatable-example">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                            <th> Parent Category</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                           
                                        </tr>
                                    </thead>
                                   <!--  <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Salary</th>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>
                           	<?php $i = 0;
								foreach($subcategory as $cat) { 
			                  $i++; ?>	 
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $cat['catName'];?></td>
                                            <td><?php echo  $cat['name'];?></td>
                                            <td><?php echo ($cat['status'] ? 'Enable' : 'Disable');?></td>
                                            <td><a href="<?php echo base_url();?>admin/subcategory/edit/<?php echo $cat['id'];?>" class="waves-effect waves-blue btn-flat m-b-xs"><i class="fa fa-pencil m-r-5"></i> Edit</a></a></td>
                                        </tr>
                            <?php } ?>            
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
  <!--  <?php
        if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green', '<?= $this->session->flashdata('ok') ?>');
                });
            </script>
    <?php } ?>   --> 
<script>
$(document).ready(function() {
    $('#example').DataTable({
        language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>' 
        }
        }
    });
    $('.dataTables_length select').addClass('browser-default');
});
</script>