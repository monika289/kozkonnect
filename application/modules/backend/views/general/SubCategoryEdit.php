  <main class="mn-inner">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">Edit Cause Subcategory</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                  <span class="card-title">Edit Cause Subcategory</span>
                                <p></p>
                                <!-- <a class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i></a> -->
                            <div class="row">
                                <form id="asubcat-form" action="<?php echo base_url();?>admin/subcategory/update" method="POST" >
                                     <input type="hidden" id="subcatid" name="subcatid" value="<?php echo $subcategory['id']?>"> 
                                    
                                      
                                                <!-- <div class="row"> -->
                                                    <!-- <div class="col m6"> -->
                                                        <div class="row">
                                                          <div class="input-field col m6 s12">
                                                                <select id="catid" name="catid" class="required validate" >
                                                                     <option value=""  required>Select Category</option>
                                                                     
                                          <?php foreach($catlist as $source){?>
                                                  <option value="<?php echo $source['id'];?>" <?php if($source['id'] == $subcategory['parent_id']){?>selected<?php }?>><?php echo $source['sdgname'];?></option>
                                            <?php }?>
                                                                    
                                                                 </select>
                                                            </div>
                                                            <div class="input-field col m6 s12">
                                                                <label for="name">Subcategory Name</label>
                                                                <input id="name" name="name" type="text" class="required validate" value="<?php echo $subcategory['name']?>">
                                                            </div>
                                                             
                                                            
                                                            
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="row">
                                                      <div class="input-field col m6 s12">
                                                                <select id="status" name="status" class="required validate">
                                                                     <option value=""  required>Select Status</option>
                                                                    
                                    <option value="1" <?php if($subcategory['status'] == 1){echo "selected"; } ?>>Enable</option>
                                    <option value="0" <?php if($subcategory['status'] == 0){echo "selected"; } ?> >Disable</option>

                                                                 </select>
                                                      </div>
                                                          <div class="input-field col m6 s12">
                                                                <select id="is_active" name="is_active" class="required validate">
                                                                     <option value="" required>Select is active</option>
                                                                     <option value="1" <?php if($subcategory['is_active'] == 1){echo "selected"; } ?>>Yes</option>
                                                                     <option value="0" <?php if($subcategory['is_active'] == 0){echo "selected"; } ?>>No</option>
                                                                    
                                                                 </select>
                                                            </div>
                                                          
                                                            
                                                        <!-- </div> -->
                                                  </div>
                                                  <div class="row">
                                                   
                                                            <div class="input-field col m6 s12">
                                                                <label for="sort_order">Sort order</label>
                                                                <input id="sort_order" name="sort_order" type="text" class="required validate" value="<?php echo $subcategory['sort_order']?>">
                                                            </div>
                                                          
                                                            
                                                        <!-- </div> -->
                                                  </div>
                               
                                              <!--     <div class="row">
          <a class="waves-effect waves-grey btn white modal-trigger" href="#uploadimg">Upload more images</a>
                          </div> -->
                                                <!-- </div> -->
       
                                                <input class="waves-effect waves-light btn blue m-b-xs" type="submit" name="" value="submit">
                                   
                                </form>
                         </div>
                </div>
                          
         
                        </div>
                    </div>
                </div>
       

            </main>

         


        <script src="<?= asset_url();?>backend\plugins\jquery-validation\jquery.validate.min.js"></script>
        <script src="<?= asset_url();?>backend\js\alpha.min.js"></script>
      
      
        <script type="text/javascript">
 



//validate

    $( document ).ready(function() {

      $.validator.setDefaults({
       ignore: []
});
        // add the rule here
 $('select').material_select();
       
    var validator = $("#asubcat-form").validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); },
        rules: {
               sort_order:{ number: true,                   
                          },
              },
              messages: {
               //status:   "Please select an item!",
              } ,
    /*  submitHandler: function (form) {
                     console.log('test');
                  //   form.submit();
                          }*/
                    
   
 });
   });



        </script>