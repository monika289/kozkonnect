     	     	<form action="" id="update-form"  name="update-form" method="post">
     	     	  <input type="hidden" id="cat_id" name="cat_id" value="<?php echo $category['id']?>"> 
					<h3 class="card-title">Edit Category</h3>
							<div class="row">
							  <div class="col-md-6">
                                  <div class="form-group form-focus focused">
                                     <label class="control-label">Category Name</label>
                                       <input type="text" id="uname" name="uname" value="<?php echo $category['name'];?>" class="form-control floating" placeholder="category name" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                              <div class="col-md-6">
					            <div class="form-group form-focus select-focus focused">
					               <label class="control-label">Status</label>
					               <select class="select form-control floating" id="ustatus" name="ustatus" required>
					                <option value="" >Select Category Status</option>
                                    <option value="1" <?php if($category['status'] == 1){echo "selected"; } ?>>Enable</option>
                                    <option value="0" <?php if($category['status'] == 0){echo "selected"; } ?> >Disable</option>
					               </select>
					             </div>
					          </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
					            <div class="form-group form-focus select-focus">
					               <label class="control-label">is active</label>
					               <select class="select form-control floating" id="uis_active" name="uis_active" required>
					                <option value="" >Select is active</option>
                                    <option value="1" <?php if($category['is_active'] == 1){echo "selected"; } ?>>Yes</option>
                                    <option value="0" <?php if($category['is_active'] == 0){echo "selected"; } ?> >No</option>
					               </select>
					             </div>
					          </div>
					            <div class="col-md-6">
                                  <div class="form-group form-focus focused">
                                     <label class="control-label">Sort order</label>
                                       <input type="text" id="usort_order" name="usort_order" class="form-control floating" placeholder="enter sort order" value="<?php echo $category['sort_order'];?>" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                            </div>
                            <div class="form-actions text-center" style="margin-top: 30px;">
                                   <!--<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check"></i> Save</button>-->
                                   <button class="btn btn-primary btn-lg" type="submit">Update </button>
                                   <!--<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default btn-lg">Cancel</button>-->
                            </div>
                     </form>
<script>
$('#update-form').bootstrapValidator({
	container: function($field, validator) {
		return $field.parent().next('.messageContainer');
   	},
    feedbackIcons: {
        validating: 'glyphicon glyphicon-refresh'
    },
    excluded: ':disabled',
    fields: {
  		uname: {
            validators: {
                notEmpty: {
                    message: 'Category Name is required and cannot be empty.'
                }
            }
        }, 
        ustatus:{
        	 validators: {
                 notEmpty: {
                     message: 'Please select category status.'
                 }
             }
        },
        uis_active:{
       	 validators: {
                notEmpty: {
                    message: 'Please select is active.'
                }
            }
       },
       usort_order:{
    	   validators: {
               notEmpty: {
                   message: 'Sort order is required and cannot be empty'
               },
               numeric: {
                	message: 'Sort order is invalid',
                   thousandsSeparator: '',
                   decimalSeparator: '.'
             	}
           }
       }
        
    }
}).on('success.form.bv', function(event,data) {
	event.preventDefault();
	updatecategory();
});
function updatecategory() {
	ajaxindicatorstart("Loading...");
	$.post(base_url+"admin/category/update", {id :$("#cat_id").val(),  name: $("#uname").val(),  status : $("#ustatus").val(),is_active:$("#uis_active").val(),sort_order:$("#usort_order").val() }, function(data){
		if(data.status == 1) {
			alert(data.msg);
			window.location.href = base_url+"admin/category/list";
		} else {
			alert(data.msg);
		}
		ajaxindicatorstop();
	},'json');
}
</script>