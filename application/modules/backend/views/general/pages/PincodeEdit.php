     	     	<form action="" id="update-form"  name="update-form" method="post">
     	     	  <input type="hidden" id="pin_id" name="pin_id" value="<?php echo $pincode['id']?>"> 
					<h3 class="card-title">Edit Pincode</h3>
							<div class="row">
							  <div class="col-md-6">
                                  <div class="form-group form-focus">
                                     <label class="control-label">Pincode</label>
                                       <input type="text" id="upincode" name="upincode" maxlength="6" value="<?php echo $pincode['pincode'];?>" class="form-control floating" placeholder="enter pincode" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                               <div class="col-md-6">
                                  <div class="form-group form-focus">
                                     <label class="control-label">Delevery message</label>
                                       <input type="text" id="umsg" name="umsg" value="<?php echo $pincode['message'];?>" class="form-control floating" placeholder="enter delivery message" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                             
                            </div>
                             <div class="row">
	                            <div class="col-md-6">
						            <div class="form-group form-focus select-focus">
						               <label class="control-label">Payment mode</label>
						               <select class="select form-control floating" id="upaymentmode" name="upaymentmode">
						                 <option value="1" <?php if($pincode['payment_mode'] == 1){?>selected<?php }?>>Online</option>
						                 <option value="0" <?php if ($pincode['payment_mode'] == 0){?>selected<?php }?>>COD</option>
						               </select>
						             </div>
						             <div class="messageContainer"></div>
						        </div>
					            <div class="col-md-6">
                                  <div class="form-group form-focus">
                                     <label class="control-label">Delivery charges</label>
                                       <input type="text" id="ucharges" name="ucharges" value="<?php echo $pincode['delivery_charges'];?>" class="form-control floating" placeholder="enter deleivery charges" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
					            <div class="form-group form-focus select-focus">
					               <label class="control-label">is active</label>
					               <select class="select form-control floating" id="uis_active" name="uis_active">
					                 <option value="1" <?php if($pincode['is_active'] == 1){?>selected<?php }?>>Yes</option>
					                 <option value="0" <?php if($pincode['is_active'] == 0){?>selected<?php }?>>No</option>
					               </select>
					             </div>
					              <div class="messageContainer"></div>
					          </div>
					           <div class="col-md-6">
					            <div class="form-group form-focus select-focus">
					               <label class="control-label">Status</label>
					               <select class="select form-control floating" id="ustatus" name="ustatus">
					                <option value="0" <?php if($pincode['status'] == 0){?>selected<?php }?>>Disable</option>
					                 <option value="1" <?php if($pincode['status'] == 1){?>selected<?php }?>>Enable</option>
					               </select>
					             </div>
					              <div class="messageContainer"></div>
					          </div>
                            </div>
                            <div class="form-actions text-center" style="margin-top: 30px;">
                                   <!--<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check"></i> Save</button>-->
                                   <button class="btn btn-primary btn-lg" type="submit">Update </button>
                                   <!--<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default btn-lg">Cancel</button>-->
                            </div>
                     </form>
<script>
$('#update-form').bootstrapValidator({
	container: function($field, validator) {
		return $field.parent().next('.messageContainer');
   	},
    feedbackIcons: {
        validating: 'glyphicon glyphicon-refresh'
    },
    excluded: ':disabled',
    fields: {
  		upincode: {
            validators: {
                notEmpty: {
                    message: 'pincode is required and cannot be empty.'
                },
              	regexp: {
                    regexp: '^[0-9]{6}$',
                    //regexp: '^[0-9]{6,14}$',
                    message: 'Invalid Pincode'
                }, 
              	 stringLength: {
                     max: 6,
                     min: 6,
                     message: 'Pincode must be 6 digit.'
                 },
            }
        }, 
      
       umsg:{
    	   validators: {
               notEmpty: {
                   message: 'Delivery message is required and cannot be empty'
               }
             
           }
       },
       ucharges : {
    	   validators: {
               notEmpty: {
                   message: 'Delivery charges is required and cannot be empty'
               },
      	  numeric: {
	        	message: 'Invalid delivery charges',
	           thousandsSeparator: '',
	           decimalSeparator: '.'
     		 },
           }
       }
    }
}).on('success.form.bv', function(event,data) {
	event.preventDefault();
	updatecategory();
});
function updatecategory() {
	ajaxindicatorstart("Loading...");
	$.post(base_url+"admin/pincode/update", {id :$("#pin_id").val(),  pincode: $("#upincode").val(),  status : $("#ustatus").val(),is_active:$("#uis_active").val(),msg: $("#umsg").val(),charges : $("#ucharges").val(),paymentmode : $("#upaymentmode").val() }, function(data){
		if(data.status == 1) {
			alert(data.msg);
			window.location.href = base_url+"admin/pincode/list";
		} else {
			alert(data.msg);
		}
		ajaxindicatorstop();
	},'json');
}
</script>