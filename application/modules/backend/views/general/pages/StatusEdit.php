     	     	<form action="" id="ustatus-form" name="ustatus-form" method="post">
     	     	  <!-- <input type="hidden" id="cat_id" name="cat_id" value="<?php echo $category['id']?>">-->
     	     	   <input type="hidden" id="ustatus_id" name="ustatus_id" value="<?php echo $leadStatus['id']?>">  
					<h3 class="card-title">Edit Lead Status</h3>
							<div class="row">
							  <div class="col-md-6">
                                  <div class="form-group form-focus focused">
                                     <label class="control-label">Status Name</label>
                                       <input type="text" id="uname" name="uname" value="<?php echo $leadStatus['status_name'];?>" class="form-control floating" placeholder="eg: invalid, in process, closed etc." autocomplete="off">
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                              <div class="col-md-6">
					            <div class="form-group form-focus select-focus focused">
					               <label class="control-label">Status</label>
					               <select class="select form-control floating" id="ustatus" name="ustatus" required>
					                <option value="" >Select Category Status</option>
                                    <option value="1" <?php if($leadStatus['is_active'] == 1){echo "selected"; } ?>>Enable</option>
                                    <option value="0" <?php if($leadStatus['is_active'] == 0){echo "selected"; } ?> >Disable</option>
					               </select>
					             </div>
					          </div>
                            </div>
                           
                            <div class="form-actions text-center" style="margin-top: 30px;">
                                   <!--<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check"></i> Save</button>-->
                                   <button class="btn btn-primary btn-lg" type="submit">Update </button>
                                   <!--<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default btn-lg">Cancel</button>-->
                            </div>
                     </form>
<script>
$('#ustatus-form').bootstrapValidator({
	container: function($field, validator) {
		return $field.parent().next('.messageContainer');
   	},
    feedbackIcons: {
        validating: 'glyphicon glyphicon-refresh'
    },
    excluded: ':disabled',
    fields: {
  		uname: {
            validators: {
                notEmpty: {
                    message: 'Lead status is required and cannot be empty'
                }
            }
        }, 
        
    }
}).on('success.form.bv', function(event,data) {
	event.preventDefault();
	updateNewStatus();
});
function updateNewStatus() {
	ajaxindicatorstart("Loading...");
	$.post(base_url+"admin/status/update", {id :$("#ustatus_id").val(),  name: $("#uname").val(),  status : $("#ustatus").val() }, function(data){
		if(data.status == 1) {
			alert(data.msg);
			ajaxindicatorstop();
			window.location.href = base_url+"admin/status/list";
			
		} else {
			alert(data.msg);
			$("#profile_response").show();
			$("#profile_response").html(data.msg);
			ajaxindicatorstop();
		}
	},'json');
}
</script>