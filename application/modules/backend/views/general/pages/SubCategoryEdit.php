     	     	<form action="" id="usubcat-form"  method="post">
     	     	  <input type="hidden" id="subcatid" name="subcatid" value="<?php echo $subcategory['id']?>"> 
					<h3 class="card-title">Edit Sub Category</h3>
							<div class="row">
							<div class="col-md-6">
                                    <div class="form-group form-focus select-focus">
                                      <label class="control-label">Select Category</label>
                                        <select id="ucatid" name="ucatid" class="select form-control floating" required>
                                         	<option value="" >Select Category</option>
                                           	<?php foreach($catlist as $source){?>
                                                  <option value="<?php echo $source['id'];?>" <?php if($source['id'] == $subcategory['parent_id']){?>selected<?php }?>><?php echo $source['name'];?></option>
                                            <?php }?>
                                        </select> 
                                    </div>
                                     <div class="messageContainer"></div>
                               </div>
							  <div class="col-md-6">
							  	<div>
                                  <div class="form-group form-focus focused">
                                     <label class="control-label">Name</label>
                                       <input type="text" id="uname" name="uname" value="<?php echo $subcategory['name']?>" class="form-control floating" placeholder="subcategory name" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              	</div>
                              </div>
                              </div>
                              <div class="row">
                              <div class="col-md-6">
                              	<div>
					            <div class="form-group form-focus select-focus">
					               <label class="control-label">Status</label>
					               <select class="select form-control floating" id="ustatus" name="ustatus" required>
					                <option value="" >Select SubCategory Status</option>
                                    <option value="1" <?php if($subcategory['status'] == 1){echo "selected"; } ?>>Enable</option>
                                    <option value="0" <?php if($subcategory['status'] == 0){echo "selected"; } ?> >Disable</option>
					               </select>
					             </div>
					              <div class="messageContainer"></div>
					             </div>
					          </div>
					           <div class="col-md-6">
                              	<div>
					            <div class="form-group form-focus select-focus">
					               <label class="control-label">is active</label>
					               <select class="select form-control floating" id="uis_active" name="uis_active" required>
					                <option value="" >Select is active</option>
                                    <option value="1" <?php if($subcategory['is_active'] == 1){echo "selected"; } ?>>Yes</option>
                                    <option value="0" <?php if($subcategory['is_active'] == 0){echo "selected"; } ?> >No</option>
					               </select>
					             </div>
					              <div class="messageContainer"></div>
					             </div>
					          </div>
                            </div>
                            <div class="row">
                             <div class="col-md-6">
                                  <div class="form-group form-focus focused">
                                     <label class="control-label">Sort order</label>
                                       <input type="text" id="usort_order" name="usort_order"  value="<?php echo $subcategory['sort_order'];?> " class="form-control floating" placeholder="enter sort order" autocomplete="off">   
                                   </div>
                              	   <div class="messageContainer"></div>
                              </div>
                            </div>
                               <div class="form-actions text-center" style="margin-top: 30px;">
                                   <!--<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check"></i> Save</button>-->
                                   <button class="btn btn-primary btn-lg" type="submit">Update </button>
                                   <!--<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default btn-lg">Cancel</button>-->
                               </div>
                     </form>
   <script type="text/javascript" src="<?php echo asset_url();?>js/bootstrapValidator.min.js" ></script>
<script>
$('#usubcat-form').bootstrapValidator({
	container: function($field, validator) {
		return $field.parent().next('.messageContainer');
   	},
    feedbackIcons: {
        validating: 'glyphicon glyphicon-refresh'
    },
    excluded: ':disabled',
    fields: {
    	ucatid:{
    		 validators: {
                 notEmpty: {
                     message: 'Please select category name.'
                 }
             }
        },
  		uname:{
            validators: {
                notEmpty: {
                    message: 'Subcategory Name is required and cannot be empty.'
                }
            }
        }, 
        ustatus:{
        	 validators: {
                 notEmpty: {
                     message: 'Please select subcategory status.'
                 }
             }
        },
        uis_active:{
       	 validators: {
                notEmpty: {
                    message: 'Please select is active.'
                }
            }
       },
       usort_order:{
    	   validators: {
               notEmpty: {
                   message: 'Sort order is required and cannot be empty'
               },
               numeric: {
                	message: 'Sort order is invalid',
                   thousandsSeparator: '',
                   decimalSeparator: '.'
             	}
           }
       }
        
    }
}).on('success.form.bv', function(event,data) {
	event.preventDefault();
	addNewStatus();
});
function addNewStatus() {
	ajaxindicatorstart("Loading...");
	$.post(base_url+"admin/subcategory/update", {id:$("#subcatid").val(),catid :$("#ucatid").val(),  name: $("#uname").val(),  status : $("#ustatus").val(),is_active:$("#uis_active").val(),sort_order:$("#usort_order").val() }, function(data){
		if(data.status == 1) {
			
			alert(data.msg);
			window.location.href = base_url+"admin/subcategory/list";
		} else {
			alert(data.msg);
		}
		ajaxindicatorstop();
	},'json');
}
</script>