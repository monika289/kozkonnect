  <main class="mn-inner">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">Edit SGD</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                  <span class="card-title">Edit SGD</span>
                                <p>Fill up details and attach document if required.</p>
                                <!-- <a class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i></a> -->
                            <div class="row">
                                <form id="editsdg" action="<?php echo base_url();?>admin/sdg/editsdg" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="sdgid" value="<?php echo $sdgs['id']?>">
                                    
                                      
                                                <!-- <div class="row"> -->
                                                    <!-- <div class="col m6"> -->
                                                        <div class="row">
                                                            <div class="input-field col m6 s12">
                                                                <label for="name">SGD Name</label>
                                                                <input id="name" name="name" type="text" class="required validate" value="<?php echo $sdgs['sdgname']?>">
                                                            </div>
                                                            <div class="input-field col m6 s12">
                                                                <label for="description">Description</label>
                                                                <input id="description" name="description" type="text" class="required validate" value="<?php echo $sdgs['description']?>">
                                                            </div>
                                                            
                                                        <!-- </div> -->
                                                    </div>
                                                <div class="row">
                                                            <div class="input-field col m6 s12">
                                                                <label for="sdgurl">SDG url</label>
                                                                <input id="sdgurl" name="sdgurl" type="url" class="required validate" data-msg="Please enter full url like http://xxxx" value="<?php echo $sdgs['sdgurl']?>">
                                                            </div>
                                                            <div class="input-field col m6 s12">
                                                                <select id="status" name="status" class="required validate">
                                                                     <option value="" required>Select Status</option>
                                                                      <option value="1" <?php if($sdgs['status'] == 1){echo "selected"; } ?>>Enable</option>
                                                                     <option value="0" <?php if($sdgs['status'] == 0){echo "selected"; }?>>Disable</option>

                                                                 </select>
                                                             </div>
                                                                   
                                                        <!-- </div> -->
                                                </div>
                    <div class="row">
            
                        <div class="input-field col m3 s9">
                          <?php if(!empty($sdgs['logo_url']))   : ?>
                         <img src="<?php echo asset_url().'backend/sdglogos/'.$sdgs['logo_url']; ?>" height="100" width="100"  />
                         <span>current logo Image</span>
                         <?php   $style='style="padding-top: 3em;"';
                          else :  $style=""; ?>
                         <span>No logo uploaded</span>
                         <?php endif;  ?>
                        </div>

                       <div class="input-field col m9 s12" >
                                            <div class="file-field input-field" <?= $style ?>>
                                                <div class="btn teal lighten-1">
                                                    <span>Edit Logo</span>
                                                    <input type="file" name="userfile">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text" name="userfile" placeholder="Upload logo img">
                                                     <small>image dimensions must must less than 400*400 px  size less than 2mb 
                                                 </small>
                                                </div>
                                            </div>
                    
                      </div>
                    </div> 
                                              <!--     <div class="row">
          <a class="waves-effect waves-grey btn white modal-trigger" href="#uploadimg">Upload more images</a>
                          </div> -->
                                                <!-- </div> -->
       
                                                <input class="waves-effect waves-light btn blue m-b-xs" type="submit" name="" value="submit">
                                   
                                </form>
                         </div>
                </div>
                          
         
                        </div>
                    </div>
                </div>
       

            </main>

          


        <script src="<?= asset_url();?>backend\plugins\jquery-validation\jquery.validate.min.js"></script>
        <script src="<?= asset_url();?>backend\js\alpha.min.js"></script>
      
       
        <script type="text/javascript">
 
   



//validate

    $( document ).ready(function() {
        // add the rule here
       $.validator.setDefaults({
       ignore: []
       });


   // additional method for validate plug-in  letter and space -->
       jQuery.validator.addMethod("lettersonly", function(value, element) 
        {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "Letters and spaces only please");     
       
    var validator = $("#editsdg").validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); },
         rules: {
                 name: { lettersonly: true },
                
        },
        messages: {

              //  userfile:{  required: "Select Image", }  

         } ,
    });

   });



        </script>