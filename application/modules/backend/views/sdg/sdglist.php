    <!-- Styles -->
       
     
      <main class="mn-inner inner-active-sidebar">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">SDG List</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                    <a href="<?= base_url('admin/sdgnew'); ?>" class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i>Add SDG</a>
                                <span class="card-title">SDG List</span>
                                <p>All SDG list  </p><br>
                                <table id="example" class="display responsive-table datatable-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>SDG Name</th>
                                            <th>status</th>
                                            <th>Create date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                   <!--  <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Salary</th>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>
                            <?php     $i = 0;
                            if(!empty($sdgs)){
                             foreach ($sdgs as $sdg){     $i++;
                                ?> 
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $sdg['sdgname'];?></td>
                                        
                                            <td><?php echo ($sdg['status'] ? 'Enable' : 'Disable');?></td>
                                            <td><?php echo date("jS M' y", strtotime($sdg['create_date']));  ?> </td>
                                            <td><a href="<?php echo base_url();?>admin/sdgnew/<?php echo $sdg['id'];?>" class="waves-effect waves-blue btn-flat m-b-xs"><i class="fa fa-pencil m-r-5"></i> Edit</a></a></td>
                                        </tr>
                            <?php } } ?>            
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
  <!--  <?php
        if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green', '<?= $this->session->flashdata('ok') ?>');
                });
            </script>
    <?php } ?>   --> 
<script>
$(document).ready(function() {
    $('#example').DataTable({
        language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>' 
        }
        }
    });
    $('.dataTables_length select').addClass('browser-default');
});
</script>