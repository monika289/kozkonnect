    <!-- Styles -->
       
     
      <main class="mn-inner inner-active-sidebar">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">Institution List</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                    <a href="<?= base_url('admin/institution/new'); ?>" class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i>Add Institution</a>
                                <span class="card-title">Institution List</span>
                                <p>All Institution list for edit approve</p><br>
                                <table id="example" class="display responsive-table datatable-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Institute Name</th>
                                            <th>Training provided</th>
                                            <th>mobile</th>
                                            <th>Create date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                   <!--  <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Salary</th>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>
                            <?php
                            if(!empty($users)){
                             foreach ($users as $users){?> 
                                        <tr>
                                            <td><?php echo $users['id'];?></td>
                                            <td><?php echo $users['company_name'];?></td>
                                            <td><?php echo $users['contribution_type'];?></td>
                                            <td><?php echo $users['mobile'];?></td>
                                            <td><?php echo date("jS M' y", strtotime($users['created_on'])); ?></td>
                                            <td><a href="<?php echo base_url();?>admin/institution/add/<?php echo $users['id'];?>" class="waves-effect waves-blue btn-flat m-b-xs"><i class="fa fa-pencil m-r-5"></i> Edit</a></a></td>
                                        </tr>
                            <?php } } ?>            
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
  <!--  <?php
        if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green', '<?= $this->session->flashdata('ok') ?>');
                });
            </script>
    <?php } ?>   --> 
<script>
$(document).ready(function() {
    $('#example').DataTable({
        language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>' 
        }
        }
    });
    $('.dataTables_length select').addClass('browser-default');
});
</script>