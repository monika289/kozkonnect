  <main class="mn-inner">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">Add Corporate</div>
                    </div>
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                  <span class="card-title">Add Corporate</span>
                                <p>Fill up details and attach document if required.</p>
                                <!-- <a class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">add</i></a> -->
                            <div class="row">
                                <form id="addngo" action="<?php echo base_url();?>admin/org/edit" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="user_id" value="<?=$user[0]['id'] ?>">
                                    
                                      
                                                <!-- <div class="row"> -->
                                                    <!-- <div class="col m6"> -->
                                                        <div class="row">
                                                            <div class="input-field col m6 s12">
                                                                <label for="name">Corporate Name</label>
                                                                <input id="name" name="name" type="text" class="required validate" value="<?=$user[0]['company_name'] ?>">
                                                            </div>
                                                            <div class="input-field col m6 s12">
                                                                <label for="org_help">how you contribute</label>
                                                                <input id="org_help" name="org_help" type="text" class="required validate" value="<?=$user[0]['contribution_type'] ?>">
                                                            </div>
                                                            
                                                            
                                                        <!-- </div> -->
                                                   <div class="row">
                                                        <?php  $address=explode("|",$user[0]['address']);  ?>
                                                            <div class="input-field col m6 s12">
                                                                <label for="address1">Address</label>
                                                                <input id="address1" name="address1" type="text" class="required validate" value="<?= $address[0]; ?>">
                                                            </div>
                                                            <div class="input-field col m6 s12">
                                                                <label for="locality"></label>
                                                                <input id="locality" name="locality" type="text" class="required validate"
                                                                value="<?= $address[1]; ?>">
                                                            </div>
                                                           
                                                            
                                                        <!-- </div> -->
                                                  </div>
                            <div class="row">
                                    
                                     <div class="input-field col m6 s12">
                                        <label for="members">Number of members</label>
                                        <input id="members" name="members" type="text" class="required validate" value="<?=$user[0]['company_size'] ?>">
                                    </div>
                                      <p >Responsible Person</p>
                                    <div class="input-field col m6 s12">
                                        <label for="members">Designation</label>
                                        <input id="designation" name="designation" type="text" class="required validate" value="<?=$user[0]['designation'] ?>">
                                    </div>
                            </div>
                                <div class="row">
                                 
                                    <div class="input-field col m6 s12">
                                        <label for="email">Email</label>
                                        <input id="email" name="email" type="email" class="required validate" value="<?=$user[0]['email'] ?>">
                                    </div>
                                     <div class="input-field col m6 s12">
                                                                <label for="phone">Phone number</label>
                                                                <input id="phone" name="phone" type="tel" class="required validate" value="<?=$user[0]['mobile'] ?>">
                                     </div>
                                </div>
                <div class="row">
                     <div class="input-field col m3 s9">
                          <?php if(!empty($logo=$user[0]['profile_img']))   : ?>
                         <img src="<?php echo asset_url().'userlogos/'.$logo; ?>" height="100" width="100"  />
                         <span>current logo Image</span>
                         <?php   $style='style="padding-top: 3em;"';
                          else :  $style=""; ?>
                         <span>No logo uploaded</span>
                         <?php endif;  ?>
                        </div>
                          <div class="input-field col m9 s12" >
                                            <div class="file-field input-field" <?= $style ?>>
                                                <div class="btn teal lighten-1">
                                                    <span>Edit Logo</span>
                                                    <input type="file" name="userfile" >
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text" name="userfile" placeholder="Upload logo img" class="required validate">
                                                    <small id="filenotice">image dimensions must less than 400*400 px  size less than 2mb 
                                                </small>
                                                </div>
                         </div>
                    </div>
                                              <!--     <div class="row">
          <a class="waves-effect waves-grey btn white modal-trigger" href="#uploadimg">Upload more images</a>
                          </div> -->
                                                <!-- </div> -->
       
                                                <input class="waves-effect waves-light btn blue m-b-xs" type="submit" name="" value="submit">
                                   
                                </form>
                         </div>
                </div>
                          
         
                        </div>
                    </div>
                </div>
       

            </main>

    <?php
        if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green', '<?= $this->session->flashdata('ok') ?>');
                });
            </script>
    <?php } ?>        


        <script src="<?= asset_url();?>backend\plugins\jquery-validation\jquery.validate.min.js"></script>
        <script src="<?= asset_url();?>backend\js\alpha.min.js"></script>
      
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqbaVKWXKDsrUY87hCEVlQMXpcCAzR-38&libraries=places&callback=initMap" async defer></script>
        <script type="text/javascript">
 
    function initMap() {
    var options = {
        componentRestrictions: {country: 'in'}
    };
    var input =  document.getElementById('locality');
    var autocomplete = new google.maps.places.Autocomplete(input,options);
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
          window.alert("Autocomplete's returned place contains no geometry");
          return;
        }
        $('#latitude').val(place.geometry.location.lat());
        $('#longitude').val(place.geometry.location.lng());
    });
}



//validate

    $( document ).ready(function() {
        // additional method for validate plug-in  letter and space -->
    jQuery.validator.addMethod("lettersonly", function(value, element) 
        {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "Letters and spaces only please");     
       
       
    var validator = $("#addngo").validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); },
        rules: {
              //  'name': { lettersonly: true },
              'designation': { lettersonly: true },
                'phone': {
                            required: true,
                            digits: true,
                            rangelength: [10, 10] 
                           },
               'members': {
              
                digits: true,
             
               },
                
        },
        messages: {

              //  userfile:{  required: "Select Image", }  

         } ,
    /*    submitHandler: function (form) {
            var file= $('[name="userfile"]').val();
                if(file===''){
                 $('#filenotice').html('<label id="userfile-error" class="error" for="userfile">Please attach file!.</label>');

                }else{
                     form.submit();
                }

                    
                  
      },  */
       
    });

   });



        </script>