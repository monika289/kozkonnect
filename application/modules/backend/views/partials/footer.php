<!--  javascript----->
<?php   if ($this->session->flashdata('ok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('green lighten-3', "<?= $this->session->flashdata('ok') ?>");
                });
            </script>
<?php } ?>  

<!-- if unsuccessfull -->
<?php   if ($this->session->flashdata('notok')) {
            ?>
            <script>
                $(document).ready(function () {
                    ShowNotificator('red lighten-3', "<?= $this->session->flashdata('notok') ?>");
                });
            </script>
<?php } ?>  
  
  
  