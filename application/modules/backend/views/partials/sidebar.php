<aside id="slide-out" class="side-nav white fixed">
    <div class="side-nav-wrapper">
        <div class="sidebar-profile">
            <div class="sidebar-profile-image">
                <img src="<?= asset_url()?>backend\images\profile-image.png" class="circle" alt="">
            </div>
            <div class="sidebar-profile-info">
                <a href="javascript:void(0);" class="account-settings-link">
                    <p>Admin</p>
                    <p class="blue-text "><u><a href="<?= base_url() ?>/admin/logout">logout</a></u></p>
                </a>
            </div>
        </div>

        <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
            <li class="no-padding active">
                <a class="waves-effect waves-grey active" href="<?= base_url() ?>/admin/dashboard">
                    <i class="material-icons">settings_input_svideo</i>
                    Dashboard
                </a>
            </li>
             <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">desktop_windows</i>SDG Master<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">
                    <ul>
                        <!-- <li><a href="<?= base_url('admin/sdgnew'); ?>">Add SDG</a></li> -->
                        <li><a href="<?= base_url('admin/sdg'); ?>">SDG List</a></li>
                    </ul>
                </div>
            </li>
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">desktop_windows</i>User Management<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">
                    <ul>
                        <!-- <li><a href="<?= base_url('admin/new'); ?>">Add NGO</a></li> -->
                        <li><a href="<?= base_url('admin/individual'); ?>">Individual List</a></li>
                        <li><a href="<?= base_url('admin/ngo'); ?>">NGO List</a></li>
                        <li><a href="<?= base_url('admin/institution'); ?>">Institution List</a></li>
                        <li><a href="<?= base_url('admin/org'); ?>">Organization List</a></li>
                    </ul>
                </div>
            </li>
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">desktop_windows</i>Cause Management<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">
                    <ul>
                        <!-- <li><a href="<?= base_url('admin/category/new'); ?>">Add Category</a></li> -->
                        <!-- <li><a href="<?= base_url('admin/category/list'); ?>">Category</a></li> -->
                        <li><a href="<?= base_url('admin/subcategory/list'); ?>">SubCategory</a></li>
                        <li><a href="<?= base_url('admin/cause'); ?>">Cause</a></li>
                    </ul>
                </div>
            </li>
             <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">desktop_windows</i>Ticket<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">
                    <ul>
                        <!-- <li><a href="<?= base_url('admin/category/new'); ?>">Add Category</a></li> -->
                        <li><a href="<?php echo base_url()?>admin/ticket/categorylist">Ticket Category</a></li>
                        <li><a href="<?php echo base_url()?>admin/ticket/subcategorylist">Ticket SubCategory</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/ticket/statuslist">Ticket Status</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/ticket/tickets">Manage Ticket</a></li>
                    </ul>
                </div>
            </li>
             
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">desktop_windows</i>Test<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="layout-blank.html">Add Test</a></li>
                    </ul>
                </div>
            </li>


        </ul>

    </div>
</aside>