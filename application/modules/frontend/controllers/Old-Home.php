<?php defined('BASEPATH') OR exit('No direct script access allowed');
//error_reporting(0);
Class Home extends MX_Controller {
	
	public function __construct() {
		parent::__construct ();
		
	}
	
	public function index() {
	     //$this->template->set('question', $this->reviewlib->get_all_question());
        // $this->template->set('active', 'category');
       // echo "string";exit;
		$this->template->set('description','');
		$this->template->set('page','home');
		$this->template->set('keywords','');
         $this->template->set_theme('default_theme');
         $this->template->set_layout('default')
                ->title('Administrator | Administrator control panel')
                ->set_partial('header', 'partials/header')
                ->set_partial('footer', 'partials/footer');
         $this->template->build('index');
		
	}	
	public function aboutus() {
	     //$this->template->set('question', $this->reviewlib->get_all_question());
        // $this->template->set('active', 'category');
       // echo "string";exit;
		$this->template->set('description','');
		$this->template->set('page','home');
		$this->template->set('keywords','');
         $this->template->set_theme('default_theme');
         $this->template->set_layout('default')
                ->title('Administrator | Administrator control panel')
                ->set_partial('header', 'partials/header')
                ->set_partial('footer', 'partials/footer');
         $this->template->build('about-us');
		
	}
	public function register() {
	     //$this->template->set('question', $this->reviewlib->get_all_question());
        // $this->template->set('active', 'category');
       // echo "string";exit;
		$this->template->set('description','');
		$this->template->set('page','home');
		$this->template->set('keywords','');
         $this->template->set_theme('default_theme');
         $this->template->set_layout('default')
                ->title('Administrator | Administrator control panel')
                ->set_partial('header', 'partials/header')
                ->set_partial('footer', 'partials/footer');
         $this->template->build('register');
		
	}
	public function otp() {
	     //$this->template->set('question', $this->reviewlib->get_all_question());
        // $this->template->set('active', 'category');
       // echo "string";exit;
		$this->template->set('description','');
		$this->template->set('page','home');
		$this->template->set('keywords','');
         $this->template->set_theme('default_theme');
         $this->template->set_layout('default')
                ->title('Administrator | Administrator control panel')
                ->set_partial('header', 'partials/header')
                ->set_partial('footer', 'partials/footer');
         $this->template->build('otp');
		
	}
	public function login() {
	     //$this->template->set('question', $this->reviewlib->get_all_question());
        // $this->template->set('active', 'category');
       // echo "string";exit;
		$this->template->set('description','');
		$this->template->set('page','home');
		$this->template->set('keywords','');
         $this->template->set_theme('default_theme');
         $this->template->set_layout('default')
                ->title('Administrator | Administrator control panel')
                ->set_partial('header', 'partials/header')
                ->set_partial('footer', 'partials/footer');
         $this->template->build('login');
		
	}
	public function resetpassword() {
	     //$this->template->set('question', $this->reviewlib->get_all_question());
        // $this->template->set('active', 'category');
       // echo "string";exit;
		$this->template->set('description','');
		$this->template->set('page','home');
		$this->template->set('keywords','');
         $this->template->set_theme('default_theme');
         $this->template->set_layout('default')
                ->title('Administrator | Administrator control panel')
                ->set_partial('header', 'partials/header')
                ->set_partial('footer', 'partials/footer');
         $this->template->build('reset-password');
		
	}


	function step2()
	{
		$this->template->set_layout (false);
	    $this->session->sess_destroy();
		$this->template->build ('step1');
	}
	function question(){
		if (session_status() == PHP_SESSION_NONE) {
		    session_start();
		    $survey=array();
		    $this->session->set_userdata('survey', $survey);
		}
		if (!isset($_SESSION['views'])) { 
		    $_SESSION['views'] = 1;
		}
		$html=$this->getQuestion(0);
		$this->template->set('html',$html);
		$this->template->set_layout (false);
	
		$this->template->build ('question');

	}
	function getQuestion($id=null,$opid=null)
	{ 

		$this->load->model('Question_model','que');
		$question=$this->que->getQuestionsById($id,$opid);
		if(empty($question))
		{   error_reporting(0);
			echo "<script>
			window.location.href = '".base_url()."laststep';
			</script>";
			exit;
			return true;
		}

		$question['options']=$this->que->getOptionById($question['id']);
		$html='<h3>Step '.$_SESSION['views'].'</h3><br>
				<h2>'.$question['questions'].'?</h2>
				<input type="hidden" name="qid" value="'.$question['id'].'">
			     <br></br></br>';
         if(empty($question['options']))  {        
        $html.='<h5><input type="radio" id="test1" name="choice" value="1" checked>
			    <label for="test1" style="margin-right: 54px;">Yes </label>
			    <input type="radio" id="test2" name="choice" value="2">
			    <label for="test2">NO</label></h5>';
        }else{
        	if($question['id']==13 || $question['id']==15 || $question['id']==16 ||  $question['id']==19 || $question['id']==20)
        	{
        		  $data1['question'] = $question;
        		    $html.= $this->load->view('rank_view', $data1, true);
        			// $this->template->set_layout (false);
	          //   	$this->template->set('question',$question);
		
	          //      	$html.=$this->template->build ('rank_view', TRUE);
        	}else{  
        	      	 $html.='<h5>';
        	 $f=1;
        	 foreach ($question['options'] as $value) {
        	 	# code...
        	 $html.='<input type="radio" id="test'.$f.'" name="choice" value="'.$value['opid'].'" '.($f==1 ? "checked" : "").'>
			    <label for="test'.$f++.'" style="margin-right: 54px;">'.$value['options'].' </label>';
			   }
			    $html.='</h5>';

			}
        //	print_r($question['options']);
        }
        if ($this->input->is_ajax_request()) { 
           echo $html;
        } else{
          return $html;	
        }
       

		//pretag($html);
	}
	function setnext()
	{   if (session_status() == PHP_SESSION_NONE) {
		    session_start();
		    $survey=array();
		    $this->session->set_userdata('survey', $survey);
		}
		$data=$_POST;
       
           
		//if(isset($data['qid']))
		//$this->session->set_userdata ($data['qid'], $data['choice']);
		//$this->session->set_userdata ( 'active', 0 );
        
		 $data1['choice']=(isset($data['choice']) ?$data['choice'] : "") ;
		 $data1['qid']=(isset($data['qid']) ?$data['qid'] : "") ;
		 if(isset($data['description']))
		 {
		  $data1['description']=$data['description'];	
		 }
		
		
		$survey = $this->session->survey;
		$survey['que_'.$data1['qid']] = $data1;
		$this->session->unset_userdata('survey');
		$this->session->set_userdata ('survey',$survey);
		//  echo "<pre>";
		// print_r($_SESSION);
		// exit();
		//$this->session->set_userdata ('que_'.$questionid,$optionid);
		 $questionid=$data['qid'];
		 $optionid =(isset($data['choice']) ?$data['choice'] : "") ;
		$optionParam=($optionid>2 ? 0 : $optionid) ;
		

		$_SESSION['views'] = $_SESSION['views']+1;

		$this->getQuestion($questionid,$optionParam);
		
		
	//	session_destroy();
	//	$this->session->sess_destroy();
		

	//	pretag($_SESSION);


	}
	function lastStep(){
		//  echo "<pre>";
		 print_r($_SESSION);
	    $this->template->set_layout (false);
	    
		$this->template->build ('laststep');

	}

	function saveservey(){
	$userdata=$_POST;
	$this->load->model('Question_model','que');
	
	$userid=$this->que->saveUser($userdata);
	//$userid=1;
	//print_r($userdata); exit;
	$questios=$_SESSION['survey'];
	$user_quetions=array();
	
	foreach ($questios as $key => $value) {
		if(is_array($value['choice'])){
            $optionarray=$value['choice'];
            asort($optionarray);
			
			//	print_r($optionarray); 
            $op_id=implode(',', array_keys($optionarray));

		}else{
             $op_id=$value['choice'];
		}
		
		$user_quetions[]=array('que_id'=>$value['qid'],'op_id'=>$op_id,'userid'=>$userid);
	}
	//$this->que->savedata($user_quetions);
	// echo 'nitin';
	 print_r($user_quetions); exit;
	echo json_encode(array('status'=>1,'msg'=>'succesful'));

	}
}
?>