<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">       
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/style.css">
        <style type="text/css">
    
/* equal card height */
.row-equal > div[class*='col-'] {
    display: flex;
    flex: 1 0 auto;
}

.row-equal .card {
   width: 100%;
}

/* ensure equal card height inside carousel */
.carousel-inner>.row-equal.active, 
.carousel-inner>.row-equal.next, 
.carousel-inner>.row-equal.prev {
    display: flex;
}

/* prevent flicker during transition */
.carousel-inner>.row-equal.active.left, 
.carousel-inner>.row-equal.active.right {
    opacity: 0.5;
    display: flex;
}
.paddingRight95px{
    padding-right: 95px;
}
.lead .btn, .lead .btn:focus, .lead .btn:active{
    border: 0px;
    background-color: transparent;
    box-shadow: none;
    outline: none;
}
.paddingLeft50px{
    padding-left: 50px;
}
.paddingRight50px{
    padding-right: 50px;
}
</style>
        
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light paddingLeft0px"> <!--bg-light-->
  <a class="navbar-brand" href="<?php echo base_url(); ?>">
    <img src="<?php echo asset_url();?>/images/Artboard – 182.png" class="logo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    </ul> 
    <form class="form-inline my-2 my-lg-0 pull-right">
        <div class="mr-sm-2 dontHave">            
            <span>Don't have an account?</span>
           
        </div>
      <button class="regBtn" type="submit">
        <a href="<?php echo base_url(); ?>login"> Login</a> 
        
      </button>
    </form>
  </div>
</nav>


<div class="container-fluid login">
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-4 leftSidepadding leftSideAdmin">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-10">
                    
                    <h1 class="adminTital">MAKE A DIFFERENCE</h1>

                    <ul class="adminList">
                        <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Create a cause</a></li>
                        <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Fallow a cause</a></li>
                        <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Valunteer a cause</a></li>
                        <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Feel amazing</a></li>
                        <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Happiness is in helping</a></li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-8 paddingTop125px">
        	<div class="row">
        		<div class="col-xl-12 col-lg-12 col-md-12 marginTop20px ">
                        
                        <div class="progressDiv">
                            <ul class="progressbar">
                                <li>Who are you?</li>
                                <li class="active">Your SDG Goal!</li>
                                <li>Ways to contribute</li>
                            </ul>
                        </div>
                        
    <div class="clr"></div>
                       
<section class="carousel slide" data-ride="carousel" id="postsCarousel">
    
    <div class="container p-t-0 m-t-2 carousel-inner paddingLeft50px">
        <div class="row row-equal carousel-item active m-t-0">
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard147.png"></i>
            </div>
            <div class="yourCard active">
                <img src="<?php echo asset_url();?>/images/Artboard148.png"></i>
            </div>
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard150.png"></i>
            </div>
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard149.png"></i>
            </div>
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard154.png"></i>
            </div>
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard151.png"></i>
            </div>
             <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard153.png"></i>
            </div>
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard152.png"></i>
            </div>
        </div>
        <div class="row row-equal carousel-item m-t-0">
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard147.png"></i>
            </div>
            <div class="yourCard active">
                <img src="<?php echo asset_url();?>/images/Artboard148.png"></i>
            </div>
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard150.png"></i>
            </div>
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard149.png"></i>
            </div>
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard154.png"></i>
            </div>
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard151.png"></i>
            </div>
             <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard153.png"></i>
            </div>
            <div class="yourCard">
                <img src="<?php echo asset_url();?>/images/Artboard152.png"></i>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-12 text-md-right lead paddingRight50px">
                <a class="btn prev" href="" title="go back">
                    <img src="<?php echo asset_url();?>/images/Artboard – 216.png">
                </a>
                <a class="btn next" href="" title="more">
                    <img src="<?php echo asset_url();?>/images/Artboard – 217.png">
                </a>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
(function($) {
    "use strict";

    // manual carousel controls
    $('.next').click(function(){ $('.carousel').carousel('next');return false; });
    $('.prev').click(function(){ $('.carousel').carousel('prev');return false; });
    
})(jQuery);
</script>







                        
                    

                        <div class="clr"></div>
                        <div class="text-center marginTop50px">
                            <button class="btn buttonBtn" type="submit">
                                <span>Continue </span>
                                <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
                            </button>   
                        </div>
                        

        		</div>
               
        	</div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).scroll(function(){
        if($(this).scrollTop() > 80)
        {   
            $('.navbar').addClass('navBg');
        }
        else{
            $('.navbar').removeClass('navBg');   
        }
    });
</script>



</body>
</html>