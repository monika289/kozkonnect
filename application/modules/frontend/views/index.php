<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>/css/stylehome.css">

<div class="container-fluid mainSlider">
    <div class="row">
        <div class="col-md-12 text-center paddingTB25px">
            <h1>Let’s all smile together</h1>
            <p>Report a cause and be a part of us</p>
         </div>
         <div class="col-md-12 text-center">
            <div class="owl-carousel">
                <div><img src="<?php echo asset_url();?>/images/home/BannerArtboard1.png" alt=""></div>
                <div><img src="<?php echo asset_url();?>/images/home/BannerArtboard2.png" alt=""></div>
                <div><img src="<?php echo asset_url();?>/images/home/BannerArtboard3.png" alt=""></div>
                <div><img src="<?php echo asset_url();?>/images/home/BannerArtboard4.png" alt=""></div>
                <div><img src="<?php echo asset_url();?>/images/home/BannerArtboard5.png" alt=""></div>
                <div><img src="<?php echo asset_url();?>/images/home/BannerArtboard6.png" alt=""></div>
                <div><img src="<?php echo asset_url();?>/images/home/BannerArtboard7.png" alt=""></div>
            </div>
        </div>       
    </div>
</div>


<section id="ngo-vertical-home">
<div class="container">
    <div id="carouselExampleControls" class="carousel vert slide" data-ride="carousel" >
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleControls" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleControls" data-slide-to="1"></li>
            <li data-target="#carouselExampleControls" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="row">
                    <div class="col-md-4 ngo-members mg-top">
                        <h2>Individual</h2>
                        <div class="member-details">
                            <div class="member-details-info">
                                <h4 class=""><a href="">Sustatajuria</a></h4>
                                <p class="">5 yrs. Pune, Maharasthra</p>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="member-details-info">
                                <h4 class=""><a href="">Sustatajuria</a></h4>
                                <p class="">5 yrs. Pune, Maharasthra</p>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="member-details-info">
                                <h4 class=""><a href="">Sustatajuria</a></h4>
                                <p class="">5 yrs. Pune, Maharasthra</p>
                            </div>
                        </div>
                        <div class=""><button type="button" class="all-members-btn">See All Members</button></div>
                    </div>
                    <div class="col-md-5 offset-md-1">
                        <div class="boxed-images">
                            <div class="image-logo">
                                <img src="<?php echo asset_url();?>/images/home/logo1.png">
                            </div>
                            <div class="mobile-image">
                                <img src="<?php echo asset_url();?>/images/home/Phone.png">
                            </div>
                            <div class="bottom-image">
                                <img src="<?php echo asset_url();?>/images/home/Artboardbg.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-md-4 ngo-members mg-top">
                        <h2>NGO</h2>
                        <div class="member-details">
                            <div class="member-details-info">
                                <h4 class=""><a href="">Sustatajuria</a></h4>
                                <p class="">5 yrs. Pune, Maharasthra</p>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="member-details-info">
                                <h4 class=""><a href="">Sustatajuria</a></h4>
                                <p class="">5 yrs. Pune, Maharasthra</p>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="member-details-info">
                                <h4 class=""><a href="">Sustatajuria</a></h4>
                                <p class="">5 yrs. Pune, Maharasthra</p>
                            </div>
                        </div>
                        <div class=""><button type="button" class="all-members-btn">See All Members</button></div>
                    </div>
                    <div class="col-md-5 offset-md-1">
                        <div class="boxed-images">
                            <div class="image-logo">
                                <img src="<?php echo asset_url();?>/images/home/logo1.png">
                            </div>
                            <div class="mobile-image">
                                <img src="<?php echo asset_url();?>/images/home/Phone.png">
                            </div>
                            <div class="bottom-image">
                                <img src="<?php echo asset_url();?>/images/home/Artboardbg.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-md-4 ngo-members mg-top">
                        <h2>Organisation/Institute</h2>
                        <div class="member-details">
                            <div class="member-details-info">
                                <h4 class=""><a href="">Sustatajuria</a></h4>
                                <p class="">5 yrs. Pune, Maharasthra</p>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="member-details-info">
                                <h4 class=""><a href="">Sustatajuria</a></h4>
                                <p class="">5 yrs. Pune, Maharasthra</p>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="member-details-info">
                                <h4 class=""><a href="">Sustatajuria</a></h4>
                                <p class="">5 yrs. Pune, Maharasthra</p>
                            </div>
                        </div>
                        <div class=""><button type="button" class="all-members-btn">See All Members</button></div>
                    </div>
                    <div class="col-md-5 offset-md-1">
                        <div class="boxed-images">
                            <div class="image-logo">
                                <img src="<?php echo asset_url();?>/images/home/logo1.png">
                            </div>
                            <div class="mobile-image">
                                <img src="<?php echo asset_url();?>/images/home/Phone.png">
                            </div>
                            <div class="bottom-image">
                                <img src="<?php echo asset_url();?>/images/home/Artboardbg.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</section>



<div class="container-fluid nearYou">
    <div class="row">
        <div class="col-md-12 text-center">
            <h2>Find causes near you </h2>
            <p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum
    lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem</p>
        </div>
    </div>
    <div class="center slider">
        <div>
          <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes1.png" alt="">
              <div class="causes-content">
                <p>Gujarat</p>
              </div>
            </div>
            </div>
            <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes2.png">
              <div class="causes-content">
                <p>Jaipur</p>
              </div>
            </div>
            </div>
        </div>
        <div>
          <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes3.png">
              <div class="causes-content">
                <p>Assam</p>
              </div>
            </div>
            </div>
            <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes4.png">
              <div class="causes-content">
                <p>Mumbai</p>
              </div>
            </div>
            </div>
        </div>
        <div>
          <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes5.png">
              <div class="causes-content">
                <p>Bengal</p>
              </div>
            </div>
            </div>
            <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes6.png">
              <div class="causes-content">
                <p>Delhi</p>
              </div>
            </div>
            </div>
        </div>
        <div>
          <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes7.png">
              <div class="causes-content">
                <p>Surat</p>
              </div>
            </div>
            </div>
            <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes8.png">
              <div class="causes-content">
                <p>Punjab</p>
              </div>
            </div>
            </div>
        </div>
        <div>
          <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes9.png">
              <div class="causes-content">
                <p>Sikkim</p>
              </div>
            </div>
            </div>
            <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes10.png">
              <div class="causes-content">
                <p>Gujarat</p>
              </div>
            </div>
            </div>
        </div>
        <div>
          <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes11.png">
              <div class="causes-content">
                <p>Chennai</p>
              </div>
            </div>
            </div>
            <div class="image">
            <div class="causes-main">
              <img src="<?php echo asset_url();?>/images/home/causes12.png">
              <div class="causes-content">
                <p>Gujarat</p>
              </div>
            </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid bePart">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="likeBtn">
                <img src="<?php echo asset_url();?>/images/Artboard11.png">
            </div>
            <h2>Be part of our next  community</h2>
        
            <div class="communityCls">
                <div class="commImage">
                    <img src="<?php echo asset_url();?>/images/Artboard – 132.png" />
                </div>
                <h5>Lorem ipsum lorem</h5>

                <p>
                    Lorem ipsum lorem ipsum lorem
                    ipsum lorem ipsum lorem ipsum
                    lorem ipsum lorem ipsum lorem
                    ipsum lorem ipsum
                </p>
            </div>
            <div class="communityCls">
                <div class="commImage">
                    <img src="<?php echo asset_url();?>/images/Artboard – 133.png" />
                </div>
                <h5>Lorem ipsum lorem</h5>
                <p>
                    Lorem ipsum lorem ipsum lorem
                    ipsum lorem ipsum lorem ipsum
                    lorem ipsum lorem ipsum lorem
                    ipsum lorem ipsum
                </p>
            </div>
            <div class="communityCls">
                <div class="commImage">
                    <img src="<?php echo asset_url();?>/images/Artboard – 132.png" />
                </div>
                <h5>Lorem ipsum lorem</h5>
                <p>
                    Lorem ipsum lorem ipsum lorem
                    ipsum lorem ipsum lorem ipsum
                    lorem ipsum lorem ipsum lorem
                    ipsum lorem ipsum
                </p>
            </div>
        </div>
        <div class="col-md-12 text-center padding30px">
            <button class="btn btn-unique btn-rounded btn-sm my-0 gotoBtn" type="submit">Discover community events  <i class="fas fa-arrow-right"></i></button>
        </div>
    </div>
</div>





    
<div class="container-fluid connectSection">
    <div class="row">
      <div class="col-md-6">
        <h2>Find your way to connect</h2>
        <p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum
            lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum
            lorem ipsum lorem ipsum lorem ipsum lorem </p>

            <button class="btn btn-unique btn-rounded btn-sm my-0 gotoBtn" type="submit">Go to your timeline  <i class="fas fa-arrow-right"></i></button>
      </div>
      <div class="col-md-6">
           <img src="<?php echo asset_url();?>/images/Artboard28.png"/>
      </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo asset_url();?>/js/slick.min.js"></script>
<!-- Carousel Auto-Cycle starts here -->
<script type="text/javascript">
    // Carousel Auto-Cycle
  $(".center").slick({
        dots: false,
        infinite: true,
        //centerMode: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
          autoplaySpeed: 2000,
      });

</script>

<!-- Carousel Auto-Cycle ends here-->

<script type="text/javascript">
    $('#myCarousel').carousel({
            interval: false
        });

        //scroll slides on swipe for touch enabled devices

        $("#myCarousel").on("touchstart", function(event){
            //alert("");
            var yClick = event.originalEvent.touches[0].pageY;
            $(this).one("touchmove", function(event){

                var yMove = event.originalEvent.touches[0].pageY;
                if( Math.floor(yClick - yMove) > 1 ){
                    $(".carousel").carousel('next');
                }
                else if( Math.floor(yClick - yMove) < -1 ){
                    $(".carousel").carousel('prev');
                }
            });
            $(".carousel").on("touchend", function(){
                //$(this).off("touchmove");
            });
        });
</script>

<!-- Vertical Slider starts here -->

<script type="text/javascript">
    
    $(document).ready(function(){
    // invoke the carousel
    $('#carouselExampleControls').carousel({
      interval:4000
    });

// scroll slides on mouse scroll 
$('#carouselExampleControls').bind('mousewheel DOMMouseScroll', function(e){

        if(e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
            $(this).carousel('prev');
        }
        else{
            $(this).carousel('next');
        }
    });

//scroll slides on swipe for touch enabled devices 

    $("#carouselExampleControls").on("touchstart", function(event){
 
        var yClick = event.originalEvent.touches[0].pageY;
        $(this).one("touchmove", function(event){

        var yMove = event.originalEvent.touches[0].pageY;
        if( Math.floor(yClick - yMove) > 1 ){
            $(".carousel").carousel('next');
        }
        else if( Math.floor(yClick - yMove) < -1 ){
            $(".carousel").carousel('prev');
        }
    });
    $(".carousel").on("touchend", function(){
            $(this).off("touchmove");
    });
});
    
});
//animated  carousel start
$(document).ready(function(){

//to add  start animation on load for first slide 
$(function(){
        $.fn.extend({
            animateCss: function (animationName) {
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                this.addClass('animated ' + animationName).one(animationEnd, function() {
                    $(this).removeClass(animationName);
                });
            }
        });
             $('.item1.active img').animateCss('slideInDown');
             $('.item1.active h2').animateCss('zoomIn');
             $('.item1.active p').animateCss('fadeIn');
             
});
    
//to start animation on  mousescroll , click and swipe
 
     $("#carouselExampleControls").on('slide.bs.carousel', function () {
        $.fn.extend({
            animateCss: function (animationName) {
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                this.addClass('animated ' + animationName).one(animationEnd, function() {
                    $(this).removeClass(animationName);
                });
            }
        });
    
    });
});
</script>

<!-- Vertical Slider ends here -->
    
</body>

</html>
