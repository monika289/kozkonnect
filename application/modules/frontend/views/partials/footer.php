<input type="hidden" id="csrf" name="<?= $csrf->name; ?>" value="<?= $csrf->hash; ?>" /> 
<div class="container-fluid footerSearch">
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12">
        <h1>Lets stay in touch</h1>
        <p>Subscibe to our newsletter and don't miss anything!</p>


      </div>
      <div class="col-xl-12 col-lg-12 col-md-12 text-center">

       <!--  <form class="form-inline mr-auto displayBlock footerSearchBox">
          <input class="form-control mr-sm-2" type="text" placeholder="Your E-mail" aria-label="Search">
          <button class="btn btn-unique btn-rounded btn-sm my-0" type="submit">Subscribe  <i class="fas fa-arrow-right"></i></button>
        </form> -->

        <div class="input-group footerSearchBox">
            <input type="text" class="form-control" placeholder="Your E-mail" id="txtSearch"/>
            <div class="input-group-btn">
              <button class="btn" type="submit">
                Subscribe <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
              </button>
            </div>
        </div>
      </div>
    </div>
</div>

<!-- Footer -->
<footer class="page-footer font-small blue pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-xl-6 col-lg-6 col-md-6 mt-md-0 mt-3">

          <!-- Content -->
          <div class="footerLogo">
            <a href=""><img src="<?php echo asset_url();?>/images/Artboard – 1.png"></a>
          </div>
          

        </div>
        <!-- Grid column -->

        <!-- <hr class="clearfix w-100 d-md-none pb-3"> -->

        <!-- Grid column -->
        <div class="col-xl-6 col-lg-6 col-md-6 mb-md-0 mb-3 footerList">

            <!-- Links -->
            <!-- <h5 class="text-uppercase">Links</h5> -->

            <ul class="list-inline">
               <li class="list-inline-item"><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
               <li class="list-inline-item"><a href="<?php echo base_url(); ?>refund-policy">Refund Policy</a></li>
               <li class="list-inline-item"><a href="">Legal</a></li>
               <li class="list-inline-item"><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
               <li class="list-inline-item"><a href="<?php echo base_url(); ?>faq">FAQ</a></li>
               <li  class="list-inline-item"> <a href=""><i class="fab fa-instagram"></i></a></li>
               <li  class="list-inline-item"> <a href=""><i class='fab fa-facebook-f'></i></a></li>
               <li  class="list-inline-item"> <a href=""><i class='fab fa-twitter'></i></a></li>

            </ul>
             <!-- Grid column -->
          <hr class="clearfix w-100 d-md-none pb-3">
            <!-- Copyright -->
            <div class="footer-copyright text-center py-3 float-right">Copyright KozKonnect  2019 | Designed By
              <a href="https://www.brandzgarage.com/"> BrandzGarage</a>
            </div>
            <!-- Copyright -->

          </div>


      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

   

  </footer>
  <!-- Footer -->



<!--  <footer class="footer">
        <div class="container">
            <img src="<?php echo asset_url(); ?>images/foot-logo.jpg" class="foot-logo">
            <ul class="foot-ul">
                <li>
                    <a href="#">Privacy Policy</a>
                </li>
                <li>
                    <a href="#">Refund Policy</a>
                </li>
                <li>
                    <a href="#">Cancellation Policy</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                </li>
            </ul>
            <div class="row">
                <ul class="foottwo-ul">
                    <li>
                        <img src="<?php echo asset_url(); ?>images/foot-icon.png">
                        <p>Email Id</p>
                    </li>
                    <li>
                        <img src="<?php echo asset_url(); ?>images/foot-icon.png">
                        <p>Phone no.</p>
                    </li>
                    <li>
                        <img src="<?php echo asset_url(); ?>images/foot-icon.png">
                        <p>Website</p>
                    </li>

                </ul>
            </div>
        </div>
    </footer>
    <div class="footer-bottom">
        <div class="row">
            <div class="col-md-4">
                <span>© Copyright 2019 GarageWorks </span>
            </div>
            <div class="col-md-4">
                <ul class="footbtm-ul">
                    <li>
                        <a href="#"><img src="<?php echo asset_url(); ?>images/fb.png"></a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo asset_url(); ?>images/twitter-circular-button.png"></a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo asset_url(); ?>images/google-plus.png"></a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo asset_url(); ?>images/instagram.png"></a>
                    </li>

                </ul>
            </div>
            <div class="col-md-4">
                <span id="txt-right">Designed by: Brandzgarage</span>
            </div>
        </div>
    </div> -->