<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">       
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/style.css">
        
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light paddingLeft0px"> <!--bg-light-->
  <a class="navbar-brand" href="<?php echo base_url(); ?>">
    <img src="<?php echo asset_url();?>/images/Artboard – 182.png" class="logo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    </ul> 
    <form class="form-inline my-2 my-lg-0 pull-right">
        <div class="mr-sm-2 dontHave">            
            <span>Don't have an account?</span>
           
        </div>
      <button class="regBtn" type="submit">
        <a href="<?php echo base_url(); ?>login"> Login</a> 
        
      </button>
    </form>
  </div>
</nav>

<div class="container-fluid otp">
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-4 leftSidepadding leftSideAdmin">
        	<div class="row">
        		<div class="col-xl-12 col-lg-12 col-md-10">
        			
        			<h1 class="adminTital">MAKE A DIFFERENCE</h1>

        			<ul class="adminList">
                        <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Create a cause</a></li>
                        <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Fallow a cause</a></li>
                        <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Valunteer a cause</a></li>
                        <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Feel amazing</a></li>
                        <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Happiness is in helping</a></li>
                    </ul>
        		</div>

        	</div>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-8 paddingTop125px ">
            <div class="LoginbgCircle1"></div>
        	<h2 class="formTitle">OTP Verification</h2>

        	<div class="row">
        		<div class="col-xl-8 col-lg-8 col-md-10 formController">
        			<div class="form-group marginOTP">                           
                        <input type="text" class="form-control" placeholder="Enter Code" >
                     </div>
                      
                   
                
                  <button class="btn buttonBtn" type="submit">
                    <span>Submit </span>
                    <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
                  </button>
                   

        		</div>
               <!--  <div class="col-md-4 bgCircle">
                    
                </div> -->
        	</div>
        </div>
        <div class="leftBottomImg"></div>
    </div>
</div>


<script type="text/javascript">
    $(document).scroll(function(){
        if($(this).scrollTop() > 80)
        {   
            $('.navbar').addClass('navBg');
        }
        else{
            $('.navbar').removeClass('navBg');   
        }
    });
</script>



</body>
</html>