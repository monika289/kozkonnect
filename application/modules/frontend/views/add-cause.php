<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">       
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/style.css">
         <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/dashboard.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light paddingLeft0px"> <!--bg-light-->
  <a class="navbar-brand" href="<?php echo base_url(); ?>">
    <img src="<?php echo asset_url();?>/images/Artboard – 182.png" class="logo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    
    </ul> 
    <form class="form-inline my-2 my-lg-0 pull-right">
        <div class="mr-sm-2 searchBox">            
            <span id="searchInput"><input class="form-control" type="search" placeholder="Search"></span>
            <!-- <i class='fas fa-search' id="searchIcon"></i> -->
            <img src="<?php echo asset_url();?>/images/Artboa9.png" id="searchIcon">
        </div>
      <button class="disasterBtn" type="submit">
        <a href=""> Disaster response</a> 
        <!-- <i class='fas fa-arrow-right'></i> -->
        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
      </button>
    </form>
  </div>
</nav>




<div style="clr"></div>

		<ol class="breadcrumb paddingTop100px">
			<!-- <li class="breadcrumb-item ">
				<a href="">Home</a>
			</li>
			<li class="breadcrumb-item">
				<a href="#">MyKonnect</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="">Cause</a>
			</li>
			<li class="breadcrumb-item active">
				<a href="">My Group</a>
			</li> -->

			<li class="breadcrumb-menu d-md-down-none">
			<div class="btn-group" role="group" aria-label="Button group">
				
				<ul class="rightSideNav">
					<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle btntext" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				         	<img class="profileImg" src="<?php echo asset_url();?>/images/Artboard48.png"> 
							<span class="nameTitle">Nilesh Watal</span>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard82.png"> Create page</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard83.png"> Active</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard85.png"> Setting</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard84.png"> Log out</a>
				        </div>
				      </li>
					
					<li><a class="btntext" href="">
					
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard46.png"> </a></li>
					<li><a class="btntext" href="#">
						
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard47.png"> </a>
					</li>

				</ul>
			</div>
			</li>
	</ol>
	<div style="" le="clr"></div>
	<style type="text/css">
		
	</style>

	
	
	 <div class="clr"></div>
	<div class="container-fluid ">
	    <div class="row">

	    	<div class="col-xl-12 col-lg-12 col-md-12 addcauseTitel">
	    		<h1>Add cause</h1>
	    		<p>Lorem Ipsum lorem Ipsum</p>
	    	</div>
	    	<div class="LoginbgCircle1"></div>
	        <div class="col-xl-6 col-lg-6 col-md-12 form">
	       			
	        	<div class="row">
	        		<div class="col-xl-6 col-lg-6 col-md-6 dropForm paddingRight0px marginBottom20px">
	        			<div class="dropdown">
						  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Select Category
						  </a>

						  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
						    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Add category</a>
						    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Lorem Ipsum</a>
						    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Lorem Ipsum</a>
						    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Lorem Ipsum</a>
						    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Lorem Ipsum</a>
						  </div>
						</div>
	        		</div>


	        		<div class="col-xl-6 col-lg-6 col-md-6 dropForm paddingLeft0px marginBottom20px">
	        			<div class="dropdown">
						  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Select Subcategory
						  </a>

						  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink1">
						    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Add subcategory</a>
						    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Lorem Ipsum</a>
						    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Lorem Ipsum</a>
						    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Lorem Ipsum</a>
						    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Lorem Ipsum</a>
						  </div>
						</div>
	        		</div>

	        		<div class="col-xl-12 col-lg-12 col-md-12 addCauseForm">
		        		 <div class="form-group">                           
	                        <input type="text" class="form-control" placeholder="Title of the cause" >
	                     </div>
	                     <div class="form-group">                           
	                        <input type="text" class="form-control" placeholder="Description" >
	                     </div>
	                     <div class="form-group">
	                     	<div class="dropdown fullDropDown">
							  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    Select NGO
							  </a>

							  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink3">
							    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard – 198.png"> Add NGO</a>
							    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Lorem Ipsum</a>
							    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard87.png"> Lorem Ipsum</a>
							  
							  </div>
							</div>
	                     </div>
	                     <div class="form-group">                           
	                        <input type="text" class="form-control" placeholder="Location" >
	                     </div>
	                     <div class="form-group">                           
	                        <input type="text" class="form-control" placeholder="Goal of the cause" >
	                     </div>
	                     <div class="form-group">                           
	                       <div class="dropdown fullDropDown">
							  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    Requirement of the cause
							  </a>

							  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink4">
							    <a class="dropdown-item" data-toggle="modal" data-target="#volunteer"><img src="<?php echo asset_url();?>/images/Artboard98.png">
							    	Volunteer
							     </a>
							    <a class="dropdown-item" data-toggle="modal" data-target="#cash"><img src="<?php echo asset_url();?>/images/Artboard97.png"> 
							    	Cash
							    </a>
							    <a class="dropdown-item" data-toggle="modal" data-target="#kind"><img src="<?php echo asset_url();?>/images/Artboard99.png"> 
							    	Kind
							    </a>
							    <a class="dropdown-item" data-toggle="modal" data-target="#all"><img src="<?php echo asset_url();?>/images/Artboard100.png"> 
							    	All
							    </a>
							  
							  </div>
							</div>
	                     </div>
	                 </div>

	                 <div class="col-xl-12 col-lg-12 col-md-12 cbtn">
	                 	<button class="disasterBtn createBtn" type="submit">
				         Create
				        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
				      </button>
				     </div>

	        	</div>
				
	       	</div>
	        
	    </div>
	    <!-- <div class="LoginbgCircle dashBgImg"></div> -->
	 </div>





	 <!-- Model Volunteer -->
		<div class="modal addCauseModelWin" id="volunteer">
		  <div class="modal-dialog">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title">Requirement of the cause - Volunteer</h4>
		        <button type="button" class="close" data-dismiss="modal" style="display:none;">&times;</button>
		      </div>

		      <!-- Modal body -->
		      	<div class="modal-body">
		        	<div class="row">
		        		<div class="col-xl-12 col-lg-12 col-md-12 addCauseForm">
			        		 <div class="form-group">                           
		                        <input type="text" class="form-control" placeholder="No. of volunteer" >
		                     </div>
			        		 <div class="form-group">                           
		                        <input type="text" class="form-control" placeholder="Background" >
		                     </div>
	                    </div>
		     	 	</div>
		     	</div>
		      <!-- Modal footer -->
		      <div class="modal-footer">
		        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->

			        <button class="disasterBtn createBtn"  data-dismiss="modal" type="submit">
			         Submit
			        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
			      </button>
		      </div>

		    </div>
		  </div>
		</div>



		<!-- Model Cash -->
		<div class="modal addCauseModelWin" id="cash">
		  <div class="modal-dialog">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title">Requirement of the cause - Cash</h4>
		        <button type="button" class="close" data-dismiss="modal" style="display:none;">&times;</button>
		      </div>

		      <!-- Modal body -->
		      	<div class="modal-body">
		        	<div class="row">
		        		<div class="col-xl-12 col-lg-12 col-md-12 addCauseForm">
			        		 <div class="form-group">                           
		                        <input type="text" class="form-control" placeholder="Amount of cash" >
		                     </div>
			        		
	                    </div>
		     	 	</div>
		     	</div>
		      <!-- Modal footer -->
		      <div class="modal-footer">
		        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->

			        <button class="disasterBtn createBtn"  data-dismiss="modal" type="submit">
			         Submit
			        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
			      </button>
		      </div>

		    </div>
		  </div>
		</div>

		<!-- Model Kind -->
		<div class="modal addCauseModelWin" id="kind">
		  <div class="modal-dialog">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title">Requirement of the cause - Kind</h4>
		        <button type="button" class="close" data-dismiss="modal" style="display:none;">&times;</button>
		      </div>

		      <!-- Modal body -->
		      	<div class="modal-body">
		        	<div class="row">
		        		<div class="col-xl-12 col-lg-12 col-md-12 addCauseForm">
			        		 <div class="form-group">                           
		                        <input type="text" class="form-control" placeholder="What kind of help " >
		                     </div>			        		
	                    </div>
		     	 	</div>
		     	</div>
		      <!-- Modal footer -->
		      <div class="modal-footer">
		        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->

			        <button class="disasterBtn createBtn"  data-dismiss="modal" type="submit">
			         Submit
			        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
			      </button>
		      </div>

		    </div>
		  </div>
		</div>


		<!-- Model All -->
		<div class="modal addCauseModelWin" id="all">
		  <div class="modal-dialog">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title">Requirement of the cause - All</h4>
		        <button type="button" class="close" data-dismiss="modal" style="display:none;">&times;</button>
		      </div>

		      <!-- Modal body -->
		      	<div class="modal-body">
		        	<div class="row">
		        		<div class="col-xl-12 col-lg-12 col-md-12 addCauseForm">
			        		 <div class="form-group">                           
		                        <input type="text" class="form-control" placeholder="No. of volunteer " >
		                     </div>	
		                     <div class="form-group">                           
		                        <input type="text" class="form-control" placeholder="Background" >
		                     </div>	
		                     <div class="form-group">                           
		                        <input type="text" class="form-control" placeholder="Amount of cash" >
		                     </div>	
		                     <div class="form-group">                           
		                        <input type="text" class="form-control" placeholder="What kind of help " >
		                     </div>			        		
	                    </div>
		     	 	</div>
		     	</div>
		      <!-- Modal footer -->
		      <div class="modal-footer">
		        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->

			        <button class="disasterBtn createBtn"  data-dismiss="modal" type="submit">
			         Submit
			        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
			      </button>
		      </div>

		    </div>
		  </div>
		</div>


<!-- searcb box js -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#searchInput').hide();
        $('.searchGroupInput1').hide();
        $('#searchIcon').click(function(){
            //alert(""); 
            $('#searchInput').toggle("slide", { direction: "right" }, 3000);
        });
        $('.searchGroupInput2').click(function(){
        	$('.searchGroupInput1').toggle("slide", { direction: "right" }, 3000);
        })

        $('.editTextBtn').click(function(){
        	$('.editTextBox').toggle("slide", { direction: "right" }, 3000);
        	$('.coverTitelText').hide();
        	$('.editTextBtn').hide();
        })
       

    })
</script>

<!-- scroll header js -->
<script type="text/javascript">
    $(document).scroll(function(){
        if($(this).scrollTop() > 80)
        {   
            $('.navbar').addClass('navBg');
        }
        else{
            $('.navbar').removeClass('navBg');   
        }
    });


</script>



</body>
</html>
	 
	