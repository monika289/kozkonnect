<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">       
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/style.css">
         <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/dashboard.css">
         <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/about_inner_15.css">
         <style type="text/css">
         	.paddingLeft0px{
         		padding-left: 0px;
         	}

         </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light paddingLeft0px"> <!--bg-light-->
  <a class="navbar-brand" href="<?php echo base_url(); ?>">
    <img src="<?php echo asset_url();?>/images/Artboard – 182.png" class="logo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    
    </ul> 
    <form class="form-inline my-2 my-lg-0 pull-right">
        <div class="mr-sm-2 searchBox">            
            <span id="searchInput"><input class="form-control" type="search" placeholder="Search"></span>
            <!-- <i class='fas fa-search' id="searchIcon"></i> -->
            <img src="<?php echo asset_url();?>/images/Artboa9.png" id="searchIcon">
        </div>
      <button class="disasterBtn" type="submit">
        <a href=""> Disaster response</a> 
        <!-- <i class='fas fa-arrow-right'></i> -->
        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
      </button>
    </form>
  </div>
</nav>




<div style="clr"></div>

		<ol class="breadcrumb paddingTop80px">
			<li class="breadcrumb-item active">
				<a href="<?php echo base_url(); ?>dashboard">Home</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>myKonnect">MyKonnect</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="<?php echo base_url(); ?>causes">Causes</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="<?php echo base_url(); ?>myGroup">My Group</a>
			</li>

			<li class="breadcrumb-menu d-md-down-none">
			<div class="btn-group" role="group" aria-label="Button group">
				
				<ul class="rightSideNav">
					<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle btntext" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				         	<img class="profileImg" src="<?php echo asset_url();?>/images/Artboard48.png"> 
							<span class="nameTitle">Nilesh Watal</span>
				        </a>
				        <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard82.png"> Create page</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard83.png"> Activity</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard85.png"> Setting</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard84.png"> Log out</a>
				        </div>
				      </li>
					
					<li><a class="btntext" href="">
					
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard46.png"> </a></li>
					<li><a class="btntext" href="#">
						
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard47.png"> </a>
					</li>

				</ul>
			</div>
			</li>
	</ol>
	<div style="clr"></div>

	<style type="text/css">
		
	</style>
	
	
	<div class="container-fluid">
	    <div class="row">
	    	<div class="disasterHomeDiv"> <!--coverPhoto-->
	    		<div class="disasterBg">
					<div class="clr"></div>
					<div class="editCoverPhoto">
						<div class="form-group coverEditBtn">
						    <label for="coverEditBtn"></label> 
						    <input type="file" class="form-control-file" id="coverEditBtn">
						 </div>
					</div>
					<div class="col-lg-12 disasterTitel">
							<h2>Disaster Response</h2>
							<p>Vivamus elementum magna lacus</p>
							<p>Vivamus elementum lacus</p>
					</div>
				</div>
			</div>

	    	<div class="clr"></div>
	    	<div class="col-xl-3 col-lg-3 paddingTop10px">
	    		<div class="disasterCardBox disasterLeftDiv">
	    			<h5 class="disasterRTitel paddingLeft0px">What we are</h5>
	    			<div class="disasterLContent">
	    				Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. Maecenas aliquet fringilla vehicula. Nullam porttitor tempus molestie. Nulla blandit dui ut aliquam tristique. Quisque bibendum, nunc quis mattis semper, odio lacus ornare enim, vel posuere dui tortor non metus. Phasellus non dictum arcu, ut convallis lectus. Nullam tempus nunc   
	    				<div class="clr"></div>
	    				<a href="" class="seeMoreF" data-toggle="collapse" data-target="#demo1">
	        				<img class="seeMoreBtn" src="<?php echo asset_url();?>/images/Artboard – 173.png"> See more</a>
						  <div id="demo1" class="collapse disasterLContent">
						    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
						    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						  </div>
	    			</div>
	    		</div>
	    		<div class="clr"></div>

	    		<div class="disasterCardBox disasterLeftDiv">
	    			<h5  class="disasterRTitel paddingLeft0px">How can you contribute</h5>
	    			<div class="disasterLContent">
	    				Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. Maecenas aliquet fringilla vehicula. Nullam porttitor tempus molestie. Nulla blandit dui ut aliquam tristique. Quisque bibendum, nunc quis mattis semper, odio lacus ornare enim, vel posuere dui tortor non metus. Phasellus non dictum arcu, ut convallis lectus. Nullam tempus nunc   
	    				<div class="clr"></div>
	    				<a href="" class="seeMoreF" data-toggle="collapse" data-target="#demo2">
	        				<img class="seeMoreBtn" src="<?php echo asset_url();?>/images/Artboard – 173.png"> See more</a>
						  <div id="demo2" class="collapse disasterLContent">
						    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
						    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						  </div>
	    			</div>
	    		</div>
	    	</div>
		   
		    <!-- col-md-6 col-sm-6 col-xs-12-->
	        <div class="col-xl-6 col-lg-6 paddingTop10px">	        
	        	<div class="disasterMiddelCardBox" id="about_inner_15">
	        		
                    <ul class="cbp_tmtimeline">
                        <li>
                            <div class="cbp_tmicon cbp_tmicon-phone fa fa-mobile">
                            	<img src="<?php echo asset_url();?>/images/Artboard – 199.png">
                            </div>
                                <div class="cbp_tmlabel">
                                <div class="boxShadowD">
                                	<div class="dmainDiv">
                                		<h5 class="dT">Ask for help</h5>
	                                    <div class="fDiv">
	                                        <p class="editable abind3">Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet.   </p>
	                                        <div class="clr"></div>
	                                        <button class="contributeBtn reportBtn" data-toggle="modal" data-target="#askf" type="submit">Help</button>
	                                    </div>
                                    </div>
                                </div>
                                   <div class="rigtdisImg">
                                   	<img src="<?php echo asset_url();?>/images/Artboard130.png">
	                               </div>

                                </div>
                            
                        </li>
                        <li>
                            <div class="cbp_tmicon cbp_tmicon-phone fa fa-mobile">
                            	<img src="<?php echo asset_url();?>/images/Artboard – 200.png">
                            </div>
                                <div class="cbp_tmlabel">
                                <div class="boxShadowD">
                                	<div class="dmainDiv">
                                		<h5 class="dT">Offer help</h5>
	                                    <div class="fDiv">
	                                        <p class="editable abind3">Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet.   </p>
	                                        <div class="clr"></div>
	                                        <button class="contributeBtn reportBtn" data-toggle="modal" data-target="#offerHelp" type="submit">Help</button>
	                                    </div>
                                    </div>
                                </div>
                                   <div class="rigtdisImg">
                                   	<img src="<?php echo asset_url();?>/images/Artboard131.png">
	                               </div>

                                </div>
                            
                        </li>
                        <li>
                            <div class="cbp_tmicon cbp_tmicon-phone fa fa-mobile">
                            	<img src="<?php echo asset_url();?>/images/Artboard – 201.png">
                            </div>
                                <div class="cbp_tmlabel">
                                <div class="boxShadowD">
                                	<div class="dmainDiv">
                                		<h5 class="dT">Ask for friend</h5>
	                                    <div class="fDiv">
	                                        <p class="editable abind3">Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet.   </p>
	                                        <div class="clr"></div>
	                                        <button class="contributeBtn reportBtn" data-toggle="modal" data-target="#askFriend" type="submit">Help</button>
	                                    </div>
                                    </div>
                                </div>
                                   <div class="rigtdisImg">
                                   	<img src="<?php echo asset_url();?>/images/Artboard132.png">
	                               </div>

                                </div>
                            
                        </li>

                        <li>
                            <div class="cbp_tmicon cbp_tmicon-phone fa fa-mobile">
                            	<img src="<?php echo asset_url();?>/images/Artboard – 202.png">
                            </div>
                                <div class="cbp_tmlabel">
                                <div class="boxShadowD">
                                	<div class="dmainDiv">
                                		<h5 class="dT">Connect to the nearest</h5>
	                                    <div class="fDiv">
	                                        <p class="editable abind3">Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet.   </p>
	                                        <div class="clr"></div>
	                                        <button class="contributeBtn reportBtn" data-toggle="modal" data-target="#connectNearest" type="submit">Help</button>
	                                    </div>
                                    </div>
                                </div>
                                   <div class="rigtdisImg">
                                   	<img src="<?php echo asset_url();?>/images/Artboard133.png">
	                               </div>

                                </div>
                            
                        </li>
                      
                    </ul>
               
	        	</div>
	        	
	        	<div class="clr"></div>
	        	<div class="disasterVideoDiv">
	        		<h3>Get inspired from the stories of helping hands</h3>
	        		<style type="text/css">
	        			
	        		</style>
	        		<div class="disVideoDiv">
	        			<div class="disVidContentM">
	        				<div class="disVF">
	        					<h4>Vivamus elementum lacus</h4>
	        					<p>	Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. Maecenas aliquet fringilla vehicula. Nullam porttitor tempus molestie. </p>
	        				</div>
	        				<div class="disVS">
	        					<a href="" data-toggle="modal" data-target="#videoPlay1"><img src="<?php echo asset_url();?>/images/Artboard – 203.png"></a>
	        				</div>
	        			</div>
	        		</div>
	        		<div class="clr"></div>
	        		<div class="disVideoDiv">
	        			<div class="disVidContentM">
	        				<div class="disVF">
	        					<h4>Vivamus elementum lacus</h4>
	        					<p>	Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. Maecenas aliquet fringilla vehicula. Nullam porttitor tempus molestie. </p>
	        				</div>
	        				<div class="disVS">
	        					<a href="" data-toggle="modal" data-target="#videoPlay1"><img src="<?php echo asset_url();?>/images/Artboard – 203.png"></a>
	        				</div>
	        			</div>
	        		</div>
	        	</div>

	        	<div class="clr"></div>
	        	<div class="disasterBtnDiv">
	        		<button class="disasterBtn createBtn" type="submit">
				         Load More
				        <img src="http://localhost/newSite/assets//images/Artboard – 181.png">
				      </button>
	        	</div>
	    	</div>
	        
	        <div class="col-xl-3 col-lg-3 paddingTop10px">
	        	<div class="disasterRPanel marginTop20px">
	        		<h5 class="disasterRTitel">Disasters around</h5>
	        		<div class="disRiCard">
		        		<div class="disasterArImg">
		        			<img src="<?php echo asset_url();?>/images/Artboard134.png">
		        		</div>
		        		<div class="clr"></div>
		        		<div class="disasterRContent">
		        			<h5>How can you contribute</h5>
		        			<div class="clr"></div>
		        			
			        			<div class="disasterRBodyContent">
			        				Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. 
			        				<div class="clr"></div>
			        				<div class="clr"></div>
				    				<a href="" class="seeMoreF" data-toggle="collapse" data-target="#demo3">
				        				<img class="seeMoreBtn" src="<?php echo asset_url();?>/images/Artboard – 173.png"> See more</a>
									  <div id="demo3" class="collapse disasterRBodyContent">
									    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
									    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									  </div>

			        			</div>
			        			
		        		</div>
		        		<div class="clr"></div>
		        	</div>
		        	<div class="clr"></div>
		        	<div class="disRiCard">
		        		<div class="disasterArImg">
		        			<img src="<?php echo asset_url();?>/images/Artboard134.png">
		        		</div>
		        		<div class="clr"></div>
		        		<div class="disasterRContent">
		        			<h5>How can you contribute</h5>
		        			<div class="clr"></div>
		        			
			        			<div class="disasterRBodyContent">
			        				Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. 
			        				<div class="clr"></div>
			        				<div class="clr"></div>
				    				<a href="" class="seeMoreF" data-toggle="collapse" data-target="#demo4">
				        				<img class="seeMoreBtn" src="<?php echo asset_url();?>/images/Artboard – 173.png"> See more</a>
									  <div id="demo4" class="collapse disasterRBodyContent">
									    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
									    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									  </div>

			        			</div>
			        			
		        		</div>
		        		<div class="clr"></div>
		        	</div>

	        	</div>
	        </div>
	        
	        <div class="LoginbgCircle1 dashBgImg"></div>





	        <!-- Ask for help -->
			<div class="modal disasterModel" id="askf">
			  <div class="modal-dialog">
			    <div class="modal-content disasterContent">

			      <!-- Modal Header -->
			    

			      <!-- Modal body -->
			      <div class="modal-body">
			       <h4 class="modal-title">Ask for help</h4>

			       		<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Name of the disaster">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Date">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Location">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<div class="dropdown show">
							  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="askf1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							    What kind of help you need
							  </a>

							  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="askf1" x-placement="bottom-start">
							  	<div class="form-check">
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio1"> Food
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio11"> Water
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio12"> Shelter
										  </label>
									
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio13"> Lorem
										  </label>
										
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio14"> Ipsum
										  </label>
										
									</a>
								</div>
							  </div>
							</div>
	                 	</div>
			      </div>

			      <!-- Modal footer -->
			      <div class="modal-footer">
			        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
			        	<button class="disasterBtn createBtn" type="submit">
				         Help
				        <img src="http://localhost/newSite/assets//images/Artboard – 181.png">
				      </button>
			      </div>

			    </div>
			  </div>
			</div>


			 <!-- Offer help -->
			<div class="modal disasterModel" id="offerHelp">
			  <div class="modal-dialog">
			    <div class="modal-content disasterContent">

			      <!-- Modal Header -->
			    

			      <!-- Modal body -->
			      <div class="modal-body">
			       <h4 class="modal-title">Offer help</h4>

			       		<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Name of the disaster">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Date">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Location">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<div class="dropdown show">
							  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="offerHelp1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							    What would you like to offer
							  </a>

							  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="offerHelp1" x-placement="bottom-start">
							  	<div class="form-check">
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" name="optradio21" class="form-check-input"> Food
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio"  name="optradio22" class="form-check-input"> Water
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio23"> Shelter
										  </label>
									
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio24"> Lorem
										  </label>
										
									</a>
								    <a class="dropdown-item" href="#">
									  <label class="form-check-label">
									    <input type="radio" class="form-check-input" name="optradio25"> Ipsum
									  </label>
									</a>
								</div>
							  </div>
							</div>
	                 	</div>
			      </div>

			      <!-- Modal footer -->
			      <div class="modal-footer">
			        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
			        	<button class="disasterBtn createBtn" type="submit">
				         Help
				        <img src="http://localhost/newSite/assets//images/Artboard – 181.png">
				      </button>
			      </div>

			    </div>
			  </div>
			</div>



			 <!-- ask for friend -->
			<div class="modal disasterModel" id="askFriend">
			  <div class="modal-dialog">
			    <div class="modal-content disasterContent">

			      <!-- Modal Header -->
			    

			      <!-- Modal body -->
			      <div class="modal-body">
			       <h4 class="modal-title">Ask for friend</h4>

			       		<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Name of the disaster">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Date">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Location">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<div class="dropdown show">
							  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="askFrd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							    What would you like to offer
							  </a>

							  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="askFrd" x-placement="bottom-start">
							  	<div class="form-check">
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio31"> Food
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio32"> Water
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio33"> Shelter
										  </label>
									
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio34"> Lorem
										  </label>
										
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio35"> Ipsum
										  </label>
										
									</a>
								</div>
							  </div>
							</div>
	                 	</div>
	                 	<div class="form-group">                           
	                   		<div class="dropdown show">
							  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="askFrd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							    What would you like to offer
							  </a>

							  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="askFrd1" x-placement="bottom-start">
							  	<div class="form-check">
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio41"> Friend1
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio42"> Friend2
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio43"> Friend3
										  </label>
									
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio44"> Friend4
										  </label>
										
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio45"> Friend5
										  </label>
										
									</a>
								</div>
							  </div>
							</div>
	                 	</div>
			      </div>

			      <!-- Modal footer -->
			      <div class="modal-footer">
			        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
			        	<button class="disasterBtn createBtn" type="submit">
				         Ask help
				        <img src="http://localhost/newSite/assets//images/Artboard – 181.png">
				      </button>
			      </div>

			    </div>
			  </div>
			</div>


			 <!-- connect to nearest -->
			<div class="modal disasterModel" id="connectNearest">
			  <div class="modal-dialog">
			    <div class="modal-content disasterContent">

			      <!-- Modal Header -->
			    

			      <!-- Modal body -->
			      <div class="modal-body">
			       <h4 class="modal-title">Connect to the nearest</h4>

			       		<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Name of the disaster">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Date">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<input type="text" class="form-control" placeholder="Location">
	                 	</div>
	                 	<div class="form-group">                           
	                   		<div class="dropdown show">
							  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="connetN1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							    What kind of help you need
							  </a>

							  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="connetN1" x-placement="bottom-start">
							  	<div class="form-check">
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio51"> Food
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio52"> Water
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio53"> Shelter
										  </label>
									
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio54"> Lorem
										  </label>
										
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio55"> Ipsum
										  </label>
										
									</a>
								</div>
							  </div>
							</div>
	                 	</div>
	                 	<div class="form-group">                           
	                   		<div class="dropdown show">
							  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="connetN2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							    Choose from the people near you
							  </a>

							  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="connetN2" x-placement="bottom-start">
							  	<div class="form-check">
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio61"> Friend1
										    <span class="frdMeter">
										    	300m
										    </span>
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio62"> Friend2
										      <span class="frdMeter">
										    	700m
										    </span>
										  </label>
										
								     </a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio63"> Friend3
										      <span class="frdMeter">
										    	1km
										    </span>
										  </label>
									
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio64"> Friend4
										      <span class="frdMeter">
										    	1.3km
										    </span>
										  </label>
										
									</a>
								    <a class="dropdown-item" href="#">
								    	
										  <label class="form-check-label">
										    <input type="radio" class="form-check-input" name="optradio65"> Friend5
										      <span class="frdMeter">
										    	2km
										    </span>
										  </label>
										
									</a>
								</div>
							  </div>
							</div>
	                 	</div>
			      </div>

			      <!-- Modal footer -->
			      <div class="modal-footer">
			        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
			        	<button class="disasterBtn createBtn" type="submit">
				         Ask help
				        <img src="http://localhost/newSite/assets//images/Artboard – 181.png">
				      </button>
			      </div>

			    </div>
			  </div>
			</div>


			<!-- connect to nearest -->
			<div class="modal disasterModel" id="videoPlay1">
			  <div class="modal-dialog">
			    <div class="modal-content disasterContent">

			      <!-- Modal Header -->
			    

			      <!-- Modal body -->
			      <div class="modal-body">

			      	<iframe src="https://www.youtube.com/embed/IP7uGKgJL8U" allowfullscreen="" width="100%" frameborder="0" height="100%"></iframe>
			      </div>

			     

			    </div>
			  </div>
			</div>





	    </div>
	 </div>













<!-- searcb box js -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#searchInput').hide();
        //alert("");
        $('#searchIcon').click(function(){
            //alert(""); 
            $('#searchInput').toggle("slide", { direction: "right" }, 3000);;
        });
    })
</script>

<!-- scroll header js -->
<script type="text/javascript">
    $(document).scroll(function(){
        if($(this).scrollTop() > 20)
        {   
            $('.navbar').addClass('navBg');
        }
        else{
            $('.navbar').removeClass('navBg');   
        }
    });


</script>



</body>
</html>
	 
	