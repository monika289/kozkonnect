<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>calendar/jquery.calendar.css">

<div class="col-xl-3 col-lg-3" id="left-nav"><!--col-md-3 col-sm-3 col-xs-12-->
	
	<img src="<?php echo asset_url();?>/images/Artboard – 184.png" class="mobileViewClose close">
	<div class="clr"></div>
	        	<div class="dashboardBox">
	        		<div class="text-center profileDiv">
	        			<div class="profileDot">
	        				<img src="<?php echo asset_url();?>/images/Artboard74.png">
	        			</div>
	        			<div class="clr"></div>
	        			<span class="loginTime">// 4 mins ago</span>
	        			<div class="userImage">
	        				
	                    	<img src="<?php echo asset_url();?>/images/Artboard114.png" class="mx-auto img-fluid rounded-circle">
	                    	<div class="imageEditor">
	                    		<!-- <i class='fas fa-pen'></i> -->
	                    		<img src="<?php echo asset_url();?>/images/Artboard63.png">
	                    	</div>
	                    </div>
	                    <h5>Responsive</h5>
	                    <p class="text-muted">Designation</p>
	                     <p class="text-muted">Age . City, State</p>
	                </div>

	        	</div>
	        	<div class="clr"></div>
	        	<div class="dashboardBox">
	        		<h5 class="dashboardBoxTitle">Intro 
	        			<span class="editorImg" id="introEditBox">
	        				<img src="<?php echo asset_url();?>/images/Artboard63.png">
	        			</span>
	        		</h5>


	        		<!-- <div class="input-group custUpdateBox introTextBox" style="display: none">
				        <input type="text" class="form-control" placeholder="Enter your Intro" id="txtSearch"/>
				        <div class="input-group-btn">
				          <button class="btn btn-primary" type="submit">
				            Update
				          </button>
				        </div>
				    </div> -->

	        		<div class="dashboardContent" id="introContent">
	        			Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. Maecenas aliquet fringilla vehicula. Nullam porttitor tempus molestie. Nulla blandit dui ut aliquam tristique. Quisque bibendum, nunc quis mattis semper, odio lacus ornare enim, vel posuere dui tortor non metus. Phasellus non dictum arcu, ut convallis lectus. Nullam tempus nunc
	        			<div class="clr"></div>
	        			<a href="" class="seeMoreF" data-toggle="collapse" data-target="#demo1">
	        				<img class="seeMoreBtn" src="<?php echo asset_url();?>/images/Artboard – 173.png"> See more</a>
						  <div id="demo1" class="collapse dashboardContent">
						    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
						    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						  </div>
	        			
	        		</div>
	        		<style type="text/css">
	        			
	        		</style>
	        		<div class="introUpdateContent" id="introUpdateContent" style="display: none;">
	        			<div class="form-group">
						  <!-- <label for="comment" class="dashboardBoxTitle">Intro Content:</label> -->
						  <textarea class="form-control dashboardContent" rows="5" id="comment"></textarea>
						</div>
	        			<div class="profileEditBioDiv">
		        			<button class="profileEditBioBtn" id="introEditCancel"  type="submit">Cancel</button>
		        			<button class="profileEditBioBtn" type="submit">Save</button>
		        		</div>
	        		</div>






	        	</div>
	        	<div class="clr"></div>
	        	<div class="dashboardBox">
	        		<h5 class="dashboardBoxTitle">Bio <span class="editorImg" id="editBio"><img src="<?php echo asset_url();?>/images/Artboard63.png"></span></h5>
	        		<div class="bioContentEdit">
		        		<ul class="bioList">
		        			<li><div class="bioImg"><img src="<?php echo asset_url();?>/images/Artboard58.png"></div><span class="bioText"> Birthdate</span></li>
		        			<li><div class="bioImg"><img src="<?php echo asset_url();?>/images/Artboard59.png"></div><span class="bioText"> Contact</span></li>
		        			<li><div class="bioImg"><img src="<?php echo asset_url();?>/images/Artboard60.png"></div><span class="bioText"> Location</span></li>
		        			<li><div class="bioImg"><img src="<?php echo asset_url();?>/images/Artboard61.png"></div><span class="bioText"> Individual</span></li>
		        			<li><div class="bioImg"><img src="<?php echo asset_url();?>/images/Artboard62.png"></div><span class="bioText"> Followers</span></li>
		        		</ul>
	        		</div>


	        		<div class="bioContentUpdate" style="display: none">
	        			<ul class="bioList">
		        			<li><input type="text" class="form-control bioText" name="" placeholder="Birthdate"></li>
		        			<li><input type="text" class="form-control bioText" name="" placeholder="Contact"></li>
		        			<li><input type="text" class="form-control bioText" name="" placeholder="Location"></li>
		        			<li><input type="text" class="form-control bioText" name="" placeholder="Individual"></li>
		        			<li><input type="text" class="form-control bioText" name="" placeholder="Followers"></li>
		        		</ul>

		        		<div class="profileEditBioDiv">
		        			<button class="profileEditBioBtn cancelBio" type="submit">Cancel</button>
		        			<button class="profileEditBioBtn" type="submit">Save</button>
		        		</div>
		        	</div>

	        	</div>
	        	<div class="clr"></div>
	        	<div class="dashboardBox">
	        		<h5 class="dashboardBoxTitle">SDG I support <span class="editorImg" style="float: right;"><img src="<?php echo asset_url();?>/images/Artboard63.png"></span></h5>
	        		<ul class="SDGList">
	        			<li>
	        				<div class="SDGImg"><img src="<?php echo asset_url();?>/images/Artboard79.png"> </div><span class="SDGText"> Name of the SDG</span></li>
	        			<li>
	        				<div class="SDGImg"><img src="<?php echo asset_url();?>/images/Artboard79.png"> </div><span class="SDGText"> Name of the SDG</span></li>
	        			
	        			<li>
	        				<div class="SDGImg"><img src="<?php echo asset_url();?>/images/Artboard79.png"> </div><span class="SDGText"> Name of the SDG</span></li>
	        			<li>
	        				<div class="SDGImg">
	        				<div class="form-group sdgUpload">
							    <label for="sdgUpload"></label> 
							    <input type="file" class="form-control-file" id="sdgUpload">
							 </div>
							</div>
							<span class="SDGText"> Add more</span>

	        			</li>
	        			<li>
	        				<a href="" class="seeMoreF" data-toggle="collapse" data-target="#demo2">
	        				<img class="seeMoreBtn" src="<?php echo asset_url();?>/images/Artboard – 173.png" style="padding-right: 0px !important;"> See more</a>
	        				<div class="clr"></div>
						  	<div id="demo2" class="collapse">
						    <img src="<?php echo asset_url();?>/images/Artboard79.png"> Name of the SDG</div>
						  </li>
	        		</ul>	        		
	        	</div>
	        	<div class="clr"></div>
	        	<div class="dashboardBox">
	        		<h5 class="dashboardBoxTitle marginBottom20px">Ratings </h5>
	        		<div class="progessBarDiv">
					  <input type="range" min="1" max="100" value="30" class="progessBar" id="myRange">
					  <p>Custom range slider:</p>
					</div>
					<div class="progessBarDiv">
					  <input type="range" min="1" max="100" value="80" class="progessBar" id="myRange">
					  <p>Custom range slider:</p>
					</div> 
					<div class="progessBarDiv">
					  <input type="range" min="1" max="100" value="10" class="progessBar" id="myRange">
					  <p>Custom range slider:</p>
					</div>
					<div class="progessBarDiv">
					  <input type="range" min="1" max="100" value="50" class="progessBar" id="myRange">
					  <p>Custom range slider:</p>
					</div>        		
	        	</div>
	        	<div class="clr"></div>
	        	<div class="dashboardBox">
	        		<h5 class="dashboardBoxTitle">My contributions </h5>
	        		 <div class="myContribution">
	        			<div class="myContImg">
	        				<img src="<?php echo asset_url();?>/images/Artboard – 178.png">
	        			</div>
	        			<div class="myContContent">
	        				<p class="myContname">Loerm Ipsum</p>
	        				<span class="mytime">13 hours ago</span>
	        			</div>
	        			
	        		</div> 
	        		<div class="myContribution">
	        			<div class="myContImg">
	        				<img src="<?php echo asset_url();?>/images/Artboard – 178.png">
	        			</div>
	        			<div class="myContContent">
	        				<p class="myContname">Loerm Ipsum</p>
	        				<span class="mytime">13 hours ago</span>
	        			</div>
	        			
	        		</div> 
	        		<div class="myContribution">
	        			<div class="myContImg">
	        				<img src="<?php echo asset_url();?>/images/Artboard – 178.png">
	        			</div>
	        			<div class="myContContent">
	        				<p class="myContname">Loerm Ipsum</p>
	        				<span class="mytime">13 hours ago</span>
	        			</div>	        			
	        		</div> 
	        		<a href="" class="seeMoreF" data-toggle="collapse" data-target="#demo3"><img class="seeMoreBtn" src="<?php echo asset_url();?>/images/Artboard – 173.png">  See more</a>
	        		<div class="clr"></div>
					<div id="demo3" class="collapse">
						<div class="myContribution">
						    <div class="myContImg">
		        				<img src="<?php echo asset_url();?>/images/Artboard – 178.png">
		        			</div>
		        			<div class="myContContent">
		        				<p class="myContname">Loerm Ipsum</p>
		        				<span class="mytime">13 hours ago</span>
		        			</div>	
	        			</div>
					</div>

	        	</div>


	        	<div class="clr"></div>
	        	<div class="dashboardBox paddingLR0px ngoDisplayNone">
	        		<h5 class="dashboardBoxTitle paddingLR15px">My calender </h5>
	        		 
	        			<div id="pnlSimpleCalendar" style="width:100%;"></div>
						<script>
						$(function () {
							$('#pnlSimpleCalendar').calendar();
						});
						</script>
	        		
	        		<div class="clr"></div>
	        		<div class="eventMainD  paddingLR15px">
	        			<div class="eventCircle todayBg"></div>
	        			<span>Today</span>
	        		</div>
	        		<div class="eventMainD  paddingLR15px">
	        			<div class="eventCircle upCommingBg"></div>
	        			<span>Upcoming events</span>
	        		</div>
	        		<div class="eventMainD  paddingLR15px">
	        			<div class="eventCircle contributeBg"></div>
	        			<span>Contribute </span>
	        		</div>
	        		<div class="eventMainD  paddingLR15px">
	        			<div class="eventCircle complEventBg"></div>
	        			<span>Completed events</span>
	        		</div>

	        	</div>


	        </div>

 <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script> -->
	<script type="text/javascript" src="<?php echo asset_url();?>calendar/jquery.calendar.js"></script>

    <script type="text/javascript">
	
	 	$(document).ready(function(){
	 		$('.mobileLRDiv').hide();
	 		var wid=$(window).width();
	 		//alert(wid);
	 		if(wid <= 768){	 			
	 			$('.mobileLRDiv').show();
	 			$('#left-nav').hide();
	 			$('#right-nav').hide();
	 		}
	 		$('#leftSide').click(function(){
	 			$('#left-nav').addClass('leftNN').show("slide", { direction: "left" }, 2000);
	 			$('.mobileViewClose').show();
	 		})
	 		$('#rightSide').click(function(){
	 			$('#right-nav').addClass('leftNN').show("slide", { direction: "right" }, 2000);
	 			$('.mobileViewClose').show();
	 		})
	 		$('.close').click(function(){
	 			$('#left-nav').hide("slide", { direction: "right" }, 2000);
 				$('#right-nav').hide("slide", { direction: ";left" }, 2000);
	 		});

	 		$('#introEditBox').click(function(){
	 			//alert("Hello");
	 			$('#introContent').hide();
	 			$('.introUpdateContent').show("slide", { direction: "top" }, 2000);
	 			$(".introUpdateContent textarea").focus();
	 		});
	 		$('#introEditCancel').click(function(){
	 			$('.introUpdateContent').hide("slide", { direction: "top" }, 2000);
	 			$('#introContent').show("slide", { direction: "top" }, 2000);
	 		});
	 		$('#editBio').click(function(){	 				
	 			$('.bioContentEdit').hide("slide", { direction: "top" }, 2000);
	 			$('.bioContentUpdate').show("slide", { direction: "top" }, 2000);
	 		});
	 		$('.cancelBio').click(function(){
	 			$('.bioContentUpdate').hide("slide", { direction: "top" }, 2000);
	 			$('.bioContentEdit').show("slide", { direction: "top" }, 2000);	 			
	 		})
	 	});
	 </script>