<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">       
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/style.css">
         <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/dashboard.css">
         <!-- <link rel="stylesheet" type="text/css" href="http://localhost/newSite/assets/css/about_inner_15.css"> -->
         <style type="text/css">
         	.editorImg{
         		display: none;
         	}
         	#left-nav{
         		margin-top: 20px;
         	}
         	#right-nav{
         		margin-top: 20px;
         	}
         	
         </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light paddingLeft0px"> <!--bg-light-->
  <a class="navbar-brand" href="<?php echo base_url(); ?>">
    <img src="<?php echo asset_url();?>/images/Artboard – 182.png" class="logo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    
    </ul> 
    <form class="form-inline my-2 my-lg-0 pull-right">
        <div class="mr-sm-2 searchBox">            
            <span id="searchInput"><input class="form-control" type="search" placeholder="Search"></span>
            <!-- <i class='fas fa-search' id="searchIcon"></i> -->
            <img src="<?php echo asset_url();?>/images/Artboa9.png" id="searchIcon">
        </div>
      <button class="disasterBtn" type="submit">
        <a href=""> Disaster response</a> 
        <!-- <i class='fas fa-arrow-right'></i> -->
        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
      </button>
    </form>
  </div>
</nav>




<div style="clr"></div>

		<ol class="breadcrumb paddingTop80px">
			<li class="breadcrumb-item active">
				<a href="<?php echo base_url(); ?>dashboard">Home</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>myKonnect">MyKonnect</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="<?php echo base_url(); ?>causes">Causes</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="<?php echo base_url(); ?>myGroup">My Group</a>
			</li>

			<li class="breadcrumb-menu d-md-down-none">
			<div class="btn-group" role="group" aria-label="Button group">
				
				<ul class="rightSideNav">
					<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle btntext" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				         	<img class="profileImg" src="<?php echo asset_url();?>/images/Artboard48.png"> 
							<span class="nameTitle">Nilesh Watal</span>
				        </a>
				        <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard82.png"> Create page</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard83.png"> Activity</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard85.png"> Setting</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard84.png"> Log out</a>
				        </div>
				      </li>
					
					<li><a class="btntext" href="">
					
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard46.png"> </a></li>
					<li><a class="btntext" href="#">
						
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard47.png"> </a>
					</li>

				</ul>
			</div>
			</li>
	</ol>
	<div style="clr"></div>

	
	
	
	<div class="container-fluid">
	    <div class="row">
	   	<div class="profileBgImg">
	   		<div class="frdshodowColor">
				<div class="clr"></div>
				<div  class="editCoverPhoto">
					<!-- <img src="<?php echo asset_url();?>/images/Artboard63.png"> -->
					<div class="form-group coverEditBtn">
					    <label for="coverEditBtn"></label> 
					    <input type="file" class="form-control-file" id="coverEditBtn">
					 </div>
				</div>


				<div class="profiePhotoChange">
					<div class="form-group profilePhotoEdit">
					    <label for="profilePhoto"></label> 
					    <input type="file" class="form-control-file" id="profilePhoto">
					 </div>
				</div>
				<div class="converTitel">
				 	<h1 class="coverTitelText"> Nilesh Watal </h1>
					<div class="clr"></div>
				</div>
			</div>
		</div>
	<style type="text/css">

	</style>
	<div class="col-offset-xl-2 col-xl-12 frdProfileMenu">
		<ul>
			<li><a href=""><img src="<?php echo asset_url();?>/images/profile/Artboard118.png" title="About us"> <span>About us</span></a></li>
			<li><a href=""><img src="<?php echo asset_url();?>/images/profile/Artboard119.png" title="Friends"> <span>Friends</span></a></li>
			<li><a href=""><img src="<?php echo asset_url();?>/images/profile/Artboard121.png" title="Photos"> <span>Photos</span></a></li>
			<li><a href=""><img src="<?php echo asset_url();?>/images/profile/Artboard120.png" title="Video"> <span>Video</span></a></li>
			<li><a href=""><img src="<?php echo asset_url();?>/images/profile/Artboard124.png" title="Activity"> <span>Activity</span></a></li>
			<li><a href=""><img src="<?php echo asset_url();?>/images/profile/Artboard123.png" title="Check in"> <span>Check in</span></a></li>
			<li><a href=""><img src="<?php echo asset_url();?>/images/profile/Artboard122.png" title="Events"> <span>Events</span></a></li>
		</ul>
	</div>

	

	    
		   
		   
	        
	    
	        
	        <!-- <div class="LoginbgCircle1 dashBgImg"></div> -->
	    </div>
	</div>

<div class="clr"></div>
	<div class="mobileLRDiv">
		<a id="leftSide"> Left Menu</a>
		<a id="rightSide" style="float: right;"> Right Menu</a>
	</div>


	<div class="container-fluid">
	    <div class="row">
	    	

		     <?php $this->load->view('left-nav')?>
		    <!-- col-md-6 col-sm-6 col-xs-12-->
	        <div class="col-xl-6 col-lg-6 paddingTop20px">
	        	<div class="dashboardSearchBox marginBottom5px" id="shareYourThought" style="display: none;">
	        		<div class="SearchBoxpadding">
		        		<div class="textArea">
		 
		        			<textarea class="form-control" rows="2" id="comment" placeholder="Share your thought here"></textarea>
		        		</div>

		        		<div class="sharePhone">
		        			<ul>
		        				<li><img src="<?php echo asset_url();?>/images/Artboard – 189.png"></li>
		        				<li class="add">
		        					<!-- <img src="<?php echo asset_url();?>/images/Artboard – 174.png"> -->
		        					<div class="form-group fileUploadBtn1">
									    <label for="exampleFormControlFile1"></label> 
									    <input type="file" class="form-control-file" id="exampleFormControlFile1">
									 </div>

		        				</li>
		        				
		        			</ul>
		        		</div>
		        		<div class="clr"></div>
		        		<div class="tagPeople">
		        			<p>Tag people you are with</p>
		        		</div>

		        		<div class="clr"></div>
		        		<div class="shareBtnMain">
		        			<ul>
		        				<li><button type="button" class="btn shareBtn">Feeling/Activity</button></li>
		        				<li><button type="button" class="btn shareBtn">Check In</button></li>
		        				<li><button type="button" class="btn shareBtn">Live</button></li>
		        				<li><button type="button" class="btn shareBtn">Event</button></li>
		        				
		        			</ul>
		        			<ul class="rightSideIcon floatRight">
		        				<li><img class="shareImgJs" src="<?php echo asset_url();?>/images/Artboard44.png"></li>
		        				<li><img src="<?php echo asset_url();?>/images/Artboard45.png"></li>
		        				<li>
		        					<div class="dropdown shareDotDropBtn">
										  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
										    <img src="<?php echo asset_url();?>/images/Artboard69.png">
										  </button>
										  <div class="dropdown-menu  dropdown-menu-right">
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard124.png"> Felling/Activity</a>
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard123.png"> Check In</a>
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard83.png"> Event</a>
										   
										    <div class="clr"></div>
										  </div>
									</div>
								</li>
		        			</ul>
		        			<div class="clr"></div>
		        		</div>
		        		<div class="clr"></div>
		        		<div class="eventGrid">
		        			<div class="row">
		        				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
		        					<div class="custCheckbox">
		        						<input type="checkbox"  id="Ford" value="Ford">
		        						<label for="Ford"></label>
		        					</div>
		        				</div>
		        				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">	        					
		        					<div class="imageWid">
		        						<img src="<?php echo asset_url();?>/images/Artboard101.png">
		        					</div>
		        				</div>
		        				<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">
		        					<div class="timeLineTitel">Timeline</div>
		        				</div>
		        				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
		        					<div class="dropdown frdBtn">
										  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
										    <!-- <img src="<?php echo asset_url();?>/images/Artboard69.png"> -->
										    Friends
										  </button>
										  <div class="dropdown-menu">
										    <!-- <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard124.png"> Felling/Activity</a> -->
										   
										   
										    <div class="clr"></div>
										  </div>
									</div>
		        				</div>
		        				<div class="clr"></div>
		        			</div>
		        			<div class="row">
		        				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
		        					<div class="custCheckbox">
		        						<input type="checkbox"  id="Ford1">
		        						<label for="Ford1"></label>
		        					</div>
		        				</div>
		        				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">	        					
		        					<div class="imageWid">
		        						<img src="<?php echo asset_url();?>/images/Artboard48.png">
		        					</div>
		        				</div>
		        				<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">
		        					<div class="timeLineTitel">Story</div>
		        				</div>
		        				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
		        					<div class="dropdown frdBtn">
										  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
										    <!-- <img src="<?php echo asset_url();?>/images/Artboard69.png"> -->
										    Friends
										  </button>
										  <div class="dropdown-menu">
										    <!-- <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard124.png"> Felling/Activity</a> -->
										  
										   
										    <div class="clr"></div>
										  </div>
									</div>
		        				</div>
		        				<div class="clr"></div>
		        			</div>
		        		</div>

	        		
	        		</div>
	        		<div>
	        			<button type="button" class="btn shareFullBtn">Share</button>
	        		</div>
	        	</div>
	        	<div class="dashboardBox marginBottom5px" id="shareMainDiv">
	        		<div class="shareDiv1">
	        			<img src="<?php echo asset_url();?>/images/Artboard49.png" style="margin-right: 15px;"> <span>Share your thought</span>
	        		</div>

	        		
	        		<div class="shareDiv2">
	        			<ul>
	        				<li><img class="shareImgJs" src="<?php echo asset_url();?>/images/Artboard44.png"></li>
	        				<li><img src="<?php echo asset_url();?>/images/Artboard45.png"></li>
	        				<li>
	        					<div class="dropdown shareDotDropBtn">
									  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
									    <img src="<?php echo asset_url();?>/images/Artboard69.png">
									  </button>
									  <div class="dropdown-menu  dropdown-menu-right">
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard124.png"> Felling/Activity</a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard123.png"> Check In</a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard122.png"> Event</a>
									   
									    <div class="clr"></div>
									  </div>
								</div>
							</li>
	        			</ul>
	        		</div>
	        		<div class="clr"></div>
	        	</div>



	        	
	        	<div class="clr"></div>
	        	<div class="dashboardBox paddingLR0px marginTop20px">
	        		<div class="midProfileDiv">
		        		<div class="paddingLR15px">
		        			

		        			<div class="row">

		        				<div class="col-xl-11 col-lg-11 col-md-11 col-10 ">
		        					<div class="midImg">
				        				<img src="<?php echo asset_url();?>/images/Artboard70.png">
				        			</div>
		        					<div class="midContent">
				        				<p class="midMyContname">Lorem ipsum lorem ipsum</p>
				        				<span class="midMytime">Date , Time</span>
				        			</div>
		        				</div>
		        				<div class="col-xl-1 col-lg-1 col-md-1 col-1">
		        					<div class="dropdown dotDropBtn">
										  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
										    <img src="<?php echo asset_url();?>/images/Artboard69.png">
										  </button>
										  <div class="dropdown-menu dropdown-menu-right">
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard98.png"> Hide post</a>
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/save post.png"> Save post</a>
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard99.png"> Unfollow name</a>
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard100.png"> Lorem ipsum</a>
										    <div class="clr"></div>
										  </div>
									</div>
		        				</div>
		        				
		        			</div>

		        			
		        			<div class="clr"></div>
		        			<div class="userCommentTitel">
		        				<div class="midMyCont">Lorem ipsum dolor sit amet, consectetur </div>	
		        				
		        				<div class="correctDiv">
									<img src="<?php echo asset_url();?>/images/Artboard – 177.png">        			
			        			</div>

							    <div class="dropdown contributeBtn">
								  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
								    Contribute
								  </button>
								  <div class="dropdown-menu dropdown-menu-right">
								    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard98.png"> Volunteer</a>
								   <!--  <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard97.png"> Casg</a> -->
								    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard99.png"> Kind</a>
								    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard100.png"> All</a>
								    <div class="clr"></div>
								  </div>
								</div>

								<div class="clr"></div>
		        			</div>
		        		</div>
	        			<div class="imageWidth">
	       					<img src="<?php echo asset_url();?>/images/Artboard67.png">
	        			</div>
	        			<div class="supportlist">
	        				<ul >
	        					<li><img src="<?php echo asset_url();?>/images/Artboard64.png"> Support</li>
	        					<li>
	        						<!-- <img src="<?php echo asset_url();?>/images/Artboard65.png"> Comment -->

	        						<div class="dropdown CommentBtn">
									  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
									   <img src="<?php echo asset_url();?>/images/Artboard65.png"> Comment
									  </button>
									  <div class="dropdown-menu">
									    <a class="dropdown-item" href="#">
									    	<img src="<?php echo asset_url();?>/images/Artboard103.png"> Share on timeline
									    </a>
									    <a class="dropdown-item" href="#"  data-toggle="modal" data-target="#exampleModalCenter">
									    	<img src="<?php echo asset_url();?>/images/Artboard104.png"> Share as message
									    </a>
									    <a class="dropdown-item" href="#">
									    	<img src="<?php echo asset_url();?>/images/Artboard106.png"> Share in a group
									    </a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard105.png"> Share on friend’s timeline
									    </a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard107.png"> Share to a page
									    </a>
									    <div class="clr"></div>
									  </div>
									</div>


	        					</li>
	        					<li><img src="<?php echo asset_url();?>/images/Artboard66.png"> Share</li>	        					
	        				</ul>
	        				<ul class="rightUl">
	        					<li><a href="">23 Supports |</a><a href=""> 2 Comments</a></li>
	        				</ul>
	        			</div>
	        			<div class="paddingLR15px">
	        				<p class="viewUser">500 views l 220 supporters l 336 volunteers l Rs.1,23,445 donation raised </p>
	        				<div class="progressBarDv">
	        					<div class="rateUser">
		        					<span class="viewUser">Rate cause </span>
		        				</div>
	        					<div class="rateMark">
	        						<ul class="stepprogressbar">
							            <li class="active"></li>
							            <li></li>
							            <li></li>
							            <li></li>
							            <li></li>
							        </ul>
	        						
								 
								</div> 
								<div class="clr"></div>
	        				</div>
	        				<div class="viewBodyContent paddingTB15px">
	        					Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. Maecenas aliquet fringilla vehicula. Nullam porttitor tempus molestie. Nulla blandit dui ut aliquam tristique. Quisque bibendum, nunc quis mattis semper, odio lacus ornare enim, vel posuere dui tortor non metus. Phasellus non dictum arcu, ut convallis lectus. Nullam tempus nunc   Read more
	        					
	        					<a href="" class="seeMoreF" data-toggle="collapse" data-target="#demo10">
		        				 See more</a>
								  <div id="demo10" class="collapse viewBodyContent">
								    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
								    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								  </div>
	        				</div>
	        				<div class="clr"></div>
	        				<div class="userCommentBox">
	        					<div class="commentImg">
	        						<img src="<?php echo asset_url();?>/images/Artboard48.png">
	        					</div>
	        					<div class="commentText">
	        						<div class="form-group">                           
				                        <input type="text" class="form-control" placeholder="Write a comment">
				                     </div>
	        					</div>
	        					<div class="clr"></div>
	        				</div>
	        				<div class="userCommentBox">
	        					<div class="commentImg">
	        						<img src="<?php echo asset_url();?>/images/Artboard72.png">
	        					</div>
	        					<div class="commentText">
	        						<p class="commenttt"><span class="commentName">Priya Nayak </span> Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips</p>
	        						<span class="replyTime">Reply , Time</span>
	        						

	        						<div class="userCommentBox commentReply" style="display: none;">
			        					<div class="commentImg">
			        						<img src="http://localhost/newSite/assets//images/Artboard48.png">
			        					</div>
			        					<div class="commentText">
			        						<div class="form-group">                           
						                        <input type="text" class="form-control" placeholder="Write a comment">
						                     </div>
			        					</div>
			        					<div class="clr"></div>
			        				</div>


	        					</div>
	        					<div class="clr"></div>
	        				</div>
	        					        			
		        			<div style="float: right;">
		        					<button class="contributeBtn reportBtn" data-toggle="modal" data-target="#reportCause" type="submit">Report</button>
							      
		        			</div>
		        			<div class="clr"></div>
	        			</div>
	        		</div>
	        		<div class="clr"></div>
	        	

	        	</div>
	        	<div class="clr"></div>
	        	<div class="dashboardBox paddingLR0px ">
	        		<div class="midProfileDiv">
		        		<div class="paddingLR15px">
		        			

		        			<div class="row">

		        				<div class="col-xl-11 col-lg-11 col-md-11 col-10 ">
		        					<div class="midImg">
				        				<img src="<?php echo asset_url();?>/images/Artboard70.png">
				        			</div>
		        					<div class="midContent">
				        				<p class="midMyContname">Lorem ipsum lorem ipsum</p>
				        				<span class="midMytime">Date , Time</span>
				        			</div>
		        				</div>
		        				<div class="col-xl-1 col-lg-1 col-md-1 col-1">
		        					<div class="dropdown dotDropBtn">
										  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
										    <img src="<?php echo asset_url();?>/images/Artboard69.png">
										  </button>
										  <div class="dropdown-menu dropdown-menu-right">
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard98.png"> Hide post</a>
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/save post.png"> Save post</a>
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard99.png"> Unfollow name</a>
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard100.png"> Lorem ipsum</a>
										    <div class="clr"></div>
										  </div>
									</div>
		        				</div>
		        				
		        			</div>

		        			
		        			<div class="clr"></div>
		        			
		        		</div>
	        			<div class="imageWidth">
	       					<!-- <img src="<?php echo asset_url();?>/images/Artboard67.png"> -->
	       					<p>Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. Maecenas aliquet fringilla vehicula. Nullam porttitor tempus molestie. Nulla blandit dui ut aliquam tristique.</p>
	        			</div>
	        			<div class="supportlist">
	        				<ul >
	        					<li><img src="<?php echo asset_url();?>/images/Artboard64.png"> Support</li>
	        					<li>
	        						<!-- <img src="<?php echo asset_url();?>/images/Artboard65.png"> Comment -->

	        						<div class="dropdown CommentBtn">
									  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
									   <img src="<?php echo asset_url();?>/images/Artboard65.png"> Comment
									  </button>
									  <div class="dropdown-menu">
									    <a class="dropdown-item" href="#">
									    	<img src="<?php echo asset_url();?>/images/Artboard103.png"> Share on timeline
									    </a>
									    <a class="dropdown-item" href="#"  data-toggle="modal" data-target="#exampleModalCenter">
									    	<img src="<?php echo asset_url();?>/images/Artboard104.png"> Share as message
									    </a>
									    <a class="dropdown-item" href="#">
									    	<img src="<?php echo asset_url();?>/images/Artboard106.png"> Share in a group
									    </a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard105.png"> Share on friend’s timeline
									    </a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard107.png"> Share to a page
									    </a>
									    <div class="clr"></div>
									  </div>
									</div>


	        					</li>
	        					<li><img src="<?php echo asset_url();?>/images/Artboard66.png"> Share</li>	        					
	        				</ul>
	        				<ul class="rightUl">
	        					<li><a href="">23 Supports |</a><a href=""> 2 Comments</a></li>
	        				</ul>
	        			</div>
	        			<div class="paddingLR15px">
	        				<p class="viewUser">500 views l 220 supporters l 336 volunteers l Rs.1,23,445 donation raised </p>
	        				
	        				
	        				<div class="clr"></div>
	        				<div class="userCommentBox">
	        					<div class="commentImg">
	        						<img src="<?php echo asset_url();?>/images/Artboard48.png">
	        					</div>
	        					<div class="commentText">
	        						<div class="form-group">                           
				                        <input type="text" class="form-control" placeholder="Write a comment">
				                     </div>
	        					</div>
	        					<div class="clr"></div>
	        				</div>
	        				<div class="userCommentBox">
	        					<div class="commentImg">
	        						<img src="<?php echo asset_url();?>/images/Artboard72.png">
	        					</div>
	        					<div class="commentText">
	        						<p class="commenttt"><span class="commentName">Priya Nayak </span> Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ips</p>
	        						<span class="replyTime">Reply , Time</span>
	        						

	        						<div class="userCommentBox commentReply" style="display: none;">
			        					<div class="commentImg">
			        						<img src="http://localhost/newSite/assets//images/Artboard48.png">
			        					</div>
			        					<div class="commentText">
			        						<div class="form-group">                           
						                        <input type="text" class="form-control" placeholder="Write a comment">
						                     </div>
			        					</div>
			        					<div class="clr"></div>
			        				</div>


	        					</div>
	        					<div class="clr"></div>
	        				</div>
	        					        			
		        			<div style="float: right;">
		        					<button class="contributeBtn reportBtn" data-toggle="modal" data-target="#reportCause" type="submit">Report</button>
							      
		        			</div>
		        			<div class="clr"></div>
	        			</div>
	        		</div>
	        		<div class="clr"></div>
	        	

	        	</div>



	        	<div class=" marginBottom20px" style="text-align: center;">
    					<button class="leadMore reportBtn" type="submit">Load More
				        <img style="margin-left: 15px;" src="<?php echo asset_url();?>/images/Artboard – 181.png"></button>
				        <div class="clr"></div>
    			</div>




	        	</div>
	        
	        <?php $this->load->view('right-nav')?> 	
	        <div class="LoginbgCircle1 dashBgImg"></div>
	    </div>
	 </div>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered commentModel" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
       <div class="modelShareComment">
       		<p>Share as a message</p>
       </div>
       <div class="clr"></div>
       <div class="dataListFrd">
       	<div class="container-fluid">
	       <div class="row">
	       		<div class="col-xl-2 col-lg-2 col-md-2  paddingTB5px CommentborderF">
	       			<span class="friTitel">Friends </span>
	       		</div>
	       		<div class="col-xl-10 col-lg-10 col-md-10 paddingTB5px CommentborderL">
	       		</div>
	       	</div>
	    </div>
       </div>
       <div class="clr"></div>
       <div class="container-fluid">
	       <div class="row commentModelBody">
	       		<div class="col-xl-3 col-lg-3 col-md-3">
	       			<img src="<?php echo asset_url();?>/images/Artboard108.png">
	       		</div>
	       		<div class="col-xl-9 col-lg-9 col-md-9">
	       			<p class="commentModelcontent">Vivamus elementum magna lacus, eu tristique dui ornare sit amet.</p>
	       			<p class="commentModelcontent">Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. Maecenas aliquet fringilla vehicula. Nullam porttitor tempus molestie. Nulla blandit dui ut aliquam tristique. Quisque bibendum, nunc quis mattis semper, odio lacus ornare enim, vel posuere dui tortor non metus. Phasellus non dictum arcu, ut convallis lectus. Nullam tempus nunc.</p>
	       		</div>
	       </div>
	   </div>
	   <div class="clr"></div>
	   <textarea class="form-control modelCommentTextarea" rows="3" placeholder="Write your thoughts on the above post"></textarea>
      </div>
      <div class="modal-footer">
        <button class="disasterBtn" type="submit">
         <span>Sure</span>
        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
      </button>
      </div>
    </div>
  </div>
</div>




<!-- The Report Modal -->
<div class="modal reportModel" id="reportCause">
  <div class="modal-dialog">
    <div class="modal-content">

    

      <!-- Modal body -->
      <div class="modal-body">
      	<div class="reportModelTitel">Report this cause</div>
        
      	<div class="form-group">
		 
		  <textarea class="form-control" rows="4" placeholder="Describe  to help us understand what’s going wrong with this"></textarea>
		</div>
		<p>Learn more about our policies</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
       
       <button class="reportBtn" type="submit">Submit
		    <img style="margin-left: 15px;" src="<?php echo asset_url();?>/images/Artboard – 181.png">
		</button>
      </div>

    </div>
  </div>
</div>






<!-- searcb box js -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#searchInput').hide();
        //alert("");
        $('#searchIcon').click(function(){
            //alert(""); 
            $('#searchInput').toggle("slide", { direction: "right" }, 3000);;
        });
         

        // share post js
        $('.shareDiv1').click(function(){
        	$('#shareMainDiv').hide("slide", { direction: "bottom" }, 2000);
        	$('#shareYourThought').show("slide", { direction: "top" }, 2000);
        	$('.sharePhone').hide();
        	$('.tagPeople').hide();
        });
        $('.shareImgJs').click(function(){
        	$('#shareMainDiv').hide("slide", { direction: "bottom" }, 2000);
        	$('#shareYourThought').show("slide", { direction: "top" }, 2000);
        	$('.sharePhone').show();
        	$('.tagPeople').show();
        });
        $('.shareFullBtn').click(function(){
        	$('#shareYourThought').hide("slide", { direction: "bottom" }, 2000);
        	$('#shareMainDiv').show("slide", { direction: "top" }, 2000);
        });




         // replay time text show
        $('.replyTime').click(function(){
        	$('.commentReply').toggle("slide", { direction: "left" }, 2000);
        })

        // step progress bar js
        
         $('.stepprogressbar li:nth-child(1)').on('click', function() {
		    $(this).addClass('active').siblings().removeClass('active');
		    $('.stepprogressbar').removeClass('redStepProgressBar').removeClass('greenStepProgressBar');
		});

		$('.stepprogressbar li:nth-child(2)').on('click', function() {
		    $(this).addClass('active').siblings().removeClass('active');
		    $('.stepprogressbar').removeClass('redStepProgressBar').removeClass('greenStepProgressBar');
		});
		$('.stepprogressbar li:nth-child(3)').on('click', function() {
		    $(this).addClass('active').siblings().removeClass('active');
		    $('.stepprogressbar').addClass('redStepProgressBar').removeClass('greenStepProgressBar');
		});
		$('.stepprogressbar li:nth-child(4)').on('click', function() {
		    $(this).addClass('active').siblings().removeClass('active');
		    $('.stepprogressbar').addClass('greenStepProgressBar').removeClass('redStepProgressBar');;
		});
		$('.stepprogressbar li:nth-child(5)').on('click', function() {
		    $(this).addClass('active').siblings().removeClass('active');
		    $('.stepprogressbar').addClass('greenStepProgressBar').removeClass('redStepProgressBar');;
		});

    })
</script>

<!-- scroll header js -->
<script type="text/javascript">
    $(document).scroll(function(){
        if($(this).scrollTop() > 80)
        {   
            $('.navbar').addClass('navBg');
        }
        else{
            $('.navbar').removeClass('navBg');   
        }
    });


</script>



</body>
</html>
	 
	