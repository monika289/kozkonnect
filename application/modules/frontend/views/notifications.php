<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">       
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/style.css">
         <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/dashboard.css">
         
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light paddingLeft0px"> <!--bg-light-->
  <a class="navbar-brand" href="<?php echo base_url(); ?>">
    <img src="<?php echo asset_url();?>/images/Artboard – 182.png" class="logo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    
    </ul> 
    <form class="form-inline my-2 my-lg-0 pull-right">
        <div class="mr-sm-2 searchBox">            
            <span id="searchInput"><input class="form-control" type="search" placeholder="Search"></span>
            <!-- <i class='fas fa-search' id="searchIcon"></i> -->
            <img src="<?php echo asset_url();?>/images/Artboa9.png" id="searchIcon">
        </div>
      <button class="disasterBtn" type="submit">
        <a href=""> Disaster response</a> 
        <!-- <i class='fas fa-arrow-right'></i> -->
        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
      </button>
    </form>
  </div>
</nav>




<div style="clr"></div>

	<ol class="breadcrumb paddingTop80px">
			<li class="breadcrumb-item active">
				<a href="<?php echo base_url(); ?>dashboard">Home</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>myKonnect">MyKonnect</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="<?php echo base_url(); ?>causes">Causes</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="<?php echo base_url(); ?>myGroup">My Group</a>
			</li>

			<li class="breadcrumb-menu d-md-down-none">
			<div class="btn-group" role="group" aria-label="Button group">
				
				<ul class="rightSideNav">
					<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle btntext" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				         	<img class="profileImg" src="<?php echo asset_url();?>/images/Artboard48.png"> 
							<span class="nameTitle">Nilesh Watal</span>
				        </a>
				        <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard82.png"> Create page</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard83.png"> Activity</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard85.png"> Setting</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard84.png"> Log out</a>
				        </div>
				      </li>
					
					<li><a class="btntext" href="">
					
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard46.png"> </a></li>
					<li><a class="btntext" href="#">
						
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard47.png"> </a>
					</li>

				</ul>
			</div>
			</li>
	</ol>
	<div style="clr"></div>
	
	
	<div class="container-fluid">
	    <div class="row">
			    	
	    	<style type="text/css">
	    		
	    	</style>
		   	<div class="col-xl-12 col-lg-12 col-md-12 notificatinTitel">
		   		Notification
		   		<div class="clr"></div>
		   	</div>

		   	<div class="notifiMainDiv">
		        <div class="col-xl-2 col-lg-2 col-md-2">
		        	<div class="notificationImgBox">
		        		<img src="<?php echo asset_url();?>/images/Artboard108.png">
		        	</div>
		     	</div>
		        
		         <div class="col-xl-10 col-lg-10 col-md-10 paddingLeft0px">
		         	<div class="notificationContentBox">
			        	<div class="notiDateTime">
			        		<span class="ndate">31 January, 19 </span> | <span class="ntime"> 03:10 pm</span>
			        	</div>
			        	<div class="clr"></div>
			        	<p class="ncontent">Ut congue mi at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus. Mauris finibus eros felis, ac malesuada neque porta sed. Nunc in viverra eros, nec fringilla ipsum. Mauris consectetur mattis nulla id eleifend. Vivamus ultrices metus vitae luctus feugiat. Praesent malesuada leo vitae augue aliquam egestas. 
			        	</p>
			        	<div class="clr"></div>
		        	</div>
		        	<div class="clr"></div>
		        </div>
	        </div>
	        <div class="notifiMainDiv">
		        <div class="col-xl-2 col-lg-2 col-md-2">
		        	<div class="notificationImgBox">
		        		<img src="<?php echo asset_url();?>/images/Artboard108.png">
		        	</div>
		     	</div>
		        
		         <div class="col-xl-10 col-lg-10 col-md-10 paddingLeft0px">
		         	<div class="notificationContentBox">
			        	<div class="notiDateTime">
			        		<span class="ndate">31 January, 19 </span> | <span class="ntime"> 03:10 pm</span>
			        	</div>
			        	<div class="clr"></div>
			        	<p class="ncontent">Ut congue mi at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus. Mauris finibus eros felis, ac malesuada neque porta sed. Nunc in viverra eros, nec fringilla ipsum. Mauris consectetur mattis nulla id eleifend. Vivamus ultrices metus vitae luctus feugiat. Praesent malesuada leo vitae augue aliquam egestas.
			        	</p>
			        	<div class="clr"></div>
		        	</div>
		        	<div class="clr"></div>
		        </div>
	        </div>
	        <div class="notifiMainDiv">
		        <div class="col-xl-2 col-lg-2 col-md-2">
		        	<div class="notificationImgBox">
		        		<img src="<?php echo asset_url();?>/images/Artboard108.png">
		        	</div>
		     	</div>
		        
		         <div class="col-xl-10 col-lg-10 col-md-10 paddingLeft0px">
		         	<div class="notificationContentBox">
			        	<div class="notiDateTime">
			        		<span class="ndate">31 January, 19 </span> | <span class="ntime"> 03:10 pm</span>
			        	</div>
			        	<div class="clr"></div>
			        	<p class="ncontent">Ut congue mi at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus. Mauris finibus eros felis, ac malesuada neque porta sed. Nunc in viverra eros, nec fringilla ipsum. Mauris consectetur mattis nulla id eleifend. Vivamus ultrices metus vitae luctus feugiat. Praesent malesuada leo vitae augue aliquam egestas.
			        	</p>
			        	<div class="clr"></div>
		        	</div>
		        	<div class="clr"></div>
		        </div>
	        </div>
	        <div class="notifiMainDiv">
		        <div class="col-xl-2 col-lg-2 col-md-2">
		        	<div class="notificationImgBox">
		        		<img src="<?php echo asset_url();?>/images/Artboard108.png">
		        	</div>
		     	</div>
		        
		         <div class="col-xl-10 col-lg-10 col-md-10 paddingLeft0px">
		         	<div class="notificationContentBox">
			        	<div class="notiDateTime">
			        		<span class="ndate">31 January, 19 </span> | <span class="ntime"> 03:10 pm</span>
			        	</div>
			        	<div class="clr"></div>
			        	<p class="ncontent">Ut congue mi at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus. Mauris finibus eros felis, ac malesuada neque porta sed. Nunc in viverra eros, nec fringilla ipsum. Mauris consectetur mattis nulla id eleifend. Vivamus ultrices metus vitae luctus feugiat. Praesent malesuada leo vitae augue aliquam egestas.
			        	</p>
			        	<div class="clr"></div>
		        	</div>
		        	<div class="clr"></div>
		        </div>
	        </div>


	        
	        <div class="LoginbgCircle1 dashBgImg"></div>
	    </div>
	 </div>






<!-- searcb box js -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#searchInput').hide();
        //alert("");
        $('#searchIcon').click(function(){
            //alert(""); 
            $('#searchInput').toggle("slide", { direction: "right" }, 3000);;
        });



    })
</script>

<!-- scroll header js -->
<script type="text/javascript">
    $(document).scroll(function(){
        if($(this).scrollTop() > 10)
        {   
            $('.navbar').addClass('navBg');
        }
        else{
            $('.navbar').removeClass('navBg');   
        }
    });


</script>



</body>
</html>
	 
	