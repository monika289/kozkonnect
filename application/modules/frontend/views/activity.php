<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">       
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/style.css">
         <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/dashboard.css">
         <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/about_inner_15.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light paddingLeft0px"> <!--bg-light-->
  <a class="navbar-brand" href="<?php echo base_url(); ?>">
    <img src="<?php echo asset_url();?>/images/Artboard – 182.png" class="logo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    
    </ul> 
    <form class="form-inline my-2 my-lg-0 pull-right">
        <div class="mr-sm-2 searchBox">            
            <span id="searchInput"><input class="form-control" type="search" placeholder="Search"></span>
            <!-- <i class='fas fa-search' id="searchIcon"></i> -->
            <img src="<?php echo asset_url();?>/images/Artboa9.png" id="searchIcon">
        </div>
      <button class="disasterBtn" type="submit">
        <a href=""> Disaster response</a> 
        <!-- <i class='fas fa-arrow-right'></i> -->
        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
      </button>
    </form>
  </div>
</nav>




<div style="clr"></div>

	<ol class="breadcrumb paddingTop80px">
			<li class="breadcrumb-item active">
				<a href="<?php echo base_url(); ?>dashboard">Home</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>myKonnect">MyKonnect</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="<?php echo base_url(); ?>causes">Causes</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="<?php echo base_url(); ?>myGroup">My Group</a>
			</li>

			<li class="breadcrumb-menu d-md-down-none">
			<div class="btn-group" role="group" aria-label="Button group">
				
				<ul class="rightSideNav">
					<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle btntext" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				         	<img class="profileImg" src="<?php echo asset_url();?>/images/Artboard48.png"> 
							<span class="nameTitle">Nilesh Watal</span>
				        </a>
				        <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard82.png"> Create page</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard83.png"> Activity</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard85.png"> Setting</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard84.png"> Log out</a>
				        </div>
				      </li>
					
					<li><a class="btntext" href="">
					
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard46.png"> </a></li>
					<li><a class="btntext" href="#">
						
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard47.png"> </a>
					</li>

				</ul>
			</div>
			</li>
	</ol>
	<div style="clr"></div>


	
	<div class="mobileLRDiv">
		<a id="leftSide"> Left Menu</a>
		<a id="rightSide" style="float: right;"> Right Menu</a>
	</div>
	<div class="container-fluid">
	    <div class="row">
	    	

		     <?php $this->load->view('left-nav')?>
		    <!-- col-md-6 col-sm-6 col-xs-12-->
	        <div class="col-xl-6 col-lg-6">
	        	<div class="dashboardSearchBox marginBottom5px" id="shareYourThought" style="display: none;">
	        		<div class="SearchBoxpadding">
		        		<div class="textArea">
		 
		        			<textarea class="form-control" rows="2" id="comment" placeholder="Share your thought here"></textarea>
		        		</div>

		        		<div class="sharePhone">
		        			<ul>
		        				<li><img src="<?php echo asset_url();?>/images/Artboard – 189.png"></li>
		        				<li class="add">
		        					<!-- <img src="<?php echo asset_url();?>/images/Artboard – 174.png"> -->
		        					<div class="form-group fileUploadBtn1">
									    <label for="exampleFormControlFile1"></label> 
									    <input type="file" class="form-control-file" id="exampleFormControlFile1">
									 </div>

		        				</li>
		        				
		        			</ul>
		        		</div>
		        		<div class="clr"></div>
		        		<div class="tagPeople">
		        			<p>Tag people you are with</p>
		        		</div>

		        		<div class="clr"></div>
		        		<div class="shareBtnMain">
		        			<ul>
		        				<li><button type="button" class="btn shareBtn">Feeling/Activity</button></li>
		        				<li><button type="button" class="btn shareBtn">Check In</button></li>
		        				<li><button type="button" class="btn shareBtn">Live</button></li>
		        				<li><button type="button" class="btn shareBtn">Event</button></li>
		        				
		        			</ul>
		        			<ul class="rightSideIcon floatRight">
		        				<li><img class="shareImgJs" src="<?php echo asset_url();?>/images/Artboard44.png"></li>
		        				<li><img src="<?php echo asset_url();?>/images/Artboard45.png"></li>
		        				<li>
		        					<div class="dropdown shareDotDropBtn">
										  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
										    <img src="<?php echo asset_url();?>/images/Artboard69.png">
										  </button>
										  <div class="dropdown-menu  dropdown-menu-right">
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard124.png"> Felling/Activity</a>
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard123.png"> Check In</a>
										    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard83.png"> Event</a>
										   
										    <div class="clr"></div>
										  </div>
									</div>
								</li>
		        			</ul>
		        			<div class="clr"></div>
		        		</div>
		        		<div class="clr"></div>
		        		<div class="eventGrid">
		        			<div class="row">
		        				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
		        					<div class="custCheckbox">
		        						<input type="checkbox"  id="Ford" value="Ford">
		        						<label for="Ford"></label>
		        					</div>
		        				</div>
		        				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">	        					
		        					<div class="imageWid">
		        						<img src="<?php echo asset_url();?>/images/Artboard101.png">
		        					</div>
		        				</div>
		        				<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">
		        					<div class="timeLineTitel">Timeline</div>
		        				</div>
		        				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
		        					<div class="dropdown frdBtn">
										  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
										    <!-- <img src="<?php echo asset_url();?>/images/Artboard69.png"> -->
										    Friends
										  </button>
										  <div class="dropdown-menu">
										    <!-- <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard124.png"> Felling/Activity</a> -->
										   
										   
										    <div class="clr"></div>
										  </div>
									</div>
		        				</div>
		        				<div class="clr"></div>
		        			</div>
		        			<div class="row">
		        				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
		        					<div class="custCheckbox">
		        						<input type="checkbox"  id="Ford1">
		        						<label for="Ford1"></label>
		        					</div>
		        				</div>
		        				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">	        					
		        					<div class="imageWid">
		        						<img src="<?php echo asset_url();?>/images/Artboard48.png">
		        					</div>
		        				</div>
		        				<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">
		        					<div class="timeLineTitel">Story</div>
		        				</div>
		        				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
		        					<div class="dropdown frdBtn">
										  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
										    <!-- <img src="<?php echo asset_url();?>/images/Artboard69.png"> -->
										    Friends
										  </button>
										  <div class="dropdown-menu">
										    <!-- <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard124.png"> Felling/Activity</a> -->
										  
										   
										    <div class="clr"></div>
										  </div>
									</div>
		        				</div>
		        				<div class="clr"></div>
		        			</div>
		        		</div>

	        		
	        		</div>
	        		<div>
	        			<button type="button" class="btn shareFullBtn">Share</button>
	        		</div>
	        	</div>



	        	<div class="dashboardBox marginBottom5px" id="shareMainDiv">
	        		<div class="shareDiv1">
	        			<img src="<?php echo asset_url();?>/images/Artboard49.png" style="margin-right: 15px;"> <span>Share your thought</span>
	        		</div>

	        		
	        		<div class="shareDiv2">
	        			<ul>
	        				<li><img class="shareImgJs" src="<?php echo asset_url();?>/images/Artboard44.png"></li>
	        				<li><img src="<?php echo asset_url();?>/images/Artboard45.png"></li>
	        				<li>
	        					<div class="dropdown shareDotDropBtn">
									  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
									    <img src="<?php echo asset_url();?>/images/Artboard69.png">
									  </button>
									  <div class="dropdown-menu  dropdown-menu-right">
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard124.png"> Felling/Activity</a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard123.png"> Check In</a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard122.png"> Event</a>
									   
									    <div class="clr"></div>
									  </div>
								</div>
							</li>
	        			</ul>
	        		</div>
	        		<div class="clr"></div>
	        	</div>



	        	<div class="dayDiv">
	        		<button type="button" class="btn btnBGActive">Today</button>
	        		<button type="button" class="btn btnBG">Tomorrow</button>
	        		<button type="button" class="btn btnBG">Yesterday</button>
	        		<button type="button" class="btn btnBG">Last week</button>
	        	</div>
	        	<div class="clr"></div>
	        	<div class="paddingLR0px" id="about_inner_15">
	        		
	        		<div class="col-xl-12 col-lg-12 col-md-12 paddingLR0px">
                    <ul class="cbp_tmtimeline">
                        <li>
                            <div class="cbp_tmicon cbp_tmicon-phone fa fa-mobile">
                            	<img src="<?php echo asset_url();?>/images/Icon 1.png">
                            </div>
                                <div class="cbp_tmlabel">
                                    <div class="fDiv">
                                        <p class="editable abind3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.   </p>
                                    </div>
                                    <div class="sDiv dateTimeText">
                                        <span>Date, Time</span>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            
                        </li>
                        <li>
                            <div class="cbp_tmicon cbp_tmicon-screen fa fa-desktop">
                            	<img src="<?php echo asset_url();?>/images/Icon 2.png">
                            </div>
                            <div class="cbp_tmlabel">
                               <div class="fDiv">
                                <p class="editable abind4">Lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                                lorem ipsum lorem ipsum lorem ipsum lorem ipsum 
                                lorem ipsum lorem ipsum</p>
                                <p class="editable abind4">Lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                                lorem ipsum lorem </p> </div>
                                <div class="sDiv dateTimeText">
                                    <span>Date, Time</span>
                                 </div>
                                <div class="clr"></div>
                            </div>
                        </li>
                        <li>
                            <div class="cbp_tmicon cbp_tmicon-mail fa fa-envelope">
                            	<img src="<?php echo asset_url();?>/images/Icon 3.png">
                            </div>
                            <div class="cbp_tmlabel">
                            	 <div class="fDiv">
	                                <p class="editable abind5">Lorem ipsum lorem ipsum lorem ipsum lorem ipsum
	                                lorem ipsum lorem lorem ipsum lorem</p>
	                                <p class="editable abind5">Lorem ipsum lorem ipsum lorem ipsum lorem ipsum
	                                lorem ipsum lorem lorem ipsum lorem</p>
                                </div>
                                <div class="sDiv dateTimeText">
                                    <span>Date, Time</span>
                                 </div>
                                <div class="clr"></div>

                            </div>
                        </li>

                        <li>
                            
                            <div class="cbp_tmicon cbp_tmicon-screen fa fa-desktop">
                            	<img src="<?php echo asset_url();?>/images/Icon 4.png">
                            </div>
                            <div class="cbp_tmlabel">
                            	 <div class="fDiv">
	                                <p class="editable abind6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum .</p>
	                                <p class="editable abind6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.</p>
                                 </div>
                                <div class="sDiv dateTimeText">
                                    <span>Date, Time</span>
                                 </div>
                                <div class="clr"></div>
                            </div>
                        </li>
                        <li>
                            
                            <div class="cbp_tmicon cbp_tmicon-phone fa fa-mobile">
                            	<img src="<?php echo asset_url();?>/images/Icon 5.png">
                            </div>
                            <div class="cbp_tmlabel">
                              	 <div class="fDiv">
                                <p class="editable abind7">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum .</p>
                                 </div>
                                <div class="sDiv dateTimeText">
                                    <span>Date, Time</span>
                                 </div>
                                <div class="clr"></div>
                            </div>
                        </li>
                    </ul>
                </div>


	        		
	        	</div>
	        		<div class="clr"></div>
	        		<div class="clr"></div>
	        	


	        	<div class=" marginBottom20px" style="text-align: center;">
    					<button class="leadMore reportBtn" type="submit">Load More
				        <img style="margin-left: 15px;" src="<?php echo asset_url();?>/images/Artboard – 181.png"></button>
				        <div class="clr"></div>
    			</div>




	        	</div>
	        
	        <?php $this->load->view('right-nav')?> 	
	        <div class="LoginbgCircle1 dashBgImg"></div>
	    </div>
	 </div>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered commentModel" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
       <div class="modelShareComment">
       		<p>Share as a message</p>
       </div>
       <div class="clr"></div>
       <div class="dataListFrd">
       	<div class="container-fluid">
	       <div class="row">
	       		<div class="col-xl-2 col-lg-2 col-md-2  paddingTB5px CommentborderF">
	       			<span class="friTitel">Friends </span>
	       		</div>
	       		<div class="col-xl-10 col-lg-10 col-md-10 paddingTB5px CommentborderL">
	       		</div>
	       	</div>
	    </div>
       </div>
       <div class="clr"></div>
       <div class="container-fluid">
	       <div class="row commentModelBody">
	       		<div class="col-xl-3 col-lg-3 col-md-3">
	       			<img src="<?php echo asset_url();?>/images/Artboard108.png">
	       		</div>
	       		<div class="col-xl-9 col-lg-9 col-md-9">
	       			<p class="commentModelcontent">Vivamus elementum magna lacus, eu tristique dui ornare sit amet.</p>
	       			<p class="commentModelcontent">Vivamus elementum magna lacus, eu tristique dui ornare sit amet. Vivamus consectetur facilisis libero eu faucibus. Phasellus dignissim hendrerit enim, eu lobortis tellus congue sit amet. Maecenas aliquet fringilla vehicula. Nullam porttitor tempus molestie. Nulla blandit dui ut aliquam tristique. Quisque bibendum, nunc quis mattis semper, odio lacus ornare enim, vel posuere dui tortor non metus. Phasellus non dictum arcu, ut convallis lectus. Nullam tempus nunc.</p>
	       		</div>
	       </div>
	   </div>
	   <div class="clr"></div>
	   <textarea class="form-control modelCommentTextarea" rows="3" placeholder="Write your thoughts on the above post"></textarea>
      </div>
      <div class="modal-footer">
        <button class="disasterBtn" type="submit">
         <span>Sure</span>
        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
      </button>
      </div>
    </div>
  </div>







<!-- searcb box js -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#searchInput').hide();
        //alert("");
        $('#searchIcon').click(function(){
            //alert(""); 
            $('#searchInput').toggle("slide", { direction: "right" }, 3000);;
        });


        // share post js
        $('.shareDiv1').click(function(){
        	$('#shareMainDiv').hide("slide", { direction: "bottom" }, 2000);
        	$('#shareYourThought').show("slide", { direction: "top" }, 2000);
        	$('.sharePhone').hide();
        	$('.tagPeople').hide();
        });
        $('.shareImgJs').click(function(){
        	$('#shareMainDiv').hide("slide", { direction: "bottom" }, 2000);
        	$('#shareYourThought').show("slide", { direction: "top" }, 2000);
        	$('.sharePhone').show();
        	$('.tagPeople').show();
        });
        $('.shareFullBtn').click(function(){
        	$('#shareYourThought').hide("slide", { direction: "bottom" }, 2000);
        	$('#shareMainDiv').show("slide", { direction: "top" }, 2000);
        });

        $('.replyTime').click(function(){
        	$('.commentReply').toggle("slide", { direction: "left" }, 2000);
        })


         // step progress bar js

        $('.stepprogressbar li').on('click', function() {
		    $(this).addClass('active').siblings().removeClass('active');
		});


    })
</script>

<!-- scroll header js -->
<script type="text/javascript">
    $(document).scroll(function(){
        if($(this).scrollTop() > 10)
        {   
            $('.navbar').addClass('navBg');
        }
        else{
            $('.navbar').removeClass('navBg');   
        }
    });


</script>



</body>
</html>
	 
	