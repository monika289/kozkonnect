<style type="text/css">
  .greenBgImg{
    left: 1.5%;
    top: 500px;
    z-index: 1
  }
  .contactCard  .form-group{
    margin-bottom: 2rem;
  }
  .contactC{
    margin: 0px 0px 30px;
  }
  .paddingTop5px{
    padding-top: 15px;
  }
  .contactCard.formController .form-group .form-control{
    font-size: 12.8px;
  }
</style>

<div class="clr"></div>
<div class="contactOverflow">
    <div class="container-fluid paddingTop80px">
      <div class="topTitelImg"></div>
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 text-center contactHead paddingTop70px">
          		<h2> Contact Us</h2>
          		<p>Get in touch with us!</p>
          </div>
        </div>

    </div>
    <div class="greenBgImg"></div>
      <!-- <div class="bottomBlueBgImg"></div> -->
      <div class="bottomGreenBgImg"></div>
    <div class="LoginbgCircle1 contactusBG"></div>
    <div class="container-fluid contactForm" style="padding-bottom: 60px;">

        <div class="row">
          
          <div class="col-xl-6 col-lg-6 col-md-6 text-center">
          		<div class="contactCard formController" >
          			<div class="form-group">                           
                        <input type="text" class="form-control" placeholder="Name">
                     </div>
                     <div class="form-group">                           
                        <input type="text" class="form-control" placeholder="Email">
                     </div>
                     <div class="form-group">                           
                        <input type="text" class="form-control" placeholder="Phone no">
                     </div>
                     <div class="form-group">                           
                        <input type="text" class="form-control" placeholder="Message (if any)">
                     </div>

                   <!--   <button class="btn buttonBtn contactBtn" type="submit">
                        <span>Submit </span>
                        <i class="fas fa-arrow-right"></i>
                      </button> -->
                      <button class="btn buttonBtn contactBtn" type="submit">
                          <span>Submit </span>
                          <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
                        </button>
          		</div>

          </div>

          <div class="col-xl-6 col-lg-6 col-md-6" >
            

          	<div class="contactContent ">
      			<div class="contactC">
      				<div class="iconLoation paddingTop5px">
      					<img src="<?php echo asset_url();?>/images/Artboard – 170.png">
        				</div> 
        			<span>9B Vinod apartment, Parihar chowk, Aundh, Pune 411017</span>
            </div>
            <div class="clr"></div>
            <div class="contactC">
              <div class="iconLoation">
                <img src="<?php echo asset_url();?>/images/Artboard – 171.png">
                </div> 
              <span> +91-9988776655</span>
            </div>

      			
            <div class="clr"></div>
      			<p class="contactC">
      				<span class="iconLoation">
      					<img src="<?php echo asset_url();?>/images/Artboard – 172.png">
      				</span> kozkonnect@gmail.com</p>

              <div>
                <ul class="socialIconContact">
                 
                   <li  class="list-inline-item"> <a href=""><i class='fab fa-facebook-f facebook'></i></a></li>
                   <li  class="list-inline-item"> <a href=""><i class='fab fa-twitter twitter'></i></a></li>
                   <li  class="list-inline-item"> <a href=""><i class="fab fa-google-plus-g googlePlus"></i></a></li>
                </ul>
                </ul>
              </div>

      				<div class="mapouter"><div class="gmap_canvas">
                <iframe width="100%" height="265" id="gmap_canvas" src="https://maps.google.com/maps?q=Pune&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net"></a></div><style>.mapouter{text-align:right;height:265px;width:500px;}.gmap_canvas {overflow:hidden;background:none!important;height:265px;width:100%;}</style></div>

      		</div>
          </div>

        </div>
    </div>
</div>
<!-- <script>
function myMap() {
var mapProp= {
  center:new google.maps.LatLng(51.508742,-0.120850),
  zoom:5,
};
var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY&callback=myMap"></script> -->