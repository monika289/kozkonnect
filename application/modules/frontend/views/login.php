<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">       
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/style.css">
        
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light paddingLeft0px"> <!--bg-light-->
  <a class="navbar-brand" href="<?php echo base_url(); ?>">
    <img src="<?php echo asset_url();?>/images/Artboard – 182.png" class="logo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    </ul> 
    <form class="form-inline my-2 my-lg-0 pull-right">
        <div class="mr-sm-2 dontHave">            
            <span>Don't have an account?</span>
           
        </div>
      <button class="regBtn" type="submit">
        <a href="<?php echo base_url(); ?>register"> Register</a> 
        
      </button>
    </form>
  </div>
</nav>



    <div class="container-fluid login">
    <div class="row">
    <div class="col-xl-4 col-lg-4 col-md-4 leftSidepadding leftSideAdmin">
            
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-10">                
                <h1 class="adminTital">MAKE A DIFFERENCE</h1>
                <ul class="adminList">
                    <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Create a cause</a></li>
                    <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Fallow a cause</a></li>
                    <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Valunteer a cause</a></li>
                    <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Feel amazing</a></li>
                    <li><a href=""><img src="<?php echo asset_url();?>/images/Artboard141.png"> Happiness is in helping</a></li>
                </ul>
            </div>

        </div>
   
            
    </div>
    <div class="col-xl-8 col-lg-8 col-md-8 paddingTop125px">
            <div class="LoginbgCircle1"></div>
            <h2 class="formTitle"> Login</h2>

            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-10 formController">
                    <div class="form-group">                           
                        <input type="text" class="form-control" placeholder="Email" >
                     </div>
                     <div class="form-group">                           
                        <input type="Password" class="form-control" placeholder="Password" >
                     </div>
                     
                     <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 text-right"> 
                            <a href="<?php echo base_url(); ?>forgot-password" class="forgotPwd">Forgot password?</a>
                        </div>
                        
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 adminSocial loginSocial">
                                <p class="socialTitle">Connect with :</p>
                                <ul>
                                    <li><a href="" class="facebook"><img src="<?php echo asset_url();?>/images/Artboard158.png"> </a></li>
                                    <li><a href="" class="twitter"><img src="<?php echo asset_url();?>/images/Artboard159.png"> </a></li>
                                    <li><a href="" class="googlePlus"><img src="<?php echo asset_url();?>/images/Artboard161.png"></a></li>
                                    <li><a href="" class="linkedin"><img src="<?php echo asset_url();?>/images/Artboard162.png"></a></li>
                                    <li><a href="" class="instagram"><img src="<?php echo asset_url();?>/images/Artboard163.png"></a></li>
                                </ul>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right loginRemember">
                                <div class="form-check">
                                    <label class="form-check-label socialTitle">
                                      <input class="form-check-input checkBox" type="checkbox"> Remember me
                                    </label>
                                  </div>
                            </div>
                       
                        <div class="clr"></div>
                        <div class="col-md-12 marginTop20px">
                            <button class="btn buttonBtn" type="submit">
                            <span>Login </span>
                            <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
                          </button>


                        </div>
                     </div>

                </div>
                
            </div>


        </div>

        <div class="leftBottomImg"></div>
    </div>
</div>

<script type="text/javascript">
    $(document).scroll(function(){
        if($(this).scrollTop() > 80)
        {   
            $('.navbar').addClass('navBg');
        }
        else{
            $('.navbar').removeClass('navBg');   
        }
    });
</script>



</body>
</html>






