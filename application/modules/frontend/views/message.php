<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">       
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/style.css">
         <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/dashboard.css">
         
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light paddingLeft0px"> <!--bg-light-->
  <a class="navbar-brand" href="<?php echo base_url(); ?>">
    <img src="<?php echo asset_url();?>/images/Artboard – 182.png" class="logo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    
    </ul> 
    <form class="form-inline my-2 my-lg-0 pull-right">
        <div class="mr-sm-2 searchBox">            
            <span id="searchInput"><input class="form-control" type="search" placeholder="Search"></span>
            <!-- <i class='fas fa-search' id="searchIcon"></i> -->
            <img src="<?php echo asset_url();?>/images/Artboa9.png" id="searchIcon">
        </div>
      <button class="disasterBtn" type="submit">
        <a href=""> Disaster response</a> 
        <!-- <i class='fas fa-arrow-right'></i> -->
        <img src="<?php echo asset_url();?>/images/Artboard – 181.png">
      </button>
    </form>
  </div>
</nav>




<div style="clr"></div>

	<ol class="breadcrumb paddingTop80px">
			<li class="breadcrumb-item active">
				<a href="<?php echo base_url(); ?>dashboard">Home</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>myKonnect">MyKonnect</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="<?php echo base_url(); ?>causes">Causes</a>
			</li>
			<li class="breadcrumb-item ">
				<a href="<?php echo base_url(); ?>myGroup">My Group</a>
			</li>

			<li class="breadcrumb-menu d-md-down-none">
			<div class="btn-group" role="group" aria-label="Button group">
				
				<ul class="rightSideNav">
					<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle btntext" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				         	<img class="profileImg" src="<?php echo asset_url();?>/images/Artboard48.png"> 
							<span class="nameTitle">Nilesh Watal</span>
				        </a>
				        <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard82.png"> Create page</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard83.png"> Activity</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard85.png"> Setting</a>
				          <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard84.png"> Log out</a>
				        </div>
				      </li>
					
					<li><a class="btntext" href="">
					
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard46.png"> </a></li>
					<li><a class="btntext" href="#">
						
						<img class="rightIcon1" src="<?php echo asset_url();?>/images/Artboard47.png"> </a>
					</li>

				</ul>
			</div>
			</li>
	</ol>
	<div style="clr"></div>


	
	
	<div class="container-fluid">
	    <div class="row">
			    	
	    	<style type="text/css">

	    	</style>
		   
		    <!-- col-md-6 col-sm-6 col-xs-12-->
	        <div class="col-xl-4 col-lg-4">
	        	<div class="messageLeftSide">
	        			<div class="messageTitel">
	        				Messages
	        				<!-- <div class="msgSetting">
	        					<img src="<?php echo asset_url();?>/images/Artboard93.png">
	        				</div>
 -->
	        				<div class="dropdown msgSetting">
									  <button type="button" class="btn dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
									    <img src="<?php echo asset_url();?>/images/Artboard93.png">
									  </button>
									  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-start">
									    <a class="dropdown-item" href="#">
									    	<img src="<?php echo asset_url();?>/images/Artboard56.png"> Active contacts</a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard57.png"> Message requests</a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard55.png"> Archived</a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard54.png"> Unread</a>
									    <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard – 96.png"> Create a group</a>
									     <a class="dropdown-item" href="#"><img src="<?php echo asset_url();?>/images/Artboard – 95.png"> Report a problem</a>
									   
									    <div class="clr"></div>
									  </div>
								</div>
	        			</div>
	        			<div class="clr"></div>
	        			<div class="messageTab fixedMessageGrp">
		        			<ul class="nav nav-tabs" role="tablist">
							  <li class="nav-item">
							    <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Friends</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Groups</a>
							  </li>
							  
							</ul>

							<style type="text/css">
								.fixedMessageGrp{
									max-width: 33.333333%;
								    position: fixed;
								    /*bottom: 0px;*/
								    max-height: 420px;
								    overflow: auto;
								    overflow-x: hidden;
								    padding: 0px 15px 0px 5px !important;
								}
								.fixedMsgBody{
									position: fixed;
								    width: 64.5%;
								    
								}
								.fixedMsgBody .shareMsgContent{
									overflow: auto;
    								overflow-x: hidden;
    								max-height: 405px;
								}
								.messageTab {
									margin: 15px 0px 0px;
								}
								/* width */
								.fixedMessageGrp::-webkit-scrollbar, .fixedMsgBody .shareMsgContent::-webkit-scrollbar {
								  width: 5px;
								}

								/* Track */
								.fixedMessageGrp::-webkit-scrollbar-track, .fixedMsgBody .shareMsgContent::-webkit-scrollbar-track {
								  background: #f1f1f1; 
								}
								 
								/* Handle */
								.fixedMessageGrp::-webkit-scrollbar-thumb, .fixedMsgBody .shareMsgContent::-webkit-scrollbar-thumb {
								  background: #888; 
								}

								/* Handle on hover */
								.fixedMessageGrp::-webkit-scrollbar-thumb:hover, .fixedMsgBody .shareMsgContent::-webkit-scrollbar-thumb:hover {
								  background: #555; 
								}
							</style>

							<!-- Tab panes -->
							<div class="tab-content">
							  <div role="tabpanel" class="tab-pane fade in active " id="profile">

									<div class="input-group frdSearchBox introTextBox">
								        <input type="text" class="form-control" placeholder="Search here" id="txtSearch"/>
								        <div class="input-group-btn">
								          <button class="btn btn-primary" type="submit">
								            <img src="<?php echo asset_url();?>/images/Artboard94.png">
								          </button>
								        </div>
								    </div>



								    <div class="frdLeftCard active">
								    	<div class="frdImgDiv">
								    		<img src="<?php echo asset_url();?>/images/Artboard – 175.png">
								    	</div>
								    	<div class="frdConetDiv">
								    		<div class="fnameT">Name of the person</div>
								    		<span class="fdate">31 Jan,19</span>
								    		<div class="clr"></div>
								    		<p class="fcontent">at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus.auris finibus lacus. </p>
								    	</div>
								    	<div class="clr"></div>
								    </div>
								    <div class="frdLeftCard frdScondCard">
								    	<div class="frdImgDiv">
								    		<img src="<?php echo asset_url();?>/images/Artboard – 175.png">
								    	</div>
								    	<div class="frdConetDiv">
								    		<div class="fnameT">Name of the person</div>
								    		<span class="fdate">31 Jan,19</span>
								    		<div class="clr"></div>
								    		<p class="fcontent">at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus.auris finibus lacus. </p>
								    	</div>

								    	<div class="iconBgf"></div>
								    	<div class="clr"></div>
								    </div>
								    <div class="frdLeftCard frdthrdCard">
								    	<div class="frdImgDiv">
								    		<img src="<?php echo asset_url();?>/images/Artboard – 175.png">
								    	</div>
								    	<div class="frdConetDiv">
								    		<div class="fnameT">Name of the person</div>
								    		<span class="fdate">31 Jan,19</span>
								    		<div class="clr"></div>
								    		<p class="fcontent">at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus.auris finibus lacus. </p>
								    	</div>

								    	<div class="iconBgf"></div>
								    	<div class="clr"></div>
								    </div>
								    <div class="frdLeftCard">
								    	<div class="frdImgDiv">
								    		<img src="<?php echo asset_url();?>/images/Artboard – 175.png">
								    	</div>
								    	<div class="frdConetDiv">
								    		<div class="fnameT">Name of the person</div>
								    		<span class="fdate">31 Jan,19</span>
								    		<div class="clr"></div>
								    		<p class="fcontent">at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus.auris finibus lacus. </p>
								    	</div>
								    	<div class="clr"></div>
								    </div>
							  </div>
							  <div role="tabpanel" class="tab-pane fade" id="buzz">
							 		<div class="input-group frdSearchBox introTextBox">
								        <input type="text" class="form-control" placeholder="Search here" id="txtSearch"/>
								        <div class="input-group-btn">
								          <button class="btn btn-primary" type="submit">
								            <img src="<?php echo asset_url();?>/images/Artboard94.png">
								          </button>
								        </div>
								    </div>



								    <div class="frdLeftCard active">
								    	<div class="frdImgDiv">
								    		<img src="<?php echo asset_url();?>/images/Artboard – 175.png">
								    	</div>
								    	<div class="frdConetDiv">
								    		<div class="fnameT">Name of the person</div>
								    		<span class="fdate">31 Jan,19</span>
								    		<div class="clr"></div>
								    		<p class="fcontent">at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus.auris finibus lacus. </p>
								    	</div>
								    	<div class="clr"></div>
								    </div>
								    <div class="frdLeftCard frdScondCard">
								    	<div class="frdImgDiv">
								    		<img src="<?php echo asset_url();?>/images/Artboard – 175.png">
								    	</div>
								    	<div class="frdConetDiv">
								    		<div class="fnameT">Name of the person</div>
								    		<span class="fdate">31 Jan,19</span>
								    		<div class="clr"></div>
								    		<p class="fcontent">at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus.auris finibus lacus. </p>
								    	</div>

								    	<div class="iconBgf"></div>
								    	<div class="clr"></div>
								    </div>
								    <div class="frdLeftCard frdthrdCard">
								    	<div class="frdImgDiv">
								    		<img src="<?php echo asset_url();?>/images/Artboard – 175.png">
								    	</div>
								    	<div class="frdConetDiv">
								    		<div class="fnameT">Name of the person</div>
								    		<span class="fdate">31 Jan,19</span>
								    		<div class="clr"></div>
								    		<p class="fcontent">at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus.auris finibus lacus. </p>
								    	</div>

								    	<div class="iconBgf"></div>
								    	<div class="clr"></div>
								    </div>
								    <div class="frdLeftCard">
								    	<div class="frdImgDiv">
								    		<img src="<?php echo asset_url();?>/images/Artboard – 175.png">
								    	</div>
								    	<div class="frdConetDiv">
								    		<div class="fnameT">Name of the person</div>
								    		<span class="fdate">31 Jan,19</span>
								    		<div class="clr"></div>
								    		<p class="fcontent">at eros vehicula, sed pretium neque laoreet. In sit amet viverra lacus.auris finibus lacus. </p>
								    	</div>
								    	<div class="clr"></div>
								    </div>
							</div>
						</div>
	        	</div>
	        	<div class="clr"></div>
	        </div>
	      </div>
	        
	         <div class="col-xl-8 col-lg-8">
	        	<div class="shareMsgBody paddingLR0px fixedMsgBody">
	       			<div class="shareMsgContent">
	       				<div class="leftMsg">
	       					at eros vehicula, sed pretium. 	       					
	       				</div>
	       				<div class="clr"></div>
	       				<div class="leftMsg1">
	       					at eros vehicula, sed pretium neque laoreet. 	       					
	       				</div>
	       				<div class="clr"></div>
	       				<div class="leftMsg2">
	       					at eros vehicula, sed pretium neque. 	       					
	       				</div>
	       				<div class="clr"></div>
	       				<div class="shareDateTime">
	       					<span>31 Jan,19 </span>
	       					<span> 12:30 pm</span>
	       				</div>
	       				<div class="clr"></div>
	       				<div class="rightMsg">
	       					at eros vehicula, sed 
	       					
	       				</div>

	       				<div class="clr"></div>
	       				<div class="rightMsg1">
	       					at eros vehicula, sed pretium  sdfsdfds
	       					
	       				</div>

	       				<div class="clr"></div>
	       				<div class="clr"></div>
	       				<div class="rightMsg">
	       					at eros vehicula 
	       					
	       				</div>

	       				<div class="clr"></div>
	       				<div class="rightMsg1">
	       					at eros vehicula, sed pretium 
	       					
	       				</div>

	       				<div class="clr"></div>
	       			</div>
	       			<div class="shareMsgInputBox">	       				
	       				<input type="text" class="form-control" placeholder="Type your message here" id="txtSearch"/>
	       			</div>
	        	</div>
	        	<div class="clr"></div>
	        </div>
	        
	        <div class="LoginbgCircle1 dashBgImg"></div>
	    </div>
	 </div>






<!-- searcb box js -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#searchInput').hide();
        //alert("");
        $('#searchIcon').click(function(){
            //alert(""); 
            $('#searchInput').toggle("slide", { direction: "right" }, 3000);;
        });

         // step progress bar js

        $('.stepprogressbar li').on('click', function() {
		    $(this).addClass('active').siblings().removeClass('active');
		});


    })
</script>

<!-- scroll header js -->
<script type="text/javascript">
    $(document).scroll(function(){
        if($(this).scrollTop() > 10)
        {   
            $('.navbar').addClass('navBg');
        }
        else{
            $('.navbar').removeClass('navBg');   
        }
    });


</script>



</body>
</html>
	 
	