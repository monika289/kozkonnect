<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo $template['title']; ?></title>
   		<meta charset="utf-8">
    	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >
    	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
     	<meta name="description" content="<?php echo $description;?>">
     	<meta name="keywords" content="<?php echo $keywords;?>">
    	<meta name="author" content="Brandzgarage">
		<meta name="categories" content="">
		<meta name="generator" content="Brandzgarage">
		<meta http-equiv="refresh" content="Brandzgarage" />
		<link rel="icon" href="<?php echo asset_url();?>images/icon.png" type="image/x-icon" />
   		<link rel="shortcut icon" href="<?php echo asset_url();?>images/favicon.ico">
    	<!-- <link rel="stylesheet" href="<?php echo asset_url();?>css/font-cion.css">
    	<link href="<?php echo asset_url();?>css/font.css" rel="stylesheet"> -->
         
    		<!--[if lt IE 9]>
            	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        	<![endif]-->
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">		
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" >

        




        <!-- main slider js and css -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>




        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/dashboard.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/style.css">
		
		


        <script type="text/javascript">
			var base_url = '<?php echo base_url(); ?>';
			var asset_url = '<?php echo asset_url();?>'; 
		</script>
        
	</head>
	<body>
	    <?php echo $template['partials']['header']; ?>
	    <?php echo $template['body']; ?>
		<?php echo $template['partials']['footer']; ?>
	</body>
        
</html>
  <script>
function ajaxindicatorstart(text)
{
    if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
    jQuery('body').append('<div id="resultLoading" style="display:none"><div><i class="fa fa-spinner fa-2x"></i><div>'+text+'</div></div><div class="bg"></div></div>');
    }

    jQuery('#resultLoading').css({
        'width':'100%',
        'height':'100%',
        'position':'fixed',
        'z-index':'10000000',
        'top':'0',
        'left':'0',
        'right':'0',
        'bottom':'0',
        'margin':'auto'
    });

    jQuery('#resultLoading .bg').css({
        'background':'#000000',
        'opacity':'0.7',
        'width':'100%',
        'height':'100%',
        'position':'absolute',
        'top':'0'
    });

    jQuery('#resultLoading>div:first').css({
        'width': '250px',
        'height':'75px',
        'text-align': 'center',
        'position': 'fixed',
        'top':'0',
        'left':'0',
        'right':'0',
        'bottom':'0',
        'margin':'auto',
        'font-size':'16px',
        'z-index':'10',
        'color':'#ffffff'

    });

    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}
function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#searchInput').hide();
        //alert("");
        $('#searchIcon').click(function(){
            //alert(""); 
            $('#searchInput').toggle("slide", { direction: "right" }, 3000);;
        })
    })
</script>

<script type="text/javascript">
    var $owl = $('.owl-carousel');

    $owl.children().each( function( index ) {
      $(this).attr( 'data-position', index ); // NB: .attr() instead of .data()
    });

    $owl.owlCarousel({
      center: true,
      loop: true,
      items: 5,
    });

    $(document).on('click', '.owl-item>div', function() {
      $owl.trigger('to.owl.carousel', $(this).data( 'position' ) );
    });


    // on scroll script code

    $(document).scroll(function(){
        if($(this).scrollTop() > 80)
        {   
            $('.navbar').addClass('navBg');
        }
        else{
            $('.navbar').removeClass('navBg');   
        }
    });



</script>

