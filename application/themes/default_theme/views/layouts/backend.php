<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
   
        <link rel="shortcut icon" type="image/x-icon" href="<?= asset_url();?>backend/images/favicon.png">
        <title><?php echo $template['title']; ?></title>
	<link type="text/css" rel="stylesheet" href="<?= asset_url();?>backend/plugins/materialize/css/materialize.min.css">
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    
        <link href="<?= asset_url();?>backend\plugins\metrojs\MetroJs.min.css" rel="stylesheet">
        <link href="<?= asset_url();?>backend\plugins\weather-icons-master\css\weather-icons.min.css" rel="stylesheet">
         <!-- <link href="<?= asset_url();?>backend\plugins\material-preloader\css\materialPreloader.min.css" rel="stylesheet"> -->
        <link href="<?=asset_url();  ?>backend/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="<?= asset_url();?>backend\css\alpha.min.css" rel="stylesheet" type="text/css">
      	 <script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
		var asset_url = '<?php echo asset_url();?>'; 
	</script>
        <script src="<?= asset_url();?>backend\plugins\jquery\jquery-2.2.0.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\materialize\js\materialize.min.js"></script>
	
    
	
</head>

<body>

	 <div class="mn-content fixed-sidebar">
		<?php echo $template['partials']['header']; ?>
		<?php echo $template['partials']['leftnav']; ?>
		<?php echo $template['body']; ?>
		<?php echo $template['partials']['footer']; ?>
	</div>
	<!-- notifiaction div -->
	<style type="text/css">
	#dvLoading
	{
	   /*background:#000 url(public/images/loader.gif) no-repeat center center;*/
	  
	   height: 100px;
	   width: 100px;
	   position: fixed;
	   z-index: 1000;
	   left: 50%;
	   top: 50%;
	   margin: -25px 0 0 -25px;
	}
	#notificator {
    display: none;
    left: 50%;
    margin-left: -100px;
    padding: 15px 25px;
    position: fixed;
    text-align: center;
    top: 80px;
    width: 300px;
    z-index: 5000;
      //background-color:#cc103c;       
    border-radius: 10px;
    background: #000; // without a background or border applied you won't be able to see if its rounded
    }
    
    </style>


<div id="notificator" class="alert"></div>
<div id="dvLoading" class="row" >
       <div class="col s12 m4 center">
                                        <div class="preloader-wrapper big active">
                                            <div class="spinner-layer spinner-blue">
                                                <div class="circle-clipper left">
                                                    <div class="circle"></div>
                                                </div><div class="gap-patch">
                                                <div class="circle"></div>
                                                </div><div class="circle-clipper right">
                                                <div class="circle"></div>
                                                </div>
                                            </div>
                                            <div class="spinner-layer spinner-red">
                                                <div class="circle-clipper left">
                                                    <div class="circle"></div>
                                                </div><div class="gap-patch">
                                                <div class="circle"></div>
                                                </div><div class="circle-clipper right">
                                                <div class="circle"></div>
                                                </div>
                                            </div>
                                            <div class="spinner-layer spinner-yellow">
                                                <div class="circle-clipper left">
                                                    <div class="circle"></div>
                                                </div><div class="gap-patch">
                                                <div class="circle"></div>
                                                </div><div class="circle-clipper right">
                                                <div class="circle"></div>
                                                </div>
                                            </div>

                                            <div class="spinner-layer spinner-green">
                                                <div class="circle-clipper left">
                                                    <div class="circle"></div>
                                                </div><div class="gap-patch">
                                                <div class="circle"></div>
                                                </div><div class="circle-clipper right">
                                                <div class="circle"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
 </div>
	 
	
</body>
        
        <script src="<?= asset_url();?>backend\plugins\material-preloader\js\materialPreloader.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\jquery-blockui\jquery.blockui.js"></script>
        <script src="<?= asset_url();?>backend\plugins\waypoints\jquery.waypoints.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\counter-up-master\jquery.counterup.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\jquery-sparkline\jquery.sparkline.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\chart.js\chart.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\flot\jquery.flot.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\flot\jquery.flot.time.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\flot\jquery.flot.symbol.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\flot\jquery.flot.resize.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\flot\jquery.flot.tooltip.min.js"></script>
        <script src="<?= asset_url();?>backend\plugins\curvedlines\curvedLines.js"></script>
        <script src="<?= asset_url();?>backend\plugins\peity\jquery.peity.min.js"></script>
         <script src="<?=asset_url();  ?>backend/plugins/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?= asset_url();?>backend\js\alpha.min.js"></script>
      
<script>
$(window).load(function(){
  $('#dvLoading').fadeOut(2000);
});

 function ajaxindicatorstart(text)
{
	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
	jQuery('body').append('<div id="resultLoading" style="display:none"><div><div class="spinner"></div><div>'+text+'</div></div><div class="bg"></div></div>');
	}

	jQuery('#resultLoading').css({
		'width':'100%',
		'height':'100%',
		'position':'fixed',
		'z-index':'10000000',
		'top':'0',
		'left':'0',
		'right':'0',
		'bottom':'0',
		'margin':'auto'
	});

	jQuery('#resultLoading .bg').css({
		'background':'#000000',
		'opacity':'0.7',
		'width':'100%',
		'height':'100%',
		'position':'absolute',
		'top':'0'
	});

	jQuery('#resultLoading>div:first').css({
		'width': '250px',
		'height':'75px',
		'text-align': 'center',
		'position': 'fixed',
		'top':'0',
		'left':'0',
		'right':'0',
		'bottom':'0',
		'margin':'auto',
		'font-size':'16px',
		'z-index':'10',
		'color':'#ffffff'

	});

    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}
function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
} 
</script>


<script type="text/javascript">

	// Top Notificator
function ShowNotificator(add_class, the_text) {
    $('div#notificator').text(the_text).addClass(add_class).slideDown('slow').delay(3000).slideUp('slow', function () {
        $(this).removeClass(add_class).empty();
    });
}

</script>
</html>